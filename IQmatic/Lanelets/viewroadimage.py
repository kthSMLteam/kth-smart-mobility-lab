

# import datetime, time
import sys, time
import pygame
import pyglet
# from socket import *
# from socket import error as SocketError
# from xml.dom import minidom
# from xml.etree import ElementTree as ET

import laneletmodule
import laneletlibrary
import laneletvisualisation


xml_file_location = 'myTwelfthCity.xml'
# timed_print('Parsing ' + xml_file_location,parent = 'WORLD')
lanelet_module = laneletmodule.LaneletModule()
lanelet_module.load_xml_world_info(xml_file_location)

oversize = 4;
# 6 resulted in out of memory

canvas_width = 1920*oversize
canvas_height = 1080*oversize

pixel_per_meter = 720/6 *oversize;

lanelet_module.set_surface_properties(canvas_width, canvas_height, 0, pixel_per_meter)

print "lanelet_module.center_pixel_x = " + str(lanelet_module.center_pixel_x)
print "lanelet_module.center_pixel_y = " + str(lanelet_module.center_pixel_y)
print "lanelet_module.pixel_per_meter = " + str(lanelet_module.pixel_per_meter)

background_roads = laneletvisualisation.create_world_surface(lanelet_module.osm_info, canvas_width, canvas_height, oversize)

# # lanelet_module.set_surface_properties(canvas_width, canvas_height, viewing_gap = 0, pixel_per_meter = 720/6)

# # background_roads = lanelet_module.get_world_surface()

# background_roads = pygame.Surface((canvas_width, canvas_height), 0, 32)

# FOREST_GREEN = (80, 163, 77)
# background_roads.fill(FOREST_GREEN)

size = canvas_width, canvas_height
# speed = [2, 2]

# BLACK = 0, 0, 0
# WHITE = 255, 255, 255

# screen = pygame.display.set_mode(size)

# screen.fill(BLACK)
# # screen.blit(ball, ballrect)
# # screen.blit(background_roads )
# # pygame.display.flip()

# ball = pygame.image.load("ball.bmp")
# # 
# FIRE_RED = (207, 62, 43)

# # for i in range(canvas_width):

# # 	for j in range(canvas_height):

# # 		background_roads.set_at((i, j), FIRE_RED)
# fill_color = WHITE
# mask_color = BLACK




# forest = pygame.image.load("resources/forest.jpg")
# # forest = pygame.image.load("forestContinuous2.jpg")
# original_width, original_height = forest.get_size()
# scale_ratio = 1.0/1.5
# new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
# forestRescaled = pygame.transform.scale(forest,  new_size )

# mask = laneletvisualisation.get_lanelet_mask(lanelet_module.osm_info, canvas_width, canvas_height, mask_color, fill_color)
# laneletvisualisation.repeat_pattern(background_roads, forestRescaled, mask, darkness = 0.7)



# # ball = pygame.image.load("mud.jpg")
# # ball = pygame.image.load("mud2.png")
# ball = pygame.image.load("resources/mud3.jpg")
# original_width, original_height = ball.get_size()
# scale_ratio = 1.0/10.0
# new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
# ballRescaled = pygame.transform.scale(ball,  new_size )
# trucks_only = True

# mask = laneletvisualisation.get_lanelet_mask(lanelet_module.osm_info, canvas_width, canvas_height, fill_color, mask_color, trucks_only)
# laneletvisualisation.repeat_pattern(background_roads, ballRescaled, mask, darkness = 1.)



# asphalt = pygame.image.load("resources/asphalt.jpg")
# original_width, original_height = asphalt.get_size()
# scale_ratio = 1.0/2.0
# new_size = (int(scale_ratio*original_width), int(scale_ratio*original_height) )
# asphaltRescaled = pygame.transform.scale(asphalt,  new_size )

# mask = laneletvisualisation.get_lanelet_mask(lanelet_module.osm_info, canvas_width, canvas_height, fill_color, mask_color)
# laneletvisualisation.repeat_pattern(background_roads, asphaltRescaled, mask)





# laneletvisualisation.draw_all_ways(lanelet_module.osm_info, background_roads, darkness = 0.9)

BLACK = 0, 0, 0
WHITE = 255, 255, 255

screen = pygame.display.set_mode(size)

screen.fill(BLACK)

filename = "generatedImage.png"

pygame.image.save(background_roads, filename)

screen.blit(background_roads, (0,0) )

pygame.display.flip()

print "DOOOOOOOOOOOOOOOOOOOOOONNNNNNNNNNNNNNNNNNNNNNNNNNNNEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"

time.sleep(10.0)



# ballrect = ball.get_rect()


# while 1:
# 	for event in pygame.event.get():
# 		if event.type == pygame.QUIT: sys.exit()

# 	ballrect = ballrect.move(speed)
# 	if ballrect.left < 0 or ballrect.right > width:
# 		speed[0] = -speed[0]
# 	if ballrect.top < 0 or ballrect.bottom > height:
# 		speed[1] = -speed[1]

# 	screen.fill(black)
	# screen.blit(ball, ballrect)
	# pygame.display.flip()

# 	time.sleep(0.03)