import xmlcomms
import laneletlibrary
import laneletvisualisation
import customclasses
import pygame
import socket, sys
import datetime

class LaneletModule(object):
	"""This class implements the Lanelet Module to be running in the SML World"""

	def __init__(self):
		super(LaneletModule, self).__init__()

		self.colors = self.get_color

	@property
	def get_color(self):
		colors = dict()
		colors['HEADER'] = '\033[95m'
		colors['OKBLUE'] = '\033[94m'
		colors['OKGREEN'] = '\033[92m'
		colors['WARNING'] = '\033[93m'
		colors['FAIL'] = '\033[91m'
		colors['ENDC'] = '\033[0m'
		colors['BOLD'] = '\033[1m'
		colors['UNDERLINE'] = '\033[4m'
		colors['WORLD'] = '\033[1;97m'
		colors['PROJECTOR'] = '\033[1;32m'
		colors['VEHICLE'] = '\033[1;95m'
		colors['CMD'] = '\033[1;93m'
		colors['QUALISYS'] = '\033[1;96m'

		return colors

	def timed_print(self, message, color=None, parent=None):
		if color in self.colors:
			color = self.colors[color]
		else:
			color = ''
		if parent in self.colors:
			parent = self.colors[parent]
		else:
			parent = ''
		print parent + self.get_current_time + self.colors['ENDC'] + ' ' + color + message + self.colors['ENDC']

	@property
	def get_current_time(self):
		return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")


	def load_xml_world_info(self, xml_file_location):
		"This method loads the information contained in the .xml created by JOSM"
		"This method has to be run before doing anything with the class"
		"TO DO: Implement this method in the constructor"

		osm_way_list = laneletlibrary.create_osm_way_list(xml_file_location)

		[osm_node_list, osm_node_dict, origin_node] = laneletlibrary.create_osm_node_list(xml_file_location)
		# osm_node_list not used anymore

		laneletlibrary.set_nodes_to_rrt(osm_way_list, osm_node_list)

		if origin_node:

			self.origin = [origin_node.x, origin_node.y]

		else:

			self.origin = [-1, -1]


		laneletlibrary.normalize_coordinates(osm_node_list, self.origin) # List of nodes makes sense

		osm_lanelet_list = laneletlibrary.create_osm_lanelet_list(osm_node_list, osm_way_list, xml_file_location)  # List of nodes makes sense

		adjacency_matrix = laneletlibrary.create_lanelet_adjacency_matrix(osm_lanelet_list, osm_way_list, osm_node_dict)

		laneletlibrary.create_pixel_values_for_nodes(osm_node_dict, osm_way_list)

		self.osm_info = customclasses.OSMInfo(osm_node_dict, osm_way_list, osm_lanelet_list, adjacency_matrix)

		self.osm_info.osm_node_list = osm_node_list # This should be removed, however I'm still using it

		laneletlibrary.solve_initial_dijkstras(self.osm_info)
		
		return

	def get_rrt_environment_points_list(self):

		rrt_way = None

		for osm_way in self.osm_info.osm_way_list:

			if osm_way.line_type != None:

				if osm_way.line_type == "free_space":

					rrt_way = osm_way

		environment_points_list = []

		for osm_node_id in rrt_way.node_ids:

			osm_node = laneletlibrary.get_osm_node_by_id(self.osm_info.osm_node_dict, osm_node_id)

			environment_points_list.append( [osm_node.x/32., osm_node.y/32.] )

		return environment_points_list

	def set_surface_properties(self, canvas_width = 1800, canvas_height = 1000, viewing_gap = 0, pixel_per_meter = -1):

		self.canvas_width = canvas_width
		self.canvas_height = canvas_height

		if self.origin[0] == -1:

			[self.center_pixel_x, self.center_pixel_y, self.pixel_per_meter] = laneletlibrary.create_pixel_values_for_nodes(self.osm_info.osm_node_dict, self.osm_info.osm_way_list, canvas_width, canvas_height, viewing_gap, pixel_per_meter)

		else:

			[self.center_pixel_x, self.center_pixel_y, self.pixel_per_meter] = laneletlibrary.create_pixel_values_for_nodes(self.osm_info.osm_node_dict, self.osm_info.osm_way_list, canvas_width, canvas_height, viewing_gap, pixel_per_meter, 
				origin_x = 0, origin_y = 0)

		return

	def get_road_info_message_string(self,log_xml):
		"This method creates a message string to be sent to the Command Central"
		"This string contains the required information about the roads, and is according to protocol"

		road_info_message_string = xmlcomms.make_road_info_xml_message_string(self.osm_info.osm_node_list, self.osm_info.osm_way_list, self.osm_info.osm_lanelet_list, log_flag = log_xml)
		road_info_message_string = road_info_message_string + "\n"

		return road_info_message_string

	def process_and_reply(self, request_string, bodies_list,log_flag = False):
		"Roughly process an incoming message, and call appropriate parsing functions"
		"Output otions guide:"
		"If message is invalid return None"
		"If message is valid and you want to send a string as reponse, return string response"
		"If its a special request (e.g.: Image request), deal with communications internally and return True"

		#request_string = request_string.split('\n')

		#if request_string[-1] == '':
		#	request_string.pop()
		#	request_string = request_string[-1]

		xml_root = []

		message_type = xmlcomms.get_message_type(request_string)

		if message_type == None:

			print "Error when trying to get message type"
			print request_string
			return None

		if message_type == "trajectory_request":
			self.timed_print('Received trajectory request from command central',parent = 'CMD')
			reply_string =  self.process_and_reply_to_trajectory_request_string(request_string,log_flag)
			self.timed_print('Sent trajectory reply to command central',parent = 'CMD')
			return reply_string

		elif message_type == "body_trajectory_request":
			self.timed_print('Received body trajectory request from command central',parent = 'CMD')
			processed_info =  self.process_body_trajectory_request_string(request_string, log_flag)
			return processed_info

		elif message_type == "body_trajectory_command":
			self.timed_print('Received body trajectory command from command central',parent = 'CMD')
			processed_info = ["body_trajectory_command", request_string]
			return processed_info

		elif message_type == "vehicle_stop_command":
			self.timed_print('Received vehicle stop command from command central',parent = 'CMD')
			processed_info = ["vehicle_stop_command", request_string]
			return processed_info

		elif message_type == "rrt_trajectory_request":
			self.timed_print('Received RRT trajectory request from command central',parent = 'CMD')
			processed_info = ["rrt_trajectory_request", request_string]
			return processed_info

		elif message_type == "reverse_trajectory_request":
			self.timed_print('Received reverse trajectory request from command central',parent = 'CMD')
			processed_info = ["reverse_trajectory_request", request_string]
			return processed_info

		elif message_type == "construction_command":
			self.timed_print('Received construction command from command central',parent = 'CMD')
			processed_info = ["construction_command", request_string]
			return processed_info

		elif message_type == "reverse_trajectory_command":
			self.timed_print('Received reverse trajectory command from command central',parent = 'CMD')
			processed_info = ["reverse_trajectory_command", request_string]
			return processed_info

		elif message_type == "image_request":

			self.timed_print('Received background image request from command central',parent = 'CMD')
			reply = self.process_image_request(request_string,log_flag)
			if reply:
				self.timed_print('Sent background image reply to command central',parent = 'CMD')
			elif reply == None:
				self.timed_print('Error while sending background image to command central','FAIL',parent='CMD')

			# When returning true, we are not replying to command central
			return True

		elif message_type == "image_properties_request":
			self.timed_print('Received background image properties request from command central',parent = 'CMD')
			reply_string = self.process_and_reply_to_image_properties_request_string(request_string, log_flag)
			self.timed_print('Sent background image properties reply to command central',parent = 'CMD')
			return reply_string

		elif message_type == "vehicle_states_request":

			return self.process_and_reply_to_vehicle_states_request_string(request_string, bodies_list, log_flag)

		elif message_type == "obstacle_command":
			self.timed_print('Received obstacle request from command central',parent='CMD')

			obstacle_info = xmlcomms.process_obstacle_message_string(request_string,log_flag)

			return obstacle_info

		elif message_type == "highlight_request":
			self.timed_print('Received highlight request from command central',parent = 'CMD')
			[vehicle_id,highlight] = self.process_highlight_request_string(request_string,log_flag)

			reply = dict()
			reply['reply_type'] = 'highlight_reply'
			reply['vehicle_id'] = vehicle_id
			reply['highlight'] = highlight

			return reply

			# When returning true, we are not replying to command central
			# return True
			
		else:

			print "Message type not handled yet: " + message_type
			return None


		return reply_string

	def process_image_request(self, request_string,log_flag = False):

		really_send, ip_string, port_number = xmlcomms.process_image_request_xml_string(request_string,log_flag)

		if really_send:

			sock = socket.socket()

			try:
				sock.connect( (ip_string, port_number) )
			except:
				return None
			
			file = open("resources/world_surface.bmp", "rb")
			total_bytes = 0
			while True:
				chunk = file.read(65536)
				if not chunk:
					break  # EOF
				sock.sendall(chunk)

			sock.close()

		return True

	def process_highlight_request_string(self, highlight_request_string, xml_log):
		"This method parses a image properties request string string received from the Command Central"
		"It then produces a reply string to be sent"

		[vehicle_id, highlight] = xmlcomms.process_highlight_message_string(highlight_request_string,log_flag=xml_log)

		return [vehicle_id, highlight]

	def process_and_reply_to_trajectory_request_string(self, trajectory_request_string,xml_log):
		"This method parses a trajectory request string received from the Command Central, following the protocol"
		"It then produces a reply string to be sent, that is also following the protocol"

		body_request = False
		[start_id, end_id] = xmlcomms.process_trajectory_request_string(trajectory_request_string, self.osm_info.osm_node_list, self.osm_info.osm_way_list, body_request,log_flag=xml_log)

		trajectory_reply_message_string = xmlcomms.get_trajectory_reply_message_string(start_id, end_id, self.osm_info.osm_lanelet_list, self.osm_info.osm_way_list, self.osm_info.osm_node_list,log_flag=xml_log)

		trajectory_reply_message_string = trajectory_reply_message_string + '\n' 

		return trajectory_reply_message_string

	def process_body_trajectory_request_string(self, body_trajectory_request_string,xml_log):
		"This method parses a trajectory request string received from the Command Central, following the protocol"

		body_request = True
		[body_id, destination_node_id] = xmlcomms.process_trajectory_request_string(body_trajectory_request_string, self.osm_info.osm_node_list, self.osm_info.osm_way_list, body_request,log_flag=xml_log)

		processed_info = []
		processed_info.append('body_trajectory_request')
		processed_info.append([body_id, destination_node_id])

		return processed_info

	def process_and_reply_to_vehicle_states_request_string(self, vehicle_states_request_string, bodies_list, xml_log):
		"This method parses a vehicle states request string received from the Command Central, following the protocol"
		"It then produces a reply string to be sent, that is also following the protocol"

		vehicle_states_reply_message_string = xmlcomms.get_vehicle_states_reply_message_string_old(bodies_list,log_flag=xml_log)

		vehicle_states_reply_message_string = vehicle_states_reply_message_string + '\n'

		return vehicle_states_reply_message_string

	def process_and_reply_to_image_properties_request_string(self, image_properties_request_string,log_xml):
		"This method parses a image properties request string string received from the Command Central"
		"It then produces a reply string to be sent"

		image_properties_reply_message_string = xmlcomms.get_image_properties_reply_message_string(self.center_pixel_x, self.center_pixel_y, self.pixel_per_meter , log_flag = log_xml)

		image_properties_reply_message_string = image_properties_reply_message_string + '\n' 

		return image_properties_reply_message_string

	def get_world_surface(self):
		"This method returns a pygame.Surface that corresponds to the image of the world"

		world_surface = laneletvisualisation.create_world_surface(self.osm_info, self.canvas_width, self.canvas_height)

		# Very simple and fast world surface, only fill colors
		# world_surface = laneletvisualisation.create_world_surface_old(self.osm_info, self.canvas_width, self.canvas_height)

		# For debugging purposes
		#pygame.image.save(world_surface, 'world_surface.bmp')

		return world_surface




