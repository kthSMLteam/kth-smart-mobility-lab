#!/usr/bin/python

from xml.etree import ElementTree as ET
from socket import *

class VehicleController(object):
	"""docstring for VehicleController"""
	def __init__(self, world_info, vehicle_type ,vehicle_id = None):
		super(VehicleController, self).__init__()

		self.buffersize = 128096
		self.type = vehicle_type
		self.id = vehicle_id
		self.world_info = world_info

		self.cli = self.connect2SMLworld(world_info)

		self.start_control()


	def connect2SMLworld(self,world_info):

		HOST = world_info['host']
		PORT = world_info['port']

		ADDR = (HOST,PORT)

		try:
			self.cli = socket(AF_INET,SOCK_STREAM)
			self.cli.connect((ADDR))

			data = self.cli.recv(self.buffersize).strip('\n').strip('\r').lower()

			print "data: " + repr(data)

			if data == 'ok':
				print "self.vehicle_type: " + str(self.type)
				self.cli.send(self.type)
		except:
			raise

	def start_control(self):

		data = self.cli.recv(self.buffersize).strip('\n').strip('\r').lower()
		


if __name__ == '__main__':


	world_info = dict()

	world_info['host'] = '130.237.43.135'
	world_info['port'] = 8000

	controller = VehicleController(world_info,'real_vehicle',17)


