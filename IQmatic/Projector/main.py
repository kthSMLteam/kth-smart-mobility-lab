#!/usr/bin/python

import projector_ui
import sys
from threading import Thread
from PyQt4 import QtCore, QtGui
from socket import *
import json
from mocap import Mocap, Body

def start_projector_client(info):

	global cli

	HOST = info['projector_host']
	PORT = info['projector_port']

	ADDR = (HOST,PORT)
	BUFSIZE = 4096

	cli = socket( AF_INET,SOCK_STREAM)
	cli.connect((ADDR))

	data = cli.recv(BUFSIZE)

	print data

	if data == 'Projector Occupied':
		pass
	else:

		info_string = json.dumps(info)

		cli.send(info_string)

		print cli.recv(BUFSIZE)

	#cli.close()

def start_btn_pressed():
	global ui
	global running

	info = dict()

	ui.run_projector.setVisible(False)
	ui.stop_projector.setVisible(True)

	info['max_x'] = float(ui.max_x.toPlainText())
	info['max_y'] = float(ui.max_x.toPlainText())
	info['min_x'] = float(ui.min_x.toPlainText())
	info['min_y'] = float(ui.min_y.toPlainText())
	info['origin'] = ui.check_origin.checkState()
	info['projector_host'] = str(ui.projector_host.toPlainText())
	info['projector_port'] = int(ui.projector_port.toPlainText())

	print info

	#ui.run_projector.setText('Stop Projector')
	#QtCore.QMetaObject.connectSlotsByName(ui)


	t = Thread(target=start_projector_client,args=[info])
	t.start()

	#result = start_projector_client(info)
	#result = projector.start_projector(info)

	#if result is 3:
	#	print "Wrong Port"

	#ui.run_projector.setText('Start Projector')

	# do return of string with information to give feedback to the user

def stop_btn_pressed():
	global cli
	global ui

	ui.run_projector.setVisible(True)
	ui.stop_projector.setVisible(False)

	cli.send('stop')
	print 'stop'


app = QtGui.QApplication(sys.argv)
screen = QtGui.QWidget()
ui = projector_ui.Ui_projector()

ui.setupUi(screen)

ui.stop_projector.setVisible(False)

QtCore.QObject.connect(ui.run_projector, QtCore.SIGNAL("clicked()"), start_btn_pressed)
QtCore.QObject.connect(ui.stop_projector, QtCore.SIGNAL("clicked()"), stop_btn_pressed)

screen.show()

sys.exit(app.exec_())
