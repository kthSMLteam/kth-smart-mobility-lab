import socket
import xmlcomms
from socket import error as socketerror
import errno

class RealVehicleManager:
	"This class implements the Real Vehicle Manager to be communicate with the real trucks controller"


	def __init__(self, server_port = 8011):

		self.server_port = server_port

		#create an INET, STREAMing socket
		self.server_socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
		#bind the socket to a public host,
		# and a well-known port
		self.server_socket.bind( ('', 0) )
		self.port = self.server_socket.getsockname()[1]
		#become a server socket
		self.server_socket.listen(5)

		self.socket_accept_timeout = 1 #seconds
		self.server_socket.settimeout(self.socket_accept_timeout)

		self.socket_communications_timeout = 0.2 #s

		self.current_state = 'Waiting For Connections'

		self.memory_buffer = ''

	def step(self,log_flag):


		if self.current_state == 'Waiting For Connections':

			return self.wait_for_connection()

		elif self.current_state == 'Waiting For Communications':

			return self.wait_for_communication(log_flag)

		else:

			raise NameError("Unknown self.current_state")

	def wait_for_connection(self):

		try:
			#print "waiting to sockect.accept"
			(clientsocket, address) = self.server_socket.accept()

		except socket.error:

			#print "wait_for_connection - Socket timed out"

			return None

		# print "--------------------------CONNECTION ACCEPTED--------------------------"

		self.comm_socket = clientsocket
		self.comm_socket.settimeout(self.socket_communications_timeout)

		self.current_state = 'Waiting For Communications'


		return None

	def wait_for_communication(self,xml_log):

		# BUFSIZE = 4096
		# BUFSIZE = 65536
		BUFSIZE = 2097152

		try:

			#print "waiting to receive string"

			request_string = self.comm_socket.recv(BUFSIZE)

			print "Received string: " + request_string

			if request_string.find('\n') != -1:

				request_string = self.memory_buffer + request_string

				print "Received string has line ending"
				self.memory_buffer = ''


			else:

				print "Received string withOUT line ending, need to wait for more"
				self.memory_buffer = self.memory_buffer + request_string
				return None





			# received_data = ''

			# while True:

			# 	if received_data.find('\n') == -1:

			# 		print "received_data = " + str(received_data)

			# 		try:
			# 			received_data = received_data + self.comm_socket.recv(BUFSIZE)
			# 		# print "received_data: " + str(received_data)

			# 		except socketerror as e:
			# 			if e.errno == errno.ECONNRESET:
			# 				# self.timed_print('Command Central disconnected', 'WARNING', parent='CMD')
			# 				# self.command_central_connected = False

			# 				print 'errno.ECONNRESET'
			# 				self.comm_socket.close()
			# 				return -9
			# 				# return -404
			# 			else:
			# 				return None

			# 	if received_data.find('\n') != -1:

			# 		data = received_data.split('\n')
			# 		while '' in data:
			# 			data.remove('')

			# 		break


			# request_string = received_data

			# print "Received string: " + request_string




			#print repr(request_string)

			if request_string == 'close\r\n':
				print 'Received close'
				self.comm_socket.close()
				return -9

			message_type = xmlcomms.get_message_type(request_string)

			if message_type == "controlled_body_info":

				controlled_ids = xmlcomms.process_controlled_body_info_message(request_string,log_flag = xml_log)

				return [message_type, controlled_ids]

			if message_type == "task_completed":

				# Gets the body_id of the finished vehicle
				body_id = xmlcomms.process_task_completed_message(request_string,log_flag = xml_log)

				return [message_type, body_id, request_string]

			if message_type == "reverse_trajectory_solution":

				return [message_type, request_string]

		except socket.error,e:
			#print e
			#print 'error request_string'
			# print "wait_for_communication - Socket timed out"

			return None

		# print "Received message from MATLAB: " + request_string

		return None

	def send_string_to_controlled_vehicles(self, string_to_reroute):

		#print "Will send string to controlled vehicles"
		#print string_to_reroute

		self.comm_socket.send(string_to_reroute)
