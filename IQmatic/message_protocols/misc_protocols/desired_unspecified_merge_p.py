from ..Message import Message
from .. import protoconst

class DesiredUnspecifiedMergeP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC, 'desired_unspecified_merge':self.desiredUnspecifiedMerge, 'do_platoon_merge':self.doPlatoonMerge,'do_rear_merge':self.doRearMerge} # message attribute:function call
		
		# change to name of protocol
		self.protocol_id = protoconst.DES_UNSP_MER

		self.C_B = False
		self.do_rear_merge_with = None
		self.do_platoon_merge_with = None
		#protocol variables


	def handleMessage(self, message): # message is Message object

		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)
				
		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendDesiredUnspecifiedMerge(self, target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('desired_unspecified_merge', 1)
		
		return out_message


	def desiredUnspecifiedMerge(self, other_id, message_value, out_message):
		# maybe do more checks here
		if self.vehicle.bwd_pair_partner:
			out_message.append('do_platoon_merge',1)
		if not self.vehicle.bwd_pair_partner:
			out_message.append('do_rear_merge',1)
		out_message.append('EOC',1)


	def doPlatoonMerge(self, other_id, message_value, out_message):
		if message_value == 1:
			self.do_platoon_merge_with = other_id

	def doRearMerge(self, other_id, message_value, out_message):
		if message_value == 1:
			self.do_rear_merge_with = other_id

	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if other_id == self.do_platoon_merge_with:
					self.vehicle.messenger.sendDesiredPlatoonMerge(self.do_platoon_merge_with)
				elif other_id == self.do_rear_merge_with:
					self.vehicle.messenger.sendDesiredMerge(self.do_rear_merge_with)

			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.do_platoon_merge_with = None
			self.do_rear_merge_with = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


