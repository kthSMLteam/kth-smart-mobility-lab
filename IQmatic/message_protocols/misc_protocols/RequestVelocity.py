from ..Message import Message
from .. import protoconst

# THIS HAS NOT YET BEEN WRITTEN

class RequestVelocity():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'request_velocity': self.requestVelocity, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.REQ_VEL

		self.C_B = False
		self.other_velocity = None
		#protocol variables


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(message.getProtocolID(), self.vehicle_id, not initiator, message.getComID())

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendVelocityRequest(self,target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('request_velocity', 1)
		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...

	def requestVelocity(self, other_id, message_value, out_message):
		out_message.append('velocity', self.vehicle.desired_velocity)
		out_message.append('EOC', 1)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.some_flag == True:
					pass
					#do commands
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		else:
			if not self.C_B:
				if self.some_flag == False:
					pass
					#do commands
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


