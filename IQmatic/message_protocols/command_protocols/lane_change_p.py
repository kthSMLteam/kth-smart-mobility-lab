from .. import protoconst
from ..Message import Message


class LaneChangeP():

	"""Handles the velocity change commands sending"""

	def __init__(self, vehicle):
		"""TODO: to be defined1. """
		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B': self.C_B, 'EOC': self.EOC, 'lane_change':self.laneChange}
		self.protocol_id = protoconst.LANE_CHANGE
		
		self.C_B = False

		self.new_desired_lane = None

	def handleMessage(self, message):
		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendLaneChange(self, target_id, new_lane):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('lane_change', new_lane)
		out_message.append('EOC', 1)
		return out_message
		

	def laneChange(self, other_id, message_value, out_message):
		if other_id == self.vehicle.new_fwd_pair_partner or other_id == self.vehicle.fwd_pair_partner:
			self.new_desired_lane = message_value
		else:
			out_message.append('C_B',"The sender is not authorized to set new lane")
			out_message.append('EOC', 0)

		return out_message



	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True

	def EOC(self, other_id, message_value, out_message):

		if not out_message.getInitiator():
			if not self.C_B:
				if self.new_desired_lane != None:
					self.vehicle.supervisory_module.addLaneChange(self.new_desired_lane)
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.new_desired_lane = None


		if message_value == 1:
			out_message.append('EOC', 0)
			
				


				
	

	




