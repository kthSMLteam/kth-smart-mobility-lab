from ..Message import Message
from .. import protoconst

class ExampleProtocolP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		
		# change to name of protocol
		self.protocol_id = protoconst.NAME_OF_PROTOCOL

		self.C_B = False
		#protocol variables


	def handleMessage(self, message): # message is Message object

		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)
				
		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self, target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('first_message', 1)
		self.some_flag = target_id
		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...


	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.some_flag == True:
					#perform operations
					pass
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		else:
			if not self.C_B:
				if self.some_flag == False:
					#perform operations
					pass
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


