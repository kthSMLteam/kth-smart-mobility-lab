from ..Message import Message
from .. import protoconst


class RequestMergeP():

	"""Requests merge from already platooning vehicle """

	""" TODO:
	request merge
	send request confirmation
	EOC

	"""

	


	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'request_merge': self.requestMerge, "making_STOM":self.makingSTOM, 'EOC': self.EOC, 'C_B': self.C_B}
		self.protocol_id = protoconst.REQ_MERGE

		self.C_B = False
		self.requested_merge_flag = None
		self.preparing_to_merge_flag = None
		self.preparing_STOM_flag = None



	def handleMessage(self, message): # message is Message object

		protocol_id = message.getProtocolID()
		com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
	
		#TODO: if message.getTargetID() != self.vehicle_id:

		#out_message = Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(protocol_id, self.vehicle_id, not initiator, com_id)
		while message.hasNext():
			next_attribute = message.getNext()
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			#else: probably communication breakdown

		if out_message.hasNext():
			return out_message
		else:
			return None


	def sendRequestMerge(self, target_id):
		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('request_merge', 1)
		self.requested_merge_flag = target_id
		
		return out_message


	def requestMerge(self, other_id, message_value, out_message):

		if self.vehicle.new_bwd_pair_partner and not self.vehicle.preparing_STOM_flag:
			out_message.append("making_STOM",1) # Sends 1 for now
			out_message.append("EOC",1)
			self.preparing_STOM_flag = other_id
		else:
			out_message.append("C_B","Requested merge doesn't match selfs new_bwd_pair_partner, or self is already preparing STOM")
			out_message.append("EOC", 1)


	def makingSTOM(self, other_id, message_value, out_message):

		if self.vehicle.new_fwd_pair_partner == other_id and self.requested_merge_flag == other_id:
			if message_value == 1:
				self.preparing_to_merge_flag = other_id
			#else: ?
		else:
			out_message.append("C_B","Selfs new_fwd_pair_partner doesn't match sent makingSTOM, or self hasn't requested merge from sender")
			out_message.append("EOC", 1)


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value
	

	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.preparing_to_merge_flag:
					self.vehicle.preparing_to_merge_flag = self.preparing_to_merge_flag
					self.vehicle.desired_merge_flag = None
					#print self.vehicle.id, "is preparing to merge"
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.requested_merge_flag = None
			self.preparing_to_merge_flag = None

		else:
			if not self.C_B:
				if self.preparing_STOM_flag:
					self.vehicle.preparing_STOM_flag = self.preparing_STOM_flag
					#print self.vehicle.id, "is preparing STOM message"
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.preparing_STOM_flag = None
			self.C_B = False


		if message_value == 1:
			out_message.append("EOC",0)
