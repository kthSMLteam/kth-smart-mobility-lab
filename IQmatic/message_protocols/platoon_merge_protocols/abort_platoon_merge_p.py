from ..Message import Message
from .. import protoconst

class AbortPlatoonMergeP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC, 'abort_platoon_merge':self.abortPlatoonMerge, 'send_fwd':self.sendForward, 'send_bwd':self.sendBackward} # message attribute:function call
		
		self.protocol_id = protoconst.ABORT_PLAT_MER

		self.C_B = False
		self.abortPlatoonMerge = None
		self.send_fwd = None
		self.send_bwd = None


	def handleMessage(self, message): # message is Message object

		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)
				
		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendAbortPlatoonMerge(self, target_id, fwd, bwd):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('abort_platoon_merge', 1)

		if fwd:
			out_message.append('send_fwd',1)
		if bwd:
			out_message.append('send_bwd',1)
		
		out_message.append('EOC',1)

		return out_message



	def abortPlatoonMerge(self, other_id, message_value, out_message):
		#self.abortPlatoonMerge = True
		
		if self.vehicle.upcoming_platoon_merge:
			self.abortPlatoonMerge = True
		else:
			pass
			#print self.vehicle.id, "got platoon merge abort, but does not have an upcoming platoon merge"
		


	def sendForward(self, other_id, message_value, out_message):
		self.send_fwd = True

	def sendBackward(self, other_id, message_value, out_message):
		self.send_bwd = True


	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True


	def EOC(self, other_id, message_value, out_message):

		if not out_message.getInitiator():

			if not self.C_B:

				if self.abortPlatoonMerge:
					self.vehicle.supervisory_module.abortPlatoonMerge()

				if self.send_fwd:
					self.vehicle.supervisory_module.sendAbortPlatoonMergeFwd()

				if self.send_bwd:
					self.vehicle.supervisory_module.sendAbortPlatoonMergeBwd()

			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False

			self.abortPlatoonMerge = None
			self.send_fwd = None
			self.send_bwd = None

		else:
			if not self.C_B:
				pass
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


