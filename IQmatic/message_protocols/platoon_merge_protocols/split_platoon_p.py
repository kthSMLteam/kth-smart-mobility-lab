from ..Message import Message
from .. import protoconst

class SplitPlatoonP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'split_plat': self.splitPlat, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.SPLIT_PLAT
		self.C_B = False
		#protocol variables


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('split_plat', 1)
		out_message.append('EOC', 1)
		self.sent_split_plat = target_id

		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...

	def splitPlat(self, other_id, message_value, out_message):
		if other_id == self.vehicle.fwd_pair_partner or other_id == self.vehicle.bwd_pair_partner:
			self.split_plat = other_id
		else:
			out_message.append('C_B', "Split platoon command received from unexpected source")
			out_message.append('EOC', 0)

	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_split_plat == other_id:
					#do commands
					if self.sent_split_plat == self.vehicle.fwd_pair_partner or self.sent_split_plat == self.vehicle.bwd_pair_partner:
						self.vehicle.supervisory_module.handlePlatSplit(self.sent_split_plat)
					else:
						print "WTTTVDJKMVK from SplitPlatP"
					
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		else:
			if not self.C_B:
				if self.split_plat == other_id:
					#do commands
					if self.split_plat == self.vehicle.fwd_pair_partner or self.split_plat == self.vehicle.bwd_pair_partner:
						self.vehicle.supervisory_module.handlePlatSplit(self.split_plat)
					else:
						print "WTFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF!!!!!! from SplitPlatP"
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


