from ..Message import Message
from .. import protoconst

class MergePrepDoneP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'merge_prep_done': self.mergePrepDone, 'STOM_partner': self.STOMPartner, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.MERGE_PREP_DONE

		self.C_B = False
		#protocol variables
		self.sent_merge_prep_done = None
		self.merge_prep_done = None


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id, STOM_partner):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('merge_prep_done', 1)
		out_message.append('STOM_partner', STOM_partner)
		out_message.append('EOC', 1)
		self.sent_merge_prep_done = target_id
		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...
	def mergePrepDone(self, other_id, message_value, out_message):
		if message_value == 1 or message_value == 0 and other_id == self.vehicle.new_bwd_pair_partner:
			if message_value == 1:
				self.merge_prep_done = message_value
			else:
				out_message.append('C_B', "merge prep not done")
				out_message.append('EOC', 0)
		else:
			out_message.append('C_B', "merge_prep_done message did not comply to specs")
			out_message.append('EOC', 0)
	
	def STOMPartner(self, other_id, message_value, out_message):
		if self.merge_prep_done == 1 and type(message_value) == int:
			self.STOM_partner = message_value
		else:
			out_message.append('C_B', "Unexpected or faulty STOM_partner message received")
			out_message.append('EOC', 0)

	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_merge_prep_done == other_id:
					#do commands
					pass
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.sent_merge_prep_done = None

		else:
			if not self.C_B:
				if self.merge_prep_done == 1 and self.STOM_partner != None:
					#do commands
					self.vehicle.supervisory_module.mergePrepDone(self.STOM_partner)
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.merge_prep_done = None
			self.STOM_partner = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


