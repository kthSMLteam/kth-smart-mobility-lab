from ..Message import Message
from .. import protoconst


class HoldToMergeP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'upcoming_platoon_merge': self.upcomingPlatoonMerge, 'hold_to_merge': self.holdToMerge, 'in_merging_platoon': self.inMergingPlatoon, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.HOLD_TO_MERGE

		self.C_B = False
		#protocol variables
		self.sent_hold_to_merge = None
		self.upcoming_platoon_merge = None



	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id, in_merging_platoon):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('upcoming_platoon_merge', 1)
		out_message.append('hold_to_merge', self.vehicle.id)
		out_message.append('in_merging_platoon', in_merging_platoon)
		out_message.append('EOC', 1)
		self.sent_hold_to_merge = target_id
		
		return out_message


	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...

	def upcomingPlatoonMerge(self, other_id, message_value, out_message):
		if message_value:
			self.upcoming_platoon_merge = True
		else:
			out_message.append('C_B', "upcoming platoon merge evaluated to false")
			out_message.append('EOC', 0)

	def holdToMerge(self, other_id, message_value, out_message):
		if message_value == other_id:
			self.hold_to_merge = message_value
		else:
			out_message.append('C_B', "hold to merge value was invalid")
			out_message.append('EOC', 0)

	def inMergingPlatoon(self, other_id, message_value, out_message):
		if message_value == 1 or message_value == 0:
			self.in_merging_platoon = message_value
		else:
			out_message.append('C_B', "wrong format of in_merging_platoon")
			out_message.append('EOC', 0)

	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_hold_to_merge == other_id:
					#do commands
					pass
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.sent_hold_to_merge = None

		else:
			if not self.C_B:
				if self.upcoming_platoon_merge and self.hold_to_merge == other_id and self.in_merging_platoon != None:
					#do commands
					self.vehicle.upcoming_platoon_merge = self.upcoming_platoon_merge
					self.vehicle.hold_to_merge = self.hold_to_merge
					self.vehicle.in_merging_platoon = self.in_merging_platoon
					self.vehicle.supervisory_module.mergePrepDone(self.hold_to_merge)
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags
			self.upcoming_platoon_merge = None
			self.hold_to_merge = None
			self.in_merging_platoon = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


