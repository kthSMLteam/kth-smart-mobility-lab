# This is init file for the platoon_merge_protocols module
__all__ = ['clear_for_merge_p', 
		'desired_platoon_merge', 
		'hold_to_merge_p', 
		'merge_prep_comparison_p', 
		'merge_prep_done_p', 
		'platoon_merge_done_p',
		'platoon_STOM_p',
		'request_platoon_merge_p',
		'split_platoon_p',
		'request_merge_length_info_p',
		'merge_length_info_p',
		'merge_length_count_completed_p',
		'abort_platoon_merge_p']