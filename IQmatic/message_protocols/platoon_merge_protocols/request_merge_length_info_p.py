from ..Message import Message
from .. import protoconst

class RequestMergeLengthInfoP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'C_B':self.C_B, 'EOC':self.EOC, 'request_in_merging_platoon_info':self.RequestInMergingPlatoonInfo, 'leader_id_info':self.LeaderIdInfo} # message attribute:function call
		
		# change to name of protocol
		self.protocol_id = protoconst.REQ_MER_LEN_INFO

		self.leader_id = None
		self.in_merging_platoon = None
		self.C_B = False
		#protocol variables


	def handleMessage(self, message): # message is Message object


		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print "Attribute",next_attribute.name, "in protocol", self.protocol_id
				out_message.append("C_B", "Attribute not in protocol dictionary")
				out_message.append("EOC", 0)
				
		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendRequestMergeLengthInfo(self, other_id, leader_id):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('request_in_merging_platoon_info', 1)
		out_message.append('leader_id_info', leader_id)
		out_message.append('EOC',1)
		
		return out_message


	def RequestInMergingPlatoonInfo(self, other_id, message_value, out_message):
		if self.vehicle.in_merging_platoon:
			self.in_merging_platoon = True
		else:
			self.in_merging_platoon = False


	def LeaderIdInfo(self, other_id, message_value, out_message):
		self.leader_id = message_value


	def C_B(self, other_id, message_value, out_message):

		print "received communication breakdown: ", message_value
		self.C_B = True


	def EOC(self, other_id, message_value, out_message):

		if not out_message.getInitiator():
			if not self.C_B:
				if self.in_merging_platoon != None and self.leader_id != None:
					self.vehicle.supervisory_module.forwardRequestMergeLengthInfo(self.in_merging_platoon, self.leader_id)
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			self.in_merging_platoon = None
			self.leader_id = None

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


