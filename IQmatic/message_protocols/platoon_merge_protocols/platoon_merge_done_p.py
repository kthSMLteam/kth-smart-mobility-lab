from ..Message import Message
from .. import protoconst

class PlatoonMergeDoneP():

	def __init__(self, vehicle):

		self.vehicle = vehicle
		self.vehicle_id = self.vehicle.id

		self.function_dict = {'platoon_merge_done': self.platoonMergeDone, 'C_B':self.C_B, 'EOC':self.EOC} # message attribute:function call
		# change to name of protocol
		self.protocol_id = protoconst.PLAT_MERGE_DONE

		self.C_B = False
		#protocol variables
		self.sent_PM_done_bwd = None
		self.sent_PM_done_fwd = None
		self.platoon_merge_done_fwd = None
		self.platoon_merge_done_bwd = None


	def handleMessage(self, message): # message is Message object

		#protocol_id = message.getProtocolID()
		#com_id = message.getComID()
		other_id = message.getVehicleID()
		initiator = message.getInitiator()
		com_id = message.getComID()
	
		#TODO: if message.getTargetID() != self.vehicle_id:
		#TODO: if message.getProtocolID() != self.protocol_id:

		#Message structure: Message(protocol id, self vehicle id, if self is initator, conversation id)
		out_message = Message(self.protocol_id, self.vehicle_id, not initiator, com_id)

		while message.hasNext():
			next_attribute = message.getNext()
			# print next_attribute.name
			if next_attribute.getName() in self.function_dict:
				self.function_dict[next_attribute.getName()](other_id, next_attribute.getValue(), out_message)
			else:
				print next_attribute.name
				out_message.append("C_B", "Part of message not in protocol dictionary")

		if out_message.hasNext():
			return out_message
		else:
			return None



	def sendFirstMessage(self,target_id, forward):

		out_message = Message(self.protocol_id, self.vehicle_id, True)
		out_message.append('platoon_merge_done', 1)
		out_message.append('EOC', 0)
		if forward:
			self.sent_PM_done_fwd = target_id
		else:
			self.sent_PM_done_bwd = target_id
		
		
		return out_message

	def platoonMergeDone(self, other_id, message_value, out_message):
		#print self.vehicle.id, "received a platoon_merge_done from", other_id
		if other_id == self.vehicle.platoon_merge_request:
			self.platoon_merge_done_fwd = other_id
		elif other_id == self.vehicle.new_bwd_pair_partner:
			self.platoon_merge_done_bwd = other_id

		else:
			out_message.append('C_B', "A platoon_merge_done was received from unexpected source")
			out_message.append('EOC', 0)
		

	#Functions for recieved messages:
	#def firstMessage(self, other_id, message_value, out_message):
	#	...


	def C_B(self, other_id, message_value, out_message):

		self.C_B = True
		print message_value


	def EOC(self, other_id, message_value, out_message):

		if out_message.getInitiator():
			if not self.C_B:
				if self.sent_PM_done_fwd == other_id:
					#do commands
					self.sent_PM_done_fwd = None
					pass
				elif self.sent_PM_done_bwd == other_id:
					self.send_PM_done_bwd = None
					pass
					
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		else:
			if not self.C_B:
				if self.vehicle.got_platoon_merge_initiation_from == other_id:
					# to make sure flags are reset for the vehicle that got the desired platoon merge
					self.vehicle.merge_prep_done = None
					self.vehicle.platoon_STOM_partner = None
					self.vehicle.merge_prep_done = None
					self.vehicle.hold_to_merge = None
					self.vehicle.in_merging_platoon = None
					self.vehicle.got_platoon_merge_initiation_from = None

				if self.platoon_merge_done_bwd == other_id:
					#do commands
					self.vehicle.bwd_pair_partner = self.vehicle.new_bwd_pair_partner
					self.vehicle.new_bwd_pair_partner = None
					self.vehicle.upcoming_platoon_merge = None
				elif self.platoon_merge_done_bwd == other_id:

					self.vehicle.platoon_merge_request = None
					# TODO is this safe?
					self.vehicle.fwd_pair_partner = other_id
					
			else:
				print "COMMUNICATION BREAKDOWN"
				self.C_B = False
			# reset flags

		if message_value == 1:
			out_message.append('EOC',0) # 0 means no expecation of response


