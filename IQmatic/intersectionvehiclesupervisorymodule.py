import time, math
import MessageHandler



class IntersectionVehicleSupervisoryModule:
	"This class is the Platooning Vehicle class used by the Platooning Manager Module"
	def __init__(self, default_velocity, vehicle):

		# These variables store the desired lane and desired lane
		# They are read by SmartVehicle module, and redirected to the Control Module
		self.desired_velocity = 0.5

		# gains access to the vehicle variables like perception_grid
		self.vehicle = vehicle

		self.VELOCITY_HIGH = 50./32.*(1000.0/3600.0)
		self.VELOCITY_LOW = 30./32.*(1000.0/3600.0)

		self.desired_velocity = self.VELOCITY_HIGH
		self.setVelocity(400)
		# self.setVelocity(400-400*self.vehicle.id)

		self.perception_module = None
		

		#Tips
		# self.state = [real_world_meters, radians]
		# print "self.state = " + str(self.state)
		# closeby_body_readings = [real_world_meters, radians]
		# print "closeby_body_readings = " + str(closeby_body_readings)
		# self.traj[0] in real_world_meters
		# self.traj[2] in radians
		# print "self.traj[2] = " + str(self.traj[2])

		self.module_start_time = time.time()

		# initialization of the start time of last overtake
		self.overtake_time = self.module_start_time

		print "VehicleSupervisoryModule started"

	def step(self):
		# This is the step function that is running at each cycle of the thread
		# in SmartVehicle


		# self.handleMessages()
		#print self.desired_velocity
		

		# if self.perception_grid:
		# 	if self.perception_grid['fm']:
		# 		pass
		# 		#print self.getPerceivedObject(self.perception_grid['fm'][0])[0]
		# 	if self.perception_grid['fm'] and not self.vehicle.control_module.ACCC:
		# 		self.setACCCTarget(self.perception_grid['fm'][0], 5.)
		# if self.perception_module:
		# 	if self.perception_module.module_ready:
		# 		pass
		# 		#print self.perception_module.closest_objects
		
		"""if time_diff > 2. and self.perception_grid:

			self.desired_velocity = self.VELOCITY_HIGH
			if self.overtakeRelCheck():
				self.desired_velocity = self.VELOCITY_LOW
				self.overtake_time = time.time()"""

	def setVelocity(self, new_velocity_kmh):
		self.desired_velocity = float(new_velocity_kmh)*(1000./3600./32.)



	def lame_velocity_change(self):
		# Just a dummy function to change the desired velocity

		#ADDITION
		self.desired_velocity = 1.5

		current_passed_time = time.time() - self.module_start_time
		acceleration_frequency = 1./10.

		# Simulator assumes that input velocity comes as SML velocity!
		desired_velocity_km_h = 40. + 10.*math.cos( 2.*math.pi*acceleration_frequency*current_passed_time )
		desired_velocity_m_s = desired_velocity_km_h*(1000./3600.)
		desired_velocity_m_s_SML = desired_velocity_m_s/32.

		self.desired_velocity = desired_velocity_m_s_SML
		# print "Supervisory Module : self.desired_velocity = " + str(self.desired_velocity)
		# self.desired_velocity = 0.35 


	
	def getPerceptionGrid(self):
		self.perception_grid = self.vehicle.perception_grid

	def updatePerceivedObjects(self):
		self.perceived_objects = self.vehicle.perceived_objects

	def getPerceivedObject(self, ID):
		key = str(ID)
		if key in self.perceived_objects:
			return self.perceived_objects[key]
		else:
			return None

	

	def setACCCTarget(self, ID, desired_distance):
		if self.vehicle.control_module:
			control_module = self.vehicle.control_module
			control_module.ACCC_target_id = ID
			control_module.ACCC_desired_distance = desired_distance
			control_module.ACCC = True
			return True
		else:
			print "No control module"
			return False

	def resetACCC(self):
		if self.vehicle.control_module:
			self.vehicle.control_module.ACCC = False
			self.vehicle.control_module.ACCC_desired_distance = None
			self.vehicle.control_module.ACCC_target_id = None

	
		

	def handleMessages(self):
		if self.vehicle.id in self.vehicle.bodies_array:
			#if len(self.vehicle.bodies_array[self.vehicle.id].v2v_network_input)>0:
				#print len(self.vehicle.bodies_array[self.vehicle.id].v2v_network_input)
			while len(self.vehicle.bodies_array[self.vehicle.id].v2v_network_input)>0:
				message = self.vehicle.bodies_array[self.vehicle.id].v2v_network_input.pop()
				#incoming_message = MessageHandler.decodeMessage(message)
				if message['vehicle_id'] == self.vehicle.id:
					continue
				self.handleNetworkMessage(message)
				self.vehicle.saved_network_messages[message['vehicle_id']] = message

			while len(self.vehicle.bodies_array[self.vehicle.id].v2v_wifi_input)>0:
				message = self.vehicle.bodies_array[self.vehicle.id].v2v_wifi_input.pop()
				if message['vehicle_id'] == self.vehicle.id:
					continue
				self.handleWifiMessage(message)
				self.vehicle.saved_wifi_messages[message['vehicle_id']] = message

			#OBS: BELOW IS TEMPORARY SOLUTION
			self.vehicle.bodies_array[self.vehicle.id].v2v_network_output = [self.vehicle.messenger.generateMessage()]
			self.vehicle.bodies_array[self.vehicle.id].v2v_wifi_output = [self.vehicle.messenger.generateMessage()]

