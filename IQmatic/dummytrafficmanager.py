# import xmlcomms

# import laneletvisualisation
# import customclasses
# import pygame
import sys, time, math, random, threading, copy
import multiprocessing
import bodyclasses
# import xml.etree.ElementTree as ET


# sys.path.append('..\Lanelets')
# import laneletlibrary


from multiprocessing import Pool

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary

import dummyvehicle, dummybusvehicle

'''ADDITION'''
import random
import threading
'''END'''


class DummyTrafficManager:
	"This class implements the Dummy Traffic Manager Module to be running in the SML World"

	def __init__(self, bodies_array, osm_info, desired_rate):

		self.osm_info = osm_info

		self.CLOSE = False

		self.vehicles = []

		self.iteration = 0

		self.vehicles_dict = dict()

		self.inputs = dict()

		# self.thread_rate = 50.
		self.thread_rate = desired_rate

		self.construction_time = time.time()

		self.simulation_step_counter = 0

		# This is the bodies array, sent from commands manager module
		# It gives us access to all of the current bodies
		# Chaning this dictionary, will produce changes in the
		# bodies array in the world class
		self.bodies_array = bodies_array

		self.non_tried_osm_node_ids_list = []
		self.set_non_tried_node_ids()

	def start_simulating_vehicles(self):
		# This function creates the independent vehicle threads
		# which will run the control loop and simulate the vehicles
		for vehicle in self.vehicles:

			self.vehicles_dict[vehicle.id] = vehicle

			print "Starting vehicle number " + str(vehicle.id)

			self.start_thread( self.vehicle_step,  args=([vehicle]) )

	def start_controlling_vehicles(self):
		# This function is an alternative to the start_simulating_vehicles()
		# function. This function will create independent vehicle threads which
		# will simply run the control loop and compute the actuator signals
		for vehicle in self.vehicles:

			self.vehicles_dict[vehicle.id] = vehicle

			print "Starting vehicle number " + str(vehicle.id)

			self.start_thread( self.vehicle_control_step,  args=([vehicle]) )

	def start_thread(self, handler, args=()):
		t = threading.Thread(target=handler, args=args)
		t.daemon = True
		# self.threads.append(t)
		t.start()

	
	def vehicle_control_step(self, vehicle):
		# This is an alternative to the vehicle_step() function
		# that runs on an independent thread function running
		# for each vehicle.
		# This function simply uses a controller to compute the 
		# desired actuator signals of the vehicle

		# print 'Started controlling vehicle ' + str(vehicle.id)
		
		# print "vehicle_control_step--------------------------"

		vehicle.set_vehicle_on_trajectory_state(0)

		while not self.CLOSE:

			tic_thread_step =  time.time()

			current_time = tic_thread_step - self.construction_time

			vehicle.get_trajectory_tracking_inputs( current_time )

			toc_thread_step = time.time()

			if toc_thread_step - tic_thread_step > 0:
				current_rate = (1. / (toc_thread_step - tic_thread_step))
				if current_rate > self.thread_rate:
					sleeping_time = 1. / self.thread_rate - 1. / current_rate
					time.sleep(sleeping_time)
				else:

					print "vehicle.id = " + str(vehicle.id) + " will not sleep. Failed the rate of " + str( float(self.thread_rate) ) + 'Hz. Total time: ' + str( toc_thread_step - tic_thread_step )
					# print "toc_thread_step - tic_thread_step = " + str(toc_thread_step - tic_thread_step)
					pass
			else:
				sleeping_time = 1. / self.thread_rate
				time.sleep(sleeping_time)


		print 'Stopped vehicle ' + str(vehicle.id)

	def step(self, simulation_period, simulator_is_closing = False):
		# Step is the original step function which computes actuator signals using a controller
		# and then simulates said actuator signals.

		'''ADDITION'''
		if not self.spawned_broken_vehicle:
			if time.time()>self.broken_spawn_count+self.broken_spawn_time:
				self.spawn_event.set()
				self.spawned_broken_vehicle = True
		'''END'''


		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.thread_rate = simulation_period

		current_vehicle_states = []

		for vehicle_tag in self.vehicles_dict:

			current_vehicle = self.vehicles_dict[vehicle_tag]

			current_state_degrees = current_vehicle.get_current_state(radians = False)
			current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_states.append(current_vehicle_state)

		return current_vehicle_states

	def command_step(self, simulation_period, simulator_is_closing = False):
		# Command Step is an alternative to the original step function. This command_step
		# function only computes the actuator signals without actually simulating the body

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.thread_rate = simulation_period
		self.thread_rate = 10

		current_vehicle_inputs = dict()

		for input_tag in self.inputs:

			current_input = dict()

			current_input['throttle'] = self.inputs[input_tag][0]
			current_input['steering'] = self.inputs[input_tag][1]
			
			# current_state_degrees = current_vehicle.get_current_state(radians = False)
			# current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_inputs[input_tag] = current_input
			
		return current_vehicle_inputs

	def get_vehicle_inputs_step(self, bodies_readings, simulation_period, simulator_is_closing = False):

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.bodies_readings = bodies_readings

		self.thread_rate = simulation_period

		current_vehicle_inputs = []

		for vehicle_tag in self.input:

			current_input = self.input[vehicle_tag]

			# current_state_degrees = current_vehicle.get_current_state(radians = False)
			# current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_inputs.append(current_input)

		return current_vehicle_inputs

	def simulate_inputs(self, inputs, simulator_is_closing = False):

		current_time = time.time()

		self.simulation_step_counter = self.simulation_step_counter + 1

		self.CLOSE = simulator_is_closing

		self.thread_rate = simulation_period

		current_vehicle_states = []

		input_ids = inputs.keys()

		for vehicle_tag in self.vehicles_dict:
			
			current_vehicle = self.vehicles_dict[vehicle_tag]

		 	
		 	if current_vehicle.id in input_ids:

		 		input_vector = inputs[current_vehicle.id]

		 		current_vehicle.set_current_input(input_vector)
		 		current_vehicle.simulate(current_time)
		 	else:

		 		print "Did not find input for current vehicle"

			# current_vehicle = self.vehicles_dict[vehicle_tag]

			current_state_degrees = current_vehicle.get_current_state(radians = False)
			current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}

			current_vehicle_states.append(current_vehicle_state)

		return current_vehicle_states

	def test_parallel(self, number_to_square):

		return number_to_square*number_to_square

	def get_trucks_in_collision(self, bodies_list):

		# print '---'

		real_bodies = []

		# print 'bodies_list = ' + str(bodies_list)

		for body in bodies_list:

			if body['id'] > 0:

				# Orientations are coming in degrees
				real_bodies.append( [ 32.0*body['x'], 32.0*body['y'], body['yaw'], body['id'] ] )


		safety_distance = 2.0

		bodies_in_collision = []

		for body in real_bodies:

			colliding = False			

			for current_vehicle in self.vehicles:

				vehicle_state = current_vehicle.get_current_state()

				for distance_ahead in range(1,17):

					x_front = body[0] + 0.5*float(distance_ahead)*math.cos( math.radians( body[2] ) )
					y_front = body[1] + 0.5*float(distance_ahead)*math.sin( math.radians( body[2] ) )

					if math.hypot( x_front - vehicle_state[0] , y_front - vehicle_state[1] ) < safety_distance:

						bodies_in_collision.append( body[3] )
						colliding = True
						# print body[3]
						# print 'Body in Collision'
						break

				if colliding:
					break

		# print 'bodies_in_collision' + str(bodies_in_collision)

		return bodies_in_collision

	def whatevs(self, current_vehicle, current_vehicle_states, body_readings, full_stop = False):

		tic = time.time()

		current_time = time.time() - self.construction_time

		success = False

		if not full_stop:

			if current_vehicle.get_id() != -300:

				success = current_vehicle.fake_controller_trajectory_tracking( current_time , body_readings)

		current_state_degrees = current_vehicle.get_current_state(radians = False)
		current_vehicle_state = {'x': current_state_degrees[0]/32., 'y': current_state_degrees[1]/32.,'z': 0, 'roll': 0 ,'pitch': 0,'yaw': current_state_degrees[2] , 'id': current_vehicle.get_id()}
		current_vehicle_states.append(current_vehicle_state)

		return current_vehicle_states

	def create_bus_vehicle(self, simulation_period, vehicle_id):

		vehicle_number = 0

		while 1:

			# temp_vehicle = dummyvehicle.DummyVehicle(self.bodies_array, simulation_period, vehicle_id)
			temp_vehicle = dummybusvehicle.DummyBusVehicle(self.bodies_array, simulation_period, vehicle_id)

			temp_vehicle.loop_trajectory = False

			state_vector = [0, 0, 0]
			temp_vehicle.state = state_vector

			input_vector = [0, 0]
			temp_vehicle.input = input_vector

			bus_stop_string = 'bus_stop_'
			bus_stop_number = 0

			bus_stops_to_process = True

			temp_vehicle.bus_trajs = []
			temp_vehicle.bus_stop_names = []

			while bus_stops_to_process:

				current_bus_stop_string = bus_stop_string + str(bus_stop_number)
				next_bus_stop_string = bus_stop_string + str(bus_stop_number + 1)

				start_id = laneletlibrary.find_node_by_tag(self.osm_info.osm_node_dict, current_bus_stop_string)
				end_id = laneletlibrary.find_node_by_tag(self.osm_info.osm_node_dict, next_bus_stop_string)
				
				if start_id == None:

					print "Unable to find the bus stop, this is probably because there are no bus stops in this map"
					print "I will cancel this bus vehicle creation and return gracefully."
					raise NameError('Trying to create buses on a non-bus map.')

					return False

				if end_id == None:

					# There are now new bus stops, must close on my self:
					bus_stops_to_process = False

					next_bus_stop_string = bus_stop_string + str(0)
					end_id = laneletlibrary.find_node_by_tag(self.osm_info.osm_node_dict, next_bus_stop_string)


				success = self.set_trajectory(temp_vehicle, self.osm_info, start_id, end_id)

				if not success:

					print "Bus creation failed!"
					raise NameError("Bus creation failed!")
					# Could not find a suitable trajectory. Try a new vehicle

				temp_vehicle.bus_stop_names.append( current_bus_stop_string )
				temp_vehicle.bus_trajs.append( copy.deepcopy(temp_vehicle.traj) )
				


				bus_stop_number += 1



			# print "'bus_stop_3' start_id = " + str(start_id)

			temp_vehicle.traj = temp_vehicle.bus_trajs[-1]

			# if not ( success_1 and success_2 and success_3 ):

			# 	print "Bus creation failed!"
			# 	# Could not find a suitable trajectory. Try a new vehicle
			# 	continue

			minimum_distance = 2.0

			safe_to_add = True

			for other_vehicle in self.vehicles:

				other_vehicle_state = other_vehicle.state
				current_vehicle_state = temp_vehicle.state

				if math.hypot( current_vehicle_state[0] - other_vehicle_state[0] , current_vehicle_state[1] - other_vehicle_state[1] ) < minimum_distance:

					#print "TOO CLOSE"
					safe_to_add = False

					break

			if not safe_to_add:
				continue

			# The new vehicle was created and added to our list of dummy vehicles
			self.vehicles.append(temp_vehicle)

			# We need to create the corresponding body object in the world class bodies_array
			new_body = bodyclasses.BusVehicle()

			new_body.id = vehicle_id

			new_body.x = temp_vehicle.state[0]
			new_body.y = temp_vehicle.state[1]
			new_body.yaw = temp_vehicle.state[2]

			new_body.commands = dict()
			new_body.commands['throttle'] = temp_vehicle.input[0]
			new_body.commands['steering'] = temp_vehicle.input[1]

			new_body.next_bus_stop = next_bus_stop_string

			self.bodies_array[vehicle_id] = new_body

			temp_vehicle.set_desired_velocity( 40.0 )

			return

	def set_hardcoded_OSM_nodes(self):

		self.known_good_node_ids = [-1778,-578,-332,-628,-274,-1700,-2539,-204,-268,-1574,-196,-1852,-662,-496,-1758,-1626,-1496,-522,-1464,-470,-1566,-1830,-392,-576,-168,-202,-1534,-362,-200,-158,-276,-478,-632,-172,-1884,-564,-446,-462,-1676,-356,-1680,-574,-242,-1898,-410,-740,-164,-610,-1600,-544]


	def create_hardcoded_vehicles(self, simulation_period, vehicle_id):

		current_node_id = self.known_good_node_ids.pop(0)

		return self.create_random_vehicle(simulation_period, vehicle_id, current_node_id)


	def create_random_vehicle(self, simulation_period, vehicle_id, OSM_Node_id = 0):
		'''
		This function will try to create a random vehicle.

		The random vehicle is given the id passed as argument.
		The random vehicle trajectory is found randomly, by 
		attempting to find circular trajectories that can be formed 
		from the OSM Nodes in self.non_tried_osm_node_ids_list
		(see self.set_circular_trajectory() for more details)

		This function will loop until it creates a vehicle with a 
		valid trajectory AND with a valid initial position (i.e.:
		not in collision) or when there are no more possible
		trajectories to try ( self.non_tried_osm_node_ids_list is 
		empty).

		In case of a created vehicle the function returns sucess = 
		True, otherwise it will return False. (Once it returns False, 
		it will always return False - since it is impossible to place
		any new cars)
		'''

		vehicle_number = 0

		minimum_squared_distance = 4.0**2

		while len( self.non_tried_osm_node_ids_list ):

			temp_vehicle = dummyvehicle.DummyVehicle(self.bodies_array, simulation_period, vehicle_id)

			state_vector = [0, 0, 0]
			temp_vehicle.state = state_vector

			input_vector = [0, 0]
			temp_vehicle.input = input_vector

			non_tried_node_id = self.get_non_tried_node_id()

			sucess = False

			if OSM_Node_id == 0:

				if non_tried_node_id:

					success = self.set_circular_trajectory(temp_vehicle, self.osm_info, node_start_id = non_tried_node_id)

			else:

				success = self.set_circular_trajectory(temp_vehicle, self.osm_info, node_start_id = OSM_Node_id)

			if not success:
				# Could not find a suitable trajectory. Try a new vehicle
				continue

			safe_to_add = True

			for other_vehicle in self.vehicles:

				other_vehicle_state = other_vehicle.state
				current_vehicle_state = temp_vehicle.state

				if ( ( current_vehicle_state[0] - other_vehicle_state[0] ) **2 + ( current_vehicle_state[1] - other_vehicle_state[1] )**2 ) < minimum_squared_distance:

					safe_to_add = False

					break

			if not safe_to_add:
				continue

			# The new vehicle was created and added to our list of dummy vehicles
			self.vehicles.append(temp_vehicle)

			# We need to create the corresponding body object in the world class bodies_array
			new_body = bodyclasses.DummyVehicle()

			new_body.id = vehicle_id

			new_body.x = temp_vehicle.state[0]
			new_body.y = temp_vehicle.state[1]
			new_body.yaw = temp_vehicle.state[2]

			new_body.commands = dict()
			new_body.commands['throttle'] = temp_vehicle.input[0]
			new_body.commands['steering'] = temp_vehicle.input[1]

			self.bodies_array[vehicle_id] = new_body

			return True

		return False

	def set_non_tried_node_ids(self):

		for osm_node_id in self.osm_info.osm_node_dict:

			self.non_tried_osm_node_ids_list.append(osm_node_id)


	def get_non_tried_node_id(self):

		if len(self.non_tried_osm_node_ids_list):

			return self.non_tried_osm_node_ids_list.pop( random.randrange( len(self.non_tried_osm_node_ids_list) ) )

		else:

			return None

	def set_circular_trajectory(self, vehicle, osm_info, node_start = -1, node_start_id = -1):
		''' Sets a vehicle on a circular trajectory (if possible)

		This function will use an OSM Node (provided in the arguments
		or generated randomly) to try an create a circular trajectory.
		A circular trajectory is defined as a trajectory which closes
		on itself (starts and ends at the same OSM Node).

		If the trajectory is found, it is set on the vehicle, and the
		vehicle is placed on the trajectory point which is closest to
		the OSM Node defining this circular trajectory

		'''

		start_id = -1

		if node_start != -1:

			node_id = osm_info.osm_node_dict.keys()[node_start]

			start_id = osm_info.osm_node_dict[ node_id ].id
			start_x = osm_info.osm_node_dict[ node_id ].x
			start_y = osm_info.osm_node_dict[ node_id ].y

		elif node_start_id != -1:

			start_id = node_start_id
			osm_node = laneletlibrary.get_osm_node_by_id(osm_info.osm_node_dict, start_id)
			start_x = osm_node.x
			start_y = osm_node.y

		else:

			random_key_idx = random.randrange( len(osm_info.osm_node_dict) )
			node_id = osm_info.osm_node_dict.keys()[random_key_idx]

			start_id = osm_info.osm_node_dict[ node_id ].id
			start_x = osm_info.osm_node_dict[ node_id ].x
			start_y = osm_info.osm_node_dict[ node_id ].y

		points_per_meter = 5.

		[traj_x, traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(start_id, osm_info, points_per_meter)

		if len(traj_x) == 0:
			#Error, could not find a suitable trajectory
			# Can happen if a vehicle is in a node for trucks only
			return False

		if math.hypot( traj_x[0] - traj_x[-1] , traj_y[0] - traj_y[-1] ) > 10.0:
			return False

		desired_velocity = 10

		vehicle.set_state_trajectory(traj_x, traj_y, desired_velocity)

		state = [start_x, start_y]
		closest_idx = vehicle.find_closest_trajectory_point( state )

		vehicle.set_vehicle_on_trajectory_state(closest_idx)

		return True

	def set_trajectory(self, vehicle, osm_info, node_start_id, node_end_id):

		points_per_meter = 5.

		# [traj_x, traj_y] = laneletlibrary.get_closed_trajectory_from_node_id(start_id, osm_info.osm_lanelet_list, osm_info.osm_way_list, osm_info.osm_node_dict, points_per_meter)
		[traj_x, traj_y] = laneletlibrary.get_trajectory_from_node_ids(node_start_id, node_end_id, osm_info.osm_lanelet_list, osm_info.osm_way_list, osm_info.osm_node_dict)

		if len(traj_x) == 0:
			#Error, could not find a suitable trajectory
			# Can happen if a vehicle is in a node for trucks only
			return False

		desired_velocity = 10

		vehicle.set_state_trajectory(traj_x, traj_y, desired_velocity)

		vehicle.set_vehicle_on_trajectory_state()

		return True



	'''ADDITION'''

	def create_broken_vehicles_thread(self, simulation_period, vehicle_id):
		spawn_timer_thread = threading.Timer(7, self.create_broken_vehicles, args = [simulation_period, vehicle_id])
		spawn_timer_thread.start()
		

	def create_broken_vehicles(self, simulation_period, vehicle_id):

		vehicle_number = 0

		while 1:

			temp_vehicle = dummyvehicle.DummyVehicle(self.bodies_array, simulation_period, vehicle_id)

			state_vector = [0, 0, 0]
			temp_vehicle.state = state_vector

			input_vector = [0, 0]
			temp_vehicle.input = input_vector

			success = self.set_circular_trajectory(temp_vehicle, self.osm_info, node_start = -1)

			if not success:
				# Could not find a suitable trajectory. Try a new vehicle
				continue

			minimum_distance = 2.0

			safe_to_add = True

			for other_vehicle in self.vehicles:

				other_vehicle_state = other_vehicle.state
				current_vehicle_state = temp_vehicle.state

				if math.hypot( current_vehicle_state[0] - other_vehicle_state[0] , current_vehicle_state[1] - other_vehicle_state[1] ) < minimum_distance:

					#print "TOO CLOSE"
					safe_to_add = False

					break

			if not safe_to_add:
				continue

			# The new vehicle was created and added to our list of dummy vehicles
			self.vehicles.append(temp_vehicle)

			# We need to create the corresponding body object in the world class bodies_array
			new_body = bodyclasses.DummyVehicle()

			new_body.id = vehicle_id

			# Random spawn
			#new_body.x = temp_vehicle.state[0]
			#new_body.y = temp_vehicle.state[1]
			#new_body.yaw = temp_vehicle.state[2]

			# Bottom right curve
			new_body.x = 33
			new_body.y = -37.5
			new_body.yaw = 0

			# Bottom left curve
			#new_body.x = -32
			#new_body.y = -37.5
			#new_body.yaw = 0

			# Top left curve
			#new_body.x = -32
			#new_body.y = 54.5
			#new_body.yaw = 130

			# Top right curve
			new_body.x = 33
			new_body.y = 54.5
			new_body.yaw = 130

			new_body.commands = dict()
			new_body.commands['throttle'] = 0
			new_body.commands['steering'] = 0

			self.bodies_array[vehicle_id] = new_body


			# Generating warning message
			warning_message = dict()
			warning_message['time'] = time.time()
			warning_message['message_id'] = hash(warning_message['time'])
			warning_message['obstacle'] = True
			warning_message['obstacle_id'] = vehicle_id
			#warning_message['restricted_velocity'] = 30/3.6
			warning_message['x'] = new_body.x
			warning_message['y'] = new_body.y
			warning_message['lane'] = 'right_lane'
			self.bodies_array[vehicle_id].v2v_network_output = [warning_message]

			print "BROKEN VEHICLE SPAWNED"

			return


	'''END'''
