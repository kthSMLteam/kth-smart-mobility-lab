import javax.swing.*;
import javax.swing.text.*;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.*;

public class View extends JFrame{

    public Controller controller;
    public JPanel contentPane;
    public String[] commandIDs = {"echo-state","merge-after", "merge-between", "merge-before", "set-speed"}; 
    private JLabel targetLabel;
    private JLabel paramLabel;

    public JTextArea incomingText;
    private JScrollPane scrollPane;
    public JTextField targetField;
    public JTextField paramField;
    public JComboBox<String> list;
    public JButton execButton;
    public Model model;
    
    public View(int portNo){

        controller = new Controller(this, portNo);
        contentPane = new JPanel();
        GroupLayout layout = new GroupLayout(contentPane);
        contentPane.setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        
        incomingText = new JTextArea(20, 30);
        incomingText.setEditable(false);

        DefaultCaret caret = (DefaultCaret)incomingText.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        scrollPane = new JScrollPane(incomingText);
        

        list = new JComboBox<String>(commandIDs);

        targetLabel = new JLabel("Target:");
        targetField = new JTextField(10);

        paramLabel = new JLabel("Parameters:");
        paramField = new JTextField(10);

        execButton = new JButton("Execute command");
        execButton.addActionListener(controller);

        layout.setHorizontalGroup(
                layout.createParallelGroup()
                .addComponent(scrollPane)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(list)
                    .addComponent(targetLabel)
                    .addComponent(targetField)
                    .addComponent(paramLabel)
                    .addComponent(paramField)
                    .addComponent(execButton)
                    )
                );

        layout.setVerticalGroup(
                layout.createSequentialGroup()
                .addComponent(scrollPane)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(list)
                    .addComponent(targetLabel)
                    .addComponent(targetField)
                    .addComponent(paramLabel)
                    .addComponent(paramField)
                    .addComponent(execButton))
                );

        /*

        contentPane.add(list);

        contentPane.add(targetLabel);
        contentPane.add(targetField);
        contentPane.add(paramLabel);
        contentPane.add(paramField);

        contentPane.add(execButton);

        */
        add(contentPane);
        pack();



        }

    public void printToView(String outString){
        incomingText.append(outString);
        incomingText.append("\n");
    }
    
}
