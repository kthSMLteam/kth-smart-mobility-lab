import java.lang.Thread;
import java.io.BufferedReader;
import java.io.IOException;

public class InThread extends Thread{
    private BufferedReader in;
    private Model model;
    private boolean done;

    public InThread(BufferedReader readerIn, Model modelIn){
        in = readerIn;
        model = modelIn;
    }

    public void run(){
        while (!done){
            try {
                String json = in.readLine();
                String readableText = model.handleJson(json);
                model.print(readableText);
            } catch(IOException e){
                e.printStackTrace();
                done = false;
            }
        }
    }
}

