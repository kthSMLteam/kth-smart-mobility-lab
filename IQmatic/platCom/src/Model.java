import java.net.*;
import java.io.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import java.util.*;



public class Model {
    public int portNo;
    public Socket socket;
    public PrintWriter out;
    public BufferedReader in;
    public InThread inThread;

    public Controller controller;

    public JSONParser parser = new JSONParser();

    private ContainerFactory factory = new ContainerFactory(){

        public List creatArrayContainer(){
            return new ArrayList();
        }

        public Map createObjectContainer(){
            return new LinkedHashMap();
        }
    };

    public Model(int portNoIn, Controller controllerIn){
        portNo = portNoIn;
        controller = controllerIn;
        try {
            socket = new Socket("localhost",portNo);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            inThread = new InThread(in, this);
            inThread.start();

        } catch(IOException e){
            e.printStackTrace();
            System.exit(0);
        }

    }


        

    public void sendCommand(String commandId, String target,String params){

        int targetInt = Integer.parseInt(target);
        params = params.replaceAll("\\s", "");
        String[] paramArray = params.split(",");
        JSONObject json = new JSONObject();
        json.put("commandID", commandId);
        json.put("target", targetInt);
        json.put("params", paramArray);


        out.println(json.toString());
    }

    public void print(String outString){
        controller.printToView(outString);
    }


    public String handleJson(String jsonString){
        try {
            Map json = (Map)parser.parse(jsonString, factory);
            StringBuilder sb = new StringBuilder();
            controller.printToView("handling json");

            String comID = (String)json.get("comID");
            
            sb.append("comID " + comID + "\n");
            List fields = (List)json.get("fields");
            List values = (List)json.get("values");

            Iterator iterFields = fields.iterator();
            Iterator iterValues = values.iterator();

            while (iterFields.hasNext() && iterValues.hasNext()) {
                sb.append((String)iterFields.next() + ": ");
                sb.append((String)iterValues.next() + "\n");
            }

            return sb.toString();

        } catch (ParseException e){
            e.printStackTrace();
            return null;
        }
        
    }

}
