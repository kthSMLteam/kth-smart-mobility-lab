/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command_central;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 *
 * @author Matteo
 */
public class Command_central extends Application {

    private Scene scene;
    private final Group root;
    private final Pane birdsEyeContainer; 
    private final Pane infoContainer;
    private final Pane actionContainer;
    private final Pane barContainer;
    private final Rectangle background;
//    private Mocap mocap;
    
    float[][] allBodies;    
    
    private final int screenNumber = 1; // 0: primary screen, 1: secondary screen
    
    private final Rectangle2D boundsPrimaryScreen = Screen.getPrimary().getVisualBounds();
    private final Rectangle2D boundsSecondaryScreen = Screen.getScreens().get(1).getVisualBounds();
    private final Rectangle2D[] screensBounds = {boundsPrimaryScreen, boundsSecondaryScreen};
    private final double scrWidth = screensBounds[screenNumber].getWidth();
    private final double scrHeight = screensBounds[screenNumber].getHeight();
    private final double scrMinX = screensBounds[screenNumber].getMinX();
    private final double scrMinY = screensBounds[screenNumber].getMinY();
    
    private MocapListenerThread mocapListenerThread;
    private final DrawDataThread drawDataThread;
    
    private final double REAL_LIFE_TOTAL_WIDTH = 6000;
    private final double BAR_HEIGHT = 50;
    private final double BIRDSEYE_WIDTH = scrWidth*2/3;
    private final double BIRDSEYE_HEIGHT = scrHeight*3/4;
    private final double CONSOLE_BUTTONS_WIDTH = 100;
    private final double LED_RADIUS = 10;
    private final double LED_OFF_OPACITY = .5;
    private final double LED_ON_OPACITY = 1;
    
    private final TextArea console;
    private final OutputStream out;
    private Circle center;
    private final BlockingQueue mocapDataQueue;
    private final int QUEUE_CAPACITY = 10;
    
    public Command_central() 
    {   
        out = new OutputStream()
        {
            @Override
            public void write(int b) throws IOException
            {
                appendText(String.valueOf((char) b));
            }
        };
//        System.setOut(new PrintStream(out, true));
        
        root = new Group();
        
        allBodies = null;
        console = new TextArea();
        
        mocapDataQueue = new ArrayBlockingQueue<>(QUEUE_CAPACITY);
        drawDataThread = new DrawDataThread("Mocap drawer", mocapDataQueue);
        drawDataThread.start();
        
        background = new Rectangle(scrWidth, scrHeight, Color.BLACK);
        barContainer = setBarPane();
        birdsEyeContainer = setBirdsEyePane();
        infoContainer = setInfoPane();
        actionContainer = setActionPane();
        
        root.getChildren().addAll(background,barContainer,birdsEyeContainer,infoContainer,actionContainer);
    }
    

    @Override
    public void start(Stage primaryStage)
    {
        
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setWidth(scrWidth);
        primaryStage.setHeight(scrHeight);

        scene  = new Scene(root);
        
        primaryStage.setScene(scene);
        primaryStage.setX(scrMinX);
        primaryStage.setY(scrMinY);
        primaryStage.setHeight(scrHeight);  
        primaryStage.setWidth(scrWidth);
        primaryStage.setOnCloseRequest((WindowEvent we) -> {
            try {
                closeStage(primaryStage);
            } catch (InterruptedException ex) {
                Logger.getLogger(Command_central.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        primaryStage.show();
        
       
        
    }

    private Pane setBarPane()
    {
        Pane thisPane = new Pane();
        
        thisPane.setPrefSize(scrWidth,BAR_HEIGHT);
        thisPane.setStyle("-fx-background-color: black;");
        
        thisPane.setLayoutX(0);
        thisPane.setLayoutY(0);
        
        HBox hbox = new HBox();
        hbox.setLayoutX(scrWidth*39/40);
//        hbox.setAlignment(Pos.CENTER_RIGHT);
        
//        Rectangle closeRect = new Rectangle();
//        closeRect.setFill(Color.WHITE);
        Text closeText = new Text("X");
        closeText.setFont(new Font(20));
        closeText.setFill(Color.ORANGE);
//        closeText.setClip(closeRect);
        hbox.getChildren().addAll(closeText);
        
        thisPane.getChildren().addAll(hbox);
        
        closeText.setOnMouseEntered((MouseEvent me)->
        {
            scene.setCursor(Cursor.HAND);
        });

        closeText.setOnMouseExited((MouseEvent me)->
        {
            scene.setCursor(Cursor.DEFAULT);
        });
        
        closeText.setOnMousePressed((MouseEvent me)->
        {
            try {
                Stage stage = (Stage) scene.getWindow();
                closeStage(stage);  
            }
            catch (Exception ex) {
                System.out.println("Error when closing: "+ex);
            }
        });
        
    
        return thisPane;
        
    }
    
    private Pane setBirdsEyePane()
    {
        Pane thisPane = new Pane();
        
//        Image birdsEyeImage = new Image("file:road_image.png");
//        ImageView birdsEyeImageView = new ImageView(birdsEyeImage);
//        
//        birdsEyeImageView.setFitWidth(BIRDSEYE_WIDTH);
//        birdsEyeImageView.setFitHeight(BIRDSEYE_HEIGHT);
        
        thisPane.setLayoutX(0);
        thisPane.setLayoutY(BAR_HEIGHT);
        thisPane.setPrefSize(BIRDSEYE_WIDTH, BIRDSEYE_HEIGHT);
        thisPane.setStyle("-fx-background-color: LightGreen ;");
        center = new Circle();
        center.setTranslateX(BIRDSEYE_WIDTH/2);
        center.setTranslateY(BIRDSEYE_HEIGHT/2);
        center.setFill(Color.BLACK);
        center.setRadius(5);
        
        thisPane.getChildren().add(center);
        
        return thisPane;
    }

    private Pane setInfoPane()
    {
        Pane backPane = new Pane();
        backPane.setLayoutX(0);
        backPane.setLayoutY(BAR_HEIGHT+BIRDSEYE_HEIGHT);
        backPane.setStyle("-fx-background-color: black;"); 
        HBox container = new HBox(0);
//        container.setLayoutX(0);
//        container.setLayoutY(BAR_HEIGHT+BIRDSEYE_HEIGHT);
//        container.setPrefSize(BIRDSEYE_WIDTH-CONSOLE_BUTTONS_WIDTH, scrHeight-BAR_HEIGHT-BIRDSEYE_HEIGHT);
        
        AnchorPane thisPane = new AnchorPane();
        thisPane.setPrefSize(BIRDSEYE_WIDTH-CONSOLE_BUTTONS_WIDTH, scrHeight-BAR_HEIGHT-BIRDSEYE_HEIGHT);
////        thisPane.setPrefSize(scrWidth, scrHeight-BAR_HEIGHT-BIRDSEYE_HEIGHT);
        thisPane.setStyle("-fx-background-color: white;");  
//        thisPane.setLayoutX(0);
//        thisPane.setLayoutY(BAR_HEIGHT+BIRDSEYE_HEIGHT);
//        thisPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
//        thisPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS); 
        thisPane.getChildren().add(console);
        AnchorPane.setTopAnchor(console, 0.0);
        AnchorPane.setLeftAnchor(console, 0.0);
        AnchorPane.setRightAnchor(console, 0.0);
        AnchorPane.setBottomAnchor(console, 0.0);
        
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(5, 5, 5, 5));
//        vbox.setAlignment(Pos.TOP_CENTER);
//        vbox.setPrefSize(CONSOLE_BUTTONS_WIDTH, scrHeight-BAR_HEIGHT-BIRDSEYE_HEIGHT);
        Button clearConsole = new Button("Clear console");
        clearConsole.setOnMousePressed((MouseEvent me)->
        {
            console.setText("");
        });
        vbox.getChildren().addAll(clearConsole);
        
        container.getChildren().addAll(thisPane,vbox);
        
        backPane.getChildren().addAll(container);
        
        return backPane;
    }

    private Pane setActionPane() 
    {
        Pane thisPane = new Pane();
        
        thisPane.setPrefSize(scrWidth-BIRDSEYE_WIDTH, scrHeight-BAR_HEIGHT);
//        thisPane.setPrefSize(scrWidth-BIRDSEYE_WIDTH, BIRDSEYE_HEIGHT);
        thisPane.setStyle("-fx-background-color: black;");
        
        thisPane.setLayoutX(BIRDSEYE_WIDTH);
        thisPane.setLayoutY(BAR_HEIGHT);
        
        thisPane = insertActionPaneContent(thisPane);
        
        return thisPane;
    
    }

    private Pane insertActionPaneContent(Pane pane)
    {        
//        double startX = pane.getBoundsInParent().getMinX();
//        double startY = pane.getBoundsInParent().getMinY();
        
        double horizontalOffset = scrWidth/40;
        double verticalOffset = scrHeight/40;
        
        VBox vbox = new VBox(verticalOffset/2);
        vbox.setTranslateX(horizontalOffset);
        vbox.setTranslateY(verticalOffset);
        
        Text mocapTitle = new Text("Mocap");
        mocapTitle.setFill(Color.WHITE);
        
        Circle mocapLed = new Circle(LED_RADIUS);
        setLedOff(mocapLed);
        
        HBox mocapTitleBox = new HBox(horizontalOffset);
        mocapTitleBox.getChildren().addAll(mocapTitle, mocapLed);
        
        HBox mocapHBox = new HBox(horizontalOffset/2);
        
        Button startMocap = new Button("Start connection");
        Button getMocapData = new Button("Get data");
        getMocapData.setDisable(true);
        Button closeMocapConnection = new Button("Close connection");
        closeMocapConnection.setDisable(true);
        
        startMocap.setOnMousePressed((MouseEvent me)->
        {
            try
            {
                createMocapThread();
            }
            catch (IOException ex)
            {
                System.out.println("ERROR: "+ex.getMessage());
            }
            startMocap.setDisable(true);
            getMocapData.setDisable(false);
            closeMocapConnection.setDisable(false);
            setLedOn(mocapLed);
        });
        
        getMocapData.setOnMousePressed((MouseEvent me)->{
            startMocapThread();
                //START LISTENING_THREAD LOOP!
//                System.out.println("Launching mocap listener ...");
//                drawAllBodies();
        });
        
        closeMocapConnection.setOnMousePressed((MouseEvent me)->{
                startMocap.setDisable(false);
                getMocapData.setDisable(true);
                closeMocapConnection.setDisable(true);
                closeMocapThread();
                setLedOff(mocapLed);
        });
        
        
        mocapHBox.getChildren().addAll(startMocap,getMocapData,closeMocapConnection);
        vbox.getChildren().addAll(mocapTitleBox,mocapHBox);
        
//        
//        Rectangle rectTrial = new Rectangle(startX,startY);
//        rectTrial.setFill(Color.WHITE);
//        pane.getChildren().add(rectTrial);
        
        pane.getChildren().addAll(vbox);
        

//        double offVB = 100;
//        
//        VBox vbox = new VBox(offVB);
//        vbox.setAlignment(Pos.CENTER);
////        vbox.setTranslateX(startX);
////        vbox.setTranslateY(startY);
//        vbox.setLayoutX(startX);
//        vbox.setLayoutY(startY);
        
//        vbox.getChildren().add(rectTrial);
//        
//        pane.getChildren().add(vbox);
        
//        Rectangle rectTrial = new Rectangle(startX,startY);
//        rectTrial.setFill(Color.WHITE);
//        pane.getChildren().add(rectTrial);
        
        return pane;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private void setLedOn(Circle led)
    {
        led.setFill(Color.GREENYELLOW);
        led.setOpacity(LED_ON_OPACITY);
    }
    
    private void setLedOff(Circle led)
    {
        led.setFill(Color.RED);
        led.setOpacity(LED_OFF_OPACITY);
    }

    private void drawAllBodies()
    {
        float bodyX;
        float bodyY;
        double bodyXDrawn;
        double bodyYDrawn;
        Circle bodyDrawn;
        int count = 0;
        
        if (allBodies != null)
        {
            Circle[] allBodiesDrawnTemp = new Circle[allBodies.length];
            for (float[] body : allBodies)
            {
                if (body[0]==body[0]) //false only if the value is NaN
                {
                    bodyX = body[0];
                    bodyY = body[1]; 
                    double offX = bodyX*BIRDSEYE_WIDTH/REAL_LIFE_TOTAL_WIDTH;
                    double offY = bodyY*BIRDSEYE_WIDTH/REAL_LIFE_TOTAL_WIDTH;
                    bodyXDrawn = BIRDSEYE_WIDTH/2+offX;
                    bodyYDrawn = BIRDSEYE_HEIGHT/2-offY;
                    bodyDrawn = new Circle(bodyXDrawn, bodyYDrawn, 10, Color.BLUE);
                    allBodiesDrawnTemp[count] = bodyDrawn;
                    count++;
                }
            }
            Circle[] allBodiesDrawn = new Circle[count];
            System.arraycopy(allBodiesDrawnTemp, 0, allBodiesDrawn, 0, count);
            for (Circle bodyToDraw : allBodiesDrawn)
                birdsEyeContainer.getChildren().addAll(bodyToDraw);
        }
    }
    
    public void appendText(String str)
    {
        Platform.runLater(() -> {
            console.appendText(str);
        });
    }


    private void createMocapThread() throws IOException
    {
        mocapListenerThread = new MocapListenerThread("Mocap listener", mocapDataQueue);
        System.out.println("Creating Mocap thread...");
    }
    
    private void startMocapThread()
    {
        mocapListenerThread.start();
        System.out.println("Starting Mocap thread...");
    }
    
    private void closeMocapThread()
    {
        mocapListenerThread.stop();
        System.out.println("Closing Mocap thread...");
    }

    private void closeStage(Stage stage) throws InterruptedException {
            System.out.println("Closing stage");
            if (mocapListenerThread!=null)
                mocapListenerThread.stop();
            drawDataThread.stop();
            stage.close();
            System.exit(0);
    }
    
    
}
