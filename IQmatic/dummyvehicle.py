import socket, sys, time, math, random

import numpy

sys.path.append('Lanelets')
import laneletmodule
import laneletlibrary

import bodyclasses

class DummyVehicle:
	"This class is the Vehicle class for the Vehicle Simulator Module"
	def __init__(self, bodies_array, simulation_period, vehicle_id):

		if not isinstance(vehicle_id, int):
			raise NameError('In class Vehicle: constructor: id must be an integer')

		self.id = vehicle_id
		self.simulation_period = 1./simulation_period
		self.state = []
		self.input = [0, 0]
		self.time = -1
		self.current_time_delay = 0.0
		self.current_trajectory_id = 0
		self.cruise_velocity = 25.0 + 15.0*random.random()

		self.vehicle_iteration = 0

		self.traj = []
		self.np_traj = []


		self.axis_length = 1.2
		self.bodies_array = bodies_array

		# If a dummy vehicle, then we want to loop the trajectory
		# If its a bus we might want it to stop at several places and switch between trajectories
		self.loop_trajectory = True
		# When a bus finishes a trajectory this flag will be set to True
		self.trajectory_finished = False

		# CC PID gain
		self.CC_k_p = 500. # 250
		self.CC_k_i = 50. # 50
		self.CC_k_d = 150.
		self.reset_CC()

	def set_vehicle_on_trajectory_state(self, idx = 0):

		# print "!!!!!!!!START!!!!!!!!!!!!!!!!!!!!!set_vehicle_on_trajectory_state"

		self.current_trajectory_id = idx

		if len( self.traj ) == 0:

			raise NameError('In class Vehicle: trying to use set_vehicle_on_trajectory_state when trajectory is not yet defined')

		if len( self.state ) == 0:
			#Uninitialized state, must create it

			for dimension in range( len( self.traj ) - 1 ):

				self.state.append( self.traj[dimension][idx] )

		else:
			#Must overwrite the state

			for dimension in range( len( self.state ) ):

				self.state[dimension] = self.traj[dimension][idx]

		self.state[2] = math.degrees(self.state[2])


		# print "self.traj[dimension][idx]  = " + str(self.traj[dimension][idx] )
		# print "self.state = " + str(self.state)

		# print "!!!!!!!!END!!!!!!!!!!!!!!!!!!!!!set_vehicle_on_trajectory_state"

	def set_state_trajectory(self, traj_x, traj_y, desired_velocity):

		# print "DO ME"
		traj_theta = []
		temp_theta = 0
		traj_time = []
		current_time = 0


		for idx in range( len ( traj_x ) - 1 ):

			delta_x = traj_x[idx+1] - traj_x[idx]
			delta_y = traj_y[idx+1] - traj_y[idx]

			temp_theta = math.atan2(delta_y, delta_x)

			traj_theta.append(temp_theta)

			traj_time.append(current_time)

			distance_moved = math.hypot(delta_x, delta_y) # Euclidean norm

			time_passed = distance_moved/desired_velocity

			current_time = current_time + time_passed

		traj_theta.append(temp_theta)
		traj_time.append(current_time)

		if len(traj_y) != len(traj_x) or len(traj_time) != len(traj_x) or len(traj_theta) != len(traj_x):

			raise NameError("Trajectory creation resulted in a mistake!")

		self.traj = [traj_x, traj_y, traj_theta, traj_time]

		self.np_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		return

	def get_closest_reference(self):

		best_distance = 10e10
		best_idx = -1

		for idx in range( len( self.traj[3] ) ):

			current_distance = math.hypot( self.state[0] - self.traj[0][idx], self.state[1] - self.traj[1][idx] )

			if current_distance < best_distance:

				best_distance = current_distance
				best_idx = idx

		current_reference = []

		for dimension in range( 3 ):

			current_reference.append( self.traj[dimension][best_idx] )

		return current_reference

	def get_steering_command(self, current_state, current_reference, max_steering_radians = 30):

		tracking_error = self.get_tracking_error(current_state, current_reference)

		while tracking_error[2] > math.pi:
			tracking_error[2] = tracking_error[2] - math.pi
		while tracking_error[2] < -math.pi:
			tracking_error[2] = tracking_error[2] + math.pi

		# steering_command = 8.0* (10.0*tracking_error[1] + 15.0*tracking_error[2]) # Stable for unicycle model
		steering_command = 1.0* (1.0*tracking_error[1] + 1.5*tracking_error[2]) 

		# Need to limit the steering command to 30 degrees
		if steering_command > math.radians(max_steering_radians):
			steering_command = math.radians(max_steering_radians)

		if steering_command < -math.radians(max_steering_radians):
			steering_command = -math.radians(max_steering_radians)

		return steering_command


	def controller_action(self, current_state, current_reference, desired_velocity):

		steering_command = self.get_steering_command(current_state, current_reference)

		# velocity_command = float(self.cruise_velocity)*(1000./3600.)
		# velocity_command = self.get_throttle_CC(self.cruise_velocity*(1000./3600.))
		throttle_command = self.get_throttle_CC(desired_velocity)
		
		command_inputs = [throttle_command, steering_command]
		# print "command_inputs = " + str(command_inputs)
		return command_inputs

	def get_tracking_error(self, current_state, current_reference):

		# current_state[2] in radians
		error_x = math.cos( current_state[2] )*(current_reference[0] - current_state[0]) + math.sin( current_state[2] )*(current_reference[1] - current_state[1])
		error_y = -math.sin( current_state[2] )*(current_reference[0] - current_state[0]) + math.cos( current_state[2] )*(current_reference[1] - current_state[1])
		error_theta = current_reference[2] - current_state[2]

		while error_theta > math.pi:
			error_theta -= 2*math.pi
		while error_theta < -math.pi:
			error_theta += 2*math.pi

		return [error_x, error_y, error_theta]

	
	def distance_to_car_in_future_trajectory(self, current_time, body_readings, safety_distance):

		# I will compute the distance to a car that is overlapping with my close future trajectory
		# Returns 100.0 if no car is detected

		best_distance = 10e10
		best_idx = 10e10

		best_idx = self.current_trajectory_id

		# meters_ahead = 10.0
		meters_ahead = 1.5*safety_distance
		colliding_distance = 1.5
		squared_colliding_distance = colliding_distance**2
		mega_safe_distance = ( 2.0*meters_ahead )**2

		future_traj_np = self.get_future_traj(best_idx, meters_ahead)
		
	 	closest_car_distance = 100.

		for idx, body_reading in enumerate( body_readings ):

			temp_body_reading = []
			temp_body_reading.append( body_reading[1] )
			temp_body_reading.append( body_reading[2] )
			temp_body_reading.append( math.radians( body_reading[3] ) )

			axis_length_obstacle_avoidance = self.axis_length

			if ( (future_traj_np[0,0] - temp_body_reading[0])**2 + (future_traj_np[1,0] - temp_body_reading[1])**2 ) > mega_safe_distance:

				# Vehicle is super far away, I do not need to check it
				continue

			# print "temp_body_reading[2] = " + str(temp_body_reading[2])

			distance_angle = math.atan2( future_traj_np[1, 0] - temp_body_reading[1], future_traj_np[0,0] - temp_body_reading[0] )

			angle_difference = distance_angle - future_traj_np[2, 0] 

			while angle_difference < 0:
				angle_difference = angle_difference + 2.0*math.pi

			while angle_difference > 2.0*math.pi:
				angle_difference = angle_difference - 2.0*math.pi

			if angle_difference < math.pi/2.0 or angle_difference > math.pi*(3.0/2.0):

				# print "Vehicle is behind me, I do not need to check it"
				continue

			other_vehicle_axle = numpy.array( [ [temp_body_reading[0]], [temp_body_reading[1]] ] )

			other_vehicle_front_axle = numpy.array([ [temp_body_reading[0] + axis_length_obstacle_avoidance*math.cos( temp_body_reading[2] )],[ 
						temp_body_reading[1] + axis_length_obstacle_avoidance*math.sin( temp_body_reading[2] )]])

			# print "other_vehicle_front_axle = " + str(other_vehicle_front_axle)

			future_traj_front_axle = numpy.copy( future_traj_np[0:2, :] )

			future_traj_front_axle[0, :] = future_traj_front_axle[0, :] + axis_length_obstacle_avoidance*numpy.cos( future_traj_np[2, :] )
			future_traj_front_axle[1, :] = future_traj_front_axle[1, :] + axis_length_obstacle_avoidance*numpy.sin( future_traj_np[2, :] )

			# print "\n future_traj_np[0,0] = " + str(future_traj_np[0,0]) + "\nfuture_traj_front_axle[0,0] = " + str(future_traj_front_axle[0,0])

			# print " future_traj_np.shape[1] = " + str( future_traj_np.shape[1])

			# Check my front axle for collision with front axle		
			temp_distance = numpy.sum((future_traj_front_axle - other_vehicle_front_axle)**2, axis = 0)
			# Find the closest trajectory point that matches my desired speed and current heading
			# best_idx = numpy.argmin(temp_distance)
			danger_points = numpy.where( temp_distance < squared_colliding_distance )

			dangerest_distance = 100.

			if len(danger_points[0]) > 0:

				dangerest_distance = 0.2*danger_points[0][0]

			# Check my front axle for collision with rear axle
			temp_distance = numpy.sum((future_traj_front_axle - other_vehicle_axle)**2, axis = 0)
			# Find the closest trajectory point that matches my desired speed and current heading
			# best_idx = numpy.argmin(temp_distance)
			danger_points = numpy.where( temp_distance < squared_colliding_distance )

			if len(danger_points[0]) > 0:

				dangerest_distance = 0.2*danger_points[0][0]
			
			# Check my axle for collision with front axle		
			temp_distance = numpy.sum((future_traj_np[0:2,:] - other_vehicle_front_axle)**2, axis = 0)
			# Find the closest trajectory point that matches my desired speed and current heading
			# best_idx = numpy.argmin(temp_distance)
			danger_points = numpy.where( temp_distance < squared_colliding_distance )

			if len(danger_points[0]) > 0:

				dangerest_distance = 0.2*danger_points[0][0]

			# Check my axle for collision with rear axle
			temp_distance = numpy.sum((future_traj_np[0:2,:] - other_vehicle_axle)**2, axis = 0)
			# Find the closest trajectory point that matches my desired speed and current heading
			# best_idx = numpy.argmin(temp_distance)
			danger_points = numpy.where( temp_distance < squared_colliding_distance )

			if len(danger_points[0]) > 0:

				dangerest_distance = 0.2*danger_points[0][0]	

			if dangerest_distance < closest_car_distance:

				closest_car_distance = dangerest_distance

		return closest_car_distance

	def get_future_traj(self, best_idx, meters_ahead):
		# Assumptions, the distance between trajectory points is 0.2 
		# (that is the sampling distance used when the trajectory is generated)
		
		start_idx = best_idx
		end_idx = int( best_idx + round(meters_ahead/0.2) )
		future_traj_ids = [0] # I will add a dummy number, in order to then overwrite it with my current state
		# print "\n"

		# print "start_idx = " + str(start_idx)
		# print "end_idx = " + str(end_idx)
		# print "self.np_traj.shape[1] = " + str(self.np_traj.shape[1])
		
		if end_idx > self.np_traj.shape[1]:

			end_idx = end_idx%self.np_traj.shape[1]
			future_traj_ids.extend(range( start_idx, self.np_traj.shape[1]) )
			future_traj_ids.extend(range( end_idx ) )

		else:

			future_traj_ids.extend(range( start_idx, end_idx) )


		future_traj_np = self.np_traj[:, future_traj_ids]

		future_traj_np[0, 0] = self.state[0]
		future_traj_np[1, 0] = self.state[1]
		future_traj_np[2, 0] = self.state[2]
		
		# print "future_traj_distance_traveled = " + str(future_traj_distance_traveled) 

		# print "len(future_traj) = " + str(len(future_traj))
		# print "len(future_traj[0]) = " + str(len(future_traj[0]))
		# print "future_traj_np.shape = " + str(future_traj_np.shape)

		# print "future_traj = " + str(future_traj)
		# print "future_traj_np = " + str(future_traj_np)



		return future_traj_np

	def find_closest_trajectory_point(self, state, current_idx = 0, number_points_ahead = 0):

		# This function will look for the closest point (in the trajectory) to state.
		# The points in the trajectory that are searched range from current_idx to current_idx + number_points_ahead 
		# (trajectory indexes wrap around)


		# Re-written making use of numpy. The closest point search becomes much faster.
		# TODO: Can be further improved by using the search_ranges

		np_state = numpy.array([[state[0]],[state[1]]])
		
		temp_distance = numpy.sum((self.np_traj[0:2, :] - np_state)**2, axis = 0)

		# Find the closest trajectory point that matches my desired speed and current heading
		best_idx = numpy.argmin(temp_distance)

		return best_idx


	def get_trajectory_tracking_inputs(self, current_time):

		# This is the one being used now!
		self.vehicle_iteration += 1

		self.state = [ self.bodies_array[self.id].x, self.bodies_array[self.id].y, math.radians( self.bodies_array[self.id].yaw ) ]
		self.sensor_readings = self.bodies_array[self.id].sensor_readings

		
		current_idx = self.current_trajectory_id

		number_points_ahead = 25

		best_idx = self.find_closest_trajectory_point( self.state, current_idx, number_points_ahead)
	
		traj_len = len( self.traj[0] )

		#if ( not self.loop_trajectory and (best_idx+5)%traj_len < self.current_trajectory_id ) or best_idx == len(self.traj[0])-1:
		#if ( not self.loop_trajectory and (best_idx+5)%traj_len < self.current_trajectory_id ) or best_idx == len(self.traj[0])-1:
		if ( not self.loop_trajectory and ( (best_idx+5)%traj_len < self.current_trajectory_id or best_idx == len(self.traj[0])-1 ) ):

			print "Trajectory Finished"
			self.bodies_array[self.id].commands['throttle'] = 0.
			self.bodies_array[self.id].commands['steering'] = 0.

			self.trajectory_finished = True

			return

		self.current_trajectory_id = best_idx

		best_idx += 5

		# reference_state = [self.traj[0][ (current_idx+best_idx)%traj_len ] , self.traj[1][ (current_idx+best_idx)%traj_len ] , self.traj[2][ (current_idx+best_idx)%traj_len ] ]
		reference_state = [self.traj[0][ (best_idx)%traj_len ] , self.traj[1][ (best_idx)%traj_len ] , self.traj[2][ (best_idx)%traj_len ] ]


		safety_distance = 10.
		full_stop_distance = 3.
		
		# Will check distance to cars in front
		distance_to_car = self.distance_to_car_in_future_trajectory(current_time, self.sensor_readings, safety_distance)

		desired_velocity = self.cruise_velocity / 3.6

		if distance_to_car < full_stop_distance:

			print "Full stop, vehicle id: " + str( self.id )

			desired_velocity = 0.
			
		elif distance_to_car < safety_distance:

			desired_velocity = (self.cruise_velocity / 3.6)*(distance_to_car/safety_distance)
		
		[throttle_command, steering_command] = self.controller_action(self.state, reference_state, desired_velocity)

		self.bodies_array[self.id].commands['throttle'] = throttle_command
		self.bodies_array[self.id].commands['steering'] = steering_command

		return

	def get_throttle_CC(self, desired_velocity):
		# This function is called when the CC mode is on

		# It simply runs a PID that outputs the longitudinal velocity
		# The error source of this PID is the error in the current distance
		# to the front vehicle, agains the desired distance

		#dt = datetime.now()
		#current_time = dt.
		current_time = time.time()

		linear_velocity = math.hypot(self.bodies_array[self.id].x_speed, self.bodies_array[self.id].y_speed)

		current_error = desired_velocity - linear_velocity

		if self.last_CC_time:

			# Compute last sampling time
			time_passed = current_time - self.last_CC_time
			self.last_CC_time = current_time

			# Integrate the error
			self.CC_I_e += time_passed*current_error


			# To avoid integration windup (i.e. big steady errors will result in big overshoots)
			max_integral_term = 20.
			# if self.CC_I_e > max_integral_term:

			# 	self.CC_I_e = max_integral_term

			max_integral_action = 1000.
			integral_action = self.CC_k_i*self.CC_I_e

			if math.fabs(integral_action) > max_integral_action:

				integral_action = math.copysign(max_integral_action, integral_action)
				self.CC_I_e = integral_action/self.CC_k_i
				# math.copysign(x, y): Return x with the sign of y. 
			


				# print "Vehicle " + str(self.vehicle_id) + " is using CC, speed = " + str( velocity_CC ) + " with integral term " + str( self.CC_I_e  )

			# Compute the derivative of the error
			if time_passed >0:
				self.derivative_error = (current_error - self.last_CC_e)/time_passed

			self.last_CC_e = current_error

			# if self.vehicle_id == -3:

				# print "self.last_CC_e = " + str(self.last_CC_e)
				# print "P = " + str(self.CC_k_p*current_error) + " I = " + str(integral_action) + " D = " + str(self.CC_k_d*self.derivative_error)

			# Compute PID output
			# velocity_CC = self.desired_velocity + self.CC_k_p*current_error + integral_action + self.CC_k_d*self.derivative_error
			velocity_CC = 0 + self.CC_k_p*current_error + integral_action + self.CC_k_d*self.derivative_error
			# print "Integration contribution = " + str(integral_action)

		else:

			# Initialization of the ACCC controller (first iteration of this controller)
			self.last_CC_time = current_time
			self.last_CC_e = current_error
			self.CC_I_e = 0.0
			self.derivative_error = 0.0

			velocity_CC = 0

		# max_CC_velocity = 18.

		# if velocity_CC > max_CC_velocity:

		# 	velocity_CC = max_CC_velocity

		# print "velocity_CC = " + str(velocity_CC)


			# print "Vehicle " + str(self.vehicle_id) + " is using CC, speed = " + str( velocity_CC ) + " with integral term " + str( self.CC_I_e  )

		return velocity_CC

	def reset_CC(self):
		# To make sure that the CC is reseted when it is not used
		self.last_CC_time = []
		self.CC_I_e = []
		self.last_CC_e = []