import time, math, random, threading


# TODO define startMerging method
# TODO define addLaneChange method for interface with Felix overtaking

# TODO: Make sure all platoon merging flags reset
#	- pretty sure this is covered now, but it's hard to know if it's flawless...

# TODO: More sophisticated merge aborting, like reverting to old platoon, setting fwd/bwd partners correctly

# TODO: make headOnCollisionAvoidance() work on smartvehicles aswell

# TODO: improve overtaking for platoons
# TODO: make platoon merge possible without fully merged tail
# TODO: make the emergency velocity change less twitchy. maybe acceleration model will take care of this?

# TODO: the accc_ready event is sometimes too slow
#	- solution: define accc_ready_margin in control module instead, and have it depend on merge distance. linear dependency or not? min/max on margin?

# TODO: why doesnt the other platoon leader do merge prep comparison?


class VehicleSupervisoryModule:
	"This class is the Platooning Vehicle class used by the Platooning Manager Module"
	def __init__(self, default_velocity, default_lane, vehicle):

		# These variables store the desired lane and desired lane
		# They are read by SmartVehicle module, and redirected to the Control Module
		self.desired_lane = default_lane

		# gains access to the vehicle variables like perception_grid
		self.vehicle = vehicle

		# Definition of string constants
		self.RIGHT_LANE_STRING = "right_lane"
		self.CENTER_LANE_STRING = "center_lane"
		self.LEFT_LANE_STRING = "left_lane"

		self.platoon_merge_distance_factor = 0.20 # Determines distance from obstacle at which platoon merge is initiated. Good value: 0.20

		self.savedSTOM_partner_candidate = None

		self.old_velocity = None
		self.emergency_break_distance = 4
		self.emergency_velocity_distance = 10
		self.emergency_velocity_distance_platoon_merge = 30
		self.overtake_distance = 14
		#self.suspended_velocity_change = None


		self.dummy_time = time.time()

		# Desired velocity is in m/s (?)
		self.vehicle.desired_velocity = 50./3.6

		self.perception_module = None
		self.module_start_time = time.time()
		self.modules_initiation_delay = 0.5
		self.start_messaging = False

		self.lane_change_time = time.time() # for now used to make sure a lane change isn't requested within 1 second of another lane change
		self.lane_change_pause = False

		self.ping_time = time.time()
		self.ping_delay = .8

		#Tips
		# self.state = [real_world_meters, radians]
		# print "self.state = " + str(self.state)
		# closeby_body_readings = [real_world_meters, radians]
		# print "closeby_body_readings = " + str(closeby_body_readings)
		# self.traj[0] in real_world_meters
		# self.traj[2] in radians
		# print "self.traj[2] = " + str(self.traj[2])

		# initialization of the start time of last overtake
		self.overtake_time = self.module_start_time

		print "VehicleSupervisoryModule started"

	def step(self):
		# This is the step function that is running at each cycle of the thread
		# in SmartVehicle

		# Messaging needs to be blocked for a short period of time while modules are initializing, can probably be less than 0.5s
		if self.start_messaging:
			self.handleBroadcasts()
		else:
			if time.time()>self.module_start_time+self.modules_initiation_delay:
				self.start_messaging = True

		# BELOW IS FOR CHECKING PLAT MERGE FLAGS
		# if time.time()>30+self.module_start_time:
		# 	if not self.vehicle.hold_to_merge == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG hold_to_merge"
		# 	if not self.vehicle.merge_comparison_id == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG merge_comparison_id"
		# 	if not self.vehicle.upcoming_platoon_merge == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG upcoming_platoon_merge"
		# 	if not self.vehicle.in_merging_platoon == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG in_merging_platoon set to", self.vehicle.in_merging_platoon
		# 	if not self.vehicle.merge_prep_done == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG merge_prep_done"
		# 	if not self.vehicle.platoon_STOM_partner == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG platoon_STOM_partner"
		# 	if not self.vehicle.clear_for_merge == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG clear_for_merge"
		# 	if not self.vehicle.platoon_merge_request == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG platoon_merge_request"
		# 	if not self.vehicle.wait_for_STOM == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG wait_for_STOM"
		# 	if not self.vehicle.platoon_STOM == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG platoon_STOM"
		# 	if not self.vehicle.platoon_new_lane == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG platoon_new_lane"
		# 	if not self.vehicle.platoon_merge_length == 0:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG platoon_merge_length"
		# 	if not len(self.vehicle.counted_merging_vehicles) == 0:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG counted_merging_vehicles"
		# 	if not self.vehicle.merge_length_count_completed == False:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG merge_length_count_completed"
		# 	if not self.vehicle.merge_length_count_timer == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG merge_length_count_timer"
		# 	if not self.vehicle.got_platoon_merge_initiation_from == None:
		# 		print self.vehicle.id, "HAS INCORRECT FLAG got_platoon_merge_initiation_from set to", self.vehicle.got_platoon_merge_initiation_from


		# Checking if self is platoon leader
		if self.vehicle.bwd_pair_partner and not self.vehicle.fwd_pair_partner:
			self.vehicle.platoon_leader = True
		else:
			self.vehicle.platoon_leader = False


		# Checking obstacles
		if self.vehicle.saved_obstacle_messages:
			if self.vehicle.platoon_leader:
				self.handleObstacles()
				# Assume that the construction site will send info about when obstacle is cleared, at which point it will be deleted from saved_obstacle_messages

		# Normal from-behind-merging checks
		self.rearMergeChecks()

		# Handle the lane change queue etc
		self.handleLaneChanging()

		# Handle collision avoidance. Right now just head on collision avoidance.
		self.handleCollisionAvoidance()



		# Pinging info to bwd partner. Will not be done during platoon merge, due to changing of pair partners
		if time.time() > self.ping_time+self.ping_delay and not self.vehicle.upcoming_platoon_merge and not self.vehicle.overtaking_vehicle_flag:
			self.pingPartnerInfo()
			# self.handleFwdPartnerInfo() is called when message is received


		# if self.suspended_velocity_change != None and not self.vehicle.emergency_velocity_flag:
		# 	print "vehicle",self.vehicle.id,"setting suspended_velocity_change:", self.suspended_velocity_change
		# 	self.setVelocity(self.suspended_velocity_change)
		# 	self.suspended_velocity_change = None


		# Checking if merge_count completed:
		self.mergeLengthCountCheck()


		# Overtaking stuff
		if not self.lane_change_pause and not self.vehicle.upcoming_platoon_merge:
			if self.vehicle.overtaking_vehicle_flag:
				self.overtakeVehicle()
			else:
				self.overtakeCheck() # is also called in the overtakeVehicle() function



	def setVelocity(self, new_velocity_mps):
		'''
		self.vehicle.desired_velocity = new_velocity_mps
		self.alertFollowers(new_velocity = new_velocity_mps)
		'''
		if not self.vehicle.emergency_velocity_flag:
			self.vehicle.desired_velocity = new_velocity_mps
			self.alertFollowers(new_velocity = new_velocity_mps)
		#if self.vehicle.emergency_velocity_flag:
		#	self.suspended_velocity_change = new_velocity_mps
		#	self.alertFollowers(new_velocity = new_velocity_mps)


	def overtakeRelCheck(self):

		if self.perception_grid['fm'] :

			if self.perception_grid['fm'][0][0] <= 6.0:

				if self.desired_lane == self.LEFT_LANE_STRING:
					if not self.perception_grid['r']:
						self.laneChange(self.CENTER_LANE_STRING)
						print "Changing to center lane"
						return True
				elif self.desired_lane == self.CENTER_LANE_STRING:
					if not self.perception_grid['l']:
						self.laneChange(self.LEFT_LANE_STRING)
						print "changing to left lane"
						return True
					elif not self.perception_grid['r']:
						self.laneChange(self.RIGHT_LANE_STRING)
						print "changing to right lane"
						return True
				elif self.desired_lane ==  self.RIGHT_LANE_STRING:
					if not self.perception_grid['l']:
						self.laneChange(self.CENTER_LANE_STRING)
						print "changing to center lane"
						return True

		return False


	def setACCCTarget(self, ID, desired_distance):
		if self.vehicle.control_module:
			control_module = self.vehicle.control_module
			control_module.ACCC_target_id = ID
			control_module.ACCC_desired_distance = desired_distance
			control_module.ACCC = True
			# Just for Rui to be able to develop his controller stuff
			# control_module.lateral_following = True
			self.vehicle.ACCC_ready.clear() 
			return True
		else:
			print "No control module"
			return False

	def resetACCC(self):
		if self.vehicle.control_module:
			self.vehicle.control_module.ACCC = False
			# Just for Rui to be able to develop his controller stuff
			self.vehicle.control_module.lateral_following = False
			self.vehicle.control_module.ACCC_desired_distance = None
			self.vehicle.control_module.ACCC_target_id = None
			self.vehicle.ACCC_ready.clear()

	

	# COMMENTED CODE IS OLD WAY OF HANDLING PLATOONING THROUGH BROADCASTING

	# def handleMessages(self):
	# 	if self.vehicle.id in self.vehicle.bodies_array:
	# 		#if len(self.vehicle.bodies_array[self.vehicle.id].v2v_network_input)>0:
	# 			#print "Vehicle",self.vehicle.id, "has",len(self.vehicle.bodies_array[self.vehicle.id].v2v_network_input), "network messages"

	# 		while len(self.vehicle.bodies_array[self.vehicle.id].v2v_network_input)>0:
	# 			network_message = self.vehicle.bodies_array[self.vehicle.id].v2v_network_input.pop()
	# 			#incoming_message = MessageHandler.decodeMessage(message)
	# 			if network_message['vehicle_id'] == self.vehicle.id: # Doesn't read it's own message
	# 				continue
	# 			self.handleNetworkMessage(network_message)
	# 			self.vehicle.saved_network_messages[network_message['vehicle_id']] = network_message

	# 		while len(self.vehicle.bodies_array[self.vehicle.id].v2v_wifi_input)>0:
	# 			wifi_message = self.vehicle.bodies_array[self.vehicle.id].v2v_wifi_input.pop()
	# 			if wifi_message['vehicle_id'] == self.vehicle.id: # Doesn't read it's own message
	# 				continue
	# 			self.handleWifiMessage(wifi_message)
	# 			self.vehicle.saved_wifi_messages[wifi_message['vehicle_id']] = wifi_message

	# 		#OBS: BELOW IS TEMPORARY SOLUTION
	# 		self.vehicle.bodies_array[self.vehicle.id].v2v_network_output = [self.vehicle.messenger.generateMessage()]
	# 		self.vehicle.bodies_array[self.vehicle.id].v2v_wifi_output = [self.vehicle.messenger.generateMessage()]


	# def handleWifiMessage(self,  message_dict):
	# 	"""TODO: Docstring for handleShortRangeMessage.

	# 	:message_dict: TODO
	# 	:returns: TODO

	# 	"""


	# 	other_coords = [message_dict['x'], message_dict['y']]
	# 	road_coords = self.vehicle.perception_module.convertToRoadCoord(other_coords, self.vehicle.desired_lane)
	# 	rel_coords = self.vehicle.perception_module.getRelativeRoadCoordsOval(road_coords, self.vehicle.desired_lane)

	# 	# Same checks as the ones over network, to make sure desired merge is initiated quickly during short range
	# 	if rel_coords[0]>0 and not self.vehicle.platooned_vehicle: # Maybe have specific merge message that overrides this, so that platoons can merge
	# 		if not self.vehicle.desired_merge_flag:
	# 			self.vehicle.desired_merge_flag = message_dict['vehicle_id']
	# 			self.vehicle.desired_merge_rel_coords = rel_coords
	# 		elif self.vehicle.desired_merge_flag == message_dict['vehicle_id']:
	# 			self.vehicle.desired_merge_rel_coords = rel_coords
	# 		elif rel_coords[0] < self.vehicle.desired_merge_rel_coords[0]:
	# 			self.vehicle.desired_merge_flag = message_dict['vehicle_id']
	# 			self.vehicle.desired_merge_rel_coords = rel_coords

	# 	if message_dict['desired_merge_flag'] == self.vehicle.id:
	# 		if not self.vehicle.merge_accept_flag and (self.vehicle.bwd_pair_partner == message_dict['vehicle_id'] or not self.vehicle.bwd_pair_partner):
	# 			self.vehicle.merge_accept_flag = message_dict['vehicle_id']
	# 			self.vehicle.bwd_pair_partner = message_dict['vehicle_id']



	# 	if self.vehicle.desired_merge_flag:

	# 		if self.vehicle.merging_flag:
	# 			if not message_dict['STOM_flag'] and message_dict['vehicle_id'] == self.vehicle.desired_merge_flag:
	# 				# TODO
	# 				self.abortMerge()
	# 				pass
	# 			if message_dict['vehicle_id'] == self.vehicle.desired_merge_flag:
	# 				#print "rel y-coords:",rel_coords[1], ", merge error:",rel_coords[0] - self.vehicle.merge_distance
	# 				if abs(rel_coords[1]) < 0.5 and abs(rel_coords[0] - self.vehicle.merge_distance) < self.vehicle.maximum_merged_error:
	# 					# TODO
	# 					self.mergerIsDone()

	# 		elif message_dict['merge_accept_flag'] == self.vehicle.id and not self.vehicle.merge_request_flag:

	# 			if abs(rel_coords[0] - self.vehicle.merge_distance) < self.vehicle.maximum_merge_error:
	# 				# TODO define initiateMergeRequest()
	# 				self.initiateMergeRequest()

	# 		elif self.vehicle.merge_request_flag and message_dict['STOM_flag']:
	# 			if self.vehicle.bwd_pair_partner == message_dict['vehicle_id']:
	# 				# TODO
	# 				self.initiateMerge(message_dict['desired_lane'])
	# 			elif self.vehicle.fwd_pair_partner == message_dict['vehicle_id'] and (self.vehicle.id == message_dict['bwd_pair_partner'] or not message_dict['bwd_pair_partner']):
	# 				# TODO
	# 				new_desired_lane = message_dict['desired_lane']
	# 				self.initiateTailMerge(new_desired_lane)

		
	# 	if self.vehicle.merge_accept_flag == message_dict['vehicle_id'] and self.vehicle.STOM_flag and abs(rel_coords[1]) < 0.5 and abs(-rel_coords[0] - self.vehicle.merge_distance) < self.vehicle.maximum_merged_error:
	# 		self.mergeeIsdone()

	# 	if self.vehicle.merge_accept_flag == message_dict['vehicle_id']:
	# 		if message_dict['merge_request_flag'] and message_dict['desired_merge_flag'] == self.vehicle.id:
	# 			# Sends true just for now
	# 			self.vehicle.STOM_flag = True
	# 		elif message_dict['merging_flag']:
	# 			pass
	# 		elif message_dict['platoon_id'] == 1 and self.vehicle.merge_accept_flag == message_dict['vehicle_id']:
	# 			self.vehicle.merge_accept_flag = None
	# 			self.vehicle.STOM_flag = False

	# def handleNetworkMessage(self, message_dict):
	# 	# Reads long range messages
	# 	other_coords = [message_dict['x'],message_dict['y']]
	# 	road_coords = self.vehicle.perception_module.convertToRoadCoord(other_coords, self.vehicle.desired_lane)
	# 	rel_coords = self.vehicle.perception_module.getRelativeRoadCoordsOval(road_coords, self.vehicle.desired_lane)

	# 	# f = open('rel_coords.txt','a')
	# 	# f.write("Vehicle " + str(message_dict['vehicle_id']) + " is " + str(rel_coords[0]) + " relative to vehicle " + str(self.vehicle.id)+"\n")
	# 	# f.close()

	# 	# Check if vehicle in front, if so set desired_merge_flag (depending on whether or not it's already set) and save relative coords
	# 	if rel_coords[0]>0 and not self.vehicle.platooned_vehicle: # Maybe have specific merge message that overrides this, so that platoons can merge
	# 		if not self.vehicle.desired_merge_flag:
	# 			self.vehicle.desired_merge_flag = message_dict['vehicle_id']
	# 			self.vehicle.desired_merge_rel_coords = rel_coords
	# 		elif self.vehicle.desired_merge_flag == message_dict['vehicle_id']:
	# 			self.vehicle.desired_merge_rel_coords = rel_coords
	# 		elif rel_coords[0] < self.vehicle.desired_merge_rel_coords[0]:
	# 			self.vehicle.desired_merge_flag = message_dict['vehicle_id']
	# 			self.vehicle.desired_merge_rel_coords = rel_coords

	# 	# Check if a desired_merge_flag has been recieved, if so check if no merge_accept_flag has been assigned and check bwd_pair_partner and then set merge_accept_flag and assign bwd_pair_partner
	# 	if message_dict['desired_merge_flag'] == self.vehicle.id:
	# 		if not self.vehicle.merge_accept_flag and (self.vehicle.bwd_pair_partner == message_dict['vehicle_id'] or not self.vehicle.bwd_pair_partner):
	# 			self.vehicle.merge_accept_flag = message_dict['vehicle_id']
	# 			self.vehicle.bwd_pair_partner = message_dict['vehicle_id']

	# 	# Check if messanger is desired merge and if messanger has accepted merge, then change velocity depending on what the messanger is doing
	# 	if self.vehicle.desired_merge_flag == message_dict['vehicle_id'] and message_dict['merge_accept_flag'] == self.vehicle.id and not self.vehicle.merging_flag:
	# 		if message_dict['desired_merge_flag']:
	# 			self.vehicle.fwd_pair_partner = message_dict['vehicle_id']
	# 			self.desired_velocity = message_dict['velocity']
	# 		else:
	# 			self.vehicle.fwd_pair_partner = message_dict['vehicle_id']
	# 			self.desired_velocity = 1.4*message_dict['velocity']
		

	# def initiateMergeRequest(self):

	# 	""" Sends the merge request and sets variables
	# 	:returns: TODO


	# 	"""

	# 	self.vehicle.merge_request_flag = True


	# def initiateMerge(self, new_desired_lane):
	# 	"""function for initiating the merge
	# 	:returns: TODO

	# 	"""
	# 	self.vehicle.merge_request_flag = False
	# 	# TODO getLaneOfOther
	# 	self.desired_lane = new_desired_lane
	# 	self.setACCCTarget(self.vehicle.fwd_pair_partner, self.vehicle.merge_distance)
	# 	self.vehicle.merging_flag = True

	# def initiateTailMerge(self, new_desired_lane):
	# 	""" Function for initiating merge to the end of a platoon
	# 	:returns: TODO

	# 	"""
	# 	self.vehicle.merge_request_flag = False
	# 	self.desired_lane = new_desired_lane
	# 	self.setACCCTarget(self.vehicle.fwd_pair_partner, self.vehicle.merge_distance)
	# 	self.vehicle.merging_flag = True


	# def abortMerge(self):
	# 	""" Aborts a merge
	# 	:returns: TODO

	# 	"""

	# 	# just a joke :)
	# 	# self.desired_velocity = 100.
	# 	# it cost me one hour of debugging :(
	# 	# self.desired_velocity = -15.*float(self.vehicle.id)/4
	# 	pass



	# def mergerIsDone(self): 
	# 	# The one who initiated the merge is done

	# 	self.vehicle.desired_merge_flag = None
	# 	self.vehicle.merging_flag = False
	# 	self.vehicle.merge_request_flag = False

	# 	self.vehicle.platoon_id = 1
	# 	self.vehicle.platooned_vehicle = True

	# def mergeeIsdone(self): 
	# 	# The one who accepted the merge is done

	# 	self.vehicle.merge_accept_flag = None
	# 	self.vehicle.STOM_flag = False

	# 	self.vehicle.platoon_id = 1
	# 	self.vehicle.platooned_vehicle = True



	def handleBroadcasts(self):
		if self.vehicle.id in self.vehicle.bodies_array:

			self.saveSmartvehicleIDS()

			if not self.vehicle.fwd_pair_partner and not self.vehicle.new_fwd_pair_partner and not self.vehicle.platoon_leader:
				

				candidate = self.searchFwdPairPartner(self.vehicle.v2v_wifi_input)

				if candidate == None:
					candidate = self.searchFwdPairPartner(self.vehicle.v2v_network_input)

				if candidate != None:
					self.vehicle.messenger.sendDesiredUnspecifiedMerge(candidate)
					#self.vehicle.messenger.sendDesiredMerge(candidate)

		# Checking for obstacles on the road (construction work)
		self.checkObstacles(self.vehicle.v2v_network_input)

		self.vehicle.v2v_wifi_input = []
		self.vehicle.v2v_network_input = []

		self.vehicle.bodies_array[self.vehicle.id].v2v_network_output = [self.vehicle.messenger.generateMessage()]
		self.vehicle.bodies_array[self.vehicle.id].v2v_wifi_output = [self.vehicle.messenger.generateMessage()]


	def saveSmartvehicleIDS(self):
		for broadcast in self.vehicle.v2v_network_input:
			if 'obstacle' in broadcast:
				continue
			else: # Maybe do "if 'smartvehicle' in broadcast"
				if broadcast['vehicle_id'] not in self.vehicle.saved_smartvehicle_ids:
					self.vehicle.saved_smartvehicle_ids.add(broadcast['vehicle_id'])


	def searchFwdPairPartner(self, broadcasts):

		candidate = None
		opt_road_coords = None

		for broadcast in broadcasts:
			if 'obstacle' in broadcast:
				continue

			if broadcast['vehicle_id'] != self.vehicle.id:
				road_coords = self.vehicle.perception_module.convertToRoadCoord([broadcast['x'], broadcast['y']], self.vehicle.getCurrentLaneTrajectoryString())
				rel_road_coords = self.vehicle.perception_module.getRelativeRoadCoordsOval(road_coords, self.vehicle.getCurrentLaneTrajectoryString())


				if rel_road_coords[0] > 0 and not broadcast['new_bwd_pair_partner'] and broadcast['desired_lane'] == self.vehicle.desired_lane:
					if candidate == None:
						candidate = broadcast['vehicle_id']
						opt_road_coords = rel_road_coords
					elif rel_road_coords[0] < opt_road_coords[0]:
						candidate = broadcast['vehicle_id']
						opt_road_coords = rel_road_coords

		return candidate


	def rearMergeChecks(self):
		if self.vehicle.desired_merge_flag:
			if self.vehicle.desired_merge_flag in self.vehicle.perceived_objects:
				rel_des_merge_coords = self.vehicle.perceived_objects[self.vehicle.desired_merge_flag]
				if abs(rel_des_merge_coords[0] - self.vehicle.merge_distance) < self.vehicle.maximum_merge_error:
					self.vehicle.messenger.sendMergeRequest(self.vehicle.desired_merge_flag)

		if self.vehicle.preparing_STOM_flag:
			#FOR NOW, JUST SEND STOM
			#print "sending stom"
			self.vehicle.messenger.sendSTOM(self.vehicle.preparing_STOM_flag)


		if self.vehicle.merging_flag:
			if self.vehicle.merging_flag in self.vehicle.perceived_objects:
				rel_merge_coords = self.vehicle.perceived_objects[self.vehicle.merging_flag]
				if abs(rel_merge_coords[1]) < 0.5 and abs(rel_merge_coords[0] - self.vehicle.merge_distance) < self.vehicle.maximum_merged_error:
					#print "sending merge complete"
					self.vehicle.messenger.sendMergeComplete(self.vehicle.merging_flag)



	def alertFollowers(self, new_velocity = None):
		""" Should alert follower, i.e. the bwd_pair_partners about speed change etc


		:new_velocity: TODO
		:returns: TODO

		"""

		if new_velocity != None:

			# if there is a platooning vehicle behind
			if self.vehicle.bwd_pair_partner:
				self.vehicle.messenger.sendNewVelocity(self.vehicle.bwd_pair_partner, new_velocity)
			# if there is an incoming merging vehicle
			if self.vehicle.new_bwd_pair_partner:
				# if also in a merge
				if self.vehicle.new_fwd_pair_partner or self.vehicle.upcoming_platoon_merge:
					#print self.vehicle.id, "sending own velocity to bwd pair partner"
					self.vehicle.messenger.sendNewVelocity(self.vehicle.new_bwd_pair_partner, new_velocity)
				else:

					self.vehicle.messenger.sendNewVelocity(self.vehicle.new_bwd_pair_partner, 1.4*new_velocity)


	def alertOthers(self, new_velocity):

		self.alertFollowers(new_velocity)

		if self.vehicle.platooned_vehicle:
			if self.vehicle.fwd_pair_partner:
				self.vehicle.messenger.sendNewVelocityFwd(self.vehicle.fwd_pair_partner, new_velocity)
			if self.vehicle.new_fwd_pair_partner:
				self.vehicle.messenger.sendNewVelocityFwd(self.vehicle.new_fwd_pair_partner, new_velocity)
					


	def pingPartnerInfo(self):
		if self.vehicle.bwd_pair_partner:
			self.vehicle.messenger.sendForwardPartnerInfo(self.vehicle.bwd_pair_partner)
			self.ping_time = time.time()
		elif self.vehicle.new_bwd_pair_partner: # assume a vehicle won't have bwd_pp and new_bwd_pp at the same time, otherwise prioritize bwd_pp. Don't send to both, it will conflict on the other side
			self.vehicle.messenger.sendForwardPartnerInfo(self.vehicle.new_bwd_pair_partner)
			self.ping_time = time.time()


	def handleFwdPartnerInfo(self):

		#Changing lane to fwd pp:s lane if you are platooned, and not currently performing platoon merge
		if self.vehicle.fwd_pair_partner == self.vehicle.forward_partner_info['vehicle_id']:
			if not self.vehicle.desired_lane == self.vehicle.forward_partner_info['lane'] and self.vehicle.platooned_vehicle:
				if not self.vehicle.upcoming_platoon_merge and not self.vehicle.overtaking_vehicle_flag: # Maybe also check if not currently performing overtaking etc
					print self.vehicle.id, "CHANGING TO LANE OF", self.vehicle.fwd_pair_partner
					self.addLaneChange(self.vehicle.forward_partner_info['lane'])



	def startPlatoonMerge(self):
		# TODO: also needs to check if closest vehicle is in platoon
		grid_check_list = ['fl','fr','l','r','rl','rr']
		if not self.vehicle.control_module.ACCC:
			# Going over each part of the grid, prioritzing fwd and left sides
			for grid_part in grid_check_list:
				if grid_part in self.vehicle.closest_objects:
					other_id = self.vehicle.closest_objects[grid_part]
					# checking if the other is a smart vehicle
					if other_id in self.vehicle.saved_smartvehicle_ids:
						self.prepareACCCForMergeStart(other_id, 6)
						break



	def doMergePrepComparison(self):
		# does the merge comparison id exist?
		if self.vehicle.merge_comparison_id != None and self.vehicle.in_merging_platoon != None:

			# does current vehicle have bwd_pair_partner to compare to?
			if not self.vehicle.bwd_pair_partner:
				self.vehicle.new_bwd_pair_partner = self.vehicle.merge_comparison_id
				# send a message forward to inform that the merge prep is done,
				# every vehicle should now have a new bwd and fwd pair partner
				in_merging_platoon = int(not self.vehicle.in_merging_platoon)
				self.vehicle.messenger.sendHoldToMerge(self.vehicle.merge_comparison_id, in_merging_platoon)

			else:

				# get the coordinates for the comparison
				#print self.vehicle.id, "comparing these vehicles: ", self.vehicle.bwd_pair_partner, self.vehicle.merge_comparison_id
				x_current_bpp = self.vehicle.perceived_objects[self.vehicle.bwd_pair_partner][0]
				x_cand_bpp = self.vehicle.perceived_objects[self.vehicle.merge_comparison_id][0]

				# TODO what happens in else?
				if x_cand_bpp < 0:

					# check to see if candidate is closer
					if x_cand_bpp > x_current_bpp:
						# set the new bpp to it
						self.vehicle.new_bwd_pair_partner = self.vehicle.merge_comparison_id
						#print self.vehicle.new_bwd_pair_partner
						# send message to initiate comparison backwards in platoon
						in_merging_platoon = int(not self.vehicle.in_merging_platoon)
						self.vehicle.messenger.sendDoMergePrepComp(self.vehicle.new_bwd_pair_partner, self.vehicle.bwd_pair_partner, in_merging_platoon)
					else:
						self.vehicle.new_bwd_pair_partner = self.vehicle.bwd_pair_partner
						in_merging_platoon = self.vehicle.in_merging_platoon
						self.vehicle.messenger.sendDoMergePrepComp(self.vehicle.new_bwd_pair_partner, self.vehicle.merge_comparison_id, in_merging_platoon)


		# reset the merge_comparison_id
		#print "The new_bwd_pair_partner of ", self.vehicle.id, " is ", self.vehicle.new_bwd_pair_partner
		self.vehicle.merge_comparison_id = None


	def mergePrepDone(self, STOM_partner_candidate):
		if self.vehicle.platoon_leader and self.vehicle.in_merging_platoon:
			# Maybe start some timer here that makes sure merging still happens even if communication breaks down somewhere. Or that it is aborted?
			self.vehicle.merge_length_count_timer = time.time()
			self.vehicle.platoon_merge_length += 1
			if self.vehicle.bwd_pair_partner:
				self.vehicle.messenger.sendRequestMergeLengthInfo(self.vehicle.bwd_pair_partner, self.vehicle.id)
			else:
				self.vehicle.merge_length_count_completed = True
			self.savedSTOM_partner_candidate = STOM_partner_candidate
			#self.mergePrepDoneContinuation(STOM_partner_candidate)


		else:
			self.mergePrepDoneContinuation(STOM_partner_candidate)


	def mergePrepDoneContinuation(self, STOM_partner_candidate):
		self.vehicle.merge_prep_done = 1

		# TODO add safety checks here!!!

		if type(self.vehicle.hold_to_merge) is int:
			if self.vehicle.hold_to_merge != self.vehicle.id:
				self.vehicle.new_fwd_pair_partner = self.vehicle.hold_to_merge
				if self.vehicle.in_merging_platoon:
					self.vehicle.platoon_STOM_partner = STOM_partner_candidate
				else:
					self.vehicle.platoon_STOM_partner = self.vehicle.id

				self.vehicle.messenger.sendMergePrepDone(self.vehicle.hold_to_merge, self.vehicle.platoon_STOM_partner)
				#print self.vehicle.id, "has", self.vehicle.new_fwd_pair_partner, "as new_fwd_pair_partner"
			else:
				#print self.vehicle.id, "received a merge_prep_done message"
				if self.vehicle.in_merging_platoon:
					self.vehicle.platoon_STOM_partner = STOM_partner_candidate
				else:
					self.vehicle.platoon_STOM_partner = self.vehicle.id

				# giving new_bwd_pp clearence to initiate (platoon) merge
				#self.vehicle.hold_to_merge = None
				self.vehicle.messenger.sendClearForMerge(self.vehicle.new_bwd_pair_partner)

		else:
			print "FATAL ERROR: EVERYONE IS GOING TO DIE OF A FUNKY DISEASE!"



	def mergeLengthCountCheck(self):
		# This function is used by the vehicle that's performing the count, though atm called by everyone (but doing nothing for the others)

		if self.vehicle.merge_length_count_timer != None:
			# If no response in 0.5 seconds, abort the merge
			if time.time() > self.vehicle.merge_length_count_timer+0.5:
				self.abortPlatoonMerge()
				self.sendAbortPlatoonMergeFwd()
				self.sendAbortPlatoonMergeBwd()

		if self.vehicle.merge_length_count_completed:
			if self.vehicle.upcoming_platoon_merge:
				self.vehicle.merge_length_count_timer = None
				self.checkMergeDistance()
			else:
				# This happens if platoon merge has already been aborted, but merge length count completion arrives late. For safety just call abortPlatoonMerge
				self.abortPlatoonMerge()


	def checkMergeDistance(self):
		if self.vehicle.emergency_velocity_flag:
			velocity_factor = math.pow(self.old_velocity,2)
		else:
			velocity_factor = math.pow(self.vehicle.desired_velocity,2)

		merge_distance = velocity_factor*math.pow(float(self.vehicle.platoon_merge_length),0.8)*self.platoon_merge_distance_factor

		if self.vehicle.saved_obstacle_messages:
			for obstacle_id in self.vehicle.saved_obstacle_messages:
				message = self.vehicle.saved_obstacle_messages[obstacle_id]
				other_coords = [message['x'], message['y']]
				road_coords = self.vehicle.perception_module.convertToRoadCoord(other_coords, self.vehicle.desired_lane)
				rel_coords = self.vehicle.perception_module.getRelativeRoadCoordsOval(road_coords, self.vehicle.desired_lane)
				if message['lane'] == self.vehicle.desired_lane and rel_coords[0]>0 and rel_coords[0]<merge_distance:
					print "Starting merge at distance:", merge_distance
					self.mergePrepDoneContinuation(self.savedSTOM_partner_candidate)
					self.vehicle.merge_length_count_completed = False
		else:
			self.mergePrepDoneContinuation(self.savedSTOM_partner_candidate)
			self.vehicle.merge_length_count_completed = False


	def forwardRequestMergeLengthInfo(self, in_merging_platoon_info, leader_id):
		self.vehicle.messenger.sendMergeLengthInfo(leader_id, in_merging_platoon_info)


		if self.vehicle.bwd_pair_partner:
			self.vehicle.messenger.sendRequestMergeLengthInfo(self.vehicle.bwd_pair_partner, leader_id)

		else:
			# Maybe have this protocol set an event which triggers merge
			self.vehicle.messenger.sendMergeLengthCountCompleted(leader_id, True)


	def handleClearForMerge(self):
		if self.vehicle.clear_for_merge == 1 and self.vehicle.upcoming_platoon_merge:
			#self.vehicle.hold_to_merge = None

			if self.vehicle.in_merging_platoon: 
				if self.vehicle.platoon_STOM_partner != self.vehicle.new_fwd_pair_partner:
					# initiating merge
					self.initiatePlatoonMerge()
				elif self.vehicle.platoon_STOM_partner == self.vehicle.new_fwd_pair_partner:
					# regular behind merge
					#print self.vehicle.id, "sending merge request"
					self.vehicle.messenger.sendMergeRequest(self.vehicle.new_fwd_pair_partner)
			
			elif not self.vehicle.in_merging_platoon:
				# forwarding merging clearance
				self.platoonMergeDone()

		else:
			print "ERROR: Clerance for merge not given"

	def initiatePlatoonMerge(self):
		#print self.vehicle.id, "initiating merge"
		self.setACCCTarget(self.vehicle.new_fwd_pair_partner, self.vehicle.merge_distance)
		self.waitForEvent(self.vehicle.ACCC_ready, self.platSplitAndMergeRequest , self.vehicle.send_platoon_merge_request_abort, function_params = [self.vehicle.platoon_STOM_partner])




	def waitForEvent(self, event, function_to_run, abort_parameter, function_params = ()):
		"""
		This function starts a thread that waits on event and then runs function_to_run with arguments function_params.

		:event: Thread waits for this event
		:function_to_run: Function to run when event is triggered
		:abort_parameter: Thread checks this to be False before running function_to_run
		:function_params: tuple or list of the parameters passed to function_to_run
		"""

		temp_thread = threading.Thread(target= self.waitForEventThread, args = (function_to_run, event, abort_parameter, function_params))
		temp_thread.daemon = True
		temp_thread.start()

	def waitForEventThread(self, function_to_run, event, abort_parameter, function_params):
		
		event.wait()

		if not abort_parameter:
			function_to_run(*function_params)

		# You shouldn't clear event, right?


	def platSplitAndMergeRequest(self, platoon_STOM_partner):
		if self.vehicle.bwd_pair_partner:
			self.vehicle.messenger.sendSplitPlat(self.vehicle.bwd_pair_partner)

		if self.vehicle.platoon_STOM_partner:
			self.vehicle.messenger.sendPlatoonMergeRequest(platoon_STOM_partner)
		else:
			print "Does not have platoon_STOM_partner"


	# def waitForMergeLengthCount(self):
	# 	self.waitForMergeLengthCount(self.vehicle.merge_length_count_completed, self.checkMergeDistance, self.send_merge_length_count_completed_abort)


	def prepareACCCForMergeStart(self, target, ACCC_desired_distance):
		self.setACCCTarget(target, ACCC_desired_distance)
		self.waitForEvent(self.vehicle.ACCC_ready, self.vehicle.messenger.sendDesiredUnspecifiedMerge, False, function_params = [target])
		print self.vehicle.id, "waiting for accc on", target

	def waitForPlatoonSTOM(self):
		#print self.vehicle.id, "is waiting for STOM from", self.vehicle.wait_for_STOM
		self.waitForEvent(self.vehicle.STOM_event, self.startMerge, self.vehicle.start_merge_abort)


	def preparePlatoonSTOM(self):
		#print self.vehicle.id, "is preparing merge and will send a STOM soon"
		self.setACCCTarget(self.vehicle.fwd_pair_partner, 2*self.vehicle.merge_distance)
		self.waitForEvent(self.vehicle.ACCC_ready, self.prepareACCCForSTOM, self.vehicle.send_STOM_abort)


	def prepareACCCForSTOM(self):
		self.setACCCTarget(self.vehicle.platoon_merge_request, self.vehicle.merge_distance)
		self.waitForEvent(self.vehicle.ACCC_ready, self.vehicle.messenger.sendPlatoonSTOM, self.vehicle.send_STOM_abort, function_params = [self.vehicle.platoon_merge_request, self.vehicle.desired_lane])


	def startMerge(self):
		if self.vehicle.platoon_STOM and self.vehicle.platoon_new_lane:
			self.addLaneChange(self.vehicle.platoon_new_lane)
		self.waitForEvent(self.vehicle.lanekeeping_ready, self.platoonMergeDone, self.vehicle.send_platoon_merge_done_abort)


	def sendAbortPlatoonMergeFwd(self):
		if self.vehicle.fwd_pair_partner:
			self.vehicle.messenger.sendAbortPlatoonMerge(self.vehicle.fwd_pair_partner, fwd = True)
		if self.vehicle.new_fwd_pair_partner:
			self.vehicle.messenger.sendAbortPlatoonMerge(self.vehicle.new_fwd_pair_partner, fwd = True)


	def sendAbortPlatoonMergeBwd(self):
		if self.vehicle.bwd_pair_partner:
			self.vehicle.messenger.sendAbortPlatoonMerge(self.vehicle.bwd_pair_partner, bwd = True)
		if self.vehicle.new_bwd_pair_partner:
			self.vehicle.messenger.sendAbortPlatoonMerge(self.vehicle.new_bwd_pair_partner, bwd = True)


	def abortPlatoonMerge(self):
		# are these all the flags that need to be reset? Check with magnus. 
		# This function needs to do more stuff, make sure fwd_pp etc are set correctly, and maybe reset into original platoons.
		print self.vehicle.id, "GOT ABORT PLATOON MERGE"
		self.vehicle.hold_to_merge = None
		self.vehicle.merge_comparison_id = None
		self.vehicle.upcoming_platoon_merge = None
		self.vehicle.in_merging_platoon = None
		self.vehicle.merge_prep_done = None
		self.vehicle.platoon_STOM_partner = None
		self.vehicle.clear_for_merge = None
		self.vehicle.platoon_merge_request = None
		self.vehicle.wait_for_STOM = None
		self.vehicle.platoon_STOM = None
		self.vehicle.platoon_new_lane = None
		self.vehicle.platoon_merge_length = 0
		self.vehicle.counted_merging_vehicles = []
		self.vehicle.merge_length_count_completed = False
		self.vehicle.merge_length_count_timer = None


	def platoonMergeDone(self):
		print "Platoon merge done for vehicle", self.vehicle.id
		if self.vehicle.in_merging_platoon:

			self.vehicle.fwd_pair_partner = self.vehicle.new_fwd_pair_partner
			self.vehicle.new_fwd_pair_partner = None
			self.vehicle.messenger.sendPlatoonMergeDone(self.vehicle.fwd_pair_partner, True)

			self.vehicle.bwd_pair_partner = self.vehicle.platoon_STOM_partner
			self.vehicle.platoon_STOM_partner = None
			self.vehicle.messenger.sendPlatoonMergeDone(self.vehicle.bwd_pair_partner, False)
			
			self.vehicle.platoon_STOM = None
			self.vehicle.platoon_new_lane = None

			self.vehicle.platoon_merge_length = 0
			self.vehicle.counted_merging_vehicles = []
			self.vehicle.merge_length_count_completed = False

		else:
			self.vehicle.fwd_pair_partner = self.vehicle.new_fwd_pair_partner
			self.vehicle.new_fwd_pair_partner = None
			self.vehicle.messenger.sendPlatoonMergeDone(self.vehicle.fwd_pair_partner, True)

			self.vehicle.platoon_STOM_partner = None

			self.vehicle.platoon_merge_request = None

		self.vehicle.clear_for_merge = None
		self.vehicle.in_merging_platoon = None

		self.vehicle.hold_to_merge = None
		self.vehicle.merge_prep_done = None
		self.vehicle.wait_for_STOM = None


		if self.vehicle.new_bwd_pair_partner:
			self.vehicle.messenger.sendClearForMerge(self.vehicle.new_bwd_pair_partner)


	def handlePlatSplit(self, split_partner):

		if split_partner == self.vehicle.bwd_pair_partner:
			self.vehicle.bwd_pair_partner = None

		elif split_partner == self.vehicle.fwd_pair_partner:
			if self.vehicle.upcoming_platoon_merge:
				if self.vehicle.new_fwd_pair_partner in self.vehicle.perceived_objects:
					self.setACCCTarget(self.vehicle.new_fwd_pair_partner, self.vehicle.merge_distance)
				else:
					print "ERROR: new_fwd_pair_partner is not visible before platoon merge when splitting"

			self.vehicle.fwd_pair_partner = None

		else: 
			print "Split partner", split_partner, "is invalid."


	''' ADDED STUFF BELOW '''

	def calcVelocitySensorReadings(self, reading):
		x_speed = reading[4]
		y_speed = reading[5]
		return math.hypot(x_speed, y_speed)


# OBSTACLE STUFF

	def checkObstacles(self, broadcasts):
		for broadcast in broadcasts:
			if 'obstacle' in broadcast:
				self.vehicle.saved_obstacle_messages[broadcast['obstacle_id']] = broadcast


	def handleObstacles(self):
		for obstacle_id in self.vehicle.saved_obstacle_messages:
			message = self.vehicle.saved_obstacle_messages[obstacle_id]
			other_coords = [message['x'], message['y']]
			road_coords = self.vehicle.perception_module.convertToRoadCoord(other_coords, self.vehicle.desired_lane)
			rel_coords = self.vehicle.perception_module.getRelativeRoadCoordsOval(road_coords, self.vehicle.desired_lane)
			if message['lane'] == self.vehicle.desired_lane and rel_coords[0]>0:# and rel_coords[0]<math.pow(self.vehicle.desired_velocity,2)*self.platoon_merge_distance_factor:
				if self.vehicle.platoon_leader and not self.vehicle.new_fwd_pair_partner:
					self.startPlatoonMerge()
			if 'restricted_velocity' in message and self.vehicle.platoon_leader and rel_coords[0]<0 and abs(rel_coords[0])<30:
				self.setVelocity(message['restricted_velocity'])


	def handleCollisionAvoidance(self):
		# if self.vehicle.emergency_velocity_flag:
		# 	self.headOnCollisionAvoidance(self.emergency_velocity_distance_platoon_merge, emergency_objects = self.vehicle.saved_obstacle_messages)

		# if self.vehicle.in_merging_platoon and not self.vehicle.emergency_velocity_flag:
		# 	self.headOnCollisionAvoidance(self.emergency_velocity_distance_platoon_merge, emergency_objects = self.vehicle.saved_obstacle_messages)

		if not self.vehicle.in_merging_platoon:
			self.headOnCollisionAvoidanceNEW(self.emergency_velocity_distance, ignored_objects = self.vehicle.saved_smartvehicle_ids)

		if self.vehicle.in_merging_platoon:
			self.headOnCollisionAvoidanceNEW(self.emergency_velocity_distance_platoon_merge, ignored_objects = self.vehicle.saved_smartvehicle_ids)


	def headOnCollisionAvoidance(self, emergency_distance, emergency_objects = None):
		if emergency_objects == None:
			# here make the function work in the general case
			pass

		if 'fm' in self.vehicle.closest_objects:
			other_id = self.vehicle.closest_objects['fm']
			if other_id in emergency_objects:
			#if other_id != self.vehicle.new_fwd_pair_partner and other_id != self.vehicle.fwd_pair_partner:

				if self.vehicle.perceived_objects[other_id][0]<self.emergency_break_distance:
					if self.old_velocity == None:
						self.old_velocity = self.vehicle.desired_velocity
					self.vehicle.desired_velocity = 0 # needs to override the emergency_velocity_flag
					self.alertFollowers(new_velocity = 0)
					self.vehicle.emergency_velocity_flag = True
					print "EMERGENCY BREAK FOR VEHICLE", self.vehicle.id

				elif self.vehicle.perceived_objects[other_id][0]<emergency_distance:
					if self.old_velocity == None:
						self.old_velocity = self.vehicle.desired_velocity
					found_vel = False
					for reading in self.vehicle.sensor_readings:
						if reading[0] == self.vehicle.closest_objects['fm']:
							self.vehicle.desired_velocity = self.calcVelocitySensorReadings(reading)*0.9 # slightly lower to account for errors etc
							found_vel = True
							break
					if not found_vel or self.vehicle.desired_velocity<4:
						#self.setVelocity(0)
						self.vehicle.desired_velocity = 4 # JUST FOR NOW, IN CASE CAR IN FRONT HAS 0 VELOCITY
					#print "EMERGENCY VELOCITY CHANGE FOR VEHICLE", self.vehicle.id
					self.alertOthers(self.vehicle.desired_velocity)
					self.vehicle.emergency_velocity_flag = True

				# no more emergency if sufficiently far away, or if it is fwd_pp and kinda far away
				elif self.vehicle.perceived_objects[other_id][0]>emergency_distance*1.2 or ((other_id == self.vehicle.fwd_pair_partner or other_id == self.vehicle.new_fwd_pair_partner) and self.vehicle.perceived_objects[other_id][0]>self.emergency_break_distance):
					self.collisionAverted()
			else:
				# no more emergency if obstacle not in 'fm'
				self.collisionAverted()
		else:
			# no more emergency if no vehicles in 'fm'
			self.collisionAverted()


	def headOnCollisionAvoidanceNEW(self, emergency_distance, ignored_objects = []):

		if 'fm' in self.vehicle.closest_objects: # use perception_grid['fm'] instead??
			other_id = self.vehicle.closest_objects['fm']
			if other_id not in ignored_objects:
			#if other_id != self.vehicle.new_fwd_pair_partner and other_id != self.vehicle.fwd_pair_partner:

				if self.vehicle.perceived_objects[other_id][0]<self.emergency_break_distance:
					if self.old_velocity == None:
						self.old_velocity = self.vehicle.desired_velocity
					self.vehicle.desired_velocity = 0 # needs to override the emergency_velocity_flag
					self.alertFollowers(new_velocity = 0)
					self.vehicle.emergency_velocity_flag = True
					print "EMERGENCY BREAK FOR VEHICLE", self.vehicle.id

				elif self.vehicle.perceived_objects[other_id][0]<emergency_distance:
					if self.old_velocity == None:
						self.old_velocity = self.vehicle.desired_velocity
					found_vel = False
					for reading in self.vehicle.sensor_readings:
						if reading[0] == self.vehicle.closest_objects['fm']:
							self.vehicle.desired_velocity = self.calcVelocitySensorReadings(reading)*0.9 # slightly lower to account for errors etc
							found_vel = True
							break
					if not found_vel or self.vehicle.desired_velocity<4:
						#self.setVelocity(0)
						self.vehicle.desired_velocity = 4 # JUST FOR NOW, IN CASE CAR IN FRONT HAS 0 VELOCITY
					#print "EMERGENCY VELOCITY CHANGE FOR VEHICLE", self.vehicle.id
					self.alertOthers(self.vehicle.desired_velocity)
					self.vehicle.emergency_velocity_flag = True

				# no more emergency if sufficiently far away, or if it is fwd_pp and kinda far away
				elif self.vehicle.perceived_objects[other_id][0]>emergency_distance*1.2 or ((other_id == self.vehicle.fwd_pair_partner or other_id == self.vehicle.new_fwd_pair_partner) and self.vehicle.perceived_objects[other_id][0]>self.emergency_break_distance):
					self.collisionAverted()
			else:
				# no more emergency if obstacle not in 'fm'
				self.collisionAverted()
		else:
			# no more emergency if no vehicles in 'fm'
			self.collisionAverted()



	def collisionAverted(self):
		if self.vehicle.emergency_velocity_flag:
			print "COLLISION AVERTED FOR VEHICLE", self.vehicle.id, "RETURNING TO VELOCITY", self.old_velocity
			self.vehicle.emergency_velocity_flag = False
			self.setVelocity(self.old_velocity)
			self.alertOthers(self.old_velocity)
			self.old_velocity = None


	''' Lane change functions '''

	def handleLaneChanging(self):
		if self.lane_change_pause:
			if self.vehicle.desired_velocity == 0:
				pause_time = 2
			else:
				pause_time = 20/self.vehicle.desired_velocity
			if pause_time>5:
				pause_time = 5
			if time.time() > self.lane_change_time + pause_time:
				self.lane_change_pause = False
				self.lane_change_time = time.time()

		# Cleaning up new_desired_lanes:
		if self.vehicle.new_desired_lanes:
			while self.vehicle.new_desired_lanes[0] == self.vehicle.desired_lane:
				#print "CLEANING UP NEW_DES_LANES"
				self.vehicle.new_desired_lanes.pop(0)
				if not self.vehicle.new_desired_lanes:
					break

		# Do a lane change
		if not self.lane_change_pause:
			if self.vehicle.new_desired_lanes:
				# for now just change, later implement checks so no crashes occur
				self.safeLaneChange(self.vehicle.new_desired_lanes.pop(0))


	def getNewLaneLeft(self, original_lane):
		if original_lane == self.RIGHT_LANE_STRING:
			return self.CENTER_LANE_STRING
		if original_lane == self.CENTER_LANE_STRING:
			return self.LEFT_LANE_STRING
		if original_lane == self.LEFT_LANE_STRING:
			return self.CENTER_LANE_STRING

	def getNewLaneRight(self, original_lane):
		if original_lane == self.RIGHT_LANE_STRING:
			return self.CENTER_LANE_STRING
		if original_lane == self.CENTER_LANE_STRING:
			return self.RIGHT_LANE_STRING
		if original_lane == self.LEFT_LANE_STRING:
			return self.CENTER_LANE_STRING



	def addLaneChange(self, lane):
		if self.vehicle.new_desired_lanes:
			if not self.vehicle.new_desired_lanes[-1] == lane:
				self.vehicle.new_desired_lanes.append(lane)
			else:
				return
		else:
			self.vehicle.new_desired_lanes.append(lane)


	def safeLaneChange(self, lane):
		if self.vehicle.desired_lane != lane:

			# this stuff could probably be checked in a neater way
			if self.vehicle.desired_lane == self.CENTER_LANE_STRING:
				if lane == self.LEFT_LANE_STRING:
					safe = self.checkLaneSafety(1, left_change = True)
				elif lane == self.RIGHT_LANE_STRING:
					safe = self.checkLaneSafety(1, right_change = True)

			elif self.vehicle.desired_lane == self.LEFT_LANE_STRING:
				if lane == self.CENTER_LANE_STRING:
					safe = self.checkLaneSafety(1, right_change = True)
				elif lane == self.RIGHT_LANE_STRING:
					safe = self.checkLaneSafety(2, right_change = True)

			elif self.vehicle.desired_lane == self.RIGHT_LANE_STRING:
				if lane == self.CENTER_LANE_STRING:
					safe = self.checkLaneSafety(1, left_change = True)
				elif lane == self.LEFT_LANE_STRING:
					safe = self.checkLaneSafety(2, left_change = True)

			if safe:
				if self.vehicle.bwd_pair_partner and not self.vehicle.overtaking_vehicle_flag:
					self.vehicle.messenger.sendLaneChange(self.vehicle.bwd_pair_partner, lane)

				self.vehicle.desired_lane = lane
				self.vehicle.lanekeeping_ready.clear()
				self.lane_change_time = time.time()
				self.lane_change_pause = True

			if not safe:
				# put lane back in queue, in first place
				#print "Not currently safe to switch lanes"
				self.vehicle.new_desired_lanes.insert(0,lane)

	def checkLaneSafety(self, number_lane_changes, left_change = False, right_change = False):
		if left_change:
			side = 'l'
			rear_side = 'rl'
			front_side = 'fl'
			print_str = "left"
		if right_change:
			side = 'r'
			rear_side = 'rr'
			front_side = 'fr'
			print_str = "right"

		
		# checking side
		if self.vehicle.perception_grid[side]:
			for other_id in self.vehicle.perception_grid[side]:
				# check if vehicle is in first side lane, or second side lane and doing 2 lane changes
				if abs(self.vehicle.perceived_objects[other_id][1])<5 or (5<abs(self.vehicle.perceived_objects[other_id][1])<10 and number_lane_changes == 2):
					# check if rel x-coord is less than 5 m
					if abs(self.vehicle.perceived_objects[other_id][0])<5:
						#print "For vehicle", self.vehicle.id,":"+"vehicle to the "+print_str+" side too close to switch lane"
					 	return False	


		# checking rear side
		if rear_side in self.vehicle.closest_objects:
			other_id = self.vehicle.closest_objects[rear_side]
			# check if vehicle is in first side lane, or second side lane and doing 2 lane changes
			if abs(self.vehicle.perceived_objects[other_id][1])<5 or (5<abs(self.vehicle.perceived_objects[other_id][1])<10 and number_lane_changes == 2):
				# check if vehicle is less than 8 m behind
				if abs(self.vehicle.perceived_objects[other_id][0]) < 8:
					found_reading = False
					for reading in self.vehicle.sensor_readings:
						if reading[0] == other_id:
							found_reading = True
							if self.calcVelocitySensorReadings(reading) > self.vehicle.desired_velocity:
								#vehicle less than 8 meters behind you in lane you're switching to, moving too fast
								print "For vehicle", self.vehicle.id,":"+"vehicle less than 8 meters in rear "+print_str+" moving too fast to switch lane"
								return False
							break
					if not found_reading:
						print "For vehicle", self.vehicle.id,":"+"vehicle less than 8 meters in rear "+print_str+" moving at unknown velocity, can not switch lane"
						return False


		# checking front side
		if front_side in self.vehicle.closest_objects:
			other_id = self.vehicle.closest_objects[front_side]
			# check if vehicle is in first side lane, or second side lane and doing 2 lane changes
			if abs(self.vehicle.perceived_objects[other_id][1])<5 or (5<abs(self.vehicle.perceived_objects[other_id][1])<10 and number_lane_changes == 2):
				# check if vehicle is less than 7 m ahead
				if abs(self.vehicle.perceived_objects[other_id][0]) < 7:
					found_reading = False
					for reading in self.vehicle.sensor_readings:
						if reading[0] == other_id:
							found_reading = True
							if self.calcVelocitySensorReadings(reading) < self.vehicle.desired_velocity:
								#vehicle less than 7 meters ahead of you in lane you're switching to, moving too slowly
								print "For vehicle", self.vehicle.id,":"+"vehicle in the front "+print_str+" too close and moving too slowly to switch lane"
								return False
							break
					if not found_reading:
						print "For vehicle", self.vehicle.id,":"+"vehicle in the front "+print_str+" too close and moving at unknown velocity, cannot switch lane"
						return False

		return True

	''' End of lane change functions '''


	''' Overtaking functions '''

	def overtakeCheck(self):
		#if not self.vehicle.fwd_pair_partner: # Uncomment if only platoon leader should be able to overtake
		if 'fm' in self.vehicle.closest_objects:
			if not self.vehicle.fwd_pair_partner == self.vehicle.closest_objects['fm'] and not self.vehicle.new_fwd_pair_partner == self.vehicle.closest_objects['fm']:
				#print self.vehicle.perceived_objects[self.vehicle.closest_objects['fm']][0]
				if self.vehicle.perceived_objects[self.vehicle.closest_objects['fm']][0]<self.overtake_distance:

					other_velocity = None
					for reading in self.vehicle.sensor_readings:
						if reading[0] == self.vehicle.closest_objects['fm']:
							other_velocity = 1.4*self.calcVelocitySensorReadings(reading)
							break			

					if other_velocity != None:
						if other_velocity < self.vehicle.desired_velocity*1.2:
							if 1.4*self.calcVelocitySensorReadings(reading)>self.vehicle.desired_velocity:
								self.setVelocity(1.4*self.calcVelocitySensorReadings(reading))
							self.vehicle.overtaking_vehicle_flag = True
							self.vehicle.overtaking_original_lanes.append(self.vehicle.desired_lane)
							self.vehicle.overtaken_vehicles.append(self.vehicle.closest_objects['fm'])
							self.vehicle.overtaking_original_velocity = self.vehicle.desired_velocity
							print self.vehicle.id,"trying to overtake", self.vehicle.closest_objects['fm']
							self.overtakeVehicle()
						else:
							return
					else:
						print "Couldnt determine velocity of vehicle in front, overtaking aborted"
						return



	def overtakeVehicle(self):
		original_lane = self.vehicle.overtaking_original_lanes[-1]
		overtaken_vehicle = self.vehicle.overtaken_vehicles[-1]

		if 'fm' in self.vehicle.closest_objects:
			if not self.vehicle.closest_objects['fm'] in self.vehicle.overtaken_vehicles:
				self.overtakeCheck()

		if overtaken_vehicle in self.vehicle.perception_grid['rl'] or overtaken_vehicle in self.vehicle.perception_grid['rr'] or overtaken_vehicle in self.vehicle.perception_grid['rm'] or not overtaken_vehicle in self.vehicle.perceived_objects:
			# overtaking finished
			#print self.vehicle.id, "has finsihed overtaking", overtaken_vehicle
			self.addLaneChange(original_lane)
			self.vehicle.overtaken_vehicles.pop()
			self.vehicle.overtaking_original_lanes.pop()
			self.setVelocity(self.vehicle.overtaking_original_velocity)
			if len(self.vehicle.overtaken_vehicles) == 0:
				self.vehicle.overtaking_vehicle_flag = False
			return
			
		if overtaken_vehicle in self.vehicle.perception_grid['fm']:
			new_lane = self.getNewLaneLeft(original_lane)
			self.addLaneChange(new_lane)
			return

	''' End of overtaking fncs '''