import rrtlibrary
import math

initial_pose = [-1.,-0.125,0,0]
goal_position = [1.125, 0]
				
space_limits=[-2.,2.,-2.,2.]

# This is an example, when assuming that the obstacle is the box we trained to bee the water.
# So the qualisys gives me the pose in the center of the box, I still have to convert it to the pose in the bottom left corner of the box.

obstacles_list = []
obstacle_pose = [0.0,-0.0, 0.0] 
obstacle_dimensions = [0.4, 0.5, 0.0]

obstacle_pose_corrected = [0, 0, 0]
obstacle_pose_corrected[0] = obstacle_pose[0] + (-obstacle_dimensions[0]/2.0)*math.cos( math.radians(obstacle_pose[2]) ) - (-obstacle_dimensions[1]/2.0)*math.sin( math.radians(obstacle_pose[2]) )
obstacle_pose_corrected[1] = obstacle_pose[1] + (-obstacle_dimensions[0]/2.0)*math.sin( math.radians(obstacle_pose[2]) ) + (-obstacle_dimensions[1]/2.0)*math.cos( math.radians(obstacle_pose[2]) )
obstacle_pose_corrected[2] = obstacle_pose[2] 

# print "obstacle_pose = " + str(obstacle_pose)
# print "obstacle_pose_corrected = " + str(obstacle_pose_corrected)

obstacle_example = [obstacle_pose_corrected, obstacle_dimensions]

# obstacle_example = [obstacle_pose, obstacle_dimensions]
print "obstacle_example = " + str(obstacle_example)
obstacles_list.append(obstacle_example)
# obstacles_list.append(obstacle_example)

# House thingy
# environment_points = [[-1.0, -1.0], [-1.2, 0.2], [-0.5, 1.0], [-0.3, 0.8], [-0.1, 1.2], [0.8, 0.1], [1.0, -1.2]]

# top_limit = 1.2
# bottom_limit = -1.0
# environment_points = [[-1.0, bottom_limit], [-1.0, top_limit], [1.0, top_limit], [1.0, bottom_limit]]



environment_points = []

num_points = 40
radius = 1.5

for angle_it in range(num_points):

	angle = angle_it*(2*math.pi/num_points)

	x = radius*math.cos(angle)
	y = radius*0.25*math.sin(angle)

	environment_points.append( [x, y] )

# environment_points = [[-2.001, -.001], [-2.001, 0.501], [1.251, 0.501], [0.751, -2.001], [0.001, -2.001], [0.001, 0.001]]
# environment_points = [[-2000.001, -000.001], [-2000.001, 2000.001], [2000.001, 2000.001], [2000.001, -2000.001], [0000.001, -2000.001], [0000.001, 0000.001]]
environment_points_list = []
environment_points_list.append(environment_points)

print "len( environment_points ) = " + str(len( environment_points ))

# pass

# environment_points = [[-2.0, -.0], [-2., 2], [2., 2.0], [2., 0]]
# environment_points_list = []
# environment_points_list.append(environment_points)

# environment_points = [[.0, .0], [2., 0], [2., -2.], [0., -2.]]
# environment_points_list.append(environment_points)

# environment_points = [ [20, 20], [22, 20], [22, 22], [20, 22] ]

# environment_points = []

# rrtlibrary.write_obstacles_to_file(obstacles_list, "cppInputObstacles.txt")
# rrtlibrary.write_boost_obstacles_to_file(obstacles_list, "cppInputObstacles.txt")


rrtlibrary.write_boost_obstacles_and_environment_to_file(obstacles_list, environment_points_list, "cppInputObstacles.txt")



[tree_nodes, solution_nodes] = rrtlibrary.call_rrt(initial_pose, goal_position, space_limits,
								goal_tolerance=.02, euler_sim_time=.01, max_steering_angle=.35)

print "obstacles_list = " + str(obstacles_list)
rrtlibrary.draw_rrt(tree_nodes, solution_nodes, space_limits, obstacles_list, environment_points_list)