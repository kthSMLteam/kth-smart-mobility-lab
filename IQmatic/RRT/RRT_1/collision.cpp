/* 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 *     Copyright (C) <2015>  <Rui Oliveira> <rfoli@kth.se>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "collision.h"

model::polygon<model::d2::point_xy<double> > get_truck_poly(float *current_state, const ProblemDefinition *prob_def_ptr){
        
    model::polygon<model::d2::point_xy<double> > truck_poly;
    
    float temp_point_x;
    float temp_point_y;
    model::d2::point_xy<double> current_point;
    
    float truck_state[3];
    truck_state[0] = current_state[0] + prob_def_ptr->trailer_length*cos(current_state[2]);
    truck_state[1] = current_state[1] + prob_def_ptr->trailer_length*sin(current_state[2]);
    truck_state[2] = current_state[3];
    
    // Top left corner of the trailer
    temp_point_x = truck_state[0] + (-prob_def_ptr->truck_width/2.0)*cos(truck_state[2]) - prob_def_ptr->truck_length*sin(truck_state[2]);
    temp_point_y = truck_state[1] + (-prob_def_ptr->truck_width/2.0)*sin(truck_state[2]) + prob_def_ptr->truck_length*cos(truck_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(truck_poly, current_point);
    
    // Top right corner of the trailer
    temp_point_x = truck_state[0] + (prob_def_ptr->truck_width/2.0)*cos(truck_state[2]) - prob_def_ptr->truck_length*sin(truck_state[2]);
    temp_point_y = truck_state[1] + (prob_def_ptr->truck_width/2.0)*sin(truck_state[2]) + prob_def_ptr->truck_length*cos(truck_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(truck_poly, current_point);
    
    // Bottom right corner of the truck
    temp_point_x = truck_state[0] + (prob_def_ptr->truck_width/2.0)*cos(truck_state[2]);
    temp_point_y = current_state[1] + (prob_def_ptr->truck_width/2.0)*sin(truck_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(truck_poly, current_point);
    
    // Bottom left corner of the trailer
    temp_point_x = truck_state[0] + (-prob_def_ptr->truck_width/2.0)*cos(truck_state[2]);
    temp_point_y = truck_state[1] + (-prob_def_ptr->truck_width/2.0)*sin(truck_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(truck_poly, current_point);
        
    return truck_poly;
    
}

model::polygon<model::d2::point_xy<double> > get_trailer_poly(float *current_state, const ProblemDefinition *prob_def_ptr){
        
    model::polygon<model::d2::point_xy<double> > trailer_poly;
        
    float temp_point_x;
    float temp_point_y;
    model::d2::point_xy<double> current_point;
        
    // Top left corner of the trailer
    temp_point_x = current_state[0] + (-prob_def_ptr->trailer_width/2.0)*cos(current_state[2]) - prob_def_ptr->trailer_length*sin(current_state[2]);
    temp_point_y = current_state[1] + (-prob_def_ptr->trailer_width/2.0)*sin(current_state[2]) + prob_def_ptr->trailer_length*cos(current_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(trailer_poly, current_point);
    
    // Top right corner of the trailer
    temp_point_x = current_state[0] + (prob_def_ptr->trailer_width/2.0)*cos(current_state[2]) - prob_def_ptr->trailer_length*sin(current_state[2]);
    temp_point_y = current_state[1] + (prob_def_ptr->trailer_width/2.0)*sin(current_state[2]) + prob_def_ptr->trailer_length*cos(current_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(trailer_poly, current_point);
    
    // Bottom right corner of the trailer
    temp_point_x = current_state[0] + (prob_def_ptr->trailer_width/2.0)*cos(current_state[2]);
    temp_point_y = current_state[1] + (prob_def_ptr->trailer_width/2.0)*sin(current_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(trailer_poly, current_point);
    
    // Bottom left corner of the trailer
    temp_point_x = current_state[0] + (-prob_def_ptr->trailer_width/2.0)*cos(current_state[2]);
    temp_point_y = current_state[1] + (-prob_def_ptr->trailer_width/2.0)*sin(current_state[2]);

    assign_values(current_point, temp_point_x, temp_point_y);
    append(trailer_poly, current_point);
        
    return trailer_poly;
    
}


bool is_in_collision_truck_and_trailer(float *current_state, const ProblemDefinition *prob_def_ptr){
    
    model::polygon<model::d2::point_xy<double> > truck_poly;    
    truck_poly = get_truck_poly(current_state, prob_def_ptr);
        
    model::polygon<model::d2::point_xy<double> > trailer_poly;
    trailer_poly = get_trailer_poly(current_state, prob_def_ptr);
    
    // Iterate over all the obstacles
    for ( unsigned i = 0; i < prob_def_ptr->polygon_obstacles_list.size(); i++ ){
        
        // Check if the truck or trailer are intersecting an obstacle
        if ( boost::geometry::intersects(prob_def_ptr->polygon_obstacles_list.at(i), truck_poly) || 
             boost::geometry::intersects(prob_def_ptr->polygon_obstacles_list.at(i), trailer_poly)  ){
            
            return true;
            
        }
        
    }
    
    return false;
    
}

bool is_in_collision(float *current_state, const ProblemDefinition *prob_def_ptr){
    
    return is_in_collision_truck_and_trailer(current_state, prob_def_ptr);
    
}

bool is_inside_environment(float *current_state, const ProblemDefinition *prob_def_ptr){
        
    model::polygon<model::d2::point_xy<double> > truck_poly;    
    truck_poly = get_truck_poly(current_state, prob_def_ptr);
       
    // Check if truck is outside the environment.
    if ( !within(truck_poly, prob_def_ptr->polygon_environment ) ){
        
        return 0;
        
    }
    
    model::polygon<model::d2::point_xy<double> > trailer_poly;
    trailer_poly = get_trailer_poly(current_state, prob_def_ptr);
    
    // Check if trailer is outside the environment.
    if ( !within(trailer_poly, prob_def_ptr->polygon_environment ) ){
        
        return 0;
        
    }
    
    return 1;
        
}