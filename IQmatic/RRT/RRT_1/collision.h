/* 
 * 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 */

// The functions here defined, are used to check for collisions and feasibility
// of states

#ifndef COLLISION_H
#define	COLLISION_H

#include "ProblemDefinition.h"

#include <stdlib.h> 
#include <vector>
#include <math.h>
#include <iostream>

// Requires include of some of the Boost C++ Libraries
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/algorithms/within.hpp>
#include <boost/geometry/geometries/adapted/boost_tuple.hpp>

BOOST_GEOMETRY_REGISTER_BOOST_TUPLE_CS(cs::cartesian)

using namespace boost::geometry;

// Returns the polygon corresponding to the truck according to current_state
model::polygon<model::d2::point_xy<double> > get_truck_poly(float *current_state, const ProblemDefinition *prob_def_ptr);

// Returns the polygon corresponding to the trailer according to current_state
model::polygon<model::d2::point_xy<double> > get_trailer_poly(float *current_state, const ProblemDefinition *prob_def_ptr);

// Checks if the truck or the trailer are in collision with the obstacles.
bool is_in_collision_truck_and_trailer(float *current_state, const ProblemDefinition *prob_def_ptr);

// A wrapper to facilitate development changes in the collision checking algorithm.
bool is_in_collision(float *current_state, const ProblemDefinition *prob_def_ptr);

// Checks if the truck and the trailer are inside the allowed environment.
bool is_inside_environment(float *current_state, const ProblemDefinition *prob_def_ptr);

#endif	/* COLLISION_H */

