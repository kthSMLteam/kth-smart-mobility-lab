/* 
 * 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 */

#ifndef TREENODE_H
#define	TREENODE_H

// This struct defines the nodes of the search tree for the Truck and Trailer
// vehicle system.
struct TreeNode {
    
    // An array to store the state configuration
    float state_configuration[4];
    
    // The index of the parent node in the tree
    int parent_node_id;
    
} ;

#endif	/* TREENODE_H */

