function [  ] = show_square_obstacles(  )
%SHOW_SQUARE_OBSTACLES Summary of this function goes here
%   Detailed explanation goes here

    fid = fopen('square_obstacles.txt');

    tline = fgets(fid);
    while ischar(tline)
    %     disp(tline)
        output = regexp(tline, ' ', 'split');
        
%         plot([str2num(output{1}) str2num(output{3})], [str2num(output{2}) str2num(output{4})])
        x = str2num(output{1});
        y = str2num(output{2});
        w = str2num(output{3});
        h = str2num(output{4});
        X = [x; x; x+w; x+w];
        Y = [y; y+h; y+h; y];
        fill(X,Y,'r');
    %     break
        tline = fgets(fid);
    end


end

