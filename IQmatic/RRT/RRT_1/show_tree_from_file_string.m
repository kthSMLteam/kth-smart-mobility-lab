function [  ] = show_tree_from_file_string( fileNameString )
%SHOW_TREE_FROM_FILE_STRING Summary of this function goes here
%   Detailed explanation goes here


fid = fopen(fileNameString);

% close all;
% figure; hold on;
% % show_square_obstacles();

% return

tline = fgets(fid);
while ischar(tline)
%     disp(tline)
    output = regexp(tline, ' ', 'split');
    
    plot([str2num(output{1}) str2num(output{3})], [str2num(output{2}) str2num(output{4})])
    plot(str2num(output{1}), str2num(output{2}), 'rx') 
%     break
    tline = fgets(fid);
end

fclose(fid);

end

