/* 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 *     Copyright (C) <2015>  <Rui Oliveira> <rfoli@kth.se>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "systemSimulator.h"
#include "ProblemDefinition.h"

float get_random_float_between(float a, float b) {
    
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
    
}

void get_random_state(float *random_state, float *goal_state, const ProblemDefinition *prob_def_ptr){
    
    if ( get_random_float_between(0, 20) < 1.0 ){
        
        random_state[0] = goal_state[0];
        random_state[1] = goal_state[1];
        random_state[2] = goal_state[2];
        random_state[3] = goal_state[3];
        
    }else{
        
        random_state[0] = get_random_float_between(prob_def_ptr->environment_min_x, prob_def_ptr->environment_max_x);
        random_state[1] = get_random_float_between(prob_def_ptr->environment_min_y, prob_def_ptr->environment_max_y);
        random_state[2] = get_random_float_between(0, 2*M_PI);
        random_state[3] = random_state[2];

        double random_state_point[][2] = {{random_state[0], random_state[1]}};
        model::polygon<model::d2::point_xy<double> > poly_position;
        assign_points(poly_position, random_state_point);

    }
       
}


void system_equation(float *initial_state, float *final_state, float *inputs, float time_step, const ProblemDefinition *prob_def_ptr){

    float truck_speed = inputs[0];
    float trailer_speed = cos(initial_state[3] - initial_state[2])*truck_speed;
    float phi = inputs[1];
    
    float truck_length = prob_def_ptr->truck_length;
    float trailer_length = prob_def_ptr->trailer_length;

    // Kinematic equations of the truck and trailer system
    final_state[0] = initial_state[0] + cos(initial_state[2])*trailer_speed*time_step;
    final_state[1] = initial_state[1] + sin(initial_state[2])*trailer_speed*time_step;
    final_state[2] = initial_state[2] + (1/trailer_length)*sin(initial_state[3] - initial_state[2])*truck_speed*time_step;
    final_state[3] = initial_state[3] + tan(phi)*(truck_speed/truck_length)*time_step;
        
}

void euler_integration(float *initial_state, float *final_state, float *input, const ProblemDefinition *prob_def_ptr){
    
    float time_step = prob_def_ptr->euler_simulation_time/((float) prob_def_ptr->number_euler_steps);
    
    float intermediate_state[prob_def_ptr->state_dimension];
    
    for (int j = 0; j < prob_def_ptr->state_dimension; j++ ){
            
        intermediate_state[j] = initial_state[j];
            
    }
    
    // Simulate the system prob_def_ptr->number_euler_steps times
    for (int i = 0; i < prob_def_ptr->number_euler_steps; i++ ){
        
        system_equation(intermediate_state, final_state, input, time_step, prob_def_ptr);
        
        for (int j = 0; j < prob_def_ptr->state_dimension; j++ ){
            
            intermediate_state[j] = final_state[j];
            
        }
                
    }  
    
    // Correct the angles, so that they are in the range from 0 to 2*M_PI
    if (final_state[2] > 2*M_PI){
        
        final_state[2] = final_state[2] - 2*M_PI;
                
    }else if (final_state[2] < 0){
        
        final_state[2] = final_state[2] + 2*M_PI;
                
    }
    
    if (final_state[3] > 2*M_PI){
        
        final_state[3] = final_state[3] - 2*M_PI;
                
    }else if (final_state[3] < 0){
        
        final_state[3] = final_state[3] + 2*M_PI;
                
    }
    
}


bool find_best_input(float *initial_state, float *goal_state, float *inputs, const ProblemDefinition *prob_def_ptr){
        
    float steering_inputs[3];
    steering_inputs[0] = -prob_def_ptr->max_steering_angle; steering_inputs[1] = 0; steering_inputs[2] = prob_def_ptr->max_steering_angle;
    inputs[0] = 1;
    
    float final_state[prob_def_ptr->state_dimension];
    
    float min_distance = FLT_MAX;
    float best_input = 0;
    
    bool possible_movement = false;
    
    // Try the possible inputs, and find the one that results in a minimum
    // distance to goal_state
    for ( int i = 0; i < 3; i++ ){
        
        inputs[1] = steering_inputs[i];
        euler_integration(initial_state, final_state, inputs, prob_def_ptr);
        
        float distance;
                
        distance = get_distance(final_state, goal_state, prob_def_ptr);
                
        if (distance < min_distance && !is_in_collision(final_state, prob_def_ptr) ){
            
            min_distance = distance;
            best_input = steering_inputs[i];
            possible_movement = true;
            
        }
        
        
    }
        
    inputs[1] = best_input;    
    
    return possible_movement;
    
}