/* 
 * 
 *     This file is a part of RRT Truck Trailer Planner 
 * 
 */

// The functions here defined, are used to simulate the system at hand, and also
// to provide functionality to some of the steps in the search algorithm.

#ifndef SYSTEMSIMULATOR_H
#define	SYSTEMSIMULATOR_H

#include "distanceComputer.h"
#include "collision.h"
#include "ProblemDefinition.h"

#include <cmath>
#include <stdlib.h>

// Requires include of some of the Boost C++ Libraries
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/adapted/c_array.hpp>

using namespace boost::geometry;

// Returns a random number taken from an uniform distribution between a and b
float get_random_float_between(float a, float b);

// Gets a random state. With a certain probability this function will return
// goal_state as a random state.
void get_random_state(float *random_state, float *goal_state, const ProblemDefinition *prob_def_ptr);

// The discrete system equation (unreallistic), which determines how the truck 
// and trailer system evolves in a certain time interval.
void system_equation(float *initial_state, float *final_state, float *inputs, float time_step, const ProblemDefinition *prob_def_ptr);

// Simulates the system, by using the system_equation, using Euler Integration.
// The parameters of the Euler Integration are defined in prob_def_ptr
void euler_integration(float *initial_state, float *final_state, float *input, const ProblemDefinition *prob_def_ptr);

// Finds the best input, that is, the input from a set of possible inputs, which
// results in the nearest distance to goal_state
bool find_best_input(float *initial_state, float *goal_state, float *inputs, const ProblemDefinition *prob_def_ptr);

#endif	/* SYSTEMSIMULATOR_H */

