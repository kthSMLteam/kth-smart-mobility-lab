function [ ] = write_obstacle_to_file( qtm, qualisys_obstacle_id, fileId )
%WRITE_OBSTACLE_TO_FILE Summary of this function goes here
%   Detailed explanation goes here

    % Get info from qualisys_obstacle_id in qualisys

%     qualisys_x = 0.5;
%     qualisys_y = -0.5;
%     qualisys_theta = pi*(1/4);
    
    [qualisys_x,qualisys_y,qualisys_theta,~] = get_pose_qualisys_id(qtm,qualisys_obstacle_id);

    box_width = 0.6;
    box_height = 0.4;
    box_margin = 0.1;

    R = [cos(qualisys_theta) -sin(qualisys_theta);
        sin(qualisys_theta) cos(qualisys_theta)];
    t = [qualisys_x; qualisys_y];

    T = [R t;0 0 1];

    % Bottom left
%     box_x_1 = qualisys_x - cos(qualisys_theta)*box_margin;
%     box_y_1 = qualisys_y - sin(qualisys_theta)*box_margin;
    trans1 = T*[-box_margin; -box_margin; 1];

    % Bottom right
%     box_x_2 = qualisys_x + cos(qualisys_theta)*(box_width+box_margin);
%     box_y_2 = qualisys_y - sin(qualisys_theta)*box_margin;
    trans2 = T*[box_width + box_margin; -box_margin; 1];

    % Top right
%     box_x_3 = qualisys_x + cos(qualisys_theta)*(box_width+box_margin);
%     box_y_3 = qualisys_y + sin(qualisys_theta)*(box_height+box_margin);
    trans3 = T*[box_width + box_margin; box_height + box_margin; 1];

    % Top left
%     box_x_4 = qualisys_x - cos(qualisys_theta)*box_margin;
%     box_y_4 = qualisys_y + sin(qualisys_theta)*(box_height+box_margin);
    trans4 = T*[-box_margin; box_height + box_margin; 1];


    % X = [box_x_1; box_x_2; box_x_3; box_x_4];
    % Y = [box_y_1; box_y_2; box_y_3; box_y_4];
    X = [trans1(1); trans2(1); trans3(1); trans4(1)];
    Y = [trans1(2); trans2(2); trans3(2); trans4(2)];

    fill(X,Y,'r')

    while ( qualisys_theta > 2*pi )
        qualisys_theta = qualisys_theta - 2*pi;
    end
    while ( qualisys_theta < 0 )
        qualisys_theta = qualisys_theta + 2*pi;
    end

    if ( (qualisys_theta > pi/2) && (qualisys_theta < (3*pi)/2) )

        mb = [X(1) 1; X(2) 1]\[Y(1); Y(2)];
        str1 = [num2str(0), ' ',num2str(mb(1)), ' ',num2str(mb(2))];
        mb = [X(3) 1; X(4) 1]\[Y(3); Y(4)];
        str2 = [num2str(1), ' ',num2str(mb(1)), ' ',num2str(mb(2))];

    else

        mb = [X(1) 1; X(2) 1]\[Y(1); Y(2)];
        str1 = [num2str(1), ' ',num2str(mb(1)), ' ',num2str(mb(2))];
        mb = [X(3) 1; X(4) 1]\[Y(3); Y(4)];
        str2 = [num2str(0), ' ',num2str(mb(1)), ' ',num2str(mb(2))];

    end

    if ( qualisys_theta < pi )

        mb = [X(2) 1; X(3) 1]\[Y(2); Y(3)];
        str3 = [num2str(0), ' ',num2str(mb(1)), ' ',num2str(mb(2))];
        mb = [X(4) 1; X(1) 1]\[Y(4); Y(1)];
        str4 = [num2str(1), ' ',num2str(mb(1)), ' ',num2str(mb(2))];

    else

        mb = [X(2) 1; X(3) 1]\[Y(2); Y(3)];
        str3 = [num2str(1), ' ',num2str(mb(1)), ' ',num2str(mb(2))];
        mb = [X(4) 1; X(1) 1]\[Y(4); Y(1)];
        str4 = [num2str(0), ' ',num2str(mb(1)), ' ',num2str(mb(2))];

    end


    % return


    % fprintf(fileId, '%d %f  %f\n',higher, m, b);
    fprintf(fileId, '%s\n','Obstacle');
    fprintf(fileId, '%s\n',str1);
    fprintf(fileId, '%s\n',str2);
    fprintf(fileId, '%s\n',str3);
    fprintf(fileId, '%s\n',str4);


end

