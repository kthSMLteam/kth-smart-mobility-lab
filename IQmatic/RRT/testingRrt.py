import sys
import os
import rrtlibrary
import pygame
import time


def append_points(points_buffer, obstacle_points, limits_points, is_obstacle, is_limit):

	if points_buffer:

		if is_obstacle:

			# print "Appending obstacle: " + str(points_buffer)

			obstacle_points.append(points_buffer[:])

		if is_limit:

			# print "Appending limits: " + str(points_buffer)

			limits_points.append(points_buffer[:])

	del points_buffer[:]

def read_obstacles_file(filename):

	f = open(filename, 'r')

	is_limit = False
	is_obstacle = False

	points_buffer = []
	limits = []
	obstacles = []

	for line in f:

		if 'Limits' in line:
			
			append_points(points_buffer, obstacles, limits, is_obstacle, is_limit)

			is_limit = True
			is_obstacle = False

		else:

			if 'Obstacle' in line:

				# print "points_buffer = " + str(points_buffer)
				append_points(points_buffer, obstacles, limits, is_obstacle, is_limit)
				# print "points_buffer = " + str(points_buffer)
				is_limit = False
				is_obstacle = True

			else:

				line = line.replace("\n","")
				parsed_words = line.split(" ")
				# points_buffer.append 
				# print "parsed_words = " + str(parsed_words)
				parsed_points = [ float(parsed_words[0]), float(parsed_words[1]) ]
				# print "parsed_points = " + str(parsed_points)

				points_buffer.append(parsed_points)

	append_points(points_buffer, obstacles, limits, is_obstacle, is_limit)

	# print "obstacle_points = " + str(obstacles)
	# print "limits_points = " + str(limits)

	# obstacles = []
	# limits = []

	return [obstacles, limits]

def read_tree_nodes_file(filename):	

	f = open(filename, 'r')

	tree_nodes = []

	for line in f:

		line = line.replace("\n","")
		parsed_words = line.split(" ")
		# points_buffer.append 
		# print "parsed_words = " + str(parsed_words)
		parsed_points = [ float(parsed_words[0]), float(parsed_words[1]), int(parsed_words[2]) ]
		# print "parsed_points = " + str(parsed_points)

		tree_nodes.append(parsed_points)

	return tree_nodes

def read_tree_solution_file(filename):	

	f = open(filename, 'r')

	tree_solution = []

	for line in f:

		line = line.replace("\n","")
		parsed_words = line.split(" ")
		# points_buffer.append 
		# print "parsed_words = " + str(parsed_words)
		parsed_index = int(parsed_words[0])
		# print "parsed_points = " + str(parsed_points)

		tree_solution.append(parsed_index)

	return tree_solution


# args_string =  "-0.187301574707 -0.482580841064 0.169974958283 0.169974958283 1.24440170323 0.228335891821 -1.0 1.0 -1.4 0.6 0.1 0.1 0.349065850399"
goal_x = 1.25
goal_y = 0.23
goal_tolerance = 0.1
goal_x = 0.7
# goal_y = 0.7
# args_string =  "-0.187301574707 -0.482580841064 0.169974958283 0.169974958283 " + str(goal_x) + " " + str(goal_y) + " -1.0 1.0 -1.4 0.6 " + str(goal_tolerance) + " 0.1 0.349065850399"
args_string =  "-0.187301574707 -0.582580841064 0.169974958283 0.169974958283 " + str(goal_x) + " " + str(goal_y) + " -1.0 1.0 -1.4 0.6 " + str(goal_tolerance) + " 0.1 0.349065850399"

if sys.platform == 'linux2':
	executable_name = "./RRT/rrt"
else:
	executable_name = "C:/Users/rui/Documents/Bitbucket/IQmatic/RRT/RRT_1/rrt_1.exe"

print "Running now"

print "args_string = " + str(args_string)

stuff_out = os.system(executable_name + ' ' + args_string)

print "Runned now"

[obstacles_list, environment_points_list] = read_obstacles_file("cppInputObstacles.txt")
tree_nodes = read_tree_nodes_file("cppOutputTreeNodes.txt")
solution_nodes = read_tree_solution_file("cppOutputTreeSolution.txt")

# print tree_nodes

# solution_nodes = []
space_limits = [-2, 2, -2, 2]

canvas_width = 800
canvas_height = 800

# rrtlibrary.draw_rrt(tree_nodes, tree_solution, space_limits, obstacles, environment_points_list = [])

resized_tree_nodes = []

rescale_x = canvas_width/( space_limits[1] - space_limits[0] )
rescale_y = canvas_height/( space_limits[3] - space_limits[2] )

for tree_node in tree_nodes:

	x_pixel = rrtlibrary.convert_x_to_pixel(tree_node[0], space_limits, rescale_x)
	y_pixel = rrtlibrary.convert_y_to_pixel(tree_node[1], space_limits, rescale_y)

	resized_tree_nodes.append( [ x_pixel, y_pixel, tree_node[2] ] )

	# resized_tree_nodes.append( [ (tree_node[0]-space_limits[0])*rescale_x, (tree_node[1]-space_limits[2])*rescale_y, tree_node[2] ] )


world_surface = pygame.Surface((canvas_width, canvas_height), 0, 32)

resized_obstacles_list = []
obstacles_points_list = []

for obstacle in obstacles_list:

	obstacle_points = []
	obstacle_pose = obstacle[0]
	obstacle_dimensions = obstacle[1]

	for obstacle_point in obstacle:

		x_pixel = rrtlibrary.convert_x_to_pixel(obstacle_point[0], space_limits, rescale_x)
		y_pixel = rrtlibrary.convert_y_to_pixel(obstacle_point[1], space_limits, rescale_y)
	
		obstacle_points.append( ( x_pixel , y_pixel ) )

	obstacles_points_list.append(obstacle_points)




fill_color = (0, 0, 0)
world_surface.fill(fill_color)

tree_color = (255, 255, 255)
branch_color = (255, 0, 0)
line_closed = False
line_thickness = int(4.0)

if environment_points_list:

	for environment_points in environment_points_list:

		print "THIS!"

		environment_points_tuple_list = []

		for point in environment_points:

			x_pixel = rrtlibrary.convert_x_to_pixel(point[0], space_limits, rescale_x)
			y_pixel = rrtlibrary.convert_y_to_pixel(point[1], space_limits, rescale_y)
			

			environment_points_tuple_list.append( ( x_pixel, y_pixel ) )

		print "environment_points_tuple_list = " + str(environment_points_tuple_list)

		pygame.draw.polygon(world_surface, (0,100,0), environment_points_tuple_list) 


for obstacle_points in obstacles_points_list:

	print "Drawing polygons"
	print obstacle_points
	pygame.draw.polygon(world_surface, (45,0,45), obstacle_points) 
	# resized_obstacles_list.append( [ (tree_node[0]-space_limits[0])*rescale_x, (tree_node[1]-space_limits[2])*rescale_y, tree_node[2] ] )



for current_tree_node_id in range(1, len(tree_nodes) ):

	current_tree_node = resized_tree_nodes[current_tree_node_id]
	current_parent_tree_node = resized_tree_nodes[ current_tree_node[2] ]

	line_tuple_list = []
	line_tuple_list.append( ( int(current_tree_node[0]),int(current_tree_node[1]) ) )
	line_tuple_list.append( ( int(current_parent_tree_node[0]),int(current_parent_tree_node[1]) ) )

	pygame.draw.lines(world_surface, tree_color, line_closed, tuple( line_tuple_list ), line_thickness)

for solution_nodes_it in range(1, len(solution_nodes) ):

	solution_node_id = solution_nodes[solution_nodes_it]
	solution_node_parent_id = solution_nodes[solution_nodes_it-1]

	current_tree_node = resized_tree_nodes[solution_node_id]
	current_parent_tree_node = resized_tree_nodes[ solution_node_parent_id ]

	line_tuple_list = []
	line_tuple_list.append( ( int(current_tree_node[0]),int(current_tree_node[1]) ) )
	line_tuple_list.append( ( int(current_parent_tree_node[0]),int(current_parent_tree_node[1]) ) )

	pygame.draw.lines(world_surface, branch_color, line_closed, tuple( line_tuple_list ), line_thickness)


goal_pixel_x = int( rrtlibrary.convert_x_to_pixel(goal_x, space_limits, rescale_x) )
goal_pixel_y = int( rrtlibrary.convert_x_to_pixel(-goal_y, space_limits, rescale_y) )
pygame.draw.circle(world_surface, (255, 0, 0), (goal_pixel_x, goal_pixel_y), 5)

size = canvas_width, canvas_height

screen = pygame.display.set_mode(size)

# screen.fill(BLACK)

screen.blit(world_surface, (0,0) )





pygame.display.flip()

print "DOOOOOOOOOOOOOOOOOOOOOONNNNNNNNNNNNNNNNNNNNNNNNNNNNEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"

time.sleep(1.0)
