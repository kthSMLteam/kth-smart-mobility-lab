import socket
import time
import json
from threading import Thread

class platoonCommand(Thread):

	"""This reads json from the specified port number and gives commands to the platooning vehicles"""

	def __init__(self, portNo, bodies_array, numberPlatVehicles):
		"""TODO: fix json reader  """
		Thread.__init__(self)

		# TODO fix socket accept

		# Initializes the port no, jsonDecoder, bodies_array 
		self.portNo = portNo
		self.decoder = json.JSONDecoder()
		self.encoder = json.JSONEncoder()
		self.bodies_array = bodies_array
		self.numberPlatVehicles = numberPlatVehicles
		self.daemon = True

	def run(self):
		
		while 1:
			self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

			self.s.bind(("localhost", self.portNo))
			self.s.listen(1);
			(self.conn, self.addr) = self.s.accept()
			print "connected to", self.addr
			while 1:
				a = self.conn.recv(4096)

				if a == "":
					break

				jsonDict = self.decoder.decode(a)

				target = jsonDict["target"]
				commandID = jsonDict["commandID"]
				params = jsonDict["params"]
				if params == [""]:
					params = []

				if commandID == "echo-state":
					self.echoState(target, params)
					
				else:
					params = [int(textInt) for textInt in params]
					self.executeCommand(target, commandID, params)

			self.s.shutdown(socket.SHUT_RDWR)
			self.s.close()
			

	def executeCommand(self, target, commandID, params):

		if target < 0 and abs(target) <= self.numberPlatVehicles:
			if commandID in self.bodies_array[target].commandDict:
				print "executing command"
				self.bodies_array[target].commandDict[commandID](*params)

	def echoState(self, target, params):

		stateList = self.bodies_array[target].getState(params)
		jsonDict = {"comID": "state"}
		fields = []
		values = []
		for t in stateList:
			fields.append(str(t[0]))
			values.append(str(t[1]))
			jsonDict["fields"] = fields
			jsonDict["values"] = values

		jsonString = self.encoder.encode(jsonDict)
		self.conn.send(jsonString + "\r\n")


