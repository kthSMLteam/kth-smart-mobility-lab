
import socket, sys, time, math, random, threading, datetime

import numpy


class VehicleControlModule:
	"This class is the Platooning Vehicle class used by the Platooning Manager Module"
	def __init__(self, vehicle, bodies_array, vehicle_id):

		self.bodies_array = bodies_array
		self.vehicle_id = vehicle_id


		self.vehicle = vehicle
		self.current_body_readings = []
		self.desired_velocity = 0.
		self.state = []

		# Variables to store the lane trajectories
		self.current_lane = self.vehicle.desired_lane

		self.traj = self.vehicle.getLaneTrajectory(self.vehicle.desired_lane)
		traj_x = self.traj[0]
		traj_y = self.traj[1]
		traj_theta = self.traj[2]
		self.np_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		self.right_lane_traj = self.vehicle.getLaneTrajectory("right_lane")
		traj_x = self.right_lane_traj[0]
		traj_y = self.right_lane_traj[1]
		traj_theta = self.right_lane_traj[2]
		self.np_right_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		self.center_lane_traj = self.vehicle.getLaneTrajectory("center_lane")
		traj_x = self.center_lane_traj[0]
		traj_y = self.center_lane_traj[1]
		traj_theta = self.center_lane_traj[2]
		self.np_center_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		self.left_lane_traj = self.vehicle.getLaneTrajectory("left_lane")
		traj_x = self.left_lane_traj[0]
		traj_y = self.left_lane_traj[1]
		traj_theta = self.left_lane_traj[2]
		self.np_left_lane_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		# Definition of string constants
		self.RIGHT_LANE_STRING = "right_lane"
		self.CENTER_LANE_STRING = "center_lane"
		self.LEFT_LANE_STRING = "left_lane"

		self.current_trajectory_id = 0

		# Variables to store the low level commands that will be
		# read by the SmartVehicle module and sent to the Simulator
		self.current_velocity_command = 0.
		self.current_steering_command = 0.

		self.perception_module = None
		self.perceived_objects = None

		self.ACCC = False
		self.CC = True
		self.lateral_following = False

		# variables for the accc to work
		self.ACCC_target_id = None
		self.ACCC_desired_distance = None
		self.ACCC_distance = None

		# variables for lateral_following
		self.lateral_following_displacement = 0

		self.debug_lane_change = False

		# The last time the ACCC controller was executed
		# Useful for computations of the Integral and Derivative part of the PID controller
		self.last_ACCC_time = []
		# The integrated error of the ACCC used for the Integral part of the PID controller
		self.ACCC_I_e = []
		# The last error of the ACCC, used for the Derivative part of the PID controller
		self.last_ACCC_e = []
	
		# ACCC PID gain
		self.ACCC_k_p = 5.
		self.ACCC_k_i = 1.
		self.ACCC_k_d = 0.


		# The last time the CC controller was executed
		# Useful for computations of the Integral and Derivative part of the PID controller
		self.last_CC_time = []
		# The integrated error of the CC used for the Integral part of the PID controller
		self.CC_I_e = []
		# The last error of the CC, used for the Derivative part of the PID controller
		self.last_CC_e = []
	
		# CC PID gain
		self.CC_k_p = 500. # 250
		self.CC_k_i = 50. # 50
		self.CC_k_d = 150.
		self.reset_CC()

		# The last time the lateral following controller was executed
		# Useful for computations of the Integral and Derivative part of the PID controller
		self.last_latfoll_time = []
		# The integrated error of the latfoll used for the Integral part of the PID controller
		self.latfoll_I_e = []
		# The last error of the latfoll, used for the Derivative part of the PID controller
		self.last_latfoll_e = []
	
		# latfoll PID gain
		self.latfoll_k_p = 0.04
		self.latfoll_k_i = 0.02
		self.latfoll_k_d = 0.05

		#Tips
		# self.state = [real_world_meters, radians]
		# print "self.state = " + str(self.state)
		# closeby_body_readings = [real_world_meters, radians]
		# print "closeby_body_readings = " + str(closeby_body_readings)
		# self.traj[0] in real_world_meters
		# self.traj[2] in radians
		# print "self.traj[2] = " + str(self.traj[2])

		print "VehicleControlModule started"

	def step(self):

		# This is the step function that is running at each cycle of the thread
		# in SmartVehicle

		if self.vehicle_id in self.bodies_array:

			self.state = [self.bodies_array[self.vehicle_id].x, self.bodies_array[self.vehicle_id].y, math.radians(self.bodies_array[self.vehicle_id].yaw) ]


		# You should now be able to use self.getPerceivedObject(id) to fetch relative coordinates of the other vehicles

		if self.ACCC:
			self.updateACCCTarget()
			# print self.ACCC_distance

		if self.lateral_following:
			self.update_lateral_following_target()

		self.update_lane()


		# self.smooth_lane_change(self.left_lane_traj)
		if self.vehicle_id in self.bodies_array:

			[speed, steering] = self.compute_low_level_inputs()



			self.vehicle.commands['throttle'] = speed
			self.vehicle.commands['steering'] = steering

		




	def updateACCCTarget(self):
		target_coords = self.vehicle.getPerceivedObject(self.ACCC_target_id)
		if target_coords:
			self.ACCC_distance = target_coords[0]
			# print "target_coords = " + str(target_coords)
			if abs(self.ACCC_desired_distance - self.ACCC_distance) < self.vehicle.ACCC_ready_margin:
				self.vehicle.ACCC_ready.set()
			else:
				self.vehicle.ACCC_ready.clear() 

	def update_lateral_following_target(self):
		target_coords = self.vehicle.getPerceivedObject(self.ACCC_target_id)
		if target_coords:
			self.lateral_following_displacement = target_coords[1]
		
	def update_lane(self):
		# Function that changes the current trajectory to be followed, if 
		# a new desired_lane is requested

		print_str = "A new lane was requested, changing now - "

		# if self.debug_lane_change:

		# 	print "self.np_traj.shape = " + str(self.np_traj.shape) + " id = " + str(self.vehicle_id) + " (" + str(self.vehicle.desired_lane) + "," + str(self.current_lane) + ")"

		if self.vehicle.desired_lane != self.current_lane:

			# print "self.np_traj.shape = " + str(self.np_traj.shape) + " id = " + str(self.vehicle_id) + " (" + str(self.vehicle.desired_lane) + "," + str(self.current_lane) + ")"
			# self.debug_lane_change = True

			if self.vehicle.desired_lane == self.CENTER_LANE_STRING:

				print_str += "Changing to center lane"
				# self.smooth_lane_change(self.center_lane_traj)
				# self.smooth_lane_change_numpy(self.center_lane_traj, self.np_center_lane_traj)
				self.dumb_lane_change_numpy(self.center_lane_traj, self.np_center_lane_traj)
				# self.traj = self.center_lane_traj
				self.current_lane = self.CENTER_LANE_STRING

			else:

				if self.vehicle.desired_lane == self.LEFT_LANE_STRING:

					print_str += "Changing to left lane"
					# self.smooth_lane_change(self.left_lane_traj)					
					# self.smooth_lane_change_numpy(self.left_lane_traj, self.np_left_lane_traj)					
					self.dumb_lane_change_numpy(self.left_lane_traj, self.np_left_lane_traj)
					# self.traj = self.left_lane_traj
					self.current_lane = self.LEFT_LANE_STRING

				else:

					if self.vehicle.desired_lane == self.RIGHT_LANE_STRING:

						print_str += "Changing to right lane"
						# self.smooth_lane_change(self.right_lane_traj)
						# self.smooth_lane_change_numpy(self.right_lane_traj, self.np_right_lane_traj)
						self.dumb_lane_change_numpy(self.right_lane_traj, self.np_right_lane_traj)
						# self.traj = self.right_lane_traj
						self.current_lane = self.RIGHT_LANE_STRING

					else:

						print_str += "ERROR: Changing to invalid lane"

						# print "self.vehicle.desired_lane = " + str(self.vehicle.desired_lane)
						# print "self.vehicle.current_lane = " + str(self.vehicle.current_lane)

			print "print_str = " + str(print_str) 

	def get_lane_merge(self, traj_1, traj_2, merging_indexes_1, merging_indexes_2):
		print "get_lane_merge = "
		if len(merging_indexes_1) >= len(merging_indexes_2):

			distances_1 = []
			current_distance_traveled = 0

			for merging_id in range( len( merging_indexes_1 ) - 1 ):

				id_current = merging_indexes_1[merging_id]
				id_next = merging_indexes_1[merging_id+1]
				
				current_distance_traveled += math.hypot( traj_1[0][id_next] -traj_1[0][id_current] , traj_1[1][id_next] -traj_1[1][id_current] )
				distances_1.append(current_distance_traveled)

			distances_2 = []
			current_distance_traveled = 0

			for merging_id in range( len( merging_indexes_2 ) - 1 ):

				id_current = merging_indexes_2[merging_id]
				id_next = merging_indexes_2[merging_id+1]
				
				current_distance_traveled += math.hypot( traj_1[0][id_next] -traj_1[0][id_current] , traj_1[1][id_next] -traj_1[1][id_current] )
				distances_2.append(current_distance_traveled)

			# print "distances_1 = " + str(distances_1)
			# print "distances_2 = " + str(distances_2)



		else:

			return self.get_lane_merge(traj_2, traj_1, merging_indexes_2, merging_indexes_1)

	
	def smooth_lane_change(self, desired_traj):

		print "SMOOOOOOTHH smooth_lane_change"

		current_idx = 0 # Not being used
		number_points_ahead = 0 # Not being used
		[best_distance, best_idx] = self.find_closest_trajectory_point(self.state, current_idx, number_points_ahead, self.traj)

		current_traj_start_idx = best_idx

		merging_indexes = 150

		current_traj_end_idx = current_traj_start_idx + merging_indexes
		current_traj_end_idx = current_traj_end_idx%len(self.traj[0])

		[best_distance, best_idx] = self.find_closest_trajectory_point(self.state, current_idx, number_points_ahead, desired_traj)

		current_lane_merge_range = []
		# print "---"
		if current_traj_start_idx < current_traj_end_idx:
			current_lane_merge_range = xrange(current_traj_start_idx, current_traj_end_idx)
		else:
			current_lane_merge_range.extend(xrange(current_traj_start_idx, len(self.traj[0])))
			current_lane_merge_range.extend(xrange(current_traj_end_idx))
			# print "current_lane_merge_range = " + str(current_lane_merge_range)
		
		# print "current_traj_start_idx = " + str(current_traj_start_idx)
		# print "current_traj_end_idx = " + str(current_traj_end_idx)
		# print "len(current_lane_merge_range) = " + str(len(current_lane_merge_range))

		desired_traj_start_idx = best_idx

		current_traj_end_state = [ self.traj[0][current_traj_end_idx], self.traj[1][current_traj_end_idx], self.traj[2][current_traj_end_idx] ]
		[best_distance, best_idx] = self.find_closest_trajectory_point(current_traj_end_state, current_idx, number_points_ahead, desired_traj)

		# current_lane_merge_range = xrange(current_traj_start_idx, current_traj_end_idx)
		# desired_traj_end_idx = best_idx	
		
		desired_traj_end_idx = desired_traj_start_idx + len(current_lane_merge_range)
		# print "desired_traj_end_idx = " + str(desired_traj_end_idx)
		desired_traj_end_idx = desired_traj_end_idx%len(desired_traj[0])
		# print "desired_traj_end_idx = " + str(desired_traj_end_idx)
		
		# print "current_traj_start_idx = " + str(current_traj_start_idx)
		# print "current_traj_end_idx = " + str(current_traj_end_idx)
		# print "desired_traj_start_idx = " + str(desired_traj_start_idx)
		# print "desired_traj_end_idx = " + str(desired_traj_end_idx)

		desired_lane_merge_range = []

		if desired_traj_start_idx < desired_traj_end_idx:
			desired_lane_merge_range = xrange(desired_traj_start_idx, desired_traj_end_idx)
		else:
			desired_lane_merge_range.extend(xrange(desired_traj_start_idx, len(desired_traj[0])))
			desired_lane_merge_range.extend(xrange(desired_traj_end_idx))
			# print "desired_lane_merge_range = " + str(desired_lane_merge_range)

		# SANITY CHECK
		if len( current_lane_merge_range ) != len( desired_lane_merge_range ):

			print "ERROR IN VEHICLE CONTROL MODULE, WRONG MERGE LANE SIZES"


		merging_traj = [[],[],[]]

		for i in range( len( current_lane_merge_range ) ):

			current_ratio = float( i )/float( len(current_lane_merge_range) )
			current_x = (1-current_ratio)*self.traj[0][ current_lane_merge_range[i] ] + current_ratio*desired_traj[0][ desired_lane_merge_range[i] ]
			current_y = (1-current_ratio)*self.traj[1][ current_lane_merge_range[i] ] + current_ratio*desired_traj[1][ desired_lane_merge_range[i] ]
			current_theta = (1-current_ratio)*self.traj[2][ current_lane_merge_range[i] ] + current_ratio*desired_traj[2][ desired_lane_merge_range[i] ]

			merging_traj[0].append(current_x)
			merging_traj[1].append(current_y)
			merging_traj[2].append(current_theta)

		# print "merging_traj[0] = " + str(merging_traj[0])

		remaining_traj = [[],[],[]]

		remaining_traj_indexes = []
		if desired_traj_start_idx < desired_traj_end_idx:
			remaining_traj_indexes.extend( xrange(desired_traj_end_idx, len(desired_traj[0]) ) )
			remaining_traj_indexes.extend( xrange( desired_traj_start_idx ) )
			# desired_lane_merge_range = xrange(desired_traj_start_idx, desired_traj_end_idx)
		else:
			remaining_traj_indexes = xrange(desired_traj_end_idx, desired_traj_start_idx)

		#print "---"
		#print "len(desired_traj[0]) = " + str(len(desired_traj[0]))
		#print "len(remaining_traj_indexes) = " + str(len(remaining_traj_indexes))

		for idx in remaining_traj_indexes:

			merging_traj[0].append( desired_traj[0][idx] )
			merging_traj[1].append( desired_traj[1][idx] )
			merging_traj[2].append( desired_traj[2][idx] )

		#print "len(merging_traj[0])  = " + str(len(merging_traj[0]) )


		#print "CHANGED TRAJECTORY"
		self.traj = merging_traj

		traj_x = merging_traj[0]
		traj_y = merging_traj[1]
		traj_theta = merging_traj[2]
		self.np_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		self.vehicle.supervisory_module.waitForEvent(self.vehicle.lanekeeping_ready, self.fixTrajAfterLaneChange, self.vehicle.fix_lane_change_abort)

		# new_traj = []

		# len(desired_lane_merge_range)

		# self.get_lane_merge( self.traj, desired_traj, current_lane_merge_range, desired_lane_merge_range)
		# self.get_simple_lane_merge( self.traj, desired_traj, current_lane_merge_range, desired_lane_merge_range)


		# print "len(current_lane_merge_range) = " + str(len(current_lane_merge_range))
		# print "len(desired_lane_merge_range) = " + str(len(desired_lane_merge_range))



		# print "desired_traj_closest_idx = " + str(desired_traj_closest_idx)
		# print "self.traj is desired_traj = " + str(self.traj is desired_traj)

		# print "len(desired_traj[0]) = " + str(len(desired_traj[0]))
		# print "len(self.traj[0]) = " + str(len(self.traj[0]))

		pass

	def smooth_lane_change_numpy(self, desired_traj, np_desired_traj):

		print "SMOOOOOOTHH NUMPY smooth_lane_change, id = " + str(self.vehicle_id)

		current_idx = 0 # Not being used
		number_points_ahead = 0 # Not being used
		# best_idx = self.find_closest_trajectory_point_numpy(self.state, current_idx, number_points_ahead, self.np_traj)
		best_idx = self.find_closest_trajectory_point_numpy(self.state, self.np_traj)

		current_traj_start_idx = best_idx

		merging_indexes = 150

		current_traj_end_idx = current_traj_start_idx + merging_indexes
		current_traj_end_idx = current_traj_end_idx%len(self.traj[0])

		# [best_distance, best_idx] = self.find_closest_trajectory_point(self.state, current_idx, number_points_ahead, desired_traj)
		# best_idx = self.find_closest_trajectory_point_numpy(self.state, current_idx, number_points_ahead, np_desired_traj)
		best_idx = self.find_closest_trajectory_point_numpy(self.state, np_desired_traj)

		current_lane_merge_range = []
		# print "---"
		if current_traj_start_idx < current_traj_end_idx:
			current_lane_merge_range = xrange(current_traj_start_idx, current_traj_end_idx)
		else:
			current_lane_merge_range.extend(xrange(current_traj_start_idx, len(self.traj[0])))
			current_lane_merge_range.extend(xrange(current_traj_end_idx))
			# print "current_lane_merge_range = " + str(current_lane_merge_range)
		
		# print "current_traj_start_idx = " + str(current_traj_start_idx)
		# print "current_traj_end_idx = " + str(current_traj_end_idx)
		# print "len(current_lane_merge_range) = " + str(len(current_lane_merge_range))

		desired_traj_start_idx = best_idx

		current_traj_end_state = [ self.traj[0][current_traj_end_idx], self.traj[1][current_traj_end_idx], self.traj[2][current_traj_end_idx] ]
		# [best_distance, best_idx] = self.find_closest_trajectory_point(current_traj_end_state, current_idx, number_points_ahead, desired_traj)
		# best_idx = self.find_closest_trajectory_point_numpy(current_traj_end_state, current_idx, number_points_ahead, np_desired_traj)
		best_idx = self.find_closest_trajectory_point_numpy(self.state, np_desired_traj)

		# current_lane_merge_range = xrange(current_traj_start_idx, current_traj_end_idx)
		# desired_traj_end_idx = best_idx	
		
		desired_traj_end_idx = desired_traj_start_idx + len(current_lane_merge_range)
		# print "desired_traj_end_idx = " + str(desired_traj_end_idx)
		desired_traj_end_idx = desired_traj_end_idx%len(desired_traj[0])
		# print "desired_traj_end_idx = " + str(desired_traj_end_idx)
		
		# print "current_traj_start_idx = " + str(current_traj_start_idx)
		# print "current_traj_end_idx = " + str(current_traj_end_idx)
		# print "desired_traj_start_idx = " + str(desired_traj_start_idx)
		# print "desired_traj_end_idx = " + str(desired_traj_end_idx)

		desired_lane_merge_range = []

		if desired_traj_start_idx < desired_traj_end_idx:
			desired_lane_merge_range = xrange(desired_traj_start_idx, desired_traj_end_idx)
		else:
			desired_lane_merge_range.extend(xrange(desired_traj_start_idx, len(desired_traj[0])))
			desired_lane_merge_range.extend(xrange(desired_traj_end_idx))
			# print "desired_lane_merge_range = " + str(desired_lane_merge_range)

		# SANITY CHECK
		if len( current_lane_merge_range ) != len( desired_lane_merge_range ):

			print "ERROR IN VEHICLE CONTROL MODULE, WRONG MERGE LANE SIZES"


		merging_traj = [[],[],[]]

		for i in range( len( current_lane_merge_range ) ):

			current_ratio = float( i )/float( len(current_lane_merge_range) )
			current_x = (1-current_ratio)*self.traj[0][ current_lane_merge_range[i] ] + current_ratio*desired_traj[0][ desired_lane_merge_range[i] ]
			current_y = (1-current_ratio)*self.traj[1][ current_lane_merge_range[i] ] + current_ratio*desired_traj[1][ desired_lane_merge_range[i] ]
			current_theta = (1-current_ratio)*self.traj[2][ current_lane_merge_range[i] ] + current_ratio*desired_traj[2][ desired_lane_merge_range[i] ]

			merging_traj[0].append(current_x)
			merging_traj[1].append(current_y)
			merging_traj[2].append(current_theta)

		# print "merging_traj[0] = " + str(merging_traj[0])

		remaining_traj = [[],[],[]]

		remaining_traj_indexes = []
		if desired_traj_start_idx < desired_traj_end_idx:
			remaining_traj_indexes.extend( xrange(desired_traj_end_idx, len(desired_traj[0]) ) )
			remaining_traj_indexes.extend( xrange( desired_traj_start_idx ) )
			# desired_lane_merge_range = xrange(desired_traj_start_idx, desired_traj_end_idx)
		else:
			remaining_traj_indexes = xrange(desired_traj_end_idx, desired_traj_start_idx)

		#print "---"
		#print "len(desired_traj[0]) = " + str(len(desired_traj[0]))
		#print "len(remaining_traj_indexes) = " + str(len(remaining_traj_indexes))

		for idx in remaining_traj_indexes:

			merging_traj[0].append( desired_traj[0][idx] )
			merging_traj[1].append( desired_traj[1][idx] )
			merging_traj[2].append( desired_traj[2][idx] )

		#print "len(merging_traj[0])  = " + str(len(merging_traj[0]) )


		#print "CHANGED TRAJECTORY"
		self.traj = merging_traj

		traj_x = merging_traj[0]
		traj_y = merging_traj[1]
		traj_theta = merging_traj[2]
		self.np_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		# print "smooth_lane_change_numpy self.np_traj  = " + str(self.np_traj ) 
		# print "merging_traj[0] = " + str(merging_traj[0]) 

		self.vehicle.supervisory_module.waitForEvent(self.vehicle.lanekeeping_ready, self.fixTrajAfterLaneChange, self.vehicle.fix_lane_change_abort)

		# new_traj = []

		# len(desired_lane_merge_range)

		# self.get_lane_merge( self.traj, desired_traj, current_lane_merge_range, desired_lane_merge_range)
		# self.get_simple_lane_merge( self.traj, desired_traj, current_lane_merge_range, desired_lane_merge_range)


		# print "len(current_lane_merge_range) = " + str(len(current_lane_merge_range))
		# print "len(desired_lane_merge_range) = " + str(len(desired_lane_merge_range))



		# print "desired_traj_closest_idx = " + str(desired_traj_closest_idx)
		# print "self.traj is desired_traj = " + str(self.traj is desired_traj)

		# print "len(desired_traj[0]) = " + str(len(desired_traj[0]))
		# print "len(self.traj[0]) = " + str(len(self.traj[0]))

		pass

	def dumb_lane_change_numpy(self, desired_traj, np_desired_traj):

		print "DUMB NUMPY smooth_lane_change, id = " + str(self.vehicle_id)


		#print "CHANGED TRAJECTORY"
		self.traj = desired_traj

		traj_x = desired_traj[0]
		traj_y = desired_traj[1]
		traj_theta = desired_traj[2]
		self.np_traj = numpy.asarray([ traj_x , traj_y , traj_theta])

		self.vehicle.supervisory_module.waitForEvent(self.vehicle.lanekeeping_ready, self.fixTrajAfterLaneChange, self.vehicle.fix_lane_change_abort)

		# new_traj = []

		# len(desired_lane_merge_range)

		# self.get_lane_merge( self.traj, desired_traj, current_lane_merge_range, desired_lane_merge_range)
		# self.get_simple_lane_merge( self.traj, desired_traj, current_lane_merge_range, desired_lane_merge_range)


		# print "len(current_lane_merge_range) = " + str(len(current_lane_merge_range))
		# print "len(desired_lane_merge_range) = " + str(len(desired_lane_merge_range))



		# print "desired_traj_closest_idx = " + str(desired_traj_closest_idx)
		# print "self.traj is desired_traj = " + str(self.traj is desired_traj)

		# print "len(desired_traj[0]) = " + str(len(desired_traj[0]))
		# print "len(self.traj[0]) = " + str(len(self.traj[0]))

		pass	

	def fixTrajAfterLaneChange(self):
		self.traj = self.vehicle.getCurrentLaneTrajectory()


	def find_closest_trajectory_point_numpy(self, state, trajectory_to_search):

		# This function will look for the closest point (in the trajectory) to state.
		# The points in the trajectory that are searched range from current_idx to current_idx + number_points_ahead 
		# (trajectory indexes wrap around)


		# Re-written making use of numpy. The closest point search becomes much faster.
		# TODO: Can be further improved by using the search_ranges
		# print trajectory_to_search.shape

		np_state = numpy.array([[state[0]],[state[1]]])

		temp_distance = numpy.sum((trajectory_to_search[0:2, :] - np_state)**2, axis = 0)

		# Find the closest trajectory point that matches my desired speed and current heading
		best_idx = numpy.argmin(temp_distance)

		return best_idx


	def find_closest_trajectory_point(self, state, current_idx, number_points_ahead, trajectory_to_search = None):
		# This function will look for the closest point (in the trajectory) to state.
		# The points in the trajectory that are searched range from current_idx to current_idx + number_points_ahead 
		# (trajectory indexes wrap around)

		# print "find_closest_trajectory_point()"

		
		search_range = range(0, number_points_ahead)



		# print "state = " + str(state)

		# print "self.traj[1] = " + str(self.traj[1])
		# self.traj[1] comes in real world metres

		# print "self.traj[2] = " + str(self.traj[2])
		# self.traj[2] comes in radians


		if not trajectory_to_search:
			# print "Default self.traj"
			trajectory_to_search = self.traj
			# temp_distance = [math.hypot( state[0]  - self.traj[0][ (current_idx+i)%traj_len ] , state[1] - self.traj[1][ (current_idx+i)%traj_len ] ) for i in search_range]
			
		whole_search_range = range(len(trajectory_to_search[0]))
		traj_len = len( trajectory_to_search[0] )
		temp_distance = [math.hypot( state[0]  - trajectory_to_search[0][ (i)%traj_len ] , state[1] - trajectory_to_search[1][ (i)%traj_len ] ) for i in whole_search_range]

		# Find the closest trajectory point that matches my desired speed and current heading
		best_distance = min(temp_distance)
		best_idx = temp_distance.index(best_distance)

		return [best_distance, best_idx]

	def compute_low_level_inputs(self):
		# This function simply returns the current low level inputs
		# to the Inputs Commands Manager
		# print "--------------------------"
		# print "compute_low_level_inputs()"

		closeby_body_readings = []

		# print "self.current_body_readings = " + str(self.current_body_readings)

		if self.current_body_readings:

			for temp_body_reading in self.current_body_readings['readings']:

				temp_closeby_body_reading = [ temp_body_reading[0]*32. , temp_body_reading[1]*32. , math.radians( temp_body_reading[2] ) ]

				distance_to_body = math.hypot( self.state[0] - temp_closeby_body_reading[0] , self.state[1] - temp_closeby_body_reading[1] )

				# print "distance_to_body = " + str(distance_to_body)

				if distance_to_body < 30.0:

					closeby_body_readings.append(temp_closeby_body_reading)

		desired_velocity = self.desired_velocity

		current_idx = self.current_trajectory_id

		number_points_ahead = 25

		# print "self.state = " + str(self.state)
		# self.state = [-39.44159273512578, -38.36738366701975, -2.3967515204479533]
		# self.state comes in real world metres and radians

		# [best_distance, best_idx] = self.find_closest_trajectory_point( self.state, current_idx, number_points_ahead)
		best_idx = self.find_closest_trajectory_point_numpy( self.state, self.np_traj)

		if self.debug_lane_change:

			print "best_idx = " + str(best_idx) + " id = " + str(self.vehicle_id)

		current_closest_trajectory_point = best_idx

		traj_len = len( self.traj[0] )

		best_idx += 5

		reference_state = [self.traj[0][ (best_idx)%traj_len ] , self.traj[1][ (best_idx)%traj_len ] , self.traj[2][ (best_idx)%traj_len ] ]
		# print "reference_state = " + str(reference_state)
		# print "self.state = " + str(self.state)

		# print "get_trajectory_tracking_inputs reference_state = " + str(reference_state)

		# converted_state = [self.state[0]/32., self.state[1]/32., self.state[2]]
		# [velocity_command, steering_command] = self.controller_action(self.state, reference_state)
		[velocity_command, steering_command] = self.controller_action_new(self.state, reference_state)

		# print "Control Module: [velocity_command, steering_command] = " + str([velocity_command, steering_command])

		self.current_velocity_command = velocity_command
		self.current_steering_command = steering_command
		# Steering command should be outputteed in DEGREES

		self.current_trajectory_id = current_closest_trajectory_point

		return [velocity_command, steering_command]

	def get_steering_lateral_following(self):
		# This function is called when the ACCC mode is on

		# It simply runs a PID that outputs the longitudinal velocity
		# The error source of this PID is the error in the current distance
		# to the front vehicle, agains the desired distance
		# desired_velocity = self.vehicle.desired_velocity

		#dt = datetime.now()
		#current_time = dt.
		current_time = time.time()

		# linear_velocity = math.hypot(self.bodies_array[self.vehicle_id].x_speed, self.bodies_array[self.vehicle_id].y_speed)

		current_error = self.lateral_following_displacement

		# print "\n"
		# print "linear_velocity = " + str(linear_velocity)
		# print "CC error = " + str( current_error )
		# print "self.CC_I_e = " + str(self.CC_I_e)

		if self.last_latfoll_time:

			# Compute last sampling time
			time_passed = current_time - self.last_latfoll_time
			self.last_latfoll_time = current_time

			if self.latfoll_I_e * current_error < 0:
				
				# print "QUICK DISCHARGE"
				self.latfoll_I_e += 10*time_passed*current_error

			else:

				# Integrate the error
				self.latfoll_I_e += time_passed*current_error


			# To avoid integration windup (i.e. big steady errors will result in big overshoots)
			max_integral_term = 30.

			



			if self.latfoll_I_e > max_integral_term:

				self.latfoll_I_e = max_integral_term
				# print "Vehicle " + str(self.vehicle_id) + " is using latfoll, speed = " + str( velocity_latfoll ) + " with integral term " + str( self.latfoll_I_e  )

			# Compute the derivative of the error
			if time_passed >0:
				self.derivative_error = (current_error - self.last_latfoll_e)/time_passed



			# Compute PID output
			steering_latfoll = self.latfoll_k_p*current_error + self.latfoll_k_i*self.latfoll_I_e + self.latfoll_k_d*self.derivative_error

			# self.last_latfoll_e = current_error

		else:

			# Initialization of the AlatfollC controller (first iteration of this controller)
			self.last_latfoll_time = current_time
			
			self.latfoll_I_e = 0.0
			self.derivative_error = 0.0

			steering_latfoll = 0.

		self.last_latfoll_e = current_error

		# print "steering_latfoll = " + str(steering_latfoll)

		# max_CC_velocity = 18.

		# if velocity_CC > max_CC_velocity:

		# 	velocity_CC = max_CC_velocity

		# print "velocity_CC = " + str(velocity_CC)


			# print "Vehicle " + str(self.vehicle_id) + " is using CC, speed = " + str( velocity_CC ) + " with integral term " + str( self.CC_I_e  )

		return steering_latfoll

	def reset_latfoll(self):
		# To make sure that the latfoll is reseted when it is not used
		self.last_latfoll_time = []
		self.latfoll_I_e = []
		self.last_latfoll_e = []

	def get_velocity_CC(self):
		# This function is called when the CC mode is on

		# It simply runs a PID that outputs the longitudinal velocity
		# The error source of this PID is the error in the current distance
		# to the front vehicle, agains the desired distance
		desired_velocity = self.vehicle.desired_velocity

		#dt = datetime.now()
		#current_time = dt.
		current_time = time.time()

		linear_velocity = math.hypot(self.bodies_array[self.vehicle_id].x_speed, self.bodies_array[self.vehicle_id].y_speed)

		current_error = desired_velocity - linear_velocity

		# print "\n"
		# print "linear_velocity = " + str(linear_velocity)
		# print "CC error = " + str( current_error )
		# print "self.CC_I_e = " + str(self.CC_I_e)

		if self.last_CC_time:

			# Compute last sampling time
			time_passed = current_time - self.last_CC_time
			self.last_CC_time = current_time

			# Integrate the error
			self.CC_I_e += time_passed*current_error


			# To avoid integration windup (i.e. big steady errors will result in big overshoots)
			max_integral_term = 30.

			if self.CC_I_e > max_integral_term:

				self.CC_I_e = max_integral_term
				# print "Vehicle " + str(self.vehicle_id) + " is using CC, speed = " + str( velocity_CC ) + " with integral term " + str( self.CC_I_e  )

			# Compute the derivative of the error
			if time_passed >0:
				self.derivative_error = (current_error - self.last_CC_e)/time_passed

			# Compute PID output
			velocity_CC = self.desired_velocity + self.CC_k_p*current_error + self.CC_k_i*self.CC_I_e + self.CC_k_d*self.derivative_error

		else:

			# Initialization of the ACCC controller (first iteration of this controller)
			self.last_CC_time = current_time
			self.last_CC_e = current_error
			self.CC_I_e = 0.0
			self.derivative_error = 0.0

			velocity_CC = self.desired_velocity

		# max_CC_velocity = 18.

		# if velocity_CC > max_CC_velocity:

		# 	velocity_CC = max_CC_velocity

		# print "velocity_CC = " + str(velocity_CC)


			# print "Vehicle " + str(self.vehicle_id) + " is using CC, speed = " + str( velocity_CC ) + " with integral term " + str( self.CC_I_e  )

		return velocity_CC

	def reset_CC(self):
		# To make sure that the CC is reseted when it is not used
		self.last_CC_time = []
		self.CC_I_e = []
		self.last_CC_e = []

	def get_velocity_ACCC(self):
		# This function is called when the ACCC mode is on

		# It simply runs a PID that outputs the longitudinal velocity
		# The error source of this PID is the error in the current distance
		# to the front vehicle, agains the desired distance



		if self.ACCC_distance:
			current_error = self.ACCC_distance - self.ACCC_desired_distance
		else:
			return self.desired_velocity

		#dt = datetime.now()
		#current_time = dt.
		current_time = time.time()


		if self.last_ACCC_time:

			# Compute last sampling time
			time_passed = current_time - self.last_ACCC_time
			self.last_ACCC_time = current_time

			# Integrate the error
			self.ACCC_I_e += time_passed*current_error


			# To avoid integration windup (i.e. big steady errors will result in big overshoots)
			max_integral_term = 30.

			if self.ACCC_I_e > max_integral_term:

				self.ACCC_I_e = max_integral_term
				# print "Vehicle " + str(self.vehicle_id) + " is using ACCC, speed = " + str( velocity_ACCC ) + " with integral term " + str( self.ACCC_I_e  )

			# Compute the derivative of the error
			if time_passed >0:
				self.derivative_error = (current_error - self.last_ACCC_e)/time_passed

			# Compute PID output
			velocity_ACCC = self.desired_velocity + self.ACCC_k_p*current_error + self.ACCC_k_i*self.ACCC_I_e + self.ACCC_k_d*self.derivative_error

		else:

			# Initialization of the ACCC controller (first iteration of this controller)
			self.last_ACCC_time = current_time
			self.last_ACCC_e = current_error
			self.ACCC_I_e = 0.0
			self.derivative_error = 0.0

			velocity_ACCC = self.desired_velocity

		if velocity_ACCC > self.vehicle.desired_velocity + self.vehicle.ACCC_max_deviation:

			velocity_ACCC = self.vehicle.desired_velocity + self.vehicle.ACCC_max_deviation

		elif velocity_ACCC < self.vehicle.desired_velocity - self.vehicle.ACCC_max_deviation:

			velocity_ACCC = self.vehicle.desired_velocity - self.vehicle.ACCC_max_deviation

		if velocity_ACCC < 0:

			velocity_ACCC = 0


			# print "Vehicle " + str(self.vehicle_id) + " is using ACCC, speed = " + str( velocity_ACCC ) + " with integral term " + str( self.ACCC_I_e  )

		return velocity_ACCC

	def reset_ACCC(self):
		# To make sure that the ACCC is reseted when it is not used
		self.last_ACCC_time = []
		self.ACCC_I_e = []
		self.last_ACCC_e = []

	def get_throttle_CC(self, desired_velocity):
		# This function is called when the CC mode is on

		# It simply runs a PID that outputs the longitudinal velocity
		# The error source of this PID is the error in the current distance
		# to the front vehicle, agains the desired distance

		#dt = datetime.now()
		#current_time = dt.
		current_time = time.time()

		linear_velocity = math.hypot(self.bodies_array[self.vehicle_id].x_speed, self.bodies_array[self.vehicle_id].y_speed)

		current_error = desired_velocity - linear_velocity

		if self.last_CC_time:

			# Compute last sampling time
			time_passed = current_time - self.last_CC_time
			self.last_CC_time = current_time

			# Integrate the error
			self.CC_I_e += time_passed*current_error


			# To avoid integration windup (i.e. big steady errors will result in big overshoots)
			max_integral_term = 20.
			# if self.CC_I_e > max_integral_term:

			# 	self.CC_I_e = max_integral_term

			max_integral_action = 1000.
			integral_action = self.CC_k_i*self.CC_I_e

			if math.fabs(integral_action) > max_integral_action:

				integral_action = math.copysign(max_integral_action, integral_action)
				self.CC_I_e = integral_action/self.CC_k_i
				# math.copysign(x, y): Return x with the sign of y. 
			


				# print "Vehicle " + str(self.vehicle_id) + " is using CC, speed = " + str( velocity_CC ) + " with integral term " + str( self.CC_I_e  )

			# Compute the derivative of the error
			if time_passed >0:
				self.derivative_error = (current_error - self.last_CC_e)/time_passed

			self.last_CC_e = current_error

			# if self.vehicle_id == -3:

				# print "self.last_CC_e = " + str(self.last_CC_e)
				# print "P = " + str(self.CC_k_p*current_error) + " I = " + str(integral_action) + " D = " + str(self.CC_k_d*self.derivative_error)

			# Compute PID output
			# velocity_CC = self.desired_velocity + self.CC_k_p*current_error + integral_action + self.CC_k_d*self.derivative_error
			velocity_CC = 0 + self.CC_k_p*current_error + integral_action + self.CC_k_d*self.derivative_error
			# print "Integration contribution = " + str(integral_action)

		else:

			# Initialization of the ACCC controller (first iteration of this controller)
			self.last_CC_time = current_time
			self.last_CC_e = current_error
			self.CC_I_e = 0.0
			self.derivative_error = 0.0

			velocity_CC = 0

		# max_CC_velocity = 18.

		# if velocity_CC > max_CC_velocity:

		# 	velocity_CC = max_CC_velocity

		# print "velocity_CC = " + str(velocity_CC)


			# print "Vehicle " + str(self.vehicle_id) + " is using CC, speed = " + str( velocity_CC ) + " with integral term " + str( self.CC_I_e  )

		return velocity_CC

	def controller_action_new(self, current_state, current_reference):

		steering_command = self.get_steering_command(current_state, current_reference)

		# velocity_command = float(self.cruise_velocity)*(1000./3600.)
		# velocity_command = self.get_throttle_CC(self.cruise_velocity*(1000./3600.))

		if self.ACCC:
			# self.reset_CC()
			desired_velocity = self.get_velocity_ACCC()
		else:
			self.reset_ACCC()
			desired_velocity = self.vehicle.desired_velocity # Desired velocity comes in km/h, must convert to m/s
			# velocity_command = self.get_velocity_CC()

		throttle_command = self.get_throttle_CC(desired_velocity)
		
		command_inputs = [throttle_command, steering_command]
		# print "command_inputs = " + str(command_inputs)
		return command_inputs

	def get_tracking_error(self, current_state, current_reference):

		# current_state[2] in radians
		error_x = math.cos( current_state[2] )*(current_reference[0] - current_state[0]) + math.sin( current_state[2] )*(current_reference[1] - current_state[1])
		error_y = -math.sin( current_state[2] )*(current_reference[0] - current_state[0]) + math.cos( current_state[2] )*(current_reference[1] - current_state[1])
		error_theta = current_reference[2] - current_state[2]

		while error_theta > math.pi:
			error_theta -= 2*math.pi
		while error_theta < -math.pi:
			error_theta += 2*math.pi

		return [error_x, error_y, error_theta]

	def get_steering_command(self, current_state, current_reference, max_steering_radians = 30):

		tracking_error = self.get_tracking_error(current_state, current_reference)

		while tracking_error[2] > math.pi:
			tracking_error[2] = tracking_error[2] - math.pi
		while tracking_error[2] < -math.pi:
			tracking_error[2] = tracking_error[2] + math.pi

		# steering_command = 8.0* (10.0*tracking_error[1] + 15.0*tracking_error[2]) # Stable for unicycle model
		steering_command = 1.0* (1.0*tracking_error[1] + 1.5*tracking_error[2]) 

		# Need to limit the steering command to 30 degrees
		if steering_command > math.radians(max_steering_radians):
			steering_command = math.radians(max_steering_radians)

		if steering_command < -math.radians(max_steering_radians):
			steering_command = -math.radians(max_steering_radians)

		return steering_command
		
	def controller_action(self, current_state, current_reference):

		tracking_error = self.tracking_error(current_state, current_reference)
		# print "tracking_error = " + str(tracking_error) 
		while tracking_error[2] > math.pi:
			tracking_error[2] = tracking_error[2] - math.pi
		while tracking_error[2] < -math.pi:
			tracking_error[2] = tracking_error[2] + math.pi
		# current_state[2] comes in radians
		# current_reference[2] comes in radians
		# current_state[2] = math.radians( current_state[2] )
		# print "current_state = " + str(current_state) 
		# print "current_reference = " + str(current_reference)
		# current_reference[2] comes in radians
		# print "tracking_error = " + str(tracking_error)
		# velocity_command = tracking_error[0]*0.01
		# velocity_command = 0.35

		# print "self.ACCC = " + str(self.ACCC)
		# Simulator assumes that input velocity comes as SML velocity!
		if self.ACCC:
			# self.reset_CC()
			velocity_command = self.get_velocity_ACCC()
		else:
			self.reset_ACCC()
			velocity_command = self.vehicle.desired_velocity # Desired velocity comes in km/h, must convert to m/s
			# velocity_command = self.get_velocity_CC()

		if self.lateral_following:

			# steering_command = 0.2* self.lateral_following_displacement
			steering_command = self.get_steering_lateral_following()
			# print " self.lateral_following_displacement = " + str(self.lateral_following_displacement)

		else:

			self.reset_latfoll()
		# steering_command = 8.0* (10.0*tracking_error[1] + 15.0*tracking_error[2]) # Stable for unicycle model
			steering_command = 1.0* (1.0*tracking_error[1] + 1.5*tracking_error[2]) 

		# Need to limit the steering command to 30 degrees
		if steering_command > math.radians(30):
			steering_command = math.radians(30)

		if steering_command < -math.radians(30):
			steering_command = -math.radians(30)



		# print "\n\n\n\nsteering_command = " + str(steering_command) 

		# steering_command = -steering_command
		# max_steering_amplitude = 100
		# steering_command = max( min(max_steering_amplitude, steering_command) , -max_steering_amplitude)
		# print "steering_command = " + str(steering_command) 


		# Working well with the normal Scania city
		# steering_command = 8.0* (10.0*tracking_error[1] + 10.0*tracking_error[2])

		# steering_command_max = math.radians(20)

		# if steering_command > steering_command_max:
		# 	# print "Max steering reached"
		# 	steering_command = steering_command_max

		# if steering_command < -steering_command_max:
		# 	# print "Max steering reached"
		# 	steering_command = -steering_command_max

		command_inputs = [velocity_command, steering_command]
		# print "command_inputs = " + str(command_inputs)
		return command_inputs

	def tracking_error(self, current_state, current_reference):

		error_x = math.cos( current_state[2] )*(current_reference[0] - current_state[0]) + math.sin( current_state[2] )*(current_reference[1] - current_state[1])
		error_y = -math.sin( current_state[2] )*(current_reference[0] - current_state[0]) + math.cos( current_state[2] )*(current_reference[1] - current_state[1])
		error_theta = current_reference[2] - current_state[2]

		# print "current_reference[2] = " + str(current_reference[2])
		# print "current_state[2] = " + str(current_state[2])

		# print "error_theta = " + str(error_theta)

		while error_theta > math.pi:
			error_theta -= 2*math.pi
		while error_theta < -math.pi:
			error_theta += 2*math.pi

		# print "error_theta = " + str(error_theta)


		return [error_x, error_y, error_theta]
		# error_x = math.cos( current_state[2] )*(current_reference[0] - current_state[0]) + math.sin( current_state[2] )*(current_reference[1] - current_state[1])

