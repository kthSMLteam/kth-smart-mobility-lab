function [ xSquircle , ySquircle ] = getSquircleNormalized( squircleFactor , time, finalTime, numberLaps)
%GETSCUIRCLE Summary of this function goes here
%   Detailed explanation goes here

    denseTimeStep = 0.001;
    denseTime = 0:denseTimeStep:finalTime;
    
    normalTimeStep = 0.1;

    xSquircle = cos(numberLaps*denseTime*((2*pi)/(finalTime)) );
    ySquircle = sin(numberLaps*denseTime*((2*pi)/(finalTime)) );

    xSquircle = sign(xSquircle).*abs(xSquircle.^squircleFactor);
    ySquircle = sign(ySquircle).*abs(ySquircle.^squircleFactor);
    
    deltaX = [0 diff(xSquircle)];
    deltaY = [0 diff(ySquircle)];
    
    dist = cumsum( (deltaX.^2 + deltaY.^2 ).^.5 ); 
      
    normalizedDist = 0:dist(end)/(length(dist)-1):dist(end);
    
    xSquircleNormalized = cos(numberLaps*time*((2*pi)/(finalTime)) );
    ySquircleNormalized = sin(numberLaps*time*((2*pi)/(finalTime)) );
    
    velocity = dist(end)/finalTime; % meters per second
    
    for i = 2:length(xSquircleNormalized)-1
        
        currentDist = velocity*normalTimeStep*i;
        
        [~, closestIndex] = min(abs(dist-currentDist));
        
        xSquircleNormalized(i) = xSquircle(closestIndex);
        ySquircleNormalized(i) = ySquircle(closestIndex);
        
        
    end
    
    xSquircle = xSquircleNormalized;
    ySquircle = ySquircleNormalized;
    
    
end

