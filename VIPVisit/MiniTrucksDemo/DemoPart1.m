clear

timeStep = 0.1;
finalTime = 60;
time = 0:timeStep:finalTime;
numberLaps = 1;

squircleFactor = .5;
% [xRoadOut, yRoadOut] = getSquircle( squircleFactor , time, finalTime, numberLaps);
[xRoadOut, yRoadOut] = getSquircleNormalized( squircleFactor , time, finalTime, numberLaps);

timeEntrance = 0:timeStep:10;
xRoadEntrance = cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) ) - max(cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) )) ;
xRoadEntrance = 0.5*xRoadEntrance + min(xRoadOut);
yRoadEntrance = 0.5*sin( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) );

insideRoadFactor = 0.9;
xRoadIn = insideRoadFactor*xRoadOut; yRoadIn = insideRoadFactor*yRoadOut;

platooningDistance = 0.05;


figure(); hold on;
set(gcf, 'position', [1921,433,1024,692])
width=6.18;
x_offset=-2.36;
height=4.4;
y_offset=-2.4;
axis([x_offset x_offset+width y_offset y_offset+height])

scaleX = 2.0;
scaleY = 2.0;
offsetX = 0.5;
offsetY = 0.0;

xRoadOut = scaleX*xRoadOut + offsetX;
yRoadOut = scaleY*yRoadOut + offsetY;
xRoadIn = scaleX*xRoadIn + offsetX;
yRoadIn = scaleY*yRoadIn + offsetY;
xRoadEntrance = scaleX*xRoadEntrance + offsetX;
yRoadEntrance = scaleY*yRoadEntrance + offsetY;

plot(xRoadOut,yRoadOut);
plot(xRoadIn,yRoadIn);
plot(xRoadEntrance,yRoadEntrance);

handleLeadTruck = plot( xRoadOut(1), yRoadOut(1) );
handleLeadFollowingTruck = plot( xRoadIn(1), yRoadIn(1) );


truck1Orders = time;
truck2Orders = time;
truck3Orders = time;

platooningDistance = 0.05;

truck2Distance = ones(size(truck1Orders))*platooningDistance;
tempIndex = round( length(truck2Distance)/4 );
truck2Distance(1:tempIndex) = truck2Distance(1:tempIndex) +  [0.2:-0.2/( (tempIndex)-1 ):0] ;

initialDistance3 = 0.3;
truck3Distance = ones(size(truck1Orders))*2*platooningDistance;
tempIndex = round( (length(truck3Distance)/4)*1.5 );
truck3Distance(1:tempIndex) = truck3Distance(1:tempIndex) +  [initialDistance3:-initialDistance3/( (tempIndex)-1 ):0] ;

truck1Orders = time/time(end);
truck2Orders = truck1Orders - truck2Distance; 
truck3Orders = truck1Orders - truck3Distance;

truck3OrdersEntry = zeros(size(time));



truck3ReferenceX = zeros(size(xRoadOut));
truck3ReferenceY = zeros(size(yRoadOut));

truck3RoadOut = zeros(size(xRoadOut));
truck3RoadIn = zeros(size(xRoadOut));
truck3LeftEntrance = zeros(size(xRoadOut));

% timeToStartEntering = 21.5;
% timeToFinishEntering = 31.5;
timeToStartEntering = 26;
timeToFinishEntering = 36;

truck3LeftEntrance( 1:floor(timeToFinishEntering/timeStep) ) = 1;
truck3RoadOutEntrance( 1:floor(timeToFinishEntering/timeStep) ) = 0;

truck3LeftEntrance( ceil(timeToFinishEntering/timeStep):end ) = 0;
truck3RoadOutEntrance( ceil(timeToFinishEntering/timeStep):end ) = 1;

truck3ReferenceX( 1:floor(timeToStartEntering/timeStep) ) = xRoadEntrance(1);
truck3ReferenceY( 1:floor(timeToStartEntering/timeStep) ) = yRoadEntrance(1);

truck3ReferenceX( ceil(timeToStartEntering/timeStep):floor(timeToFinishEntering/timeStep) ) = xRoadEntrance;
truck3ReferenceY( ceil(timeToStartEntering/timeStep):floor(timeToFinishEntering/timeStep) ) = yRoadEntrance;


platooningDistanceIndex = 60;
truck3ReferenceX( ceil(timeToFinishEntering/timeStep):end ) = xRoadOut( ceil((timeToFinishEntering /timeStep)-platooningDistanceIndex):end-platooningDistanceIndex);
truck3ReferenceY( ceil(timeToFinishEntering/timeStep):end ) = yRoadOut( ceil((timeToFinishEntering /timeStep)-platooningDistanceIndex):end-platooningDistanceIndex);

% return



% handleLeadTruck = plot( leadTruckReference(1), leadTruckReference(2), 'ro');
% handleLeadFollowingTruck = plot( followingTruckReference(1), followingTruckReference(2), 'go');
% handleThirdTruck = plot( thirdTruckReference(1), thirdTruckReference(2), 'ko');

startingIndex = 1;
% startingIndex = floor(length(time)*(1/3));

xReferenceTruck1 = 0*time;
yReferenceTruck1 = 0*time;
xReferenceTruck2 = 0*time;
yReferenceTruck2 = 0*time;
xReferenceTruck3 = 0*time;
yReferenceTruck3 = 0*time;


for i = startingIndex:length(time)
    
%     leadTruckReference = [xRoadOut(i) yRoadOut(i)];
%     followingTruckReference = [xRoadIn(i) yRoadIn(i)];

%     leadTruckReference = getTruckReference(truck1Orders, roadValue);
    [ xReference, yReference ] = getTruckReference(truck1Orders(i), xRoadOut, yRoadOut, 0);
    leadTruckReference = [ xReference, yReference ];
    xReferenceTruck1(i) = xReference; yReferenceTruck1(i) = yReference;
    
    [ xReference, yReference ] = getTruckReference(truck2Orders(i), xRoadOut, yRoadOut, 0);
    followingTruckReference = [ xReference, yReference ];
    xReferenceTruck2(i) = xReference; yReferenceTruck2(i) = yReference;
%     [ xReference, yReference ] = getTruckReference3(truck3Orders(i), truck3OrdersEntry(i), xRoadOut, yRoadOut, xRoadEntrance, yRoadEntrance, 0);
%     thirdTruckReference = [ xReference, yReference ];
    thirdTruckReference = [ truck3ReferenceX(i), truck3ReferenceY(i) ];
    xReferenceTruck3(i) = truck3ReferenceX(i); yReferenceTruck3(i) = truck3ReferenceY(i);
    
%     truckDistance = 10;
%     
%     if i-truckDistance > 0
%         followingTruckReference = [xRoadIn(i-truckDistance) yRoadIn(i-truckDistance)];
%     else
%         followingTruckReference = [xRoadIn(1) yRoadIn(1)];
%     end
    
    if (i>startingIndex)

        delete(handleLeadTruck);
        delete(handleLeadFollowingTruck);
        delete(handleThirdTruck);
    
    end
    handleLeadTruck = plot( leadTruckReference(1), leadTruckReference(2), 'ro');
    handleLeadFollowingTruck = plot( followingTruckReference(1), followingTruckReference(2), 'go');
    handleThirdTruck = plot( thirdTruckReference(1), thirdTruckReference(2), 'ko');
    
    pause(timeStep);
    
end

% save('demo1References','xReferenceTruck1','yReferenceTruck1','xReferenceTruck2','yReferenceTruck2','xReferenceTruck3','yReferenceTruck3')
