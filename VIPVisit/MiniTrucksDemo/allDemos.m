clear

timeStep = 0.1;
finalTime = 60;
time = 0:timeStep:finalTime;
numberLaps = 1;

squircleFactor = .5;
% [xRoadOut, yRoadOut] = getSquircle( squircleFactor , time, finalTime, numberLaps);
[xRoadOut, yRoadOut] = getSquircleNormalized( squircleFactor , time, finalTime, numberLaps);

timeEntrance = 0:timeStep:10;
xRoadEntrance = cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) ) - max(cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) )) ;
xRoadEntrance = 0.5*xRoadEntrance + min(xRoadOut);
yRoadEntrance = 0.5*sin( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) );

xRoadExit = cos( -(pi/2)*timeEntrance/(timeEntrance(end)) ) - max(cos( (pi/2)-(pi/2)*timeEntrance/(timeEntrance(end)) )) ;
xRoadExit = 0.5*xRoadExit + min(xRoadOut);
yRoadExit = 0.5*sin( -(pi/2)*timeEntrance/(timeEntrance(end)) );

insideRoadFactor = 0.9;
xRoadIn = insideRoadFactor*xRoadOut; yRoadIn = insideRoadFactor*yRoadOut;

platooningDistance = 0.05;


figure(); hold on;
set(gcf, 'position', [1921,433,1024,692])
width=6.18;
x_offset=-2.36;
height=4.4;
y_offset=-2.4;
axis([x_offset x_offset+width y_offset y_offset+height])

scaleX = 2.0;
scaleY = 2.0;
offsetX = 0.5;
offsetY = 0.0;

xRoadOut = scaleX*xRoadOut + offsetX;
yRoadOut = scaleY*yRoadOut + offsetY;
xRoadIn = scaleX*xRoadIn + offsetX;
yRoadIn = scaleY*yRoadIn + offsetY;
xRoadEntrance = scaleX*xRoadEntrance + offsetX;
yRoadEntrance = scaleY*yRoadEntrance + offsetY;
xRoadExit = scaleX*xRoadExit + offsetX;
yRoadExit = scaleY*yRoadExit + offsetY;

plot(xRoadOut,yRoadOut);
plot(xRoadIn,yRoadIn);
plot(xRoadEntrance,yRoadEntrance);
plot(xRoadExit,yRoadExit);

load demo1References

xAllTruck1 = xReferenceTruck1;
yAllTruck1 = yReferenceTruck1;
xAllTruck2 = xReferenceTruck2;
yAllTruck2 = yReferenceTruck2;
xAllTruck3 = xReferenceTruck3;
yAllTruck3 = yReferenceTruck3;

load demo2References

xAllTruck1 = [xAllTruck1 xReferenceTruck1];
yAllTruck1 = [yAllTruck1 yReferenceTruck1];
xAllTruck2 = [xAllTruck2 xReferenceTruck2];
yAllTruck2 = [yAllTruck2 yReferenceTruck2];
xAllTruck3 = [xAllTruck3 xReferenceTruck3];
yAllTruck3 = [yAllTruck3 yReferenceTruck3];

load demo3References

xAllTruck1 = [xAllTruck1 xReferenceTruck1];
yAllTruck1 = [yAllTruck1 yReferenceTruck1];
xAllTruck2 = [xAllTruck2 xReferenceTruck2];
yAllTruck2 = [yAllTruck2 yReferenceTruck2];
xAllTruck3 = [xAllTruck3 xReferenceTruck3];
yAllTruck3 = [yAllTruck3 yReferenceTruck3];

startingIndex = 1;

for i = startingIndex:3*length(time)

    leadTruckReference = [ xAllTruck1(i), yAllTruck1(i) ];
    
    followingTruckReference = [ xAllTruck2(i), yAllTruck2(i) ];
    
    thirdTruckReference = [ xAllTruck3(i), yAllTruck3(i) ];
    
    
%     [ xReference, yReference ] = getCarReference(carOrders(i), xRoadOut, yRoadOut, xRoadIn, yRoadIn, xRoadExit, yRoadExit, carLane(i), i-carExitingIndex+1);
%     carReference = [ xReference, yReference ];

    if (i>startingIndex)

        delete(handleLeadTruck);
        delete(handleLeadFollowingTruck);
        delete(handleThirdTruck);
%         delete(handleCar);
    
    end
    
    handleLeadTruck = plot( leadTruckReference(1), leadTruckReference(2), 'ro');
    handleLeadFollowingTruck = plot( followingTruckReference(1), followingTruckReference(2), 'go');
    handleThirdTruck = plot( thirdTruckReference(1), thirdTruckReference(2), 'ko');
%     handleCar = plot( carReference(1), carReference(2), 'kx');


    pause(timeStep/5)
    
end