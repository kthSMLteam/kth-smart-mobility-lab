function [ xReference, yReference ] = getTruckReference3(truckOrders, xRoad, yRoad, xEntrance, yEntrance, roadValue)
%GETTRUCKREFERENCE Summary of this function goes here
%   Detailed explanation goes here

    while truckOrders <= 0
        truckOrders = truckOrders +1;
    end
    
    index = round(truckOrders*length(xRoad));
    if index == 0
        index = 1;
    end
    
    xReference = xRoad(index)*truckOrders + xEntrance(index)*(1-truckOrders);
    yReference = yRoad(index)*truckOrders + yEntrance(index)*(1-truckOrders);
    

end

