function [ xSquircle , ySquircle ] = getSquircle( squircleFactor , time, finalTime, numberLaps)
%GETSCUIRCLE Summary of this function goes here
%   Detailed explanation goes here

    xSquircle = cos(numberLaps*time*((2*pi)/(finalTime)) );
    ySquircle = sin(numberLaps*time*((2*pi)/(finalTime)) );

    xSquircle = sign(xSquircle).*abs(xSquircle.^squircleFactor);
    ySquircle = sign(ySquircle).*abs(ySquircle.^squircleFactor);
    
    deltaX = [0 diff(xSquircle)];
    deltaY = [0 diff(ySquircle)];
    
    dist = cumsum( (deltaX.^2 + deltaY.^2 ).^.5 ); 
    
%     plot(dist)
    
    size(xSquircle)
    size(dist)
    
    normalizedDist = 0:dist(end)/(length(dist)-1):dist(end);
    
    xSquircleNormalized = xSquircle;
    ySquircleNormalized = ySquircle;
    
    for i = 1:length(normalizedDist)-1
        
        currentDist = normalizedDist(i);
        
        differences = dist - currentDist;
        
        [value,closestIndex] = min(abs(differences));
        firstIndex = closestIndex;      
        firstDistance = value;
        
        if ( differences(closestIndex) > 0 )
            secondIndex = closestIndex-1;
        else
            secondIndex = closestIndex+1;
        end
        
        secondDistance = abs(differences(secondIndex));
        
        
        
        normalizingTerm = firstDistance + secondDistance;
        
        xSquircleNormalized(i) = (normalizingTerm-firstDistance)*xSquircleNormalized(firstIndex) + (normalizingTerm-secondDistance)*xSquircleNormalized(secondIndex);
        xSquircleNormalized(i) = xSquircleNormalized(i)/normalizingTerm;
        
        ySquircleNormalized(i) = (normalizingTerm-firstDistance)*ySquircleNormalized(firstIndex) + (normalizingTerm-secondDistance)*ySquircleNormalized(secondIndex);
        ySquircleNormalized(i) = ySquircleNormalized(i)/normalizingTerm;
        
    end
    
    xSquircle = xSquircleNormalized;
    ySquircle = ySquircleNormalized;
    
    
end

