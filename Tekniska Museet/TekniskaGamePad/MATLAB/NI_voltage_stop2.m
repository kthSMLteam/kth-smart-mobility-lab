function []=NI_voltage_stop2(s)
%NI_VOLTAGE_OUTPUT Send voltage to NI outputs
%   It will limit the voltage to [0,3.2]

% Must limit the voltage to [0,3.2], to not fry the Scania Truck Remote
% Control
%     v1=3.2/2;
    v1 = 1.7;
    v2=v1;
    v3=v1;
    v4=v1;
    v5=v1;
    v6=v1;
    v7=v1;
    v8=v1;
%     v7=v1;
%     v8=v1;
%     v9=0;

    v1=min(3.2,v1);
    v1=max(0,v1);
    v2=min(3.2,v2);
    v2=max(0,v2);
    v3=min(3.2,v3);
    v3=max(0,v3);
    v4=min(3.2,v4);
    v4=max(0,v4);
    v5=min(3.2,v5);
    v5=max(0,v5);    
    v6=min(3.2,v6);
    v6=max(0,v6);
    v7=min(3.2,v7);
    v7=max(0,v7);
    v8=min(3.2,v8);
    v8=max(0,v8);
%     v7=min(3.2,v7);
%     v7=max(0,v7);
%     v8=min(3.2,v8);
%     v8=max(0,v8);
%     v9=min(3.2,v9);
%     v9=max(0,v9);

    s.outputSingleScan([3.2 3.2 v2 v1 v4 v3 v6 v5 v8 v7]);

end

