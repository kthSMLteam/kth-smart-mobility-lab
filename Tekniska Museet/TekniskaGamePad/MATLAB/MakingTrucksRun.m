function MakingTrucksRun(handles)

disp ('entered')

s=NI_initialization2(handles);
NI_voltage_stop2(s)

%set(handles.up1,'BackgroundColor',[1 1 1]);
%set(handles.up1,'HighlightColor',[0 0 0]);
%clear JoyMEX

%JoyMEX('init',0);
%JoyMEX('init',1);

global playwith stop

while ~stop
    
    playwith
    
    v1=0;
    v2=v1;
    v3=v1;
    v4=v1;
    v5=v1;
    v6=v1;
    v7=v1;
    v8=v1;
    
    rightAnalogLongitudinal = zeros(1,4);
    leftAnalogLateral = zeros (1,4);
    
    ksteer=1;
    kspeed=1.3;
    
    ksteer = str2num(get(handles.ksteer,'String'));
    kspeed = str2num(get(handles.kspeed,'String'));
    
    if playwith(1)
        a =zeros(1,5);
        try
            [a ab] = JoyMEX(0);
%         catch
%             try
%                 clear JoyMex
%                 JoyMEX('init',0);
%                 JoyMEX('init',1);
%                 JoyMEX('init',2);
%                 JoyMEX('init',3);
%             end
        end
        leftAnalogLateral(1) = a(1);
        rightAnalogLongitudinal(1) = -a(5);
        v1 = 1.6 + kspeed*rightAnalogLongitudinal(1);
        set(handles.speed1,'Visible','on')
        set(handles.speed1,'String',sprintf('%.2f',v1))
        if v1 > 1.6
            set(handles.up1,'Visible','on')
            set(handles.down1,'Visible','off')
        elseif v1 < 1.6
            set(handles.up1,'Visible','off')
            set(handles.down1,'Visible','on')
        end
        v2 = 1.6 + ksteer*leftAnalogLateral(1);
        set(handles.steer1,'Visible','on')
        set(handles.steer1,'String',sprintf('%.2f',v2))
        
        if v2 < 1.6
            set(handles.left1,'Visible','on')
            set(handles.right1,'Visible','off')
        elseif v2 > 1.6
            set(handles.left1,'Visible','off')
            set(handles.right1,'Visible','on')
        end
    end
    if playwith(2)
        b =zeros(1,5);
        try
            [b bb] = JoyMEX(1);
%         catch
%             try
%                 clear JoyMex
%                 JoyMEX('init',0);
%                 JoyMEX('init',1);
%                 JoyMEX('init',2);
%                 JoyMEX('init',3);
%             end
        end
        leftAnalogLateral(2) = b(1);
        rightAnalogLongitudinal(2) = -b(5);
        v3 = 1.7 + kspeed*rightAnalogLongitudinal(2);
        set(handles.speed2,'Visible','on')
        set(handles.speed2,'String',sprintf('%.2f',v3))
        if v3 > 1.6
            set(handles.up2,'Visible','on')
            set(handles.down2,'Visible','off')
        elseif v3 < 1.6
            set(handles.up2,'Visible','off')
            set(handles.down2,'Visible','on')
        end
        v4 = 1.6 + ksteer*leftAnalogLateral(2);
        set(handles.steer2,'Visible','on')
        set(handles.steer2,'String',sprintf('%.2f',v4))
        if v4 < 1.6
            set(handles.left2,'Visible','on')
            set(handles.right2,'Visible','off')
        elseif v4 > 1.6
            set(handles.left2,'Visible','off')
            set(handles.right2,'Visible','on')
        end
    end
    if playwith(3)
        try
            [c cb] = JoyMEX(2);
        catch
%             try
%                 clear JoyMex
%                 JoyMEX('init',0);
%                 JoyMEX('init',1);
%                 JoyMEX('init',2);
%                 JoyMEX('init',3);
%             end
        end
        leftAnalogLateral(3) = c(1);
        rightAnalogLongitudinal(3) = -c(5);
        v5 = 1.6 + kspeed*rightAnalogLongitudinal(3);
        set(handles.speed3,'Visible','on')
        set(handles.speed3,'String',sprintf('%.2f',v5))
        
        if v5 > 1.6
            set(handles.up3,'Visible','on')
            set(handles.down3,'Visible','off')
        elseif v5 < 1.6
            set(handles.up3,'Visible','off')
            set(handles.down3,'Visible','on')
        end
        v6 = 1.6 + ksteer*leftAnalogLateral(3);
        set(handles.steer3,'Visible','on')
        set(handles.steer3,'String',sprintf('%.2f',v6))
        if v6 < 1.6
            set(handles.left3,'Visible','on')
            set(handles.right3,'Visible','off')
        elseif v6 > 1.6
            set(handles.left3,'Visible','off')
            set(handles.right3,'Visible','on')
        end
    end
    if playwith(4)
        try
            [d db] = JoyMEX(3);
        catch
%             try
%                 clear JoyMex
%                 JoyMEX('init',0);
%                 JoyMEX('init',1);
%                 JoyMEX('init',2);
%                 JoyMEX('init',3);
%             end
        end
        
        
        leftAnalogLateral(4) = d(1);
        v7 = 1.6;
        v8 = 1.6;
        if db(5)
            v7 = 3.2;
        elseif db(6)
            v7 = 0;
        end
        set(handles.speed4,'Visible','on')
        set(handles.speed4,'String',sprintf('%.2f',v7))
        if v7 > 1.6
            set(handles.up4,'Visible','on')
            set(handles.down4,'Visible','off')
        elseif v7 < 1.6
            set(handles.up4,'Visible','off')
            set(handles.down4,'Visible','on')
        end
        v8 = 1.6 + ksteer*leftAnalogLateral(4);
        set(handles.steer4,'Visible','on')
        set(handles.steer4,'String',sprintf('%.2f',v8))
        if v8 < 1.6
            set(handles.left4,'Visible','on')
            set(handles.right4,'Visible','off')
        elseif v8 > 1.6
            set(handles.left4,'Visible','off')
            set(handles.right4,'Visible','on')
        end
    end
    
    NI_voltage_output2(s,v1,v2,v3,v4,v5,v6,v7,v8);
    
    
    [v1,v2,v3,v4,v5,v6,v7,v8]
    
    
    pause(0.05)
    
end
disp ('exited')
stop = false;
release(s);