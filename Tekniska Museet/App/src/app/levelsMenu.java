/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import static app.Globals.getButtonRect;
import static app.Globals.getButtonRect;
import static app.Globals.setText;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;

/**
 *
 * @author Matteo
 */
class levelsMenu implements ControlledScreen
{
    
    private PageSwitcher myController;
    private Pane pane;
    
    public levelsMenu()
    {
        pane = setPane();
    }
    
    private Pane setPane()
    {
        Pane p = new Pane();
        Rectangle bg = new Rectangle(Globals.screenWidth, Globals.screenHeight, Color.BLACK);
        
        Text mainTitle = setMainTitle("LEVELS");
        
        double vOff = 100;
        
        VBox vbLeft = new VBox();
        vbLeft.setSpacing(vOff);
        vbLeft.setLayoutX(Globals.screenWidth/2-Globals.buttonWidth*3/2);
        vbLeft.setLayoutY(mainTitle.getBoundsInParent().getMaxY()+vOff);
        
        VBox vbRight= new VBox();
        vbRight.setSpacing(vOff);
        vbRight.setLayoutX(Globals.screenWidth/2+Globals.buttonWidth/2);
        vbRight.setLayoutY(mainTitle.getBoundsInParent().getMaxY()+vOff);
        
        Rectangle tutorial = getButtonRect();
        Rectangle l1 = getButtonRect();
        Rectangle l2 = getButtonRect();
        Rectangle l3 = getButtonRect();
        Rectangle l4 = getButtonRect();
        Rectangle l5 = getButtonRect();
        
//        Rectangle tutorial = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*5/10);
//        Rectangle l1 = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*3/10);
//        Rectangle l2 = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*5/10);
//        Rectangle l3 = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*7/10);
//        Rectangle l4 = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*7/10);
//        Rectangle l5 = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*7/10);
        
        Text tutorialText = setText("LEVEL 1", tutorial);
        Text l1Text = setText("LEVEL 2", l1);
        Text l2Text = setText("LEVEL 3", l2);
        Text l3Text = setText("LEVEL 4", l3);
        Text l4Text = setText("LEVEL 5", l4);
        Text l5Text = setText("LEVEL 6", l5);
        
        Group tutorialGroup = new Group(tutorial,tutorialText);
        Group l1Group = new Group(l1,l1Text);
        Group l2Group = new Group(l2,l2Text);
        Group l3Group = new Group(l3,l3Text);
        Group l4Group = new Group(l4,l4Text);
        Group l5Group = new Group(l5,l5Text);
        
        vbLeft.getChildren().addAll(tutorialGroup,l1Group,l2Group);
        vbRight.getChildren().addAll(l3Group,l4Group,l5Group);
        
        tutorialGroup.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("tutorialLevel");
        });
        
        l1Group.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("level1");
        });
        
        l2Group.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("level2");
        });
        
        l3Group.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("level3");
        });
        
        l4Group.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("level4");
        });
        
        l5Group.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("level5");
        });
        
        Rectangle home = getButtonRect((Globals.screenWidth-Globals.buttonWidth)/2,Globals.screenHeight*8/10);
        Text homeText = setText("HOME", home);
        Group homeButton = new Group(home,homeText);
        homeButton.setOnMousePressed((me)->
        {
            myController.setPage("videoMenu");
        });
        
        p.getChildren().addAll(bg);
        p.getChildren().addAll(mainTitle);
        p.getChildren().addAll(vbLeft,vbRight);
        p.getChildren().addAll(homeButton);
        
        return p;
        
    }
    
    @Override
    public Pane getPane()
    {
        return pane;
    }

    @Override
    public void setScreenParent(PageSwitcher screenParent) {
        myController = screenParent;
    }
    
    @Override
    public PageSwitcher getController()
    {
        return myController;
    }
    
    private Text setMainTitle(String s)
    {
        Text mainTitle = new Text(s);
        mainTitle.setFill(Globals.textColor);
        mainTitle.setFont(new Font("Calibri",50));
        double titleWidth = mainTitle.getBoundsInLocal().getWidth();
        double titleHeight = mainTitle.getBoundsInLocal().getHeight();
        mainTitle.setLayoutX((Globals.screenWidth-titleWidth)/2);
        mainTitle.setLayoutY(Globals.screenHeight/10);
        
        return mainTitle;
    }
    
}
