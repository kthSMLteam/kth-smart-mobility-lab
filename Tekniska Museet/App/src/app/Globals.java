/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;

/**
 *
 * @author Matteo
 */
public class Globals {
    
    public static double fontStretch = 1.25;
    public static double horizontalStretch = 1.527;
//    public static double verticalStretch = 1.165;
    public static double verticalStretch = 1.05;
    public static double screenWidth = 1920;
    public static double screenHeight = 1080;
    
    
    // Game map dimensions
    public static int numberBlocksX = 10;
    public static int numberBlocksY = 10;
    
    public static double blockWidth = 100;
    public static double blockHeight = 100;
    
    static final double buttonWidth = screenWidth/5;
    static final double buttonHeight = screenHeight/10;
    
    static final Color textColor = new Color(53/255.,240/255.,217/255.,1.0);
    static final Color buttonColor = new Color(35/255.,53/255.,61/255.,1.0);
    static final int timeOutSeconds = 112;
    
    static boolean isSwedishTheLanguage = true;
    
    public static Rectangle getButtonRect(double rectX, double rectY)
    {
        Rectangle b = new Rectangle(rectX,rectY,Globals.buttonWidth,Globals.buttonHeight);
        b.setFill(Globals.buttonColor);
        b.setStrokeWidth(5);
        b.setStroke(Color.WHITE);
        b.setArcHeight(40);
        b.setArcWidth(40);
        
        return b;
    }
    
    public static Rectangle getButtonRect(double width)
    {
        Rectangle b = new Rectangle();
        b.setWidth(width);
        b.setHeight(buttonHeight);
        b.setFill(Globals.buttonColor);
        b.setStrokeWidth(5);
        b.setStroke(Color.WHITE);
        b.setArcHeight(40);
        b.setArcWidth(40);
        
        return b;
    }
    
    public static Rectangle getButtonRect()
    {
        Rectangle b = new Rectangle();
        b.setWidth(Globals.buttonWidth);
        b.setHeight(Globals.buttonHeight);
        b.setFill(Globals.buttonColor);
        b.setStrokeWidth(5);
        b.setStroke(Color.WHITE);
        b.setArcHeight(40);
        b.setArcWidth(40);
        
        return b;
    }
    
    public static Text setText(String s, Rectangle r)
    {
        Text t = new Text();
        t.setText(s);
        t.setFill(Globals.textColor);
        t.setFont(new Font("Calibri",50));
        t.setTextAlignment(TextAlignment.CENTER);
        t.setBoundsType(TextBoundsType.VISUAL);
        t.setLayoutX(r.getX()+r.getBoundsInLocal().getWidth()/2-t.getBoundsInLocal().getWidth()/2);
        t.setLayoutY(r.getY()+r.getBoundsInLocal().getHeight()/2+t.getBoundsInLocal().getHeight()/2);

        return t;
    }
    
}
