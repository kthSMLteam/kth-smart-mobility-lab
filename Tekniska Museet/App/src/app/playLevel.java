/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import static app.Globals.getButtonRect;
import static app.Globals.getButtonRect;
import static app.Globals.getButtonRect;
import static app.Globals.setText;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 *
 * @author Matteo
 */
class playLevel implements ControlledScreen {
    
    private PageSwitcher myController;
    private Pane pane;
    private int levelNum;

    public playLevel(int argLevNum)
    {
        levelNum = argLevNum;
        pane = setLevelPane(levelNum);
    }

    @Override
    public void setScreenParent(PageSwitcher screenParent) {
        myController = screenParent;
    }

    @Override
    public PageSwitcher getController() {
        return myController;
    }

    @Override
    public Pane getPane() {
        return pane;
    }

    public Pane setLevelPane(int levelNum)
    {
        Levels currentLevel = new Levels();
        
        Group level;
        
        if ( levelNum == 0 ){
        
            level = currentLevel.getTutorialLevel();
            
        }else{
            
            level = currentLevel.getLevel(levelNum);
                    
        }
        
        double levW = level.getBoundsInLocal().getWidth();
        double levH = level.getBoundsInLocal().getHeight();
        
//        double leftOffset = (Globals.screenWidth/2.0-levW/2.0-Globals.buttonWidth)/2;
//        double vertOffset = Globals.screenHeight/10.0*8.0; //Globals.screenHeight/2-Globals.buttonHeight/2
        
        Rectangle mainHome = getButtonRect();
        Text mainHomeText = setText("HOME", mainHome);
        Group mainHomeButton = new Group(mainHome,mainHomeText);
        mainHomeButton.setOnMousePressed((me)->
        {
            currentLevel.resetLevel();
            myController.setPage("videoMenu");
        });
        
        Rectangle b = getButtonRect();
        Text t = setText("LEVELS MENU",b);
        
        Group home = new Group(b,t);
        home.setOnMousePressed((MouseEvent me)->{
            currentLevel.resetLevel();
            myController.setPage("levelsMenu");
        });
                
        b = getButtonRect();
        t = setText("RESTART LEVEL",b);
                
        double navButtonsWidth = 150;
        
        ImageView restartImageView = new ImageView(new Image("file:repeat1Black.png"));
        restartImageView.setPreserveRatio(true);
        restartImageView.setFitWidth(navButtonsWidth);
        Group restart = new Group(restartImageView);
//        Group restart = new Group(b,t);
        
        String thisLevel = "level"+levelNum;
        restart.setOnMousePressed((MouseEvent me)->{
            currentLevel.resetLevel();
//            myController.setPage(thisLevel);
        });
        
        b = getButtonRect();
        t = setText("NEXT LEVEL",b);
        
        boolean nextExists = true;
        ImageView nextImageView = new ImageView(new Image("file:next1Black.png"));
        nextImageView.setPreserveRatio(true);
        nextImageView.setFitWidth(navButtonsWidth);
        Group next = new Group(nextImageView);
//        Group next = new Group(b,t);
        
        b = getButtonRect();
        t = setText("PLAY",b);
        
        ImageView playImageView = new ImageView(new Image("file:play1Black.png"));
        playImageView.setPreserveRatio(true);
        playImageView.setFitWidth(navButtonsWidth);
        Group play = new Group(playImageView);
        play.setOnMousePressed((MouseEvent me)->{
            currentLevel.playButtonPressedLevel();
//            myController.setPage(thisLevel);
        });
//        Group play = new Group(b,t);
        

        
        String nextLevel = "level"+(levelNum+1);
        
//        ImageView backImageView = new ImageView(new Image("file:back.png"));
//        backImageView.setPreserveRatio(true);
//        backImageView.setFitWidth(150);
//        Group back = new Group(backImageView);
        
//        back.setLayoutX(1920*0.8);
        
        b = getButtonRect();
        t = setText("PREVIOUS LEVEL",b);
        
        ImageView backImageView = new ImageView(new Image("file:back1Black.png"));
        backImageView.setPreserveRatio(true);
        backImageView.setFitWidth(navButtonsWidth);
        Group back = new Group(backImageView);
//        Group back = new Group(b,t);
        
        
        String backLevel = "level"+(levelNum-1);
        
//        Pane lp = new Pane(level);
//        lp.setLayoutX((Globals.screenWidth-Globals.buttonWidth-levW)/3.0);
//        lp.setLayoutY((Globals.screenHeight-levH)/2.0);
        
        double layoutOffsetX = (Globals.screenWidth-Globals.buttonWidth-levW)/3.0;
        double layoutOffsetY = (Globals.screenHeight-levH)/2.0;
        
        level.setLayoutX(layoutOffsetX);
        level.setLayoutY(layoutOffsetY);
        
        currentLevel.setLayoutOffsets(layoutOffsetX, layoutOffsetY);
        
//        VBox vb = new VBox(100);
//        vb.setPadding(Insets.EMPTY);
        Group vb = new Group();
        
        vb.getChildren().addAll(mainHomeButton,home,restart,play);
        
        mainHomeButton.setLayoutY(1080./5.);
        home.setLayoutY((2.*1080.)/5.);
        
        double navButtonsY = ((3.*1080.)/5.);
        double navButtonsX = navButtonsWidth;
        
        
        back.setLayoutX(-2*navButtonsX + Globals.buttonWidth/2.);
        back.setLayoutY(navButtonsY);
        
        restart.setLayoutX(-1*navButtonsX + Globals.buttonWidth/2.);
        restart.setLayoutY(navButtonsY);
        
        play.setLayoutX(0*navButtonsX + Globals.buttonWidth/2.);
        play.setLayoutY(navButtonsY);
        
        next.setLayoutX(1*navButtonsX + Globals.buttonWidth/2.);
        next.setLayoutY(navButtonsY);
        
        if (levelNum < 5)
        {
            vb.getChildren().add(next);
//            vb.setLayoutY(Globals.screenHeight/2.0-Globals.buttonHeight*2-150);
//            vb.setLayoutY(Globals.screenHeight/2.0);
            next.setOnMousePressed((MouseEvent me)->{
                currentLevel.resetLevel();
                myController.setPage(nextLevel);
            });
        }
        else
        {
//            vb.setLayoutY(Globals.screenHeight/2.0-Globals.buttonHeight*1.5-100);
        }
        
        
        System.out.println("-------------------------levelNum = " + levelNum);
        
        if (levelNum > 1)
        {
            System.out.println("-------------------------Adding Back Group");
            
//            back.setLayoutX(100);
//            back.toFront();
            vb.getChildren().add(back);
//            back.setLayoutX(100);
//            back.toFront();
//            vb.setLayoutY(Globals.screenHeight/2.0-Globals.buttonHeight*2-150);
//            vb.setLayoutY(Globals.screenHeight/2.0);
            
            back.setOnMousePressed((MouseEvent me)->{
                currentLevel.resetLevel();
                myController.setPage(backLevel);
            });
        }
        else
        {
//            vb.setLayoutY(Globals.screenHeight/2.0-Globals.buttonHeight*1.5-100);
        }
        
        vb.setLayoutX( Globals.screenWidth*(2.8/4.) );
//        vb.setLayoutY(Globals.screenHeight/2.0);
                
//        vb.setLayoutX(Globals.screenWidth-Globals.buttonWidth-(Globals.screenWidth-Globals.buttonWidth-levW)/3.0);
                
        Rectangle backgroundRect = new Rectangle(Globals.screenWidth, Globals.screenHeight, Color.BLACK);
        
        Pane p = new Pane();
        p.getChildren().addAll(backgroundRect,level,vb);
        
//        vb.toFront();
        
        return p;
    }
    
    
}
