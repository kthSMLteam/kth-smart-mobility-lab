/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import static net.java.games.input.Controller.Type.WHEEL;
import static net.java.games.input.Controller.Type.GAMEPAD;

/**
 *
 * @author rui
 */
public class UsingTheWheel extends Application {
    
    Group root;
    
    
    
        
    @Override
    public void start(Stage primaryStage) {
                
        root = new Group();
        
        Scene scene = new Scene(root, 1920, 1080);
        
        WheelGameManager wheelGameManager = new WheelGameManager();
        
        Button startButton = new Button("START");
        
        
        
        startButton.setLayoutX(100);
        startButton.setLayoutY(500);
        
        Button restartButton = new Button("RESTART");
        
        
        
        restartButton.setLayoutX(100);
        restartButton.setLayoutY(600);
        
        Button stopButton = new Button("STOP");
        
        stopButton.setLayoutX(100);
        stopButton.setLayoutY(700);
        
        startButton.setOnAction((event) -> {
            // Button was clicked, do something...
//            outputTextArea.appendText("Button Action\n");
            
            
            
            
            if ( root.lookup("#wheelGameScreen") == null ){
                
                System.out.println("Game not added yet.");
                
                Group wheelGameGroup = wheelGameManager.createGameWindow();
                wheelGameGroup.setId("wheelGameScreen");
                
                root.getChildren().add(wheelGameGroup);
            
            }else{
            
                System.out.println("Game already added, do nothing.");
            
            }
            
            startButton.toFront();
            restartButton.toFront();
            stopButton.toFront();
            
        });
        
        restartButton.setOnAction((event) -> {
            // Button was clicked, do something...
//            outputTextArea.appendText("Button Action\n");
            
            wheelGameManager.restartGame();
            
            startButton.toFront();
            restartButton.toFront();
            stopButton.toFront();
            
        });
        
        stopButton.setOnAction((event) -> {
            // Button was clicked, do something...
//            outputTextArea.appendText("Button Action\n");
            
            wheelGameManager.stopGame();
            
            startButton.toFront();
            restartButton.toFront();
            stopButton.toFront();
            
        });
        
        
        
        
        
        root.getChildren().addAll(startButton, restartButton, stopButton);
        
        primaryStage.setTitle("Using the Wheel...");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
