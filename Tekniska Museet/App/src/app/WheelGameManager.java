/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import static net.java.games.input.Controller.Type.GAMEPAD;
import static net.java.games.input.Controller.Type.WHEEL;

/**
 *
 * @author rui
 */
public class WheelGameManager {
    
    Group wheelGameRoot;
    
    Group leadTruckGroup;
    Group myTruckGroup;
    
    Group backgroundGroup;
    
    Group treesGroup;
    
    Group carsLane0Group, carsLane1Group, carsLane2Group;
    
    double screenWidth = GlobalsUsingTheWheel.screenWidth;
    double screenHeight = GlobalsUsingTheWheel.screenHeight;
    
    int truckLane = 0;
    int myTruckLane = 0;
    int lastLeadTruckLane = 0;
    
    long startTime;
//    long endTime = System.nanoTime();
    long lastTime;
    
    boolean gameStopped = false;
    
    ParallelTransition ptMyTruck, ptLeadTruck;
    
    TreeManager treeManager;
    TrafficManager trafficManager;
    ScoreManager scoreManager;
    
    Random randomGenerator;
    
    double randomWait;
    
    double realisticWheelAngle = 0;
    
    JInputJoystickTest inputWheel;
    
    double myTruckLaneDouble, leadTruckLaneDouble;
    
    public WheelGameManager(){
        
        wheelGameRoot = new Group();
        
        treeManager = new TreeManager();
        trafficManager = new TrafficManager();
        scoreManager = new ScoreManager();
        randomGenerator = new Random();
//        randomWait = randomGenerator.nextFloat()*500;
        randomWait = trafficManager.getWait();
        
        startTime = System.nanoTime();
        lastTime = startTime;
        
        inputWheel = new JInputJoystickTest();
//        inputWheel.pollControllerAndItsComponents(WHEEL);
//        inputWheel.pollControllerAndItsComponents(GAMEPAD);
        

        
        
        
    }
    
    

    
    public Group createGameWindow(){
        
        addBackground();
//        root.getChildren().add(backgroundGroup);
        
        createLeadTruck();
//        root.getChildren().add(leadTruckGroup);
        
        moveLeadTruck(truckLane);
        
        createMyTruck();
//        root.getChildren().add(myTruckGroup);
        
        moveMyTruck(myTruckLane);
        
        treesGroup = new Group();
        carsLane0Group = new Group();
        carsLane1Group = new Group();
        carsLane2Group = new Group();
        
        Group fuelBarGroup = scoreManager.createFuelGauge();
        
        Group scoreBoardGroup = scoreManager.createScoreBoard();
        
        wheelGameRoot.getChildren().addAll(backgroundGroup, treesGroup, carsLane0Group, carsLane1Group, carsLane2Group, leadTruckGroup, myTruckGroup, scoreBoardGroup, fuelBarGroup);
        
//        root.getChildren().add( treeManager.getTree() );
        
//        root.getChildren().add( treeManager.getRoadSign() );
        
        setUpRealTimeInterpolator();
//        
        return wheelGameRoot;
        
    }
    
    public void restartGame(){
        

        gameStopped = false;
        setUpRealTimeInterpolator();
        
        
    }
    
    public void stopGame(){
        
        ptMyTruck.stop();
        ptLeadTruck.stop();
        
        gameStopped = true;
        
        carsLane0Group.getChildren().clear();
        carsLane1Group.getChildren().clear();
        carsLane2Group.getChildren().clear();
        treesGroup.getChildren().clear();
        
    }
    
    private double getCurrentTimeSeconds(){
        
        double currentTimeSeconds;
        
        currentTimeSeconds = ( System.nanoTime() - startTime  )/1000000000.;
        
        return currentTimeSeconds;
        
    }
    
    private void addBackground(){
        
        backgroundGroup = new Group();
        
//        Image backgroundImage = new Image("file:background2.png");
//        Image backgroundImage = new Image("file:backgroundRealistic.png");
//        Image backgroundImage = new Image("file:backgroundOnlyRoad.png");
        Image backgroundImage = new Image("file:backgroundOnlyRoad2.png");
        double backgroundImageWidth = backgroundImage.getWidth();
        double backgroundImageHeight = backgroundImage.getHeight();
        ImageView backgroundImageView = new ImageView(backgroundImage);
        
        backgroundImageView.setPreserveRatio(false);
        backgroundImageView.setFitWidth(screenWidth);
        backgroundImageView.setFitHeight(screenHeight*(1.1/2.));
        backgroundImageView.setLayoutY( screenHeight-backgroundImageView.getBoundsInLocal().getHeight() );
//        
//        backgroundGroup.getChildren().add(backgroundImageView);
                
        Image treeLineImage = new Image("file:treeLine.png");
        ImageView treeLineImageView = new ImageView(treeLineImage);
        
        treeLineImageView.setPreserveRatio(true);
        treeLineImageView.setFitWidth(screenWidth);
        
        treeLineImageView.setLayoutY(screenHeight/2. - 235);
        
        Image mountainsImage = new Image("file:mountains.png");
        ImageView mountainsImageView = new ImageView(mountainsImage);
        
        mountainsImageView.setPreserveRatio(true);
        mountainsImageView.setFitWidth(screenWidth);
        
        TranslateTransition tt = new TranslateTransition(Duration.millis(60*1000), mountainsImageView);
        tt.setByY(-300);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);

        tt.play();
        
//        ImageView.setLayoutY(screenHeight/2.);
        
        backgroundGroup.getChildren().addAll(mountainsImageView, backgroundImageView, treeLineImageView);
        
    }
    
    private void createMyTruck(){
        
        myTruckGroup = new Group();
        
        Image myTruckImage = new Image("file:myTruck.png");
        ImageView myTruckImageView = new ImageView(myTruckImage);
        
        myTruckImageView.setPreserveRatio(true);
        myTruckImageView.setFitWidth(500);
        
        
        myTruckGroup.getChildren().add(myTruckImageView);
        
        myTruckGroup.setOnMousePressed((MouseEvent me) -> {
            
            if (me.isPrimaryButtonDown()) {
                myTruckLane--;
                if (myTruckLane<=0){
                    myTruckLane = 0;
                }
            }
            if (me.isSecondaryButtonDown()) {
                myTruckLane++;
                if (myTruckLane>=3){
                    myTruckLane = 3;
                }
            }
            
            moveMyTruck(myTruckLane);
            
        });
        
        TranslateTransition ttRight = new TranslateTransition(Duration.millis(1700), myTruckGroup);
        ttRight.setByX(30);
        ttRight.setCycleCount(Animation.INDEFINITE);
        ttRight.setAutoReverse(true);
        
        TranslateTransition ttLeft = new TranslateTransition(Duration.millis(1300), myTruckGroup);
        ttLeft.setByX(-20);
        ttLeft.setCycleCount(Animation.INDEFINITE);
        ttLeft.setAutoReverse(true);

        SequentialTransition seqSide = new SequentialTransition (ttRight, ttLeft);

        TranslateTransition ttUp = new TranslateTransition(Duration.millis(2*1700), myTruckGroup);
        ttUp.setByY(10);
        ttUp.setCycleCount(Animation.INDEFINITE);
        ttUp.setAutoReverse(true);
        
        TranslateTransition ttDown = new TranslateTransition(Duration.millis(2*2500), myTruckGroup);
        ttDown.setByY(-6);
        ttDown.setCycleCount(Animation.INDEFINITE);
        ttDown.setAutoReverse(true);

        SequentialTransition seqUp = new SequentialTransition (ttUp, ttDown);
        
        
//        ParallelTransition pt = new ParallelTransition(rect, ft, tt, rt, st);
        ptMyTruck = new ParallelTransition(seqSide, seqUp);
        ptMyTruck.play();
        
        
        
    }
    
    private void createLeadTruck(){
        
        leadTruckGroup = new Group();
        
        Image trailerBackImage = new Image("file:trailerBack.png");
        ImageView trailerBackImageView = new ImageView(trailerBackImage);
        
        trailerBackImageView.setPreserveRatio(true);
        trailerBackImageView.setFitWidth(200);
        
        
        leadTruckGroup.getChildren().add(trailerBackImageView);
        
//        leadTruckGroup.setOnMousePressed((MouseEvent me) -> {
//            
//            truckLane++;
//            if (truckLane>=3){
//                truckLane = 0;
//            }
//            moveLeadTruck(truckLane);
//            
//        });
        
        TranslateTransition ttRight = new TranslateTransition(Duration.millis(2000), leadTruckGroup);
        ttRight.setByX(30);
        ttRight.setCycleCount(Animation.INDEFINITE);
        ttRight.setAutoReverse(true);
        
        TranslateTransition ttLeft = new TranslateTransition(Duration.millis(1700), leadTruckGroup);
        ttLeft.setByX(-20);
        ttLeft.setCycleCount(Animation.INDEFINITE);
        ttLeft.setAutoReverse(true);

        SequentialTransition seqSide = new SequentialTransition (ttRight, ttLeft);

        TranslateTransition ttUp = new TranslateTransition(Duration.millis(2*1700), leadTruckGroup);
        ttUp.setByY(10);
        ttUp.setCycleCount(Animation.INDEFINITE);
        ttUp.setAutoReverse(true);
        
        TranslateTransition ttDown = new TranslateTransition(Duration.millis(2*2500), leadTruckGroup);
        ttDown.setByY(-6);
        ttDown.setCycleCount(Animation.INDEFINITE);
        ttDown.setAutoReverse(true);

        SequentialTransition seqUp = new SequentialTransition (ttUp, ttDown);
        
        
//        ParallelTransition pt = new ParallelTransition(rect, ft, tt, rt, st);
        ptLeadTruck = new ParallelTransition(seqSide, seqUp);
        ptLeadTruck.play();
        
        double laneY = 600;
        double sideLaneDisplacement = 300;
        double laneX = screenWidth/2. - sideLaneDisplacement;
        
        double leadTruckX = laneX - leadTruckGroup.getBoundsInParent().getWidth()/2.;
        double leadTruckY = laneY - leadTruckGroup.getBoundsInParent().getHeight()/2.;
                
        leadTruckGroup.setLayoutX(leadTruckX);
        leadTruckGroup.setLayoutY(leadTruckY);
        
    }
    
    private void moveLeadTruck(int argLane){
        
        leadTruckLaneDouble = argLane;
        
        if ( lastLeadTruckLane == argLane){            
            
            return;
            
        }
        
        
        
        double leadTruckX;
        double leadTruckY;
        
        double laneX;
        double laneY;
        
        double prevLaneX;
        
        laneY = 600;
        
        double sideLaneDisplacement = 300;
        
        switch (argLane) {
            case 0:  laneX = screenWidth/2. - sideLaneDisplacement;
                     break;
            case 1:  laneX = screenWidth/2.;
                     break;
            case 2:  laneX = screenWidth/2. + sideLaneDisplacement;
                     break;
            default: laneX = 100;
                     break;
        }
        
        switch (lastLeadTruckLane) {
            case 0:  prevLaneX = screenWidth/2. - sideLaneDisplacement;
                     break;
            case 1:  prevLaneX = screenWidth/2.;
                     break;
            case 2:  prevLaneX = screenWidth/2. + sideLaneDisplacement;
                     break;
            default: prevLaneX = 100;
                     break;
        }
        
        
        leadTruckX = laneX - leadTruckGroup.getBoundsInParent().getWidth()/2.;
        leadTruckY = laneY - leadTruckGroup.getBoundsInParent().getHeight()/2.;
        
        
//        leadTruckGroup.setLayoutX(leadTruckX);
//        leadTruckGroup.setLayoutY(leadTruckY);
        
        TranslateTransition tt = new TranslateTransition(Duration.millis(500), leadTruckGroup);
        tt.setByX(laneX - prevLaneX);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        EventHandler onTranslationFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                
                leadTruckGroup.setLayoutX(leadTruckX);
                leadTruckGroup.setLayoutY(leadTruckY);
                
            }
        };
        
        tt.setOnFinished(onTranslationFinished);

        tt.play();
        
        lastLeadTruckLane = argLane;
                                
    }
    
    private void moveMyTruck(int argLane){
        
        double myTruckX;
        double myTruckY;
        
        double laneX;
        double laneY;
        
//        laneY = 600;
        
        double sideLaneDisplacement = 700;
        
        switch (argLane) {
            case 0:  laneX = screenWidth/2. - sideLaneDisplacement;
                     break;
            case 1:  laneX = screenWidth/2.;
                     break;
            case 2:  laneX = screenWidth/2. + sideLaneDisplacement;
                     break;
            default: laneX = 100;
                     break;
        }
        
        
        myTruckX = laneX - myTruckGroup.getBoundsInParent().getWidth()/2.;
        myTruckY = screenHeight - myTruckGroup.getBoundsInParent().getHeight();
        
        myTruckGroup.setLayoutX(myTruckX);
        myTruckGroup.setLayoutY(myTruckY);
        
                
    }
    
    private void moveMyTruckByWheel(double argWheelAngle, boolean realistic){
        
        double myTruckX;
        double myTruckY;
        
        double laneX;
        double laneY;
        
//        laneY = 600;
        
        double sideLaneDisplacement = 700;
        
        double wheelAngle;
        
        if (realistic){
            wheelAngle = realisticWheelAngle + argWheelAngle/30.;
            realisticWheelAngle = wheelAngle;
            
            if (realisticWheelAngle > 90){
                realisticWheelAngle = 90;
            }
            if (realisticWheelAngle < -90){
                realisticWheelAngle = -90;
            }
            
        }else{
            wheelAngle = argWheelAngle;
        }
        
        laneX = screenWidth/2. + (wheelAngle/90)*sideLaneDisplacement;
        
        myTruckX = laneX - myTruckGroup.getBoundsInParent().getWidth()/2.;
        myTruckY = screenHeight - myTruckGroup.getBoundsInParent().getHeight();
        
        myTruckGroup.setLayoutX(myTruckX);
        myTruckGroup.setLayoutY(myTruckY);
        
        myTruckLaneDouble = 1 + realisticWheelAngle/90;
                
    }
    
    
    public class TreeMovementInterpolator extends Interpolator {
    @Override
    protected double curve(double t) {
        
        timingFunction();
        return t;
    }
    
    }
    
    private void setUpRealTimeInterpolator(){
        
        Rectangle rect = new Rectangle (10, 10, 10, 10);
        rect.setFill(Color.BLACK);
        rect.setOpacity(0.);

        RotateTransition rt = new RotateTransition(Duration.millis(3000), rect);
        rt.setByAngle(180);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.setAutoReverse(false);
        
        rt.setInterpolator(new TreeMovementInterpolator() );
        
        rt.play();        
        
    }
    
    private void clearOldItems(Group argGroup){
        
        int removed = 0;
        
        for ( int i = 0 ; i < argGroup.getChildren().size() ; i++ ){
            
            Group tempItem = (Group) argGroup.getChildren().get( i - removed );
            
            if ( tempItem.getBoundsInParent().getMinY() > screenHeight){
                
                argGroup.getChildren().remove(tempItem);
                removed++;
                
            }
            
        }
        
//        System.out.println("Removed " + removed + " elements.");
                
    }
    
    private void clearLaneForLeadingTruck(int laneId){
        
        int removed = 0;
        
        Group laneGroup;
        
        if (laneId == 0){
            laneGroup = carsLane0Group;
        }else if (laneId == 1){
            laneGroup = carsLane1Group;
        }else{
            laneGroup = carsLane2Group;
        }
        
        for ( int i = 0 ; i < laneGroup.getChildren().size() ; i++ ){
            
            Group tempItem = (Group) laneGroup.getChildren().get( i - removed );
            
            if ( tempItem.getBoundsInParent().getMaxY() < leadTruckGroup.getBoundsInParent().getMaxY() &&
                    tempItem.getBoundsInParent().getMaxY() > leadTruckGroup.getBoundsInParent().getMaxY() - leadTruckGroup.getBoundsInParent().getHeight()/3. ){
                
                laneGroup.getChildren().remove(tempItem);
                removed++;
                
            }
            
        }
        
                
    }
    
    private int getPossibleLane(double argCurrentTimeSeconds){
        
        int possibleLane = -1;
        
        double minTime = 1.5;
        double maxTime = 3.2;
        
        int minIndex = (int) (minTime/GlobalsUsingTheWheel.timeStep);
        int maxIndex = (int) (maxTime/GlobalsUsingTheWheel.timeStep);
        
        int currentIndex = (int) (argCurrentTimeSeconds/GlobalsUsingTheWheel.timeStep);
        
        boolean lane0Possible = true;
        boolean lane1Possible = true;
        boolean lane2Possible = true;
        
        for ( int i = currentIndex + minIndex ; i < currentIndex + maxIndex ; i++ ){
            
            switch (GlobalsUsingTheWheel.laneHistory[i]) {
                
            case 0:  lane0Possible = false;
                     break;
            case 1:  lane1Possible = false;
                     break;
            case 2:  lane2Possible = false;
                     break;
            default: System.out.println("SHOULD NOT HAPPEN");
                     break;
                
            }
            
        }
        
        if ( lane0Possible && lane1Possible && lane2Possible ){
            double randomValue = randomGenerator.nextFloat();
            
            if ( randomValue <0.33){
                return 0;
            }else if ( randomValue <0.66){
                return 1;
            }else{
                return 2;                
            }
            
        }
        
        boolean randomBool = randomGenerator.nextBoolean();            
        
        if ( lane0Possible && lane1Possible){
            if ( randomBool ){
                return 0;
            }else{
                return 1;                
            }
        }
        if ( lane0Possible && lane2Possible){
            if ( randomBool ){
                return 0;
            }else{
                return 2;                
            }
        }
        if ( lane1Possible && lane2Possible){
            if ( randomBool ){
                return 1;
            }else{
                return 2;                
            }
        }
        
        
        return possibleLane;
        
    }
        
    
    
    private void timingFunction(){
        
        if (gameStopped){
            return;
        }
        
        if( scoreManager.getCurrentFuel() < 0. ){
            
            crashDetected();
            stopGame();
            
        }
        
        double nowTime = System.nanoTime();
        double passedTime = (nowTime - startTime)/1000000000.;  //divide by 1000000 for milliseconds.
        
        
        double wheelValue;
        if (GlobalsUsingTheWheel.isWheel){
            wheelValue = inputWheel.RuiPollControllerAndItsComponents(WHEEL);
        }else{
            wheelValue = inputWheel.RuiPollControllerAndItsComponents(GAMEPAD);
        }
        
        boolean realistic = true;
        
        moveMyTruckByWheel(wheelValue*90, realistic);
        
        scoreManager.updateFuel(myTruckLaneDouble, leadTruckLaneDouble);
        
        
        
//        double passedTimeSeconds = (double) passedTime;
        
//        System.out.println("passedTime = " + passedTime);
        
        
//        int leadTruckLane = trafficManager.getLeadTruckLane();
        
        int leadTruckLane = GlobalsUsingTheWheel.laneHistory[ (int) (passedTime/GlobalsUsingTheWheel.timeStep) ];
        
        moveLeadTruck(leadTruckLane);
        
        trafficManager.setDifficulty( getCurrentTimeSeconds() );
        
        int maxElements = 20;
        
        if ( treesGroup.getChildren().size() > maxElements ){
//            System.out.println("------- TREES");
            clearOldItems(treesGroup);  
        }
        
        if ( carsLane0Group.getChildren().size() > maxElements ){
//            System.out.println("------- LANE 0");
            clearOldItems(carsLane0Group);
        }
        if ( carsLane1Group.getChildren().size() > maxElements ){
//            System.out.println("------- LANE 1");
            clearOldItems(carsLane1Group);
        }
        if ( carsLane2Group.getChildren().size() > maxElements ){
//            System.out.println("------- LANE 2");
            clearOldItems(carsLane2Group);
        }
        
//        clearLaneForLeadingTruck(leadTruckLane);
        
        if ( trafficManager.checkCollision(myTruckGroup, carsLane0Group) ||
                trafficManager.checkCollision(myTruckGroup, carsLane1Group) ||
                trafficManager.checkCollision(myTruckGroup, carsLane2Group)){
            
            crashDetected();
            stopGame();
            
        }
        
        
        long currentTime = System.nanoTime();
        
        scoreManager.updateScoreBoard(currentTime);
        
        long duration = (currentTime - lastTime)/1000000;  //divide by 1000000 for milliseconds.
        
        
        
        if ( duration > randomWait){
            
            Group tempTreeGroup;
            
            if ( randomWait < 10 && randomWait > 5 ){
            
                tempTreeGroup = treeManager.getRoadSign();
                
            }else if(randomWait < 5){
            
                tempTreeGroup = treeManager.getMoose();
            
            }else{
            
                tempTreeGroup = treeManager.getTree(randomGenerator.nextBoolean(), randomGenerator.nextBoolean());
                
                
            }
            
//            System.out.println("-------------");
//            System.out.println("duration = " + duration);
//            System.out.println("trafficManager.getCarGenerationProbability() = " + trafficManager.getCarGenerationProbability());
            
            if ( randomGenerator.nextFloat() < trafficManager.getCarGenerationProbability() ){
            
            
                
                Group carTreeGroup;
                
//                int tempTruckLane = (int) (2.*(randomGenerator.nextFloat()) +.5 ) ;
                
                int tempTruckLane = getPossibleLane(passedTime);
                
                if (tempTruckLane == -1){
                    
                }else{
                
                    carTreeGroup = trafficManager.getCar(tempTruckLane, true);

                    if ( tempTruckLane == 0 ){
                        carsLane0Group.getChildren().add( carTreeGroup );  
                    }
                    if ( tempTruckLane == 1 ){
                        carsLane1Group.getChildren().add( carTreeGroup );  
                    }
                    if ( tempTruckLane == 2 ){
                        carsLane2Group.getChildren().add( carTreeGroup );  
                    }

                    carTreeGroup.toBack();
                
                }
                
            }
            
            treesGroup.getChildren().add( tempTreeGroup );         
            tempTreeGroup.toBack();
            
            lastTime= currentTime;
            
            randomWait = trafficManager.getWait();
            
        }
        
        
    }
    
    
    private void crashDetected(){
        
        Group gameOverGroup = new Group();
        
        carsLane0Group.getChildren().clear();
        carsLane1Group.getChildren().clear();
        carsLane2Group.getChildren().clear();
        
        double boxWidth = screenWidth*(3./4.);
        double boxHeight = screenHeight*(3./4.);
        
        Rectangle rectangleGameOver = new Rectangle(boxWidth, boxHeight);
        
        rectangleGameOver.setFill(Color.BLUE);
        
        rectangleGameOver.setLayoutX(screenWidth/2. - boxWidth/2.);
        rectangleGameOver.setLayoutY(screenHeight/2. - boxHeight/2.);
        
        Text gameOverText = new Text();
        gameOverText.setFont(new Font("OCR A Extended", 100));
        
        gameOverText.setText("YOU CRASHED\n\n GAME OVER \n\n");
        
        gameOverText.setLayoutX(screenWidth/2 - gameOverText.getBoundsInLocal().getWidth()/2.);
        gameOverText.setLayoutY(screenHeight*(1./3.));
        
        Text yourScoreText = new Text();
        yourScoreText.setFont(new Font("OCR A Extended", 100));
        
        yourScoreText.setText("YOUR SCORE: 1000 METERS");
        
        yourScoreText.setLayoutX(screenWidth/2 - yourScoreText.getBoundsInLocal().getWidth()/2.);
        yourScoreText.setLayoutY(screenHeight*(2./3.));
        
        
        gameOverGroup.getChildren().addAll(rectangleGameOver, gameOverText, yourScoreText);
        
        wheelGameRoot.getChildren().add(gameOverGroup);
        
    }
    
    
    
    
    
    
    
    
}
