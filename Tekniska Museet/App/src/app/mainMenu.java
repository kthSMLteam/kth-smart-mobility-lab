/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import static app.Globals.getButtonRect;
import static app.Globals.setText;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;

/**
 *
 * @author Matteo
 */
class mainMenu implements ControlledScreen
{
    
    private PageSwitcher myController;
    private Pane pane;
    
    public mainMenu()
    {
        pane = setPane();
    }
    
    @Override
    public Pane getPane()
    {
        return pane;
    }
    
    private Pane setPane()
    {
        Pane p = new Pane();
        Rectangle bg = new Rectangle(Globals.screenWidth, Globals.screenHeight, Color.BLACK);
        
        Text mainTitle = setMainTitle("PLATOONING EXPRESS");
        
        Rectangle play = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*3/10);
//        Rectangle tutorial = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*5/10);
        Rectangle video = getButtonRect(Globals.screenWidth/2-Globals.buttonWidth/2,Globals.screenHeight*5/10);
        
        Text playText = setText("PLAY", play);
        //Text tutorialText = setText("TUTORIAL", tutorial);
        Text videoText = setText("VIDEO", video);
        
        Group top = new Group(play,playText);
        //Group middle = new Group(tutorial,tutorialText);
        Group bottom = new Group(video,videoText);
        
        top.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("levelsMenu");
        });
        
        bottom.setOnMousePressed((MouseEvent me) ->
        {
            myController.setPage("videoMenu");
        });
        
        p.getChildren().addAll(bg);
        p.getChildren().addAll(mainTitle);
        p.getChildren().addAll(top,bottom);
        
        return p;
    }

    @Override
    public void setScreenParent(PageSwitcher screenParent) {
        myController = screenParent;
    }
    
    @Override
    public PageSwitcher getController()
    {
        return myController;
    }
    
        private Text setMainTitle(String s)
    {
        Text mainTitle = new Text(s);
        mainTitle.setFill(Globals.textColor);
        mainTitle.setFont(new Font("Calibri",50));
        double titleWidth = mainTitle.getBoundsInLocal().getWidth();
        double titleHeight = mainTitle.getBoundsInLocal().getHeight();
        mainTitle.setLayoutX((Globals.screenWidth-titleWidth)/2);
        mainTitle.setLayoutY(Globals.screenHeight/10);
        
        return mainTitle;
    }
    
}
