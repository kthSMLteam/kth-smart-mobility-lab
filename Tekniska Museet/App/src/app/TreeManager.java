/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class TreeManager {
    
    double screenWidth = GlobalsUsingTheWheel.screenWidth;
    double screenHeight = GlobalsUsingTheWheel.screenHeight;
    String treePath1, treePath2, roadSignPath, bannerPath, moosePath;
    
    double initialTreeWidth = screenWidth/38.4;
    double finalTreeWidth = screenWidth/3.2;
    double treeMovementTimeSeconds = 5;
    
    public TreeManager(){
        
        screenWidth = 1920;
        screenHeight = 1080;
        treePath1 = "file:tree1Small.png";
        treePath2 = "file:tree2Small.png";
        roadSignPath = "file:roadSign.png";
        bannerPath = "file:banner.png";
        moosePath = "file:moose.png";
        
    }
    
    public class TreeMovementInterpolator extends Interpolator {
    @Override
        protected double curve(double t) {

            double speedOfChange;
    //        System.out.println("t = " + t);
            speedOfChange = Math.pow(t,3.5);
    //        System.out.println("speedOfChange = " + speedOfChange);
            return speedOfChange;
    //        return Math.abs(0.5-t)*2 ;
        }
    }
    
    public Group getTree(boolean left, boolean treeType){
        
        Group treeGroup = new Group();
        
        Image treeImage;
        
        if( treeType ){
            treeImage = new Image(treePath1);
        }else{
            treeImage = new Image(treePath2);
        }
        
        ImageView treeImageView = new ImageView(treeImage);
        
        treeImageView.setPreserveRatio(true);
        treeImageView.setFitWidth(initialTreeWidth);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX;
        double treeEndX;
        
        double treeStartY;
        double treeEndY;
        
        if (left){
            
            treeStartX = screenWidth/2. - 200 - initialTreeWidth/2.;
            treeEndX = -screenWidth*0.8;

            treeStartY = 450;
            treeEndY = screenHeight*1.5;
            
        }else{
            
            treeStartX = screenWidth/2. + 200 - initialTreeWidth/2.;
            treeEndX = screenWidth*1.8;

            treeStartY = 450;
            treeEndY = screenHeight*1.5;
            
        }
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(treeImageView, tt,  st);
        
        
        
        pt.play();
        
        treeGroup.getChildren().add(treeImageView);
        
        return treeGroup;
        
    }
    
    public Group getRoadSign(){
        
        Group treeGroup = new Group();
        
        Image treeImage = new Image(roadSignPath);
        ImageView treeImageView = new ImageView(treeImage);
        
        treeImageView.setPreserveRatio(true);
        treeImageView.setFitWidth(initialTreeWidth*0.3);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX = screenWidth/2 + 150;
        double treeEndX = screenWidth*1.8;
        
        double treeStartY = 450;
        double treeEndY = screenHeight*1.5;
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(treeImageView, tt,  st);
        
        
        
        pt.play();
        
        treeGroup.getChildren().add(treeImageView);
        
        return treeGroup;
        
    }
    
    public Group getMoose(){
        
        Group treeGroup = new Group();
        
        Image treeImage = new Image(moosePath);
        ImageView treeImageView = new ImageView(treeImage);
        
        treeImageView.setPreserveRatio(true);
        treeImageView.setFitWidth(initialTreeWidth*0.5);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX = screenWidth/2 - 150;
        double treeEndX = -screenWidth*.8;
        
        double treeStartY = 450;
        double treeEndY = screenHeight*1.5;
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(treeImageView, tt,  st);
        
        pt.play();
        
        treeGroup.getChildren().add(treeImageView);
        
        return treeGroup;
        
    }
    
    public Group getBanner(){
        
        Group bannerGroup = new Group();
        
        Image bannerImage = new Image(bannerPath);
        ImageView bannerImageView = new ImageView(bannerImage);
        
        double initialBannerWidth = initialTreeWidth*4.5;
        
        bannerImageView.setPreserveRatio(true);
        bannerImageView.setFitWidth(initialBannerWidth);
        
        double scaleFactor = 2*finalTreeWidth/initialTreeWidth;
        
        double bannerStartX = screenWidth/2 - initialBannerWidth/2.;
        double bannerEndX = screenWidth/2 - scaleFactor*(initialBannerWidth/2.)/2. + 100;
        
        double bannerStartY = 350;
        double bannerEndY = screenHeight*1.5;
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX( bannerStartX );
        tt.setToX( bannerEndX );
        tt.setFromY(bannerStartY);
        tt.setToY(bannerEndY);
        tt.setCycleCount(Animation.INDEFINITE);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(Animation.INDEFINITE);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(bannerImageView, tt,  st);
        pt.play();
        
        bannerGroup.getChildren().add(bannerImageView);
        
        return bannerGroup;
        
    }
    
}
