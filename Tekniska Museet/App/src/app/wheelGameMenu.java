/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import static app.Globals.getButtonRect;
import static app.Globals.getButtonRect;
import static app.Globals.setText;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;

/**
 *
 * @author Matteo
 */
class wheelGameMenu implements ControlledScreen
{
    
    private PageSwitcher myController;
    private Pane pane;
    private WheelGameManager wheelGameManager;
    
    public wheelGameMenu()
    {
        wheelGameManager = new WheelGameManager();
        pane = setPane();
    }
    
    private Pane setPane()
    {
        Pane p = new Pane();
        
        p.getChildren().add( wheelGameManager.createGameWindow() );
        
        return p;
        
    }
    
    @Override
    public Pane getPane()
    {
        return pane;
    }

    @Override
    public void setScreenParent(PageSwitcher screenParent) {
        myController = screenParent;
    }
    
    @Override
    public PageSwitcher getController()
    {
        return myController;
    }
    
    private Text setMainTitle(String s)
    {
        Text mainTitle = new Text(s);
        mainTitle.setFill(Globals.textColor);
        mainTitle.setFont(new Font("Calibri",50));
        double titleWidth = mainTitle.getBoundsInLocal().getWidth();
        double titleHeight = mainTitle.getBoundsInLocal().getHeight();
        mainTitle.setLayoutX((Globals.screenWidth-titleWidth)/2);
        mainTitle.setLayoutY(Globals.screenHeight/10);
        
        return mainTitle;
    }
    
}
