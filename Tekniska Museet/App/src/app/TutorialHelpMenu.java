/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class TutorialHelpMenu {
    
    double screenWidth;
    double screenHeight;
    
    double blockWidth;
    double blockHeight;
    
    int numberBlocksX;
    int numberBlocksY;
    
    double helpMenuGroupOpacity;
    Group helpMenuGroup;
    int stageOfProgram;
    double fontSize;
    String infoTextString0EN, infoTextString1EN, infoTextString2EN, infoTextString3EN, infoTextString4EN, infoTextString5EN; 
    String infoTextString0SV, infoTextString1SV, infoTextString2SV, infoTextString3SV, infoTextString4SV, infoTextString5SV; 
    
    SequentialTransition fingerAnimation;
    double fingerWidth = 200;
    
    double fingerTipOffsetX = -0;
    double fingerTipOffsetY = -0;        
    
    double[][] fingerPath;
    
    TutorialHelpMenu(int argStageOfProgram){
        
        screenWidth = Globals.screenWidth;
        screenHeight = Globals.screenHeight;
        blockWidth = Globals.blockWidth;
        blockHeight = Globals.blockHeight;
        numberBlocksX = Globals.numberBlocksX;
        numberBlocksY = Globals.numberBlocksY;
        
        helpMenuGroupOpacity = 0.9;
        fontSize = 50;
        helpMenuGroup = new Group();
        stageOfProgram = argStageOfProgram;
        
        infoTextString0EN = "     NONE NONE NONE NONE NONE NONE\n\n";
        infoTextString1EN = "Solve the level by dragging the truck from its original position until the goal.";
        infoTextString2EN = "The truck has limited fuel, you must choose the path wisely.";
        infoTextString3EN = "In order to solve this level you must platoon. Make sure the red truck follows behind the yellow truck during part of the road.\n\n";
        infoTextString4EN = "You can make use of the timers to delay your departure!";
        infoTextString5EN = "The benefits of platooning are more evident in the long time, so here we show how much you can save on a yearly basis.\n\n"
                + "You can also check how much the platooning can help sparing money and the environment, by checking company/country statistics.";
        
        infoTextString0SV = "     NONE NONE NONE NONE NONE NONE\n\n";
        infoTextString1SV = "Lös banorna genom att dra lasbilarna till rätt slutpunkt.";
        infoTextString2SV = "Lastbilarna har ont om bränsle. Du måste välja den effektivaste vägen.";
        infoTextString3SV = "För att lösa banan måste du köra i kolonn. Se till att den röda lastbilen följer den gula under en del av vägen.\n\n";
        infoTextString4SV = "Du kan använda klockorna för att fördröja bilarnas starttider.";
        infoTextString5SV = "Vinsterna med kolonnkörning blir tydligare om vi tittar över längre tid.\n\n"
                + "You can also check how much the platooning can help sparing money and the environment, by checking company/country statistics.";
        
    }
    
    public ImageView createHelpButton(){
        
        Image helpImage = new Image("file:helpImage.png");
        ImageView helpImageView = new ImageView(helpImage);
        
        helpImageView.setOnMousePressed((MouseEvent me) -> {
            
            setHelpMenuVisible(true);
            
        });
        
        helpImageView.setPreserveRatio(true);
        helpImageView.setFitWidth(50);
        
        helpImageView.setLayoutX( helpImageView.getBoundsInLocal().getWidth()/2. );
        helpImageView.setLayoutY( screenHeight - helpImageView.getBoundsInLocal().getHeight()*(3./2.) );
        
//        RotateTransition rt = new RotateTransition(Duration.millis(7500), helpImageView);
        RotateTransition rt = new RotateTransition(Duration.millis(4000), helpImageView);
        Point3D rotationAxis = new Point3D(0.,1.,0.);
        rt.setAxis(rotationAxis);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setByAngle(10*180);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.setAutoReverse(false);
//        rt.play();
        
        ScaleTransition st = new ScaleTransition(Duration.millis(2000), helpImageView);
        st.setByX(0.5f);
        st.setByY(0.5f);
        st.setCycleCount(Animation.INDEFINITE);
        st.setAutoReverse(true);
        
        
//        ParallelTransition parallelTransition = new ParallelTransition();
//        
//        parallelTransition.getChildren().addAll(rt, parallelTransition);
        ParallelTransition pt = new ParallelTransition(rt, st);
        pt.play();
        
        return helpImageView;
//        helpImageView.toFront();
        
        
    }
    
    
    public Group createHelpTextRectangle(String argTextString, double argRectangleWidth, double argRectangleHeight){
        
        Rectangle infoBox = new Rectangle();
        
        double rectangleWidth = argRectangleWidth;
        double rectangleHeight = argRectangleHeight;
        
        double strokeWidth = 10;
        double arcSize = 150;
        
        infoBox.setWidth(rectangleWidth);
        infoBox.setHeight(rectangleHeight);
        infoBox.setFill(Color.BLANCHEDALMOND);
        infoBox.setStrokeWidth(strokeWidth);
        infoBox.setStroke(Color.BLACK);
        infoBox.setOpacity(helpMenuGroupOpacity);
        infoBox.setArcWidth(arcSize);
        infoBox.setArcHeight(arcSize);
        
        Text infoText = new Text();
        
        infoText.setText(argTextString);
        infoText.setWrappingWidth(0.8*rectangleWidth);
        infoText.setBoundsType(TextBoundsType.VISUAL);
        infoText.setFont(new Font(fontSize));
        
        double boxCenterX = infoBox.getBoundsInLocal().getMinX() + infoBox.getBoundsInLocal().getWidth()/2.;
        double boxCenterY = infoBox.getBoundsInLocal().getMinY() + infoBox.getBoundsInLocal().getHeight()/2.;
        
        infoText.setLayoutX( boxCenterX - infoText.getBoundsInLocal().getWidth()/2. );
        infoText.setLayoutY( boxCenterY - infoText.getBoundsInLocal().getHeight()/2. + 0.5*infoText.getBaselineOffset() );
        
        
        Group rectanglerTextGroup = new Group(infoBox, infoText);
        
        return rectanglerTextGroup;
        
    }
    
    
    public Group createHelpMenu(){
        
        double rectangleWidth = numberBlocksX*blockWidth*0.8;
        double rectangleHeight = numberBlocksY*blockHeight*0.4;
        
        String infoTextString;
        
        if (Globals.isSwedishTheLanguage){
        
            switch (stageOfProgram) {
                case 0:  infoTextString = infoTextString0SV;
                         break;
                case 1:  infoTextString = infoTextString1SV; // Starts here
                         break;
                case 2:  infoTextString = infoTextString2SV;
                         break;
                case 3:  infoTextString = infoTextString3SV;
                         break;
                case 4:  infoTextString = infoTextString4SV;
                     break;
                case 5:  infoTextString = infoTextString5SV;
                     break;
                default: infoTextString = "INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID ";
                         break;
                    
            }
        }else{
            
            switch (stageOfProgram) {
                case 0:  infoTextString = infoTextString0EN;
                         break;
                case 1:  infoTextString = infoTextString1EN; // Starts here
                         break;
                case 2:  infoTextString = infoTextString2EN;
                         break;
                case 3:  infoTextString = infoTextString3EN;
                         break;
                case 4:  infoTextString = infoTextString4EN;
                     break;
                case 5:  infoTextString = infoTextString5EN;
                     break;
                default: infoTextString = "INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID ";
                         break;
        }
               
        }
                
        Group helpGroup = createHelpTextRectangle(infoTextString, rectangleWidth, rectangleHeight);
        helpGroup.setId("helpGroup");
        
        Group fingerGroup = createFinger();
        helpMenuGroup.getChildren().addAll(helpGroup, fingerGroup);
        
        return helpMenuGroup;        
        
    }
    
    public Group createTutorialEnd(){
        
        double rectangleWidth = numberBlocksX*blockWidth*0.6;
        double rectangleHeight = numberBlocksY*blockHeight*0.3;
        
        Group textRectangle;
        
        if (Globals.isSwedishTheLanguage){
           
            textRectangle = createHelpTextRectangle("Avslutad tutorial!", rectangleWidth, rectangleHeight);

        }else{
        
            textRectangle = createHelpTextRectangle("Tutorial finished!", rectangleWidth, rectangleHeight);
        
        }
        
        Group fingerToNextGroup = getFingerToNext();
        
        textRectangle.setLayoutX(numberBlocksX*blockWidth*0.2);
        textRectangle.setLayoutY(numberBlocksY*blockHeight*0.1);
        
        Group tutorialEndGroup = new Group(textRectangle, fingerToNextGroup);
        
//        return textRectangle;
        return tutorialEndGroup;
        
    }
    
    public Group getFingerToPlay(){
        
        if ( stageOfProgram != 1 ){
            
            TutorialHelpMenu tutorialHelpMenu = new TutorialHelpMenu(stageOfProgram);
            return null;
            
        }
        
        Image fingerImage = new Image("file:finger.png");
        ImageView fingerImageView = new ImageView(fingerImage);
        Group fingerGroup = new Group(fingerImageView);
        
        fingerImageView.setPreserveRatio(true);
        fingerImageView.setFitWidth(fingerWidth);
        
        fingerImageView.setRotate(0);
//        fingerImageView.setLayoutY(-120);
//        fingerImageView.setLayoutX(75);
        
        TranslateTransition tt = new TranslateTransition(Duration.millis(200), fingerImageView);

        double tempFingerTipOffsetX = 1420;
        double tempFingerTipOffsetY = 725;
        
        tt.setFromX(blockWidth * 0.0 + tempFingerTipOffsetX);
        tt.setToX(blockWidth * 0.25 + tempFingerTipOffsetX);

        tt.setFromY(blockHeight * 0.0 + tempFingerTipOffsetY);
        tt.setToY(blockHeight * 0.25 + tempFingerTipOffsetY);

        tt.setAutoReverse(true);
        tt.setCycleCount(2);

        tt.setInterpolator(Interpolator.LINEAR);

        PauseTransition ptTemp = new PauseTransition(Duration.millis(500));

        SequentialTransition fingerAnimationTemp = new SequentialTransition(tt, ptTemp);

        fingerAnimationTemp.setAutoReverse(true);
        fingerAnimationTemp.setCycleCount(Animation.INDEFINITE);
            
        fingerAnimationTemp.play();
        
        double rectangleWidth = numberBlocksX*blockWidth*0.4;
        double rectangleHeight = numberBlocksY*blockHeight*0.15;
        
        Group textRectangle = createHelpTextRectangle("Press to Play", rectangleWidth, rectangleHeight);
        
        textRectangle.setLayoutX(numberBlocksX*blockWidth*0.45);
        textRectangle.setLayoutY(numberBlocksY*blockHeight*0.05);
        
//        fingerGroup.getChildren().add(textRectangle);
        
        return fingerGroup;
        
    }
    
    public Group getFingerToNext(){
        
        if ( stageOfProgram != 1 ){
            
            TutorialHelpMenu tutorialHelpMenu = new TutorialHelpMenu(stageOfProgram);
            return null;
            
        }
        
        Image fingerImage = new Image("file:finger.png");
        ImageView fingerImageView = new ImageView(fingerImage);
        Group fingerGroup = new Group(fingerImageView);
        
        fingerImageView.setPreserveRatio(true);
        fingerImageView.setFitWidth(fingerWidth);
        
        fingerImageView.setRotate(0);
//        fingerImageView.setLayoutY(-120);
//        fingerImageView.setLayoutX(75);
        
        TranslateTransition tt = new TranslateTransition(Duration.millis(200), fingerImageView);

        double tempFingerTipOffsetX = 1575;
        double tempFingerTipOffsetY = 725;
        
        tt.setFromX(blockWidth * 0.0 + tempFingerTipOffsetX);
        tt.setToX(blockWidth * 0.25 + tempFingerTipOffsetX);

        tt.setFromY(blockHeight * 0.0 + tempFingerTipOffsetY);
        tt.setToY(blockHeight * 0.25 + tempFingerTipOffsetY);

        tt.setAutoReverse(true);
        tt.setCycleCount(2);

        tt.setInterpolator(Interpolator.LINEAR);

        PauseTransition ptTemp = new PauseTransition(Duration.millis(500));

        SequentialTransition fingerAnimationTemp = new SequentialTransition(tt, ptTemp);

        fingerAnimationTemp.setAutoReverse(true);
        fingerAnimationTemp.setCycleCount(Animation.INDEFINITE);
            
        fingerAnimationTemp.play();
        
        double rectangleWidth = numberBlocksX*blockWidth*0.4;
        double rectangleHeight = numberBlocksY*blockHeight*0.15;
        
        Group textRectangle = createHelpTextRectangle("Press to Play", rectangleWidth, rectangleHeight);
        
        textRectangle.setLayoutX(numberBlocksX*blockWidth*0.45);
        textRectangle.setLayoutY(numberBlocksY*blockHeight*0.05);
        
//        fingerGroup.getChildren().add(textRectangle);
        
        return fingerGroup;
        
    }
    
    
    public Group createFinger(){
        
        Image fingerImage = new Image("file:finger.png");
        ImageView fingerImageView = new ImageView(fingerImage);
        Group fingerGroup = new Group(fingerImageView);
        
        fingerImageView.setPreserveRatio(true);
        fingerImageView.setFitWidth(fingerWidth);
        
        Duration SEC_1 = Duration.millis(500);
        Duration SEC_2 = Duration.millis(1000);
        Duration SEC_3 = Duration.millis(3000);
        
        FadeTransition ft = new FadeTransition(SEC_1, fingerImageView);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
//        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        
        FadeTransition ftInv = new FadeTransition(SEC_1, fingerImageView);
        ftInv.setFromValue(1.0);
        ftInv.setToValue(0.0);
        ft.setCycleCount(1);
        ftInv.setAutoReverse(false);
        
        SequentialTransition fingerMovement = new SequentialTransition();
        
        
        
        System.out.println("fingerPath = " + fingerPath);
        
        loadFingerPath();
        
        System.out.println("fingerPath.length = " + fingerPath.length);
        
        for ( int i = 1 ; i < fingerPath.length ; i++ ){
            
            TranslateTransition tt = new TranslateTransition(SEC_1, fingerImageView);
            
            double totalTranslation = 0;
            
            System.out.println("fingerPath[i-1][0] = " + fingerPath[i-1][0]);
            
            tt.setFromX(blockWidth * fingerPath[i-1][0] + fingerTipOffsetX);
            tt.setToX(blockWidth * fingerPath[i][0] + fingerTipOffsetY);
            
            totalTranslation += (int) Math.abs( (double)fingerPath[i][0] - (double)fingerPath[i-1][0] );
            
            tt.setFromY(blockHeight * fingerPath[i-1][1] + fingerTipOffsetX);
            tt.setToY(blockHeight * fingerPath[i][1] + fingerTipOffsetY);
            
            totalTranslation += Math.abs( (double)fingerPath[i][1] - (double)fingerPath[i-1][1] );
            
            System.out.println("totalTranslation = " + totalTranslation);
            
            tt.setDuration(Duration.millis(totalTranslation*250));
            
            tt.setCycleCount(1);
            tt.setAutoReverse(false);
            
            fingerMovement.getChildren().add(tt);            
            
        }
        
        TranslateTransition ttRestart = new TranslateTransition(Duration.millis(50), fingerImageView);
        ttRestart.setFromX(blockWidth * fingerPath[fingerPath.length-1][0] + fingerTipOffsetX);
        ttRestart.setToX(blockWidth * fingerPath[0][0] + fingerTipOffsetY);
        ttRestart.setFromY(blockHeight * fingerPath[fingerPath.length-1][1] + fingerTipOffsetX);
        ttRestart.setToY(blockHeight * fingerPath[0][1] + fingerTipOffsetY);
        
        PauseTransition pt = new PauseTransition(Duration.millis(1000));
        
//        fingerAnimation = new SequentialTransition(ft, fingerMovement, ftInv, ttRestart);
        fingerAnimation = new SequentialTransition(fingerMovement,pt);
        fingerAnimation.setCycleCount( Animation.INDEFINITE );
        
        if (stageOfProgram == 4){
            
            fingerImageView.setRotate(-90);
            fingerImageView.setLayoutY(-120);
            fingerImageView.setLayoutX(75);
            
        }
        
        
        if (stageOfProgram == 2 || stageOfProgram == 4){
            
            TranslateTransition tt = new TranslateTransition(Duration.millis(200), fingerImageView);
            
            tt.setFromX(blockWidth * fingerPath[0][0] + fingerTipOffsetX);
            tt.setToX(blockWidth * fingerPath[1][0] + fingerTipOffsetX);
            
            tt.setFromY(blockHeight * fingerPath[0][1] + fingerTipOffsetY);
            tt.setToY(blockHeight * fingerPath[1][1] + fingerTipOffsetY);
            
            tt.setAutoReverse(true);
            tt.setCycleCount(2);
            
            tt.setInterpolator(Interpolator.LINEAR);
            
            PauseTransition ptTemp = new PauseTransition(Duration.millis(500));
            
            fingerAnimation = new SequentialTransition(tt, ptTemp);
            
            fingerAnimation.setAutoReverse(true);
            fingerAnimation.setCycleCount(Animation.INDEFINITE);
            
        }
        
        
                
        return fingerGroup;
        
    }
    
    private void loadFingerPath(){
        
        
        
        switch (stageOfProgram) {
            case 1:  loadFingerPath0();
                     break;
            case 2:  loadFingerPath1();
                     break;
            case 3:  loadFingerPath2();
                     break;
            case 4:  loadFingerPath3();
                     break;
            default: System.out.println("ERROR ERROR ERROR");
                     break;
        }
        
        
    }
    
    private void loadFingerPath0(){
        
        fingerPath = new double[2][2];
        
        fingerPath[0][0] = 8.5;
        fingerPath[0][1] = 6.5;
        
        fingerPath[1][0] = 1.5;
        fingerPath[1][1] = 6.5;
        
    }
    
    private void loadFingerPath1(){
        
//        fingerPath = new double[6][2];
//        
//        fingerPath[0][0] = 8.5;
//        fingerPath[0][1] = 7.5;
//        
//        fingerPath[1][0] = 6.5;
//        fingerPath[1][1] = 7.5;
//        
//        fingerPath[2][0] = 6.5;
//        fingerPath[2][1] = 6.5;
//        
//        fingerPath[3][0] = 2.5;
//        fingerPath[3][1] = 6.5;
//        
//        fingerPath[4][0] = 2.5;
//        fingerPath[4][1] = 8.5;
//        
//        fingerPath[5][0] = 1.5;
//        fingerPath[5][1] = 8.5;
        
        fingerPath = new double[2][2];
        
        fingerPath[0][0] = 8.5;
        fingerPath[0][1] = 0.5;
        
        fingerPath[1][0] = 8.5;
        fingerPath[1][1] = 0.75;
        
        
    }
    
    private void loadFingerPath2(){
        
        fingerPath = new double[6][2];
        
        // for the second movement
        
        fingerPath[0][0] = 7.5;
        fingerPath[0][1] = 6.5;
        
        fingerPath[1][0] = 6.5;
        fingerPath[1][1] = 6.5;
        
        fingerPath[2][0] = 6.5;
        fingerPath[2][1] = 7.5;
        
        fingerPath[3][0] = 2.5;
        fingerPath[3][1] = 7.5;
        
        fingerPath[4][0] = 2.5;
        fingerPath[4][1] = 5.5;
        
        fingerPath[5][0] = 1.5;
        fingerPath[5][1] = 5.5;
        
    }
    
    private void loadFingerPath3(){
        
        fingerPath = new double[2][2];
        
        fingerPath[0][0] = 3.5;
        fingerPath[0][1] = 8.5;

        fingerPath[1][0] = 3.75;
        fingerPath[1][1] = 8.5;
        
    }
    
    public void playFingerAnimation(){
        
        fingerAnimation.play();
        
    }
            
    public void stopFingerAnimation(){
        
        fingerAnimation.stop();
        
    }     
        
    public void setHelpMenuVisible(boolean visibility){
        
        if ( visibility ){
            helpMenuGroup.setOpacity(1.);
            helpMenuGroup.toFront();
        }else{
            helpMenuGroup.setOpacity(0.);
            helpMenuGroup.toBack();
        }
        
    }
    
}
