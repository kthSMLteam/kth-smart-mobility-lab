/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package platooningexpress;

/**
 *
 * @author rui
 */
public class Globals {

    public static double fontStretch = 1.25;
    public static double horizontalStretch = 1.527;
//    public static double verticalStretch = 1.165;
    public static double verticalStretch = 1.05;
    public static double screenWidth = 1920;
    public static double screenHeight = 1080;
    
    
    // Game map dimensions
    public static int numberBlocksX = 10;
    public static int numberBlocksY = 10;
    
    public static double blockWidth = 100;
    public static double blockHeight = 100;
        
}
