/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package platooningexpress;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author rui
 */
public class PlatooningExpress extends Application {
    
    double screenWidth = Globals.screenWidth;
    double screenHeight = Globals.screenHeight;
    
    int numberBlocksX = Globals.numberBlocksX;
    int numberBlocksY = Globals.numberBlocksY;
    
    double blockWidth = Globals.blockWidth;
    double blockHeight = Globals.blockHeight;
    
    
    @Override
    public void start(Stage primaryStage) {
        
        Group root = new Group();
        
        // Crating the class instance, creating the map and adding it to root
        MapDrawer mapDrawer = new MapDrawer();
//        int levelNumber = 1;
        int levelNumber = 2;
                
        Group background = mapDrawer.getMapBackground();
        
        background.setId("background");
        root.getChildren().add( background );
        root.getChildren().add( mapDrawer.getMapDrawing(levelNumber) );
        
        Level currentLevel = mapDrawer.getLevel();
        
        if ( currentLevel == null ){
            
            System.out.println("No level loaded, value is null.");
            
        }
        
        FingerSlider fingerSlider = new FingerSlider(root, currentLevel);
        
        Scene scene = new Scene(root, numberBlocksX*blockWidth, numberBlocksY*blockHeight);
        
        primaryStage.setTitle("Rui's Platooning Express!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
