/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package usingthewheel;

import java.util.Arrays;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class TreeManager {
    
    double screenWidth = GlobalsUsingTheWheel.screenWidth;
    double screenHeight = GlobalsUsingTheWheel.screenHeight;
    String treePath1, treePath2, roadSignPath, bannerPath, moosePath;
    
    double initialTreeWidth = screenWidth/38.4;
    double finalTreeWidth = screenWidth/3.2;
    double treeMovementTimeSeconds = 5;
    
    public TreeManager(){
        
        screenWidth = 1920;
        screenHeight = 1080;
        treePath1 = "file:tree1Small.png";
        treePath2 = "file:tree2Small.png";
        roadSignPath = "file:roadSign.png";
        bannerPath = "file:banner.png";
        moosePath = "file:moose.png";
        
        setUpTreeHistory();
        
    }

    Group getGasPump() {
        
        Group treeGroup = new Group();
        
        Image treeImage;
        
        treeImage = new Image("file:gasPump.png");
        
        ImageView treeImageView = new ImageView(treeImage);
        
        treeImageView.setPreserveRatio(true);
        treeImageView.setFitWidth(initialTreeWidth);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX;
        double treeEndX;
        
        double treeStartY;
        double treeEndY;
        
//        if (left){
            
            treeStartX = screenWidth/2. - 200 - initialTreeWidth/2.;
            treeEndX = -screenWidth*0.8;

            treeStartY = 450;
            treeEndY = screenHeight*1.5;
            
//        }else{
//            
//            treeStartX = screenWidth/2. + 200 - initialTreeWidth/2.;
//            treeEndX = screenWidth*1.8;
//
//            treeStartY = 450;
//            treeEndY = screenHeight*1.5;
//            
//        }
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(treeImageView, tt,  st);
        
        
        
//        pt.play();
        
        PauseTransition pauseTransition = new PauseTransition(Duration.millis(2000));
        
        SequentialTransition sq = new SequentialTransition(pauseTransition, pt);
        
        sq.play();
        
        treeGroup.getChildren().add(treeImageView);
        
        return treeGroup;
        
        
        
    }    
    
    public class TreeMovementInterpolator extends Interpolator {
    @Override
        protected double curve(double t) {

            double speedOfChange;
    //        System.out.println("t = " + t);
            speedOfChange = Math.pow(t,3.5);
    //        System.out.println("speedOfChange = " + speedOfChange);
            return speedOfChange;
    //        return Math.abs(0.5-t)*2 ;
        }
    }
    
    
     private void setUpTreeHistory(){
        
        GlobalsUsingTheWheel.treeHistory[0] = 1;
        
        double timeToChangeSeconds;
        int sameLaneCnt = 0;
        int timeToChange;
        
        for ( int i = 1 ; i < GlobalsUsingTheWheel.treeHistory.length ; i++ ){
            
            GlobalsUsingTheWheel.treeHistory[i] = 1;
            
        }
        
        GlobalsUsingTheWheel.treeHistory[ (int) ( GlobalsUsingTheWheel.treeHistory.length*(1./4.) ) ] = 0;
        GlobalsUsingTheWheel.treeHistory[ (int) ( GlobalsUsingTheWheel.treeHistory.length*(2./4.) ) ] = 0;
        GlobalsUsingTheWheel.treeHistory[ (int) ( GlobalsUsingTheWheel.treeHistory.length*(3./4.) ) ] = 0;
        
//        for ( int i = 1 ; i < GlobalsUsingTheWheel.treeHistory.length ; i++ ){
//            
//            double currentTime = i*GlobalsUsingTheWheel.timeStep;
//            
//            if ( currentTime < 10 ){
//                timeToChangeSeconds = 5;
//            }else if ( currentTime < 20 ){
//                timeToChangeSeconds = 4.5;
//            }else if ( currentTime < 30 ){
//                timeToChangeSeconds = 4.;
//            }else if ( currentTime < 40 ){
//                timeToChangeSeconds = 3.5;
//            }else if ( currentTime < 50 ){
//                timeToChangeSeconds = 3.;
//            }else{
//                timeToChangeSeconds = 2.5;
//            }
//            
//            timeToChange = (int)(timeToChangeSeconds/GlobalsUsingTheWheel.timeStep);
//            
//            if ( sameLaneCnt >= timeToChange ){
//                
//                if ( GlobalsUsingTheWheel.treeHistory[i-1] == 1){
//                    
//                    if ( randomGenerator.nextBoolean() ){
//                        GlobalsUsingTheWheel.treeHistory[i] = 0;
//                    }else{
//                        GlobalsUsingTheWheel.treeHistory[i] = 2;
//                    }
//                    
//                }else{
//                    
//                    GlobalsUsingTheWheel.treeHistory[i] = 1;
//                    
//                }
//                sameLaneCnt = 0;
//                
//            }else{
//                
//                GlobalsUsingTheWheel.treeHistory[i] = GlobalsUsingTheWheel.treeHistory[i-1];
//                sameLaneCnt++;
//                
//            }
//            
//            
//            
//        }
        
        
        
        System.out.println("Arrays.toString(Globals.treeHistory) = " + Arrays.toString(GlobalsUsingTheWheel.treeHistory) );
              
                
    }
    
    public Group getTree(boolean left, boolean treeType){
        
        Group treeGroup = new Group();
        
        Image treeImage;
        
        if( treeType ){
            treeImage = new Image(treePath1);
        }else{
            treeImage = new Image(treePath2);
        }
        
        ImageView treeImageView = new ImageView(treeImage);
        
        treeImageView.setPreserveRatio(true);
        treeImageView.setFitWidth(initialTreeWidth);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX;
        double treeEndX;
        
        double treeStartY;
        double treeEndY;
        
        if (left){
            
            treeStartX = screenWidth/2. - 200 - initialTreeWidth/2.;
            treeEndX = -screenWidth*0.8;

            treeStartY = 450;
            treeEndY = screenHeight*1.5;
            
        }else{
            
            treeStartX = screenWidth/2. + 200 - initialTreeWidth/2.;
            treeEndX = screenWidth*1.8;

            treeStartY = 450;
            treeEndY = screenHeight*1.5;
            
        }
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(treeImageView, tt,  st);
        
        
        
        pt.play();
        
        treeGroup.getChildren().add(treeImageView);
        
        return treeGroup;
        
    }
    
    public Group getRoadSign(){
        
        Group treeGroup = new Group();
        
        Image treeImage = new Image(roadSignPath);
        ImageView treeImageView = new ImageView(treeImage);
        
        treeImageView.setPreserveRatio(true);
        treeImageView.setFitWidth(initialTreeWidth*0.3);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX = screenWidth/2 + 150;
        double treeEndX = screenWidth*1.8;
        
        double treeStartY = 450;
        double treeEndY = screenHeight*1.5;
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(treeImageView, tt,  st);
        
        
        
        pt.play();
        
        treeGroup.getChildren().add(treeImageView);
        
        return treeGroup;
        
    }
    
    public Group getMoose(){
        
        Group treeGroup = new Group();
        
        Image treeImage = new Image(moosePath);
        ImageView treeImageView = new ImageView(treeImage);
        
        treeImageView.setPreserveRatio(true);
        treeImageView.setFitWidth(initialTreeWidth*0.5);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX = screenWidth/2 - 150;
        double treeEndX = -screenWidth*.8;
        
        double treeStartY = 450;
        double treeEndY = screenHeight*1.5;
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(treeImageView, tt,  st);
        
        pt.play();
        
        treeGroup.getChildren().add(treeImageView);
        
        return treeGroup;
        
    }
    
    
    public boolean canIGetTree(double argCurrentTime){
        
        boolean treePossible = true;
        
        int currentIndex = (int) (argCurrentTime/GlobalsUsingTheWheel.timeStep);
        
//        int timeBefore = (int) (10./GlobalsUsingTheWheel.timeStep );
        
        int startingIndex = Math.max(0,currentIndex);
        int timeWithoutTrees = (int) (5./GlobalsUsingTheWheel.timeStep);
        
        
        for ( int i = startingIndex ; i < Math.min(startingIndex + timeWithoutTrees, GlobalsUsingTheWheel.treeHistory.length-1 ); i++ ){
            
            if (  GlobalsUsingTheWheel.treeHistory[i] == 0 ){
                
                return false;
                
            } 
            
            
        }       
        
        return treePossible;
        
    }
    
    public Group getBanner(){
        
        Group bannerGroup = new Group();
        
        Image bannerImage = new Image(bannerPath);
        ImageView bannerImageView = new ImageView(bannerImage);
        
        double initialBannerWidth = initialTreeWidth*4.5;
        
        bannerImageView.setPreserveRatio(true);
        bannerImageView.setFitWidth(initialBannerWidth);
        
        double scaleFactor = 2*finalTreeWidth/initialTreeWidth;
        
        double bannerStartX = screenWidth/2 - initialBannerWidth/2.;
        double bannerEndX = screenWidth/2 - scaleFactor*(initialBannerWidth/2.)/2. + 100;
        
        double bannerStartY = 350;
        double bannerEndY = screenHeight*1.5;
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX( bannerStartX );
        tt.setToX( bannerEndX );
        tt.setFromY(bannerStartY);
        tt.setToY(bannerEndY);
        tt.setCycleCount(Animation.INDEFINITE);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new TreeMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(Animation.INDEFINITE);
        st.setAutoReverse(false);
        
        st.setInterpolator( new TreeMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(bannerImageView, tt,  st);
        pt.play();
        
        bannerGroup.getChildren().add(bannerImageView);
        
        return bannerGroup;
        
    }
    
}
