/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package usingthewheel;

import java.util.Arrays;
import java.util.Random;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class TrafficManager {
    
    double screenWidth = GlobalsUsingTheWheel.screenWidth;
    double screenHeight = GlobalsUsingTheWheel.screenHeight;
    String carPath1, carPath2, carPath3, roadSignPath, bannerPath, moosePath;
    
    double initialTreeWidth = screenWidth/38.4;
    double finalTreeWidth = screenWidth/3.2;
    double treeMovementTimeSeconds = 5;
    double waitingTime;
    
    Random randomGenerator;
    
    int[] laneHistory;
    int extraSameTime;
    
    double difficulty = 250.;
    double carGenerationProbability = 0.2;
    
    
    
    public TrafficManager(){
        
//        carPath1 = "file:backCar.png";
//        carPath2 = "file:backCar.png";
        
        carPath1 = "file:carSmall1Resize.png";
        carPath2 = "file:carSmall2Resize.png";
        carPath3 = "file:carSmall4Resize.png";
        
        laneHistory =  new int[500];
        for ( int i = 0; i < laneHistory.length ; i++){
            laneHistory[i] = 1;
        }
        
        randomGenerator = new Random();
        
        extraSameTime = (int) (( difficulty*randomGenerator.nextFloat() )+0.5);
        
        waitingTime = 500;
        
        setUpLaneHistory();
        
    }
    
    public class CarMovementInterpolator extends Interpolator {
    @Override
        protected double curve(double t) {

            double speedOfChange;
    //        System.out.println("t = " + t);
            speedOfChange = Math.pow(t,3.5);
    //        System.out.println("speedOfChange = " + speedOfChange);
            return speedOfChange;
    //        return Math.abs(0.5-t)*2 ;
        }
    }
    
    public double getCarGenerationProbability(){
        return carGenerationProbability;
    }
    
    private void setUpLaneHistory(){
        
        GlobalsUsingTheWheel.laneHistory[0] = 1;
        
        double timeToChangeSeconds;
        int sameLaneCnt = 0;
        int timeToChange;
        
        
        for ( int i = 1 ; i < GlobalsUsingTheWheel.laneHistory.length ; i++ ){
            
            double currentTime = i*GlobalsUsingTheWheel.timeStep;
            
            if ( currentTime < 10 ){
                timeToChangeSeconds = 5;
            }else if ( currentTime < 20 ){
                timeToChangeSeconds = 4.5;
            }else if ( currentTime < 30 ){
                timeToChangeSeconds = 4.;
            }else if ( currentTime < 40 ){
                timeToChangeSeconds = 3.5;
            }else if ( currentTime < 50 ){
                timeToChangeSeconds = 3.;
            }else{
                timeToChangeSeconds = 2.5;
            }
            
            timeToChange = (int)(timeToChangeSeconds/GlobalsUsingTheWheel.timeStep);
            
            if ( sameLaneCnt >= timeToChange ){
                
                if ( GlobalsUsingTheWheel.laneHistory[i-1] == 1){
                    
                    if ( randomGenerator.nextBoolean() ){
                        GlobalsUsingTheWheel.laneHistory[i] = 0;
                    }else{
                        GlobalsUsingTheWheel.laneHistory[i] = 2;
                    }
                    
                }else{
                    
                    GlobalsUsingTheWheel.laneHistory[i] = 1;
                    
                }
                sameLaneCnt = 0;
                
            }else{
                
                GlobalsUsingTheWheel.laneHistory[i] = GlobalsUsingTheWheel.laneHistory[i-1];
                sameLaneCnt++;
                
            }
            
            
            
        }
        
        
        
        System.out.println("Arrays.toString(Globals.laneHistory) = " + Arrays.toString(GlobalsUsingTheWheel.laneHistory) );
              
                
    }
    
   
    
    public void setDifficulty(double timeElapsedSeconds){
        
        if ( timeElapsedSeconds < GlobalsUsingTheWheel.maxGameTime*(1./6.) ){
            difficulty = 250;
            carGenerationProbability = 0.2;
            waitingTime = 500;
        }else if ( timeElapsedSeconds < GlobalsUsingTheWheel.maxGameTime*(2./6.) ){
            difficulty = 200;
            carGenerationProbability = 0.25;
            waitingTime = 450;
        }else if ( timeElapsedSeconds < GlobalsUsingTheWheel.maxGameTime*(3./6.) ){
            difficulty = 150;
            carGenerationProbability = 0.3;
            waitingTime = 400;
        }else if ( timeElapsedSeconds < GlobalsUsingTheWheel.maxGameTime*(4./6.) ){
            difficulty = 100;
            carGenerationProbability = 0.35;
            waitingTime = 300;
        }else if ( timeElapsedSeconds < GlobalsUsingTheWheel.maxGameTime*(5./6.) ){
            difficulty = 50;
            carGenerationProbability = 0.4;
            waitingTime = 200;
        }else{
            difficulty = 20;
            carGenerationProbability = 0.45;
            waitingTime = 100;
        }
        
    }
    
    public double getWait(){
        
        return waitingTime;        
        
    }
    
    public Group getCar(int argLane, boolean carType){
        
        Group carGroup = new Group();
        
        Image carImage;
        
//        if( carType ){
//            carImage = new Image(carPath1);
//        }else{
//            carImage = new Image(carPath2);
//        }
        
        double randomDouble = randomGenerator.nextDouble();
        
        if ( randomDouble < 0.33 ){
            carImage = new Image(carPath1);
        }else if( randomDouble < 0.66 ){
            carImage = new Image(carPath2);
        }else{
            carImage = new Image(carPath3);
        }
        
        ImageView carImageView = new ImageView(carImage);
        
        carImageView.setPreserveRatio(true);
        carImageView.setFitWidth(initialTreeWidth);
        
        double scaleFactor = finalTreeWidth/initialTreeWidth;
        
        double treeStartX = 0;
        double treeEndX = 0;
        
        double treeStartY = 0;
        double treeEndY = 0;
        
        treeStartY = 450;
        treeEndY = screenHeight*1.5;
        
        switch (argLane) {
            case 0:  treeStartX = screenWidth/2. - 80 - initialTreeWidth/2.;
                    treeEndX = -screenWidth*0.2;

                     break;
            case 1:  treeStartX = screenWidth/2. - 25;
                    treeEndX = screenWidth/2.;

                     break;
            case 2:  treeStartX = screenWidth/2. + 80 - initialTreeWidth/2.;
                    treeEndX = screenWidth*1.2;

                    
                     break;
            default: System.out.println("ERROR");;
                     break;
        }
        
        
        Duration treeMovementDuration = Duration.millis(treeMovementTimeSeconds*1000);
        
        TranslateTransition tt = new TranslateTransition(treeMovementDuration);
        tt.setFromX(treeStartX);
        tt.setToX(treeEndX);
        tt.setFromY(treeStartY);
        tt.setToY(treeEndY);
        tt.setCycleCount(1);
        tt.setAutoReverse(false);
        
        tt.setInterpolator( new CarMovementInterpolator() );
        
        ScaleTransition st = new ScaleTransition(treeMovementDuration);
        st.setByX(scaleFactor);
        st.setByY(scaleFactor);
        st.setCycleCount(1);
        st.setAutoReverse(false);
        
        st.setInterpolator( new CarMovementInterpolator() );
        
        ParallelTransition pt = new ParallelTransition(carImageView, tt,  st);
        
        
        
        pt.play();
        
        carGroup.getChildren().add(carImageView);
        
        return carGroup;
        
    }
    
    public boolean checkCollision(Group argMyTruckImageView, Group objects){
        
        boolean isColliding = false;
        
               
        for (int i = 0 ; i < objects.getChildren().size() ; i++){
            
            Group tempObject = (Group) objects.getChildren().get(i);
            
            double testPointX = tempObject.getBoundsInParent().getMinX();
            double testPointY = tempObject.getBoundsInParent().getMinY();
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            testPointX = tempObject.getBoundsInParent().getMinX();
            testPointY = tempObject.getBoundsInParent().getMaxY();
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            
            
            testPointX = tempObject.getBoundsInParent().getMaxX();
            testPointY = tempObject.getBoundsInParent().getMinY();
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            testPointX = tempObject.getBoundsInParent().getMaxX();
            testPointY = tempObject.getBoundsInParent().getMaxY();
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            
        }
        
        
        return isColliding;
        
    }
    
    public boolean checkCollisionTree(Group argMyTruckImageView, Group objects){
        
        boolean isColliding = false;
        
        double polygonInside = 150;
        
        for (int i = 0 ; i < objects.getChildren().size() ; i++){
            
            Group tempObject = (Group) objects.getChildren().get(i);
            
            double testPointX = tempObject.getBoundsInParent().getMinX()+polygonInside;
            double testPointY = tempObject.getBoundsInParent().getMinY()+polygonInside;
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            testPointX = tempObject.getBoundsInParent().getMinX()+polygonInside;
            testPointY = tempObject.getBoundsInParent().getMaxY()-polygonInside;
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            
            
            testPointX = tempObject.getBoundsInParent().getMaxX()-polygonInside;
            testPointY = tempObject.getBoundsInParent().getMinY()+polygonInside;
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            testPointX = tempObject.getBoundsInParent().getMaxX()-polygonInside;
            testPointY = tempObject.getBoundsInParent().getMaxY()-polygonInside;
            
            if ( isInsideGroup(argMyTruckImageView, testPointX, testPointY) ){
                
                isColliding = true;
                break;
                
            }
            
            
        }
        
        
        return isColliding;
        
    }
    
    
    private boolean isInsideGroup(Group argMyTruckImageView, double testPointX, double testPointY){
        
        boolean isInside = false;
        
        if ( testPointX > argMyTruckImageView.getBoundsInParent().getMinX() && testPointX < argMyTruckImageView.getBoundsInParent().getMaxX() ){
            
            if ( testPointY > argMyTruckImageView.getBoundsInParent().getMinY() && testPointY < argMyTruckImageView.getBoundsInParent().getMaxY() ){
            
                isInside = true;            
            
            }
                        
        }
               
        
        return isInside;
        
    }
    
    public int getLeadTruckLane(){
        
        int newLane;
        
        int sameLaneTime = 0;
        
        for ( int i = 0; i < laneHistory.length-1; i++){
            
            if ( laneHistory[laneHistory.length-1 - i] == laneHistory[laneHistory.length- 2 - i] ){
                
                sameLaneTime ++;
                
            }else{
                
                break;
                
            }
            
        }
        
        
        
        for ( int i = 0; i < laneHistory.length-1 ; i++){
            laneHistory[i] = laneHistory[i+1];
        }
        
        
//        int difficultyChange = int (( 50.0*randomGenerator.nextFloat() )+0.5);
        
        if ( sameLaneTime > difficulty ){
            
            if( laneHistory[ laneHistory.length - 1 ] == 0 || laneHistory[ laneHistory.length - 1 ] == 2){
                
                laneHistory[ laneHistory.length - 1 ] = 1;
                
            }else{
                
                if ( randomGenerator.nextFloat() < 0.5 ){
                
                    laneHistory[ laneHistory.length - 1 ] = 0;
                    
                }else{
                    
                    laneHistory[ laneHistory.length - 1 ] = 2;
                    
                }
                
            }
            
//            extraSameTime = (int) (( difficulty*randomGenerator.nextFloat() )+0.5);
            
            
            
            
            
        }
        
        return laneHistory[ laneHistory.length - 1 ];
        
    }
    
    
    
}
