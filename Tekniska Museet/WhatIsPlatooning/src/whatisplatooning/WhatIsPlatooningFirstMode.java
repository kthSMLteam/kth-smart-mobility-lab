///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package whatisplatooning;
//
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//import javafx.animation.Animation;
//import javafx.animation.FadeTransition;
//import javafx.animation.Interpolator;
//import javafx.animation.KeyFrame;
//import javafx.animation.KeyValue;
//import javafx.animation.ParallelTransition;
//import javafx.animation.PathTransition;
//import javafx.animation.PathTransitionBuilder;
//import javafx.animation.RotateTransition;
//import javafx.animation.ScaleTransition;
//import javafx.animation.SequentialTransition;
//import javafx.animation.Timeline;
//import javafx.animation.TranslateTransition;
//import javafx.application.Application;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.geometry.Point2D;
//import javafx.scene.Group;
//import javafx.scene.Scene;
//import javafx.scene.control.Button;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.input.MouseEvent;
//import javafx.scene.layout.StackPane;
//import javafx.scene.media.Media;
//import javafx.scene.media.MediaPlayer;
//import javafx.scene.media.MediaView;
//import javafx.scene.paint.Color;
//import javafx.scene.shape.Circle;
//import javafx.scene.shape.LineTo;
//import javafx.scene.shape.MoveTo;
//import javafx.scene.shape.Path;
//import javafx.scene.shape.Rectangle;
//import javafx.scene.text.Font;
//import javafx.scene.text.Text;
//import javafx.stage.Stage;
//import javafx.stage.StageStyle;
//import javafx.util.Duration;
//
///**
// *
// * @author rui
// */
//
//
//
//
//
//public class WhatIsPlatooning extends Application {
//    
//    Group root;
//    
//    Stage stage;
//    
//    private Point2D dragAnchor;
//    private double initX;
//    private double initY;
//    static private double newXPosition, newYPosition;
//    
//    static private Text infoText;
//    static private Text infoTextNumber;
//    static private Text fuelSavingsText;
//    static private Text fuelSavingsNumberText;
//    static private Text safeText;
//    
//    static Random rand = new Random();
//    
//    static boolean textShowing = true;
////    String fontString = "Century Gothic";
//    String fontString = "";
//    
//    static long startTime = System.nanoTime();
//    static long endTime = System.nanoTime();
//
//    static long duration = (endTime - startTime)/1000000;
//    
//    static private double meterPerPixel = 5.831 / 1222;
//    
//    static private double frontTruckFraction = 0;
//    static private double truckFraction = 0;
//    static private double frontTruckTransitionOffsetX = 20;
//    static private double truckTransitionOffsetX = -30;
//    static private double frontTruckTransitionOffsetY = 15;
//    static private double truckTransitionOffsetY = -20;
//    static private double frontWheelOffsetX = 62;
//    static private double frontWheelOffsetY = 210;
//    static private double backWheelOffsetX = 385;
//    static private double backWheelOffsetY = 215;
//    static private double frontTruckBackWheelOffsetX = 385;
//    static private double frontTruckBackWheelOffsetY = 215;
//    
//    static private double screenWidth = 1920;
////    static private double screenHeight = 1200;
//    static private double screenHeight = 1080;
//
//    static private double backgroundOffsetY = -250;
//    
//    int animationTime = 5000;
//    
//    static DecimalFormat dfMeters = new DecimalFormat("#0.0");
//    static DecimalFormat dfFuelSavings = new DecimalFormat("#00");
//    
//    static SequentialTransition ftInv = new SequentialTransition();
//    
//    static Text slideTruckText;
//    
//    static ImageView truckImageView;
//    static ImageView frontWheelImageView;
//    static ImageView backWheelImageView;
//    static ImageView frontTruckBackWheelImageView;
//    static Group truckGroup = new Group();
//    static Group frontTruckGroup = new Group();
//    
//    static ImageView frontTruckImageView;
//    
////    static Image snowflakeImage = new Image("file:snowflake50.png");
//    static Image snowflakeImage = new Image("file:snowflake50White.png");
//    double numSnowFlakes = 400;
//    double numTurbulenceSnowFlakes = 100;
//    double snowFlakeOpacity = 0.4;
//    static ImageView snowflakeImageView;
//    static Timeline airDragAnimation;
//    
//    static ImageView fuelGaugeImageView;
//    static ImageView fuelNeedleImageView;
//    static Group fuelGroup = new Group();
//    
//    static boolean firstTime = true;
//    
//    @Override
//    public void start(Stage primaryStage) {
//        root = new Group();
//        Scene scene;
//        
//        
//
////        scene = new Scene(root, 1200, 899, Color.BLACK);
//        scene = new Scene(root, screenWidth, screenHeight, Color.BLACK);
//        
//        stage = primaryStage;
//        primaryStage.initStyle(StageStyle.UNDECORATED);
////        Image backgroundImage = new Image("file:roadSideviewExtended.jpg");
////        Image backgroundImage = new Image("file:roadSideviewExtendedTwiceRui.jpg");
////        Image backgroundImage = new Image("file:roadSideviewExtendedTwice.jpg");
////        Image backgroundImage = new Image("file:roadSideviewExtendedTwiceSign.jpg");
////        Image backgroundImage = new Image("file:roadSideviewExtendedTwiceSignFullScreen.jpg");
//        Image backgroundImage = new Image("file:roadSideviewExtendedTwiceSignTouchScreen.jpg");
////        Image truckImage = new Image("file:truckSide.png");
////        Image truckImage = new Image("file:truckSideNoWheels1.png");
////        Image frontTruckImage = new Image("file:truckSideNoWheels1.png");
//                Image truckImage = new Image("file:truckSideNoWheels1Blue.png");
//        Image frontTruckImage = new Image("file:truckSideNoWheels1Blue.png");
////        Image truckImage = new Image("file:truckSideNoWheels1Red.png");
////        Image frontTruckImage = new Image("file:truckSideNoWheels1Red.png");
//        Image frontWheelImage = new Image("file:frontWheel1.png");
//                
//        ImageView backgroundImageView = new ImageView(backgroundImage);
//        backgroundImageView.setX( -backgroundImage.getWidth()*(2/3.) );
//        backgroundImageView.setY(backgroundOffsetY);
//        
//        
//        truckImageView = new ImageView(truckImage);
//        frontWheelImageView  = new ImageView(frontWheelImage);
//        backWheelImageView  = new ImageView(frontWheelImage);
//        frontTruckBackWheelImageView  = new ImageView(frontWheelImage);
//        
//        double truckYPosition = 800;
//        
//        truckImageView.setPreserveRatio(true);
//        truckImageView.setFitWidth(600);
//        newXPosition = 600;
//        truckImageView.setX(newXPosition);
//        truckImageView.setY(truckYPosition + 1.2*backgroundOffsetY);
//        
//        frontWheelImageView.setPreserveRatio(true);
//        frontWheelImageView.setFitWidth(70);
//        frontWheelImageView.setX(newXPosition + frontWheelOffsetX);
//        frontWheelImageView.setY(truckYPosition + frontWheelOffsetY + 1.2*backgroundOffsetY);
//        
//        backWheelImageView.setPreserveRatio(true);
//        backWheelImageView.setFitWidth(70);
//        backWheelImageView.setX(newXPosition + backWheelOffsetX);
//        backWheelImageView.setY(truckYPosition + backWheelOffsetY + 1.2*backgroundOffsetY);
//        
//        frontTruckBackWheelImageView.setPreserveRatio(true);
//        frontTruckBackWheelImageView.setFitWidth(70);
//        frontTruckBackWheelImageView.setX(-200 + frontTruckBackWheelOffsetX);
//        frontTruckBackWheelImageView.setY(truckYPosition + frontTruckBackWheelOffsetY + 1.2*backgroundOffsetY);
//        
//        
//        slideTruckText = new Text(newXPosition + 200, truckYPosition + 85 + 1.2*backgroundOffsetY," Slide here to \nmove the truck!");
//        slideTruckText.setFont( new Font( fontString , 50) );
//        slideTruckText.setFill(Color.WHITE);
//        
//        
//        truckGroup.getChildren().add(truckImageView);
//        truckGroup.getChildren().add(frontWheelImageView);
//        truckGroup.getChildren().add(backWheelImageView);
//        truckGroup.getChildren().add(slideTruckText);
//        
//        
//        
//        dragAnchor = new Point2D(newXPosition, truckYPosition);
//        
//        frontTruckImageView = new ImageView(frontTruckImage);
//        
//        frontTruckGroup.getChildren().add(frontTruckImageView);
//        frontTruckGroup.getChildren().add(frontTruckBackWheelImageView);
//        
//        
//        frontTruckImageView.setPreserveRatio(true);
//        frontTruckImageView.setFitWidth(600);
//        frontTruckImageView.setX(-200);
//        frontTruckImageView.setY(truckYPosition + 1.2*backgroundOffsetY);
//        
////        background
//                
//        Circle circle = new Circle();
//        circle.setCenterX(400);
//        circle.setCenterY(500);
//        circle.setRadius(50.0f);
//        
//        
//        Path path = new Path();
//        
//         // milliseconds
//        
////        double xTranslation = 0;
////        double yTranslation = 0;
////        
////        for ( int i = 0 ; i < numFrames ; i++ ){
////            
////            xTranslation = (((double)i)/((double)numFrames))*backgroundImage.getWidth()/2;
////            System.out.println(xTranslation);
////            path.getElements().add( new LineTo( -backgroundImage.getWidth()/2 + xTranslation , yTranslation ) );
////            
////        }
//        
//        System.out.println("path.getElements().size()");
//        System.out.println(path.getElements().size());
//        
//        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(animationTime), backgroundImageView);
//        translateTransition.setFromX( 0 );
//        translateTransition.setToX( backgroundImage.getWidth()*(2./3) );
//        translateTransition.setCycleCount(Timeline.INDEFINITE);
//        translateTransition.setAutoReverse(false);
//        translateTransition.setInterpolator(Interpolator.LINEAR);
//        
//        translateTransition.play();
//        
//        TranslateTransition truckTranslateTransition = new TranslateTransition(Duration.millis(animationTime/4), truckGroup);
//        truckTranslateTransition.setFromX( 0 );
//        truckTranslateTransition.setToX( truckTransitionOffsetX );
//        truckTranslateTransition.setCycleCount(Timeline.INDEFINITE);
//        truckTranslateTransition.setAutoReverse(true);
////        truckTranslateTransition.setInterpolator(Interpolator.LINEAR);
//        
//        double rotationDuration = 500;
//        
//        RotateTransition rt = new RotateTransition(Duration.millis(rotationDuration), frontWheelImageView);
//        rt.setByAngle(-360);
//        rt.setCycleCount(Timeline.INDEFINITE);
//        rt.setAutoReverse(false);
//        rt.setInterpolator(Interpolator.LINEAR);
//
//        rt.play();
//        
//        rt = new RotateTransition(Duration.millis(rotationDuration), backWheelImageView);
//        rt.setByAngle(-360);
//        rt.setCycleCount(Timeline.INDEFINITE);
//        rt.setAutoReverse(false);
//        rt.setInterpolator(Interpolator.LINEAR);
//
//        rt.play();
//        
//        rt = new RotateTransition(Duration.millis(rotationDuration), frontTruckBackWheelImageView);
//        rt.setByAngle(-360);
//        rt.setCycleCount(Timeline.INDEFINITE);
//        rt.setAutoReverse(false);
//        rt.setInterpolator(Interpolator.LINEAR);
//
//        rt.play();
//        
//        FadeTransition ft = new FadeTransition(Duration.millis(500), slideTruckText);
//        ft.setFromValue(1.0);
//        ft.setToValue(0.0);
//        ft.setCycleCount(1);
//        ft.setAutoReverse(false);
//        
//        
//        class TruckInterpolator extends Interpolator {
//            @Override
//            protected double curve(double t) {
//        //        System.out.println("Interpolating");
//
//                setInterpolatorTruck(t);
////                System.out.println("t: "+t);
//        //        return Math.abs(0.5-t)*2 ;
//        //        WhatIsPlatooning.setInterpolatorTruck(t);
//        //        WhatIsPlatooning.updateText();
//                return t;
//            }
//        }
//        
//        class FrontTruckInterpolator extends Interpolator {
//            @Override
//            protected double curve(double t) {
//        //        System.out.println("Interpolating");
//
//
//        //        return Math.abs(0.5-t)*2 ;
//                setInterpolatorFrontTruck(t);
//        //        WhatIsPlatooning.updateText();
//                return t;
//            }
//        }
//        
////        AnimationBooleanInterpolator yInterp = new AnimationBooleanInterpolator();
//        TruckInterpolator truckInterpolator = new TruckInterpolator();
//        truckTranslateTransition.setInterpolator( truckInterpolator );
//        
//        truckTranslateTransition.play();
//        
//
//        
//        TranslateTransition frontTruckTranslateTransition = new TranslateTransition(Duration.millis(animationTime/3), frontTruckGroup);
//        frontTruckTranslateTransition.setFromX( 0 );
//        frontTruckTranslateTransition.setToX( frontTruckTransitionOffsetX );
//        frontTruckTranslateTransition.setCycleCount(Timeline.INDEFINITE);
//        frontTruckTranslateTransition.setAutoReverse(true);
////        truckTranslateTransition.setInterpolator(Interpolator.LINEAR);
//        
//        FrontTruckInterpolator frontTruckInterpolator = new FrontTruckInterpolator();
//        frontTruckTranslateTransition.setInterpolator( frontTruckInterpolator );
//        
//        frontTruckTranslateTransition.play();
//        
//        
//        
//        
//        TranslateTransition truckTranslateTransitionVertical = new TranslateTransition(Duration.millis(animationTime/3), truckGroup);
//        truckTranslateTransitionVertical.setFromY( 0 );
//        truckTranslateTransitionVertical.setToY( truckTransitionOffsetY );
//        truckTranslateTransitionVertical.setCycleCount(Timeline.INDEFINITE);
//        truckTranslateTransitionVertical.setAutoReverse(true);
//        
//        truckTranslateTransitionVertical.play();
//        
//        TranslateTransition frontTruckTranslateTransitionVertical = new TranslateTransition(Duration.millis(animationTime/2), frontTruckGroup);
//        frontTruckTranslateTransitionVertical.setFromY( 0 );
//        frontTruckTranslateTransitionVertical.setToY( frontTruckTransitionOffsetY );
//        frontTruckTranslateTransitionVertical.setCycleCount(Timeline.INDEFINITE);
//        frontTruckTranslateTransitionVertical.setAutoReverse(true);
//        
//        frontTruckTranslateTransitionVertical.play();
//        
//        
////        PathTransition pathTransition = new PathTransition();
////        pathTransition.setDuration(Duration.millis(animationTime));
////        pathTransition.setPath(path);
////        pathTransition.setNode(backgroundImageView);
//////        pathTransition.setCycleCount(Timeline.INDEFINITE);
////        pathTransition.setCycleCount(1);
////        pathTransition.setAutoReverse(false);
////        pathTransition.play();
//        
//        
//        initializeText();
//        
//        
//        
//        Rectangle r = new Rectangle();
//        fuelSavingsText = new Text();
//        fuelSavingsNumberText = new Text();
//        infoText = new Text();
//        infoTextNumber = new Text();
//        safeText = new Text();
//        
//        fuelGaugeImageView = new ImageView( new Image("file:fuelBar.jpg") );
//        fuelNeedleImageView = new ImageView( new Image("file:fuelNeedleCenter3.png") );
//        fuelGroup = new Group();
//        fuelGroup.getChildren().addAll(r, fuelGaugeImageView, fuelNeedleImageView, fuelSavingsText, fuelSavingsNumberText, infoText, infoTextNumber, safeText);        
//        
//        double fuelGaugeX = screenWidth - 400;
//        double fuelGaugeY;
////        double fuelGaugeWidth = 300;
//        double fuelGaugeHeight = - backgroundOffsetY/1.2;
////        fuelGaugeX = screenWidth - fuelGaugeWidth;
//        fuelGaugeY = screenHeight - fuelGaugeHeight - 30;
////        double fuelNeedleOffsetX = fuelGaugeWidth/2;
//        double fuelNeedleOffsetX;
//        double fuelNeedleOffsetY = -10;
//        double fuelNeedleWidth = 400;
//        
//        double rectangleOffsetY = -100;
//        r.setX(0);
////        r.setY( fuelGaugeY + rectangleOffsetY );
//        r.setY(screenHeight - fuelGaugeHeight);
//        r.setWidth(screenWidth);
////        r.setHeight(-rectangleOffsetY);
//        r.setHeight(fuelGaugeHeight);
//        
//        fuelGaugeImageView.setPreserveRatio(true);
////        fuelGaugeImageView.setFitWidth(fuelGaugeWidth);
//        fuelGaugeImageView.setFitHeight(fuelGaugeHeight);
//        fuelGaugeImageView.setX(fuelGaugeX);
//        fuelGaugeImageView.setY(fuelGaugeY);
//        
//        System.out.println("fuelGaugeImageView.getFitWidth():"+ fuelGaugeImageView.getFitWidth());
//        
//        fuelNeedleOffsetX = fuelGaugeImageView.getFitWidth();
//        
//        fuelNeedleImageView.setPreserveRatio(true);
//        fuelNeedleImageView.setFitWidth(fuelNeedleWidth);
////        fuelNeedleImageView.setX(fuelGaugeX + fuelGaugeImageView.getFitWidth()/2 - fuelNeedleImageView.getFitWidth()/2 );
//        fuelNeedleImageView.setX(fuelGaugeX + fuelGaugeImageView.getBoundsInParent().getWidth()/2 - fuelNeedleImageView.getBoundsInParent().getWidth()/2  );
////        double fuelNeedleOffsetY = fuelNeedleImageView.getFitWidth()/2;
//        fuelNeedleImageView.setY(fuelGaugeY  + fuelNeedleOffsetY - fuelNeedleImageView.getFitHeight()/2 );
//        
//        double fuelTextOffsetX = -325;
//        double fuelTextOffsetY = -backgroundOffsetY*(1/4.) + 25;
//        
//        fuelSavingsText.setText("");
////        fuelSavingsText.setX( fuelGaugeX + fuelTextOffsetX );
////        fuelSavingsText.setY( fuelGaugeY + fuelTextOffsetY );
//        fuelSavingsText.setX( fuelGaugeX + fuelTextOffsetX );
//        fuelSavingsText.setY( fuelGaugeY + fuelTextOffsetY );
//        fuelSavingsText.setFont(new Font( fontString , 50));
//        fuelSavingsText.setFill(Color.WHITE);
//        
//        double fuelTextNumberOffsetX = fuelTextOffsetX + 30;
//        double fuelTextNumberOffsetY = fuelTextOffsetY + 100;
//        
//        fuelSavingsNumberText.setText("Rui");
////        fuelSavingsNumberText.setX( fuelGaugeX + fuelTextNumberOffsetX );
////        fuelSavingsNumberText.setY( fuelGaugeY + fuelTextNumberOffsetY );
//        fuelSavingsNumberText.setX( fuelGaugeX + fuelTextNumberOffsetX );
//        fuelSavingsNumberText.setY( fuelGaugeY + fuelTextNumberOffsetY );
//        fuelSavingsNumberText.setFont(new Font( fontString , 80));
//        fuelSavingsNumberText.setFill(Color.WHITE);
//        
//        double infoTextOffsetX = -screenWidth*(7./10);
//        double infoTextOffsetY = -backgroundOffsetY*(1/4.) + 25;
//         
////        infoText.setText("Distance");
////        infoText.setX( fuelGaugeX + infoTextOffsetX );
////        infoText.setY( fuelTextOffsetY );
//        infoText.setX( fuelGaugeX + infoTextOffsetX );
//        infoText.setY( fuelGaugeY + infoTextOffsetY );
//        infoText.setFont(new Font( fontString , 50));
//        infoText.setFill(Color.WHITE);
//        
//        double infoTextNumberOffsetX = infoTextOffsetX + 20;
//        double infoTextNumberOffsetY = infoTextOffsetY + 100;
//        
//        infoTextNumber.setText("Distance");
////        infoText.setX( fuelGaugeX + infoTextNumberOffsetX );
////        infoText.setY( fuelTextOffsetY );
//        infoTextNumber.setX( fuelGaugeX + infoTextNumberOffsetX );
//        infoTextNumber.setY( fuelGaugeY + infoTextNumberOffsetY );
//        infoTextNumber.setFont(new Font( fontString , 70));
//        infoTextNumber.setFill(Color.WHITE);
//        
//        
//        double safeTextOffsetX = -screenWidth*(2.3/5);
//        double safeTextOffsetY = -backgroundOffsetY*(13/20.) ;
//        
//        safeText.setText("  SAFE  ");
////        infoText.setX( fuelGaugeX + infoTextNumberOffsetX );
////        infoText.setY( fuelTextOffsetY );
//        safeText.setX( fuelGaugeX + safeTextOffsetX );
//        safeText.setY( fuelGaugeY + safeTextOffsetY );
//        safeText.setFont(new Font( fontString , 150));
//        safeText.setFill(Color.GREEN);
//        
//        
//        FadeTransition ftBlinkInitial = new FadeTransition(Duration.millis(400), slideTruckText);
//        ftBlinkInitial.setFromValue(1.0);
//        ftBlinkInitial.setToValue(0.7);
//        ftBlinkInitial.setCycleCount(Timeline.INDEFINITE);
//        ftBlinkInitial.setAutoReverse(true);
//
////        ftBlinkInitial.play();
//        
////        rt = new RotateTransition(Duration.millis(1000), fuelNeedleImageView);
////        rt.setByAngle(-360);
////        rt.setCycleCount(Timeline.INDEFINITE);
////        rt.setAutoReverse(false);
////        rt.setInterpolator(Interpolator.LINEAR);
////
////        rt.play();
//        
//        
//        root.getChildren().add(backgroundImageView);
//        root.getChildren().add(truckGroup);
//        root.getChildren().add(frontTruckGroup);
//        root.getChildren().add(fuelGroup);
////        root.getChildren().add(circle);
//        
//
//        initX = newXPosition;
//        initY = newYPosition;
//
//        dragAnchor = new Point2D(400, 0);
//        
//        
////        truckImageView.setOnDragDetected((MouseEvent me) ->
//////        truckImageView.setOnMouseDragEntered((me) ->
//////        truckImageView.setOnMouseDragged((me) ->
////        {
////            
////            System.out.println("Mouse pressed on truck");
////            
////            dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
////            initX = truckImageView.getTranslateX();
////            initY = truckImageView.getTranslateY();
////            
////            
////            
////
////        });
//        
//        
//        
//
//        truckGroup.setOnMousePressed((me) ->
////        truckGroup.setOnDragDetected((me) ->
//
//        {
//            
////            System.out.println("\n\n\n\n\n");
////            System.out.println("Mouse clicked on truck");
//            
//            dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
//            
////            System.out.println("me.getSceneX(): "+ me.getSceneX() );
//            
//            if (firstTime){
//            
////                initX = truckImageView.getTranslateX();
////                initY = truckImageView.getTranslateY();
//                
//                firstTime = false;
//                
////                FadeTransition ft = new FadeTransition(Duration.millis(5000), slideTruckText);
////                ft.setFromValue(1.0);
////                ft.setFromValue(0.0);
////                
////                ft.play();
//                
//                
//
////                ftBlinkInitial.stop();
////                ftBlinkInitial.s
//                ft.play();
//                
//                textShowing = false;
//                startTime = System.nanoTime();
//                
////                slideTruckText.setOpacity(0.0);
//            
//            }
//            
//
//        });
//        
//        
//        truckGroup.setOnMouseDragged((MouseEvent me) ->
////        truckGroup.setOnDragDetected((MouseEvent me) ->
//        {
//            
////            System.out.println("Mouse dragged on truck");
////            System.out.println("me.getSceneX(): "+ me.getSceneX() );
//            double dragX = me.getSceneX() - dragAnchor.getX();
//            double dragY = me.getSceneY() - dragAnchor.getY();
//            
//            //calculate new position of the circle
//            newXPosition = initX + dragX ;
//            newYPosition = initY + dragY ;
//            
//
//
//            
////            truckImageView.setTranslateX(newXPosition);
//            truckImageView.setX(newXPosition);
////            frontWheelImageView.setTranslateX(newXPosition);
//            frontWheelImageView.setX(newXPosition + frontWheelOffsetX);
//            backWheelImageView.setX(newXPosition + backWheelOffsetX);
//            slideTruckText.setX(newXPosition + 200);
////            truckGroup.setX(newXPosition);
//            
//            updateText();
//            
//            
//            
//        });
//        
//        truckGroup.setOnMouseReleased((MouseEvent me) ->
//        {
//
////            System.out.println("Mouse released truck");
//            
//            initX = newXPosition;
//            initY = newYPosition;
//            
//            dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
//
//        });
//
//        
//        FadeTransition ftInvAppear = new FadeTransition(Duration.millis(500), slideTruckText);
//        ftInvAppear.setFromValue(0.0);
//        ftInvAppear.setToValue(1.0);
//        ftInvAppear.setCycleCount(1);
//        ftInvAppear.setAutoReverse(false);
//        
//        FadeTransition ftBlink = new FadeTransition(Duration.millis(400), slideTruckText);
//        ftBlink.setFromValue(1.0);
//        ftBlink.setToValue(0.6);
//        ftBlink.setCycleCount(Timeline.INDEFINITE);
//        ftBlink.setAutoReverse(true);
//        
//        ftInv.getChildren().add(ftInvAppear);
//        ftInv.getChildren().add(ftBlink);
//        
//        
//        double offsetTime = 0;
//        double offsetY = 0;
//        
//        
//        int randomOffset = (int) ( ((screenWidth/numSnowFlakes)/2.) +.5);
//        
//
//        
//        double turbulenceRandomOffsetX;
//        double turbulenceRandomOffsetY;
//        
//        double noTurbulenceWidth = 50;
//        double turbulenceOffsetX = -50;
//        
//        double turbulenceRandomRadiusX = 100;
//        double turbulenceRandomRadiusY = truckImageView.getBoundsInLocal().getHeight() - 2* noTurbulenceWidth;
//        
//        double airDragElementRadius = 20;
//        
//        for ( int i = 0; i < numTurbulenceSnowFlakes ; i++){
//        
////            offsetTime = rand.nextInt(50);
////            offsetTime = 0;
//            
//            turbulenceRandomOffsetX = turbulenceRandomRadiusX*rand.nextDouble() - turbulenceRandomRadiusX/2. + turbulenceOffsetX;
//            turbulenceRandomOffsetY = turbulenceRandomRadiusY*rand.nextDouble() - turbulenceRandomRadiusY/2. - .5*noTurbulenceWidth;
//            
//            
//            double tempPositionPhaseOffset = rand.nextDouble()*Math.PI;
//            
//            ImageView tempSnowflakeImageView = new ImageView(snowflakeImage);
//            root.getChildren().add(tempSnowflakeImageView);
//            createTurbulenceAnimation(tempSnowflakeImageView, i, (int) (turbulenceRandomOffsetX+0.5) , (int) (turbulenceRandomOffsetY+0.5) , tempPositionPhaseOffset);
//            
////            Circle airDragElement = new Circle(airDragElementRadius, Color.WHITE);
////            root.getChildren().add(airDragElement);
////            createTurbulenceAnimation(airDragElement, i, (int) (turbulenceRandomOffsetX+0.5) , (int) (turbulenceRandomOffsetY+0.5) , tempPositionPhaseOffset);
//        
//        }
//        
//        
//
//        
//        
//        for ( int i = 0; i < numSnowFlakes ; i++){
//        
////            offsetTime = rand.nextInt(50);
////            offsetTime = 0;
//            
//            offsetY = 50*rand.nextDouble() - 25;
//            
//            ImageView tempSnowflakeImageView = new ImageView(snowflakeImage);
//            root.getChildren().add(tempSnowflakeImageView);
//            createAirDragAnimation(tempSnowflakeImageView, i, offsetY);
//            
////            Circle airDragElement = new Circle(airDragElementRadius, Color.WHITE);
////            root.getChildren().add(airDragElement);
////            createAirDragAnimation(airDragElement, i, offsetY);
//        
//        }
//            
//        primaryStage.setScene(scene);
//        primaryStage.setTitle("What is Platooning");
//        primaryStage.show();
//        
////        weirdDijkstraPrototype();
//    }
//    
//    private void initializeText(){
//        
//        double distance = newXPosition - ( -200 + 600);
//        distance = distance * meterPerPixel;
//        
////        infoText = new Text(800, 600, "The distance is "+distance);
//        
//        
//        
//        
//        
//        
////        System.out.println("newXPosition: "+newXPosition+" distance: "+distance);
//        
//    }
//    
//    static public void setInterpolatorFrontTruck(double fraction){
//        
//        endTime = System.nanoTime();
//
//        duration = (endTime - startTime)/1000000;
////        System.out.println(duration);
//        
//        
//        if (duration > 10000){
//            
//            if ( !textShowing ){
//                ftInv.play();
//                textShowing = true;
//            }
//            firstTime = true;
//            
//            
//        }
//        
//            
//        frontTruckFraction = fraction;
////        System.out.println("fraction: "+fraction);
////        System.out.println("Interpolating front Truck");
//        updateText();
//        
//    }
//    
//    static public void setInterpolatorTruck(double fraction){
//        
//        truckFraction = fraction;
////        System.out.println("fraction: "+fraction);
//        updateText();
//        
//    }
//    
//    public void createAirDragAnimation(ImageView argSnowflakeImageView, double offsetTime, double offsetY){
////    public void createAirDragAnimation(Circle argSnowflakeImageView, double offsetTime, double offsetY){
////        System.out.println("\n\n createAirDragAnimation");
//        
////        snowflakeImageView = new ImageView(snowflakeImage);
////        root.getChildren().add(snowflakeImageView);
//        
//        Timeline tempAirDragAnimation = new Timeline();
////        airDragAnimation = new Timeline();
//        
//        double truckY = frontTruckImageView.getY() - 25;
//        double frontTruckEndX = frontTruckImageView.getBoundsInLocal().getMaxX();
//        
//        double truckX = truckImageView.getX();
//        double truckEndX = truckImageView.getBoundsInLocal().getMaxX();
//               
////        System.out.println("truckY:" +truckY);
////        System.out.println("frontTruckEndX:" +frontTruckEndX);
////        System.out.println("truckX:" +truckX);
//        
//        double startingX = -100;
//        double endingX = screenWidth + 100;
//        double stepX = endingX - startingX;
//        
//        double airDragResolution = 50;
//        double airDragTime = 500;
//        
//        
//        double timeOffset = (int) ( airDragTime*(offsetTime/numSnowFlakes) + .5 );
//        timeOffset = timeOffset + 50.*rand.nextDouble();
////        double timeOffset = 0;
//        
//        double currentX = 0, currentY = 0, currentTime = 0;
//        
//        double maxAirDragHole = 200 ;
//        double airDragHole = 0.5*maxAirDragHole * ( ((truckX - frontTruckEndX)* meterPerPixel) /5.);
//        double maxAirDragWidth = 300;
//        
//        
////        airDragAnimation.getKeyFrames().add(textKeyFrame);
////        System.out.println("\n\n\n\n\n\n");
//        for ( int i = 0 ; i < airDragResolution ; i++ ){
//            
//            currentTime = airDragTime*(i/airDragResolution);
//            currentX = (i/airDragResolution)*stepX + startingX;
//            
//            // I CAN REMOVE THIS IF IF I CREATE INSTEAD THREE LOOPS!!!!!!
//            if ( currentX < frontTruckEndX ){
//                
//                currentY = truckY;
//                
//            }else if ( currentX < truckX ){
//                
////                airDragHole = 50*(truckX - frontTruckEndX) ;
////                airDragHole = 100;
//                currentY = truckY + airDragHole*(1 - Math.cos( (2*Math.PI)*( (currentX - frontTruckEndX)/( truckX - frontTruckEndX ) ) ) );
//                
////                System.out.println("currentY:"+currentY);
////                System.out.println("truckY:"+truckY);
////                System.out.println("currentTime: "+currentTime);
////                System.out.println("(currentX - frontTruckEndX)/( truckX - frontTruckEndX ): " + (currentX - frontTruckEndX)/( truckX - frontTruckEndX ));
////                System.out.println("Math.cos( (2*Math.PI)*( (currentX - frontTruckEndX)/( truckX - frontTruckEndX ) ): " +
////                        Math.cos( (2*Math.PI)*( (currentX - frontTruckEndX)/( truckX - frontTruckEndX ) ) ) );
//                
//            }else if ( currentX < truckEndX){
//                
//                currentY = truckY;
//                
//            }else if ( currentX < truckEndX + maxAirDragWidth){ 
//                
//                currentY = truckY + maxAirDragHole*( Math.sin( (0.5*Math.PI)*( (currentX - truckEndX)/( maxAirDragWidth ) ) ) );
//                
////                System.out.println("(currentX - truckEndX)/( maxAirDragWidth ): " + (currentX - truckEndX)/( maxAirDragWidth ) );
//                
//            }else{
//                
//                currentY = truckY + maxAirDragHole;
//                
//            }
//            
//            
////            KeyValue kvX = new KeyValue(snowflakeImageView.translateXProperty(), currentX);
////            KeyValue kvY = new KeyValue(snowflakeImageView.translateYProperty(), currentY - 25);
//            KeyValue kvX = new KeyValue(argSnowflakeImageView.translateXProperty(), currentX );
//            KeyValue kvY = new KeyValue(argSnowflakeImageView.translateYProperty(), currentY - 25 + offsetY );
//            KeyValue kOpacity;
//            
//            if( currentX < 0 || currentX > screenWidth ){
//            
//                kOpacity = new KeyValue( argSnowflakeImageView.opacityProperty() , 0.0);
//                
//            }else{
//                
//                kOpacity = new KeyValue(argSnowflakeImageView.opacityProperty(), snowFlakeOpacity);
//                
//            }
//            
//            KeyFrame kf = new KeyFrame(Duration.millis( currentTime + timeOffset ), kvX);
//            tempAirDragAnimation.getKeyFrames().add(kf);  
//            kf = new KeyFrame(Duration.millis( currentTime + timeOffset ), kvY);
//            tempAirDragAnimation.getKeyFrames().add(kf);  
//            kf = new KeyFrame(Duration.millis( currentTime + timeOffset ), kOpacity);
//            tempAirDragAnimation.getKeyFrames().add(kf);  
//            
//            if ( i == 0){
//                
////                System.out.println("First keyframe:");
////                System.out.println("currentX: "+ currentX );
////                System.out.println("currentY - 25 + offsetY: "+ (currentY - 25 + offsetY) );
////                System.out.println("currentTime + timeOffset: " + (currentTime + timeOffset) );
//                
//            }
//            
//        }
//        
////        System.out.println("Last keyframe:");
////        System.out.println("currentX: "+ currentX );
////        System.out.println("currentY - 25 + offsetY: "+ (currentY - 25 + offsetY) );
////        System.out.println("currentTime + timeOffset: " + (currentTime + timeOffset) );
//        
//        tempAirDragAnimation.setCycleCount(1);
//        tempAirDragAnimation.setAutoReverse(false);
//        
//        tempAirDragAnimation.setOnFinished((ActionEvent event) -> {
//            
//            tempAirDragAnimation.pause();
//            createAirDragAnimation(argSnowflakeImageView, offsetTime, offsetY);
//            
////            if ( realTimeVisualisationExited == true ){
////                
////                return;
////                
////            }
////            
////           BB.changeLabel("skip","replay");
////            buttonsEvents(buttonsHB);
////            mainRoot.getChildren().removeAll(platooningText);
////            showSavedPercentage();
////            showFinalStatsMenu();
//            
//       });
//        
//        
//        tempAirDragAnimation.play();
//        
//        
//    }
//  
//    
//    
//    
//    
//    public void createTurbulenceAnimation(ImageView argSnowflakeImageView, int index, int positionX, int positionY, double phaseOffset){
////    public void createTurbulenceAnimation(Circle argSnowflakeImageView, int index, int positionX, int positionY, double phaseOffset){    
////        System.out.println("\n\n createAirDragAnimation");
//        
////        snowflakeImageView = new ImageView(snowflakeImage);
////        root.getChildren().add(snowflakeImageView);
//        
////        double airDragHole = 0.5*maxAirDragHole * ( ((truckX - frontTruckEndX)* meterPerPixel) /5.);
//        double currentDistance = ( truckImageView.getBoundsInLocal().getMinX() - frontTruckImageView.getBoundsInLocal().getMaxX() )* meterPerPixel;
//        
//        double ratioShowing = currentDistance/5;
//        if (ratioShowing > 1){
//            
//            ratioShowing = 1;
//            
//        }
//            
//        ratioShowing = 1 - ratioShowing; 
//        
//        double currentIndexFraction = ((double)index)/((double)numTurbulenceSnowFlakes);
//        
//        if ( currentIndexFraction > ratioShowing){
//            
//            argSnowflakeImageView.setOpacity(snowFlakeOpacity);
//            
//        }else{
//            
//            argSnowflakeImageView.setOpacity(0.0);
//            
//        }
//        
//        
//        Timeline turbulenceAnimation = new Timeline();
////        airDragAnimation = new Timeline();
//        
//        
//        
//        
//        
//        double truckX = truckImageView.getX() - 50;
//        double truckY = truckImageView.getBoundsInLocal().getMinY() + truckImageView.getBoundsInLocal().getHeight()/2. ;
//        
//        double turbulenceWidth = 75;
//        double turbulenceHeight = frontTruckImageView.getBoundsInLocal().getHeight();
//        double turbulenceRadius = 15;
//        
//        double turbulenceResolution = 50;
//        double turbulenceTime = 500;
//        
//        
//        
//        double currentTime;
//        double currentX;
//        double currentY;
//        
//        
//        for ( int i = 0 ; i < turbulenceResolution ; i++ ){
//            
//            
//            currentTime = turbulenceTime*(i/turbulenceResolution);
//            currentX = truckX + positionX + turbulenceRadius*Math.cos( 2*Math.PI*(currentTime/turbulenceTime + phaseOffset) );
//            currentY = truckY + positionY + turbulenceRadius*Math.sin( 2*Math.PI*(currentTime/turbulenceTime + phaseOffset) );
//            
//            
//            KeyValue kvX = new KeyValue(argSnowflakeImageView.translateXProperty(), currentX );
//            KeyValue kvY = new KeyValue(argSnowflakeImageView.translateYProperty(), currentY );
//            
//            KeyFrame kf = new KeyFrame(Duration.millis( currentTime  ), kvX);
//            turbulenceAnimation.getKeyFrames().add(kf);  
//            kf = new KeyFrame(Duration.millis( currentTime ), kvY);
//            turbulenceAnimation.getKeyFrames().add(kf);  
//                        
//            
//            
//        }
//        
//        
//        
//        turbulenceAnimation.setCycleCount(1);
//        turbulenceAnimation.setAutoReverse(false);
//        
//        turbulenceAnimation.setOnFinished((ActionEvent event) -> {
//            
//            turbulenceAnimation.pause();
//            createTurbulenceAnimation(argSnowflakeImageView, index, positionX, positionY, phaseOffset);
//            
////            if ( realTimeVisualisationExited == true ){
////                
////                return;
////                
////            }
////            
////           BB.changeLabel("skip","replay");
////            buttonsEvents(buttonsHB);
////            mainRoot.getChildren().removeAll(platooningText);
////            showSavedPercentage();
////            showFinalStatsMenu();
//            
//       });
//        
//        
//        turbulenceAnimation.play();
//        
//        
//    }
//    
//    
//    
//    
//    static public void updateText(){
//        
//        double distance;
////        distance = newXPosition - ( -200 + 600);
//        distance = ( newXPosition + truckFraction*truckTransitionOffsetX ) - ( -200 + 600 + frontTruckFraction*frontTruckTransitionOffsetX);
//        distance = distance * meterPerPixel;
//        
////        System.out.println( truckImageView.getX() );
////        System.out.println("newXPosition: "+newXPosition);
////        System.out.println("Updating text to:" + distance);
//        
////        infoText = Text(800, 600, "The distance is "+distance);
////        infoText.setText("The distance is "+distance);
////        infoText.setText("Distance to truck ahead: " + df.format(distance) + " metres.");'
//        
//        double backTruckForwardX = ( newXPosition + truckFraction*truckTransitionOffsetX );
//        double frontTruckBackwardX = ( -200 + 600 + frontTruckFraction*frontTruckTransitionOffsetX);
//        distance = backTruckForwardX - frontTruckBackwardX;
//        distance = distance * meterPerPixel;
////        infoText.setText("Distance to truck: " + dfMeters.format(distance) + " metres");
//        infoText.setText("Distance to truck:" );
//        
//        
//        
//        if ( distance > 0 ){
//            
////            infoText.setFill(Color.WHITE);
//            safeText.setText("  SAFE  ");
//            safeText.setFill(Color.GREEN);
//            
//            infoTextNumber.setText(dfMeters.format(distance) + " metres");
//            
//        }else{
//            
////            infoText.setFill(Color.RED);
//            safeText.setText(" CRASH  ");
//            safeText.setFill(Color.RED);
//            
//            infoTextNumber.setText(dfMeters.format(0) + " metres");
//            
//        }
//        
//        updateFuel(distance);
//        
////        System.out.println("newXPosition: "+newXPosition+" distance: "+distance);
//    }
//    
//    static public void updateFuel(double distance){
//        
//        if (distance < 0){
//            
//            distance = 0;
//        
//        }
//        
//        double savings = Math.exp(5-distance)/6;
//        
//        double degOffset = -125;
//        double maxAngle = 130;
//        double currentAngle = (savings/25)*maxAngle;
//        
//        if ( currentAngle > 120 ){
//            
//            currentAngle = 120;
//            
//        }
//        
//        if ( currentAngle < 14 ){
//            
//            currentAngle = 14;
//            
//        }
//        
//        fuelNeedleImageView.setRotate( degOffset + currentAngle );
//        
//        
//        
//        fuelSavingsText.setText( "Fuel Saving:" );
//        fuelSavingsNumberText.setText( dfFuelSavings.format(savings)+"%" );
//        
//                
//    }
//    
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        launch(args);
//    }
//    
//}
