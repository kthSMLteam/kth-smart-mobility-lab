function varargout = control(varargin)
% CONTROL MATLAB code for control.fig
%      CONTROL, by itself, creates a new CONTROL or raises the existing
%      singleton*.
%
%      H = CONTROL returns the handle to a new CONTROL or the handle to
%      the existing singleton*.
%
%      CONTROL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CONTROL.M with the given input arguments.
%
%      CONTROL('Property','Value',...) creates a new CONTROL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before control_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to control_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help control

% Last Modified by GUIDE v2.5 12-Nov-2014 12:46:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @control_OpeningFcn, ...
                   'gui_OutputFcn',  @control_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end


% End initialization code - DO NOT EDIT


% --- Executes just before control is made visible.
function control_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to control (see VARARGIN)

% Choose default command line output for control

global auto stop first gauge_k gauge_m pointer handles_v;

handles.output = hObject;
load_system('tekniska_cs')

addpath('JoyMEX/Matlab')

addpath gauge

auto=false;
stop=true;
first=true;

gauge_k = imread('gauge_kmh.png');
gauge_m = imread('gauge_mps.png');
pointer = imread('pointer_big_black.png');

set(handles.stop,'Visible','off');
set(handles.platoon_v,'Visible','off');
set(handles.platoon_text,'Visible','off');
set(handles.driver_v,'Visible','off');
set(handles.driver_text,'Visible','off');
set(handles.speed_text,'Visible','off');
set(handles.speed_title,'Visible','off');
set(handles.distance_text,'Visible','off');
set(handles.speed_title,'Visible','off');
set(handles.driver_d,'Visible','off');
set(handles.platoon_d,'Visible','off');
set(handles.speed,'Visible','off');
set(handles.time,'Visible','off');
set(handles.text,'Visible','off');
set(handles.meters,'Visible','off');
set(handles.plusone,'Visible','off');
set(handles.plusten,'Visible','off');
set(handles.minusten,'Visible','off');
set(handles.minusone,'Visible','off');
set(handles.driver_axis,'Visible','off');
set(handles.platoon_axis,'Visible','off');

set(handles.distance_text,'Value',1);
set(handles.time,'Value',0);
set(handles.distance_text,'BackgroundColor',[0 0.498 0]);
set(handles.time,'BackgroundColor',[0 0 0]);

set(handles.meters,'Value',0)
set(handles.speed_text,'Value',0)
set(handles.meters,'BackgroundColor',[0 0.498 0]);
set(handles.speed_text,'BackgroundColor',[0 0 0]);

%set(handles.figure1,'Position',[1925 665 941 496]);

% set(handles.manual_button,'String','AUTO');
% set(handles.manual_button,'FontSize',15);

addpath winontop ;
%p = get(0,'monitorpositions');
set(gcf,'Visible','on');
WinOnTop(gcf);
% set(handles.figure1,'Position',[1923 668 960 531]);
handles_v=handles;
movegui( handles_v.figure1,[0 535]);
guidata(hObject, handles);



% set(gcf,'Position',[2888 132 941 496]);



% UIWAIT makes control wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = control_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
clear JoyMEX

varargout{1} = handles.output;


% --- Executes on button press in start.
function start_Callback(~, ~, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global stop auto speed handles_v
%stop = false;
movegui( handles_v.figure1,[0 535]);

try
    clear JoyMex
end
JoyMEX('init',0);

if auto
    set(handles.speed,'Visible','off');
    set(handles.speed_title,'Visible','off');
    set(handles.plusone,'Visible','off');
    set(handles.plusten,'Visible','off');
    set(handles.minusten,'Visible','off');
    set(handles.minusone,'Visible','off');
    set(handles.manual_button,'Visible','off');
    set(handles.start,'Visible','off')
end

speed=0;

%t = tcpip('localhost', 50002, 'NetworkRole', 'server')
%fopen(t)

%sim('tekniska_cs');

%set_param('tekniska_cs/Actros_1851_1/Constant1','Value','0');

% echotcpip('on',4000)
% t= tcpip('localhost', 4000);
% fopen(t)

set_param('tekniska_cs','SimulationCommand','Start')

%movegui(gcf,[2888 132]);
drawnow

% --- Executes on button press in stop.
function stop_Callback(~, ~, handles)
% hObject    handle to stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global stop
stop = true;

clear JoyMEX

set_param('tekniska_cs','SimulationCommand','Stop')

set(handles.manual_button,'Visible','on');
set(handles.start,'Visible','on')
set(handles.stop,'Visible','off');
set(handles.platoon_v,'Visible','off');
set(handles.platoon_text,'Visible','off');
set(handles.driver_v,'Visible','off');
set(handles.driver_text,'Visible','off');
set(handles.speed_text,'Visible','off');
set(handles.speed_title,'Visible','off');
set(handles.distance_text,'Visible','off');
set(handles.speed_title,'Visible','off');
set(handles.driver_d,'Visible','off');
set(handles.platoon_d,'Visible','off');
set(handles.speed,'Visible','off');
set(handles.time,'Visible','off');
set(handles.meters,'Visible','off');
set(handles.plusone,'Visible','off');
set(handles.plusten,'Visible','off');
set(handles.minusten,'Visible','off');
set(handles.minusone,'Visible','off');
set(handles.driver_axis,'Visible','off');
set(handles.platoon_axis,'Visible','off');
cla(handles.platoon_axis)
cla(handles.driver_axis)

set(handles.driver_d,'String','1000');
set(handles.platoon_d,'String','1000');

function speed_Callback(hObject, eventdata, handles)
% hObject    handle to speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of speed as text
%        str2double(get(hObject,'String')) returns contents of speed as a double
global speed;
speed=str2double(get(handles.speed,'String'));


% --- Executes on button press in manual_button.
function manual_button_Callback(hObject, eventdata, handles)
% hObject    handle to manual_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global auto stop
% if auto
%     auto=false;
%     if ~stop
%         set(handles.speed,'Visible','on');
%         set(handles.speed_title,'Visible','on');
%         set(handles.plusone,'Visible','on');
%         set(handles.plusten,'Visible','on');
%         set(handles.minusten,'Visible','on');
%         set(handles.minusone,'Visible','on');
%     end
%     set(handles.manual_button,'String','AUTO');
%     set(handles.manual_button,'FontSize',15);
% 
%     set(handles.manual_button,'BackgroundColor',[0 0 0]);
% else
%     auto=true;
%     set(handles.manual_button,'BackgroundColor',[0.847 0.161 0]);
%     set(handles.speed,'Visible','off');
%     set(handles.speed_title,'Visible','off');
%     set(handles.plusone,'Visible','off');
%     set(handles.plusten,'Visible','off');
%     set(handles.minusten,'Visible','off');
%     set(handles.minusone,'Visible','off');
%     set(handles.manual_button,'String','MANUAL');
%     set(handles.manual_button,'FontSize',13);
% end  


% --- Executes on button press in plusten.
function plusten_Callback(hObject, eventdata, handles)
% hObject    handle to plusten (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global speed
speed=min(speed+10,100);
set(handles.speed,'String',num2str(speed));


% --- Executes on button press in plusone.
function plusone_Callback(hObject, eventdata, handles)
% hObject    handle to plusone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global speed
speed=min(speed+1,100);
set(handles.speed,'String',num2str(speed));

% --- Executes on button press in minusone.
function minusone_Callback(hObject, eventdata, handles)
% hObject    handle to minusone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global speed
speed=max(speed-1,0);
set(handles.speed,'String',num2str(speed));

% --- Executes on button press in minusten.
function minusten_Callback(hObject, eventdata, handles)
% hObject    handle to minusten (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global speed
speed=max(speed-10,0);
set(handles.speed,'String',num2str(speed));

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
%stop = true;
%set_param('tekniska_cs','SimulationCommand','Stop')
delete(hObject);

% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
drawnow

% --- Executes on button press in distance_text.
function distance_text_Callback(hObject, eventdata, handles)
% hObject    handle to distance_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of distance_text
set(handles.time,'Value',0)
set(handles.distance_text,'BackgroundColor',[0 0.498 0]);
set(handles.time,'BackgroundColor',[0 0 0]);

% --- Executes on button press in time.
function time_Callback(hObject, eventdata, handles)
% hObject    handle to time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of time
set(handles.distance_text,'Value',0)
set(handles.time,'BackgroundColor',[0 0.498 0]);
set(handles.distance_text,'BackgroundColor',[0 0 0]);

% --- Executes on button press in speed_text.
function speed_text_Callback(hObject, eventdata, handles)
% hObject    handle to speed_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of speed_text
set(handles.meters,'Value',0)
set(handles.speed_text,'BackgroundColor',[0 0.498 0]);
set(handles.meters,'BackgroundColor',[0 0 0]);

% --- Executes on button press in meters.
function meters_Callback(hObject, eventdata, handles)
% hObject    handle to meters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of meters
set(handles.speed_text,'Value',0)
set(handles.meters,'BackgroundColor',[0 0.498 0]);
set(handles.speed_text,'BackgroundColor',[0 0 0]);

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global handles_v
movegui( handles_v.figure1,[0 535]);

% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global handles_v
movegui( handles_v.figure1,[0 535]);
