function msfcn_driver(block)
% Handles input to the gui panel that displays the map and updates it.

setup(block);
%endfunction

function setup( block )


%% define number of input and output ports
block.NumInputPorts  = 3;
block.NumOutputPorts = 0;


%% port properties
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

block.InputPort(1).Complexity = 'real'; % x
block.InputPort(1).DataTypeId = 0; %real
block.InputPort(1).SamplingMode = 'Sample';
block.InputPort(1).Dimensions = 1;

block.InputPort(2).Complexity = 'real'; % y
block.InputPort(2).DataTypeId = 0; %real
block.InputPort(2).SamplingMode = 'Sample';
block.InputPort(2).Dimensions = 1;

block.InputPort(3).Complexity = 'real'; % speed
block.InputPort(3).DataTypeId = 0; %real
block.InputPort(3).SamplingMode = 'Sample';
block.InputPort(3).Dimensions = 1;


%% Register parameters
block.NumDialogPrms     = 1;
dfreq = block.DialogPrm(1).Data;

%% block sample time
block.SampleTimes = [1/dfreq 0];

%% register methods
block.RegBlockMethod('PostPropagationSetup', @DoPostPropSetup);
block.RegBlockMethod('Start', @Start);
block.RegBlockMethod('Outputs', @Outputs);

%% Block runs on TLC in accelerator mode.
block.SetAccelRunOnTLC(true);
%endfunction

function DoPostPropSetup( block )
% block.NumDworks = 1;
% block.Dwork(1).Name = 'handles';
% block.Dwork(1).Dimensions = 3;
% block.Dwork(1).DatatypeID = 0;
% block.Dwork(1).Complexity = 'Real';
%endfunction

function Start(block)

%block.OutputPort(1).Data = 1;
%if block.DialogPrm(2).Data % Display switch ON / OFF
%     try
%         close (map)
%     end

%h_dlg        = control;
% global handle_v;
% h_dlg = handle_v.figure1;
% h_speed      = findobj(h_dlg, 'Tag', 'driver_v');
% h_distance      = findobj(h_dlg, 'Tag', 'driver_d');
% block.Dwork(1).Data = [h_dlg h_speed h_distance];
%endfunction Start
%end

function Outputs(block)

% h_dlg        = block.Dwork(1).Data( 1);
% h_speed        = block.Dwork(1).Data(2);
% h_distance  = block.Dwork(1).Data(3);
%h_x      = block.Dwork(1).Data( 2);
%h_y     = block.Dwork(1).Data( 3);
%cla(h_axes);
%set(h_axes,'NextPlot', 'add');
%set(h_axes, 'YTick', []);
%set(h_axes, 'XTick', []);

global handles_v stop first time_p time_d

%movegui(gcf,[2888 132]);


if ~stop
    set(handles_v.manual_button,'Visible','off');
    set(handles_v.start,'Visible','off');
    set(handles_v.stop,'Visible','on');
    set(handles_v.driver_v,'Visible','off');
    set(handles_v.driver_text,'Visible','on');
    set(handles_v.speed_text,'Visible','on');
    set(handles_v.distance_text,'Visible','on');
    set(handles_v.driver_d,'Visible','on');
    set(handles_v.time,'Visible','on');
    set(handles_v.meters,'Visible','on');
    set(handles_v.text,'Visible','on');
end


x=block.InputPort(1).Data;
y=block.InputPort(2).Data;
driver_v=block.InputPort(3).Data;

x_inter=74.69;
y_inter=157.49;

d_inter=sqrt((x-x_inter)^2+(y-y_inter)^2);


time_d = d_inter/driver_v; 

if get(handles_v.time,'Value')
    d_inter=d_inter/driver_v;
end

if get(handles_v.speed_text,'Value')
    velocity = driver_v*3.6;
    set(handles_v.driver_v,'String',num2str(driver_v*3.6));
    drawGauge(velocity,handles_v.driver_axis,'kmh')
else
    velocity = driver_v;
    set(handles_v.driver_v,'String',num2str(driver_v));
    drawGauge(velocity,handles_v.driver_axis,'mps')
end

if (x-x_inter)>0
    set(handles_v.driver_d,'String','')    
else
    if get(handles_v.time,'Value')
        d = [sprintf('%.1f',d_inter),' s'];
    else
        d = [sprintf('%.0f',d_inter),' m'];
    end    
    set(handles_v.driver_d,'String',d)
end

if stop && ~first
    set(handles_v.manual_button,'Visible','on');
    set(handles_v.start,'Visible','on');
    set(handles_v.stop,'Visible','off');
    set(handles_v.platoon_v,'Visible','off');
    set(handles_v.platoon_text,'Visible','off');
    set(handles_v.driver_v,'Visible','off');
    set(handles_v.driver_text,'Visible','off');
    set(handles_v.speed_text,'Visible','off');
    set(handles_v.speed_title,'Visible','off');
    set(handles_v.distance_text,'Visible','off');
    set(handles_v.speed_title,'Visible','off');
    set(handles_v.driver_d,'Visible','off');
    set(handles_v.platoon_d,'Visible','off');
    set(handles_v.speed,'Visible','off');
    set(handles_v.time,'Visible','off');
    set(handles_v.meters,'Visible','off');
    set(handles_v.plusone,'Visible','off');
    set(handles_v.plusten,'Visible','off');
    set(handles_v.minusten,'Visible','off');
    set(handles_v.minusone,'Visible','off');
    set(handles_v.driver_axis,'Visible','off');
    set(handles_v.platoon_axis,'Visible','off');
    cla(handles_v.platoon_axis)
    cla(handles_v.driver_axis)
    
    set(handles_v.driver_d,'String','1000');
    set(handles_v.platoon_d,'String','1000');
end

first=false;

drawnow


%endfunction Outputs
