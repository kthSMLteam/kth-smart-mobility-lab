function rotate_wheel(hIn,wheel,hleft,hright)

try
    clear JoyMex
end

JoyMEX('init',0);

while 1
    [a ab] = JoyMEX(0);
    a(1)
    angle = -a(1)*60;
    rotated = imrotate(wheel,angle,'nearest','crop');
    rotated(rotated==0)=255;
    cla(hIn);
    imshow(rotated,'Parent',hIn);

    drawnow
    
    right=ab(5);
    left=ab(6);
    
    if left
        set(hleft,'Visible','on')
    else
        set(hleft,'Visible','off')
    end
    if right
        set(hright,'Visible','on')
    else
        set(hright,'Visible','off')
    end
    if left && right
        break
    end
    
end


end

