function drawGauge(velocity,hIn,type)
global gauge_m gauge_k pointer

%     close all,clc

if strcmp(type,'kmh')
    velocity = min(velocity,120.0);
    velocity = max(velocity,0.0);
    angle = -2.0*(velocity-60.0);
    bg = gauge_k;
elseif strcmp(type,'mps')
    velocity = min(velocity,32.0);
    velocity = max(velocity,0.0);
    angle = -(velocity*240.0/32.0)+120;
    bg = gauge_m;
end

pointer = imread('pointer_big_black.png');

rotated = imrotate(pointer,angle,'nearest','crop');

%figure, imshow(rotated),title('rotated');

%rotated = pointer;

bw_pointer = im2bw(rotated,0.4);

%figure, imshow(bw_pointer),title('mask');

cla(hIn);
hold(hIn,'on');
imshow(bg,'Parent',hIn);
h=imshow(rotated,'Parent',hIn);
set(h,'AlphaData',bw_pointer);
hold(hIn,'off');
end

