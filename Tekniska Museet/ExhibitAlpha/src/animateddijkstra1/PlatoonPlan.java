/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import java.util.Arrays;

/**
 *
 * @author rui
 */
public class PlatoonPlan {
    
    double fuelSavingRate = 0.15; // 15% fuel savings while in platoon
    double litresPerMeter = 30./(100.*1000.); // 30 litres per 100 km 
    double fuelSavedPerMeterInPlatoon = fuelSavingRate*litresPerMeter;
    boolean possible = false;
    int[] path;
    int[] sharedPath;
    double[] sharedTimes;
    double savedFuel;
    double sharedDistance;
    Truck truck;
    
    public double getDepartureDelay(Truck myTruck){
        
//        System.out.println("");
//        System.out.println("");
//        System.out.println("getDepartureDelay");
//        
//        System.out.println("myTruck.departureTime");
//        System.out.println(myTruck.departureTime);
//        
//        System.out.println("myTruck.path");
//        System.out.println(Arrays.toString(myTruck.path));
//        
//        System.out.println("myTruck.times");
//        System.out.println(Arrays.toString(myTruck.times));
//                
//        System.out.println("truck.path");
//        System.out.println(Arrays.toString(truck.path));
//        
//        System.out.println("truck.times");
//        System.out.println(Arrays.toString(truck.times));
//        
//        System.out.println("sharedPath");
//        System.out.println(Arrays.toString(sharedPath));
//               
//        System.out.println("sharedTimes");
//        System.out.println(Arrays.toString(sharedTimes));
                
       
        
        
        double departureDelay = 0;
        
        for ( int i = 0 ; i < myTruck.path.length ; i++ ){
            
            if ( myTruck.path[i] == sharedPath[0] ){
                
                departureDelay = sharedTimes[0] - myTruck.times[i];
                break;
            }
            
        }
        
//        System.out.println("departureDelay");
//        System.out.println(departureDelay);
               
        return departureDelay;
                
    }
    
    
    public void setPlatoonPossibility(boolean argPossible){
        possible = argPossible;
    }
    
    public void setPlatoonTimes(double[] argSharedTimes){
        sharedTimes = argSharedTimes;        
    }
    
    public void setSharedPlatoonPath(int[] argSharedPath){
        sharedPath = argSharedPath;        
    }
    
    public void setSharedDistance(double argSharedDistance){
        sharedDistance = argSharedDistance;
        computeSavedFuel();
    }
    
    public void computeSavedFuel(){
        savedFuel = sharedDistance * fuelSavedPerMeterInPlatoon;
    }
    
    public void setFollowedTruck(Truck argTruck){
        truck = argTruck;
    }
    
    public boolean getPlatoonPossibility(){
       return possible;
    }
    
    public double[] getPlatoonTimes(){
        return sharedTimes;        
    }
    
    public int[] getSharedPlatoonPath(){
        return sharedPath;        
    }
    
    public double getDepartureTime(){
        return sharedTimes[0];
    }
    
    public double getSavedFuel(){
        return savedFuel;
    }
    
    public double getSharedDistance(){
        return sharedDistance;
    }
    
    public Truck getFollowedTruck(){
        return truck;
    }
    
    public void printPlatoonTimes(){
        System.out.println(Arrays.toString(sharedTimes));        
    }
    
//    public int[] getHourTimes(double[] times){
//        int[] hourTimes = new int[times.length];
//        for ( int i = 0 ; i < times.length ; i++ ){
//            hourTimes[i] = (int)times[i]/(60*60);
//            
//        }               
//        return hourTimes;
//    }
//    
//    public int[] getMinuteTimes(double[] times){
//        int[] minuteTimes = new int[times.length];
//        for ( int i = 0 ; i < times.length ; i++ ){
//            minuteTimes[i] = (int)times[i]%(3600)/60;
//        }               
//        return minuteTimes;
//    }
    
//    public void displayPlatoonClockTimes(){
//        
//        int[] hourTimes = getHourTimes(sharedTimes);
//        int[] minuteTimes = getMinuteTimes(sharedTimes);
//        
//        String pathTimes = new String("Times: ");
//        
//        for ( int i = 0 ; i < hourTimes.length ; i++ ){
//            if ( hourTimes[i] < 10 && minuteTimes[i] < 10){
//                pathTimes = pathTimes + " -> 0"+hourTimes[i]+":0"+minuteTimes[i];
//                //System.out.println("Time: 0"+hourTimes[i]+":0"+minuteTimes[i]);
//            }
//            if ( minuteTimes[i] < 10  && hourTimes[i] >= 10){
//                pathTimes = pathTimes + " -> "+hourTimes[i]+":0"+minuteTimes[i];
//                //System.out.println("Time: "+hourTimes[i]+":0"+minuteTimes[i]);
//            }
//            if ( minuteTimes[i] >= 10  && hourTimes[i] < 10){
//                pathTimes = pathTimes + " -> 0"+hourTimes[i]+":"+minuteTimes[i];
//                //System.out.println("Time: 0"+hourTimes[i]+":"+minuteTimes[i]);
//            }
//            if ( hourTimes[i] >= 10 && minuteTimes[i] >= 10){
//                pathTimes = pathTimes + " -> "+hourTimes[i]+":"+minuteTimes[i];
//                //System.out.println("Time: "+hourTimes[i]+":"+minuteTimes[i]);
//            }
//        }
//        
//        System.out.println(pathTimes);
//        
//    }
    
}

