/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.pow;
import static java.lang.Math.random;
import static java.lang.Math.sqrt;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Arrays.sort;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 *
 * @author rui
 */


class SharedPath {
    
    public int[] selfPath, truckPath, sharedPath;
    public double savedFuel, sharedDistance;

    
}


public class SwedenPlatoon {

    /**
     * @param args the command line arguments
     */
    
    boolean bigMode = Globals.bigMode;
    double fontStretch = Globals.fontStretch;
    double horizontalStretch = Globals.horizontalStretch;
    double verticalStretch = Globals.verticalStretch;
    
    public double[][] adjMatrix;
    public int numberCities;
    public int[][] cityPositions;
    public int[][] cityLinks;
    DijkstraSolver swedenDijkstra;
    PlatoonScheduler scheduler;
    public Truck myTruck;
    Truck[] truckList;
    public int numTrucks = 100000;
    public int numPossiblePlatoons;
    int platoonTruckId;

    
    // Map to Graph Methods:
    
    public double[][] getCityPositionsDouble(){
        
        double[][] doubleCityPositions = new double[numberCities][2];
        
        for ( int i = 1 ; i < numberCities ; i++ ){
            
            doubleCityPositions[i][0] = cityPositions[i][0];
            doubleCityPositions[i][1] = cityPositions[i][1];
            
        }
        
        return doubleCityPositions;
        
    }
    
    public Truck getTruck(int truckId){
        
        return truckList[truckId];
        
    }
    
//    public setMyTruck( )
    
    
    
    public Truck getMyTruck(){
        
        return myTruck;
        
    }
    
    public int[][] getCityPositionsInt(){
        
        return cityPositions;
        
    }

    public List<RoadWaypoints> getRoadWayPoints(){
        
        WaypointsHandler waypointsHandler = new WaypointsHandler();
        return waypointsHandler.getRoadWaypoints();
                       
    }
    
    
    // Dijkstra Search Methods
    
    public void getDijkstraIterations() {
        
        
        // DO CODE HERE
        
    }    
    
    
    // Truck shared paths
    
    public void setDepartureTimeDelayMyTruck(double departureDelay){
        
        
        
        
//        System.out.println("Original times: ");
//        System.out.println(Arrays.toString(myTruck.times));
        
        for ( int i = 0 ; i < myTruck.times.length ; i++ ){
        
            myTruck.times[i] += departureDelay; 
                
        }
        
//        System.out.println("Delayed times: ");
//        System.out.println(Arrays.toString(myTruck.times));
        
    }
    
    public SharedPath getSharedPath(int truckId) {
        
        SharedPath sharedPath = new SharedPath();
        PlatoonPlan tempPlatoon= new PlatoonPlan();
        
        tempPlatoon = scheduler.findPlatoonPossibility(myTruck, truckList[truckId]);

        sharedPath.sharedPath = tempPlatoon.sharedPath;
        sharedPath.truckPath = tempPlatoon.path;
        sharedPath.selfPath = myTruck.path;
        
        tempPlatoon.computeSavedFuel();
        sharedPath.savedFuel = tempPlatoon.savedFuel;
        sharedPath.sharedDistance = tempPlatoon.sharedDistance;        
        
        return sharedPath;
        
    }
    
    // Trucks moving according to time
    public int[] getTruckPositionInGraph(int truckId, double argTime){
       
        int[] position = new int[2];
        
        Truck truck;
        
        if ( truckId == - 99){
            
            truck = myTruck;
            
        }else{
            
            truck = truckList[truckId];
            
        }
        
//        System.out.println("getTruckPositionInGraph");
        
//        System.out.println(Arrays.toString(truck.path));
        
        if ( argTime <= truck.times[0] ){
            
            // Truck is at starting point
//            System.out.println("Truck is at starting point: " +  truck.path[0]);
            
            
            position[0] = cityPositions[ truck.path[0] ][0];
            position[1] = cityPositions[ truck.path[0] ][1];
                        
        }else{
            
            if ( argTime >= truck.times[ truck.times.length - 1 ] ){
                
                // Truck is at ending point
//                System.out.println("Truck is at ending point"+ truck.path[ truck.path.length - 1 ]);
                position[0] = cityPositions[ truck.path[ truck.path.length - 1 ] ][0];
                position[1] = cityPositions[ truck.path[ truck.path.length - 1 ] ][1];
                
                
            }else{
                
                // Truck is somewhere in between
                for( int i = 1 ; i < truck.times.length ; i++ ){
               
                    if (argTime < truck.times[i]){                    
                        
//                        System.out.println("Truck is somewhere in between");
                        
                        double weightStart, weightEnd;
                        
//                        System.out.println( " truck.times[i-1]: " + truck.times[i-1] + " argTime: " + argTime + " truck.times[i]: " + truck.times[i] );
                        
                        weightStart = (argTime - (double)truck.times[i-1])/((double)truck.times[i] - (double)truck.times[i-1]) ;
                        weightEnd = ((double)truck.times[i]- argTime)/((double)truck.times[i] - (double)truck.times[i-1]) ;
                        
                        
//                        System.out.println(weightStart);
//                        System.out.println(weightEnd);
                        
                        position[0] = (int)( weightStart * (double)cityPositions[ truck.path[i] ][0] + weightEnd * (double)cityPositions[ truck.path[i-1] ][0] );
                        position[1] = (int)( weightStart * (double)cityPositions[ truck.path[i] ][1] + weightEnd * (double)cityPositions[ truck.path[i-1] ][1] );  

                        break;
                        
                    }    
                    
                }
                                
            }
             
        }
        
        return position;
        
    }
    
    public int[] getTruckPositionInMap(int truckId, double argTime){
        
        WaypointsHandler waypointsHandler = new WaypointsHandler();        
        List<RoadWaypoints> listRoadWaypoints = waypointsHandler.getRoadWaypoints();
        
        
        int[] position = new int[2];
        
        Truck truck;
        
        if ( truckId == - 99){
            
            truck = myTruck;
            
        }else{
            
            truck = truckList[truckId];
            
        }
        
//        System.out.println("getTruckPositionInGraph");
        
//        System.out.println(Arrays.toString(truck.path));
        
//        int startOffsetX = 10;
//        int startOffsetY = 10;
//        int stopOffsetX = -10;
//        int stopOffsetY = -10;
        int startOffsetX = 0;
        int startOffsetY = 0;
        int stopOffsetX = -0;
        int stopOffsetY = -0;
        
        if ( argTime <= truck.times[0] ){
            
            // Truck is at starting point
//            System.out.println("Truck is at starting point: " +  truck.path[0]);
            
            int currentNode = truck.path[0];
            
            for ( int i = 0 ; i < listRoadWaypoints.size() ; i++ ){
                
                RoadWaypoints tempWaypoints = listRoadWaypoints.get(i);
                if ( tempWaypoints.srcId == currentNode ){
                    
                    position[0] = (int) tempWaypoints.waypoints[0][0];
                    position[1] = (int) tempWaypoints.waypoints[0][1];
                    
                    position[0] = position[0] + startOffsetX;
                    position[1] = position[1] + startOffsetY;
                            
                    break;
                    
                }
                
                if ( tempWaypoints.dstId == currentNode ){
                    
                    position[0] = (int) tempWaypoints.waypoints[tempWaypoints.waypoints.length - 1][0];
                    position[1] = (int) tempWaypoints.waypoints[tempWaypoints.waypoints.length - 1][1];
                    
                    position[0] = position[0] + startOffsetX;
                    position[1] = position[1] + startOffsetY;
                    
                    break;
                    
                }
                
                
            }
            
            
                        
        }else{
            
            if ( argTime >= truck.times[ truck.times.length - 1 ] ){
                
                // Truck is at ending point
//                System.out.println("Truck is at ending point"+ truck.path[ truck.path.length - 1 ]);
                
                int currentNode = truck.path[ truck.path.length - 1 ];
                
                for ( int i = 0 ; i < listRoadWaypoints.size() ; i++ ){
                
                    RoadWaypoints tempWaypoints = listRoadWaypoints.get(i);

                    if ( tempWaypoints.srcId == currentNode ){

                        position[0] = (int) tempWaypoints.waypoints[0][0];
                        position[1] = (int) tempWaypoints.waypoints[0][1];
                        
                        position[0] = position[0] + stopOffsetX;
                        position[1] = position[1] + stopOffsetY;
                        
                        break;

                    }

                    if ( tempWaypoints.dstId == currentNode ){

                        position[0] = (int) tempWaypoints.waypoints[tempWaypoints.waypoints.length - 1][0];
                        position[1] = (int) tempWaypoints.waypoints[tempWaypoints.waypoints.length - 1][1];
                        
                        position[0] = position[0] + stopOffsetX;
                        position[1] = position[1] + stopOffsetY;
                        
                        break;

                    }

                }
                
                
            }else{
                
                // Truck is somewhere in between
                for( int i = 1 ; i < truck.times.length ; i++ ){
               
                    if (argTime < truck.times[i]){                    
                        
//                        double weightStart, weightEnd;
//                        
//                        weightStart = (argTime - (double)truck.times[i-1])/((double)truck.times[i] - (double)truck.times[i-1]) ;
//                        weightEnd = ((double)truck.times[i]- argTime)/((double)truck.times[i] - (double)truck.times[i-1]) ;
//                       
//                        position[0] = (int)( weightStart * (double)cityPositions[ truck.path[i] ][0] + weightEnd * (double)cityPositions[ truck.path[i-1] ][0] );
//                        position[1] = (int)( weightStart * (double)cityPositions[ truck.path[i] ][1] + weightEnd * (double)cityPositions[ truck.path[i-1] ][1] );  
//
//                        break;
                        
                        
                        int currentSrcNode = truck.path[i-1];
                        int currentDstNode = truck.path[i];
                        
                        double weightStart, weightEnd;
                        
                        weightStart = (argTime - (double)truck.times[i-1])/((double)truck.times[i] - (double)truck.times[i-1]) ;
                        weightEnd = ((double)truck.times[i]- argTime)/((double)truck.times[i] - (double)truck.times[i-1]) ;
                        
                        
                        for ( int j = 0 ; j < listRoadWaypoints.size() ; j++ ){
                
                            RoadWaypoints tempWaypoints = listRoadWaypoints.get(j);
                            if ( tempWaypoints.srcId == currentSrcNode && tempWaypoints.dstId == currentDstNode ){

                                position[0] = (int) tempWaypoints.waypoints[ (int) ( 0.5 + (weightStart*( tempWaypoints.waypoints.length - 1 )) ) ][0];
                                position[1] = (int) tempWaypoints.waypoints[ (int) ( 0.5 + (weightStart*( tempWaypoints.waypoints.length - 1 )) ) ][1];

                                break;

                            }

                            if ( tempWaypoints.srcId == currentDstNode && tempWaypoints.dstId == currentSrcNode ){

                                position[0] = (int) tempWaypoints.waypoints[ (int) ( 0.5 + (weightEnd*(tempWaypoints.waypoints.length - 1)) ) ][0];
                                position[1] = (int) tempWaypoints.waypoints[ (int) ( 0.5 + (weightEnd*(tempWaypoints.waypoints.length - 1)) ) ][1];

                                break;

                            }

                        }
                        
                        break;
                                                
                    }    
                    
                }
                                
            }
             
        }
        
        return position;
        
    }
    
    
    
    public int[] getMyTruckPositionInGraph(double time){
       
        int[] position = getTruckPositionInGraph(-99, time);
        
        return position;
        
    }
    
    
    
    public int[] getMyTruckPositionInMap(double time){
       
        int[] position = getTruckPositionInMap(-99, time);
        
        return position;
        
    }
    
    
    private Truck[] getRandomTrucks(int numTrucks, DijkstraSolver pathSolver) {
        
        // Allocate memory for array of Truck's
        Truck[] truckList = new Truck[numTrucks];
        
        // Create Random instance
        Random randomGen = new Random();
                
        int sourceCity, destCity, numCities;
        double departureTime ;
        double departureTimeSlots = 24*6;
        
        
        numCities = pathSolver.getNumberCities();
        
        for ( int i = 0 ; i < truckList.length ; i++ ) {
            
            sourceCity = randomGen.nextInt(numCities);
            destCity = randomGen.nextInt(numCities);
            
//            sourceCity = 0;
//            destCity = 10;
            
//            departureTime = randomGen.nextInt( (int) departureTimeSlots)*(24.0/departureTimeSlots)*60*60;
            departureTime = randomGen.nextInt( (int) departureTimeSlots)*(2.0/departureTimeSlots)*60*60;
//            departureTime = 0;
            
            
            //if ( departureTime == 10*60 ){
            //    System.out.println(departureTime);
            //}
            
            while (destCity == sourceCity){
                destCity = randomGen.nextInt(numCities);
            }
            
            truckList[i] = new Truck(i, sourceCity, destCity, departureTime, pathSolver);
                        
        }
        
        return truckList;
        
    }
    
   
    
    public void getPossiblePlatoonPlans(List<PlatoonPlan> possiblePlatoons){
        
        PlatoonPlan tempPlatoon= new PlatoonPlan();
        
        for ( int i = 0 ; i < truckList.length ; i++ ){

            tempPlatoon = scheduler.findPlatoonPossibility(myTruck, truckList[i]);

            if ( tempPlatoon.getPlatoonPossibility() ){

                possiblePlatoons.add(tempPlatoon);
                
            }

        }

    }
    
    public void getFastestPossiblePlatoonPlans(List<PlatoonPlan> fastestPlatoons, int numberPlatoons){
        
        // They must have a different starting point than my Truck!
        // They must not be all equal!
        
        List<PlatoonPlan> possiblePlatoons = new ArrayList<>();
        
        PlatoonPlan tempPlatoon = new PlatoonPlan();
        
        
        
        
        
        for ( int i = 0 ; i < truckList.length ; i++ ){

            tempPlatoon = scheduler.findPlatoonPossibility(myTruck, truckList[i]);

            if ( tempPlatoon.getPlatoonPossibility() ){

                possiblePlatoons.add(tempPlatoon);
                
            }

        }
        
        
        boolean splitAtEnd = true;
        boolean differentPathsRequired = true;
        boolean findAnythingPlease = false;
        boolean differentSourceRequired = true;
        
//        System.out.println("\nTrying with the following conditions");
//        System.out.println("splitAtEnd: "+splitAtEnd);
//        System.out.println("differentPathsRequired: "+differentPathsRequired);
//        System.out.println("findAnythingPlease: "+findAnythingPlease);
//        System.out.println("differentSourceRequired: "+differentSourceRequired);
        
        selectivePlatoonSearch2(numberPlatoons, possiblePlatoons, splitAtEnd, differentPathsRequired, differentSourceRequired, findAnythingPlease, fastestPlatoons);
        
        if ( fastestPlatoons.size() == 0){
            
            
            
//            splitAtEnd = false;
            differentPathsRequired = false;
            
//            System.out.println("\nTrying with the following conditions");
//            System.out.println("splitAtEnd: "+splitAtEnd);
//            System.out.println("differentPathsRequired: "+differentPathsRequired);
//            System.out.println("findAnythingPlease: "+findAnythingPlease);
//            System.out.println("differentSourceRequired: "+differentSourceRequired);
        
            
            selectivePlatoonSearch2(numberPlatoons, possiblePlatoons, splitAtEnd, differentPathsRequired, differentSourceRequired, findAnythingPlease,fastestPlatoons);
            
            if ( fastestPlatoons.size() == 0){
                

                
                differentPathsRequired = true;
                splitAtEnd = false;
                
//                System.out.println("\nTrying with the following conditions");
//                System.out.println("splitAtEnd: "+splitAtEnd);
//                System.out.println("differentPathsRequired: "+differentPathsRequired);
//                System.out.println("findAnythingPlease: "+findAnythingPlease);
//                System.out.println("differentSourceRequired: "+differentSourceRequired);
                
                selectivePlatoonSearch2(numberPlatoons, possiblePlatoons, splitAtEnd, differentPathsRequired, differentSourceRequired, findAnythingPlease,fastestPlatoons);
                
            }
            
            
        }
        

        
        
                
        
        
        
        
        
        
//        double[] departureDelays = new double[ possiblePlatoons.size() ] ;
//        
//        for ( int i = 0 ; i < departureDelays.length ; i++ ){
//            
//            departureDelays[i] = tempPlatoon.getDepartureDelay(myTruck);
//            
//        }

    }
    
    
    
    public void selectivePlatoonSearch(int argNumberPlatoons, List<PlatoonPlan> possiblePlatoons, boolean splitAtEnd, List<PlatoonPlan> fastestPlatoons){
        
        int numberPlatoons = argNumberPlatoons;
        
        PlatoonPlan currentBestPlatoon = new PlatoonPlan();
        
        PlatoonPlan tempPlatoon = new PlatoonPlan();
        
        int[] companiesChosen = new int[numberPlatoons];
        for ( int i = 0 ; i < companiesChosen.length ; i++ ){
            companiesChosen[i] = -1;
        }
        
        double min_time = Double.POSITIVE_INFINITY;
        int min_index = -1;
        
        for ( int i = 0 ; i < numberPlatoons ; i++ ){
            
            for ( int j = 0 ; j < possiblePlatoons.size() ; j++ ){
                
                
                
                
                tempPlatoon = possiblePlatoons.get(j);
                
                
                if (splitAtEnd){
                    
                    boolean finishedOnMyTruckPath = false;
                    
                    for ( int k = 0 ; k < myTruck.path.length ; k++ ){
                        
                        if(  myTruck.path[k] == tempPlatoon.sharedPath[tempPlatoon.sharedPath.length-1] ){
                        
                            finishedOnMyTruckPath = true;
                            
                        }
                        
                    }
                    
                    if (finishedOnMyTruckPath){
                        
                        continue;
                        
                    }
                    
                }
                
                int currentCompany = tempPlatoon.truck.company;
                boolean repeatedCompany = false;    
                
                for ( int k = 0 ; k < companiesChosen.length ; k++ ){
                    
                    if ( companiesChosen[k] == currentCompany ){
                        
                        repeatedCompany= true;
                        
                    }
                    
                }
                
                if (repeatedCompany){
                    
                    continue;
                    
                }
                
                if ( tempPlatoon.getDepartureDelay(myTruck) < min_time && tempPlatoon.path[0] != myTruck.source ){
                    
                    if ( i == 1){
                        
                        if ( Arrays.equals(tempPlatoon.path, fastestPlatoons.get(0).path) ){
                            
                            continue;
                            
                        }
                                                
                    }
                    
                    if ( i == 2){
                        
                        if ( Arrays.equals(tempPlatoon.path, fastestPlatoons.get(0).path) || Arrays.equals(tempPlatoon.path, fastestPlatoons.get(1).path) ){
                            
                            continue;
                            
                        }
                        
                    }                  
                    
                    min_time =  tempPlatoon.getDepartureDelay(myTruck);
                    min_index = j;
                    
                }
                
            }
            
            if (splitAtEnd){
                
                if ( min_index < 0 ){
                    continue;
                }
                
            }
            
            
            if ( min_index < 0 ){
                min_index = 0;
                System.out.println("This code must be revised. The variable should never be smaller than zero!:");
            } 
            
            
            
            currentBestPlatoon = possiblePlatoons.get(min_index);
            fastestPlatoons.add(currentBestPlatoon);
            
            for ( int k = 0 ; k < companiesChosen.length ; k++ ){

                if ( companiesChosen[k] == -1 ){

                    companiesChosen[k] = currentBestPlatoon.truck.company;
                    break;

                }

            }
            
            
            
            possiblePlatoons.remove(min_index);
            
            min_time = Double.POSITIVE_INFINITY;
            min_index = -1;
            
        }
        
        
                
        
    }
    
    
    
    public void selectivePlatoonSearch2(int argNumberPlatoons, List<PlatoonPlan> possiblePlatoons,
            boolean splitAtEnd, boolean differentPathsRequired, boolean differentSourceRequired, boolean findAnythingPlease, List<PlatoonPlan> fastestPlatoons){
        
        int numberPlatoons = argNumberPlatoons;
        
        PlatoonPlan currentBestPlatoon = new PlatoonPlan();
        
        PlatoonPlan tempPlatoon = new PlatoonPlan();
        
        int[] companiesChosen = new int[numberPlatoons];
        for ( int i = 0 ; i < companiesChosen.length ; i++ ){
            companiesChosen[i] = -1;
        }
        
        double min_time = Double.POSITIVE_INFINITY;
        int min_index = -1;
        
        for ( int i = 0 ; i < numberPlatoons ; i++ ){
            
            for ( int j = 0 ; j < possiblePlatoons.size() ; j++ ){
                
                tempPlatoon = possiblePlatoons.get(j);
                
                // Checking if platoon Truck does not stop in our path
                if (splitAtEnd){
                    
                    boolean finishedOnMyTruckPath = false;
                    
                    for ( int k = 0 ; k < myTruck.path.length ; k++ ){
                        
                        if(  myTruck.path[k] == tempPlatoon.path[tempPlatoon.path.length-1] ){
                            
                            finishedOnMyTruckPath = true;
                            
                        }
                        
                    }
                    
                    if (finishedOnMyTruckPath){
                        
                        continue;
                        
                    }
                    
                }
                
                
                // Checking if platoon truck is not from an already chosen company
                int currentCompany = tempPlatoon.truck.company;
                boolean repeatedCompany = false;    
                
                for ( int k = 0 ; k < companiesChosen.length ; k++ ){
                    
                    if ( companiesChosen[k] == currentCompany ){
                        
                        repeatedCompany= true;
                        
                    }
                    
                }
                
                if (repeatedCompany){
                    
                    continue;
                    
                }
                
                
                
//                if ( tempPlatoon.getDepartureDelay(myTruck) < min_time && tempPlatoon.path[0] != myTruck.source ){
                if ( tempPlatoon.getDepartureDelay(myTruck) < min_time ){   
                    
                    if ( differentSourceRequired){
                        
                        if (tempPlatoon.path[0] != myTruck.source) {
                            
                        }else{
                            
                            continue;
                            
                        }
                        
                    }
                    
                    // Checking if path is not the same as previously
                    if (differentPathsRequired){
                    
                        if ( i == 1){

                            if ( Arrays.equals(tempPlatoon.path, fastestPlatoons.get(0).path) ){

                                continue;

                            }

                        }

                        if ( i == 2){

                            if ( Arrays.equals(tempPlatoon.path, fastestPlatoons.get(0).path) || Arrays.equals(tempPlatoon.path, fastestPlatoons.get(1).path) ){

                                continue;

                            }

                        }   
                        
                    }
                    
                    min_time =  tempPlatoon.getDepartureDelay(myTruck);
                    min_index = j;
                    
                }
                
            }
            
//            if (splitAtEnd){
//                
//                if ( min_index < 0 ){
//                    continue;
//                }
//                
//            }
//            
            
            
//            if (!findAnythingPlease){
//            
//                if ( min_index < 0 ){
//                    
//                    continue;
//                    
//                }
//                
//            } else{
//                
//                if ( min_index < 0 ){
//                    
//                    min_index = 0;
//                    System.out.println("This code must be revised. The variable should never be smaller than zero!:");
//
//                }
//            
//            }
//            
            
            if ( min_index < 0 ){
                    
                continue;

            }
            
            
            currentBestPlatoon = possiblePlatoons.get(min_index);
            fastestPlatoons.add(currentBestPlatoon);
            
            for ( int k = 0 ; k < companiesChosen.length ; k++ ){

                if ( companiesChosen[k] == -1 ){

                    companiesChosen[k] = currentBestPlatoon.truck.company;
                    break;

                }

            }
            
            
            
            possiblePlatoons.remove(min_index);
            
            min_time = Double.POSITIVE_INFINITY;
            min_index = -1;
            
        }
        
        
                
        
    }
    
    public void selectivePlatoonSearch3(int argNumberPlatoons, List<PlatoonPlan> possiblePlatoons,
            boolean splitAtEnd, boolean differentPathsRequired, boolean findAnythingPlease, List<PlatoonPlan> fastestPlatoons){
        
        int numberPlatoons = argNumberPlatoons;
        
        PlatoonPlan currentBestPlatoon = new PlatoonPlan();
        
        PlatoonPlan tempPlatoon = new PlatoonPlan();
        
        int[] companiesChosen = new int[numberPlatoons];
        for ( int i = 0 ; i < companiesChosen.length ; i++ ){
            companiesChosen[i] = -1;
        }
        
        double min_time = Double.POSITIVE_INFINITY;
        int min_index = -1;
        
        for ( int i = 0 ; i < numberPlatoons ; i++ ){
            
            for ( int j = 0 ; j < possiblePlatoons.size() ; j++ ){
                
                tempPlatoon = possiblePlatoons.get(j);
                
                // Checking if platoon Truck does not stop in our path
                if (splitAtEnd){
                    
                    boolean finishedOnMyTruckPath = false;
                    
                    for ( int k = 0 ; k < myTruck.path.length ; k++ ){
                        
                        if(  myTruck.path[k] == tempPlatoon.path[tempPlatoon.sharedPath.length-1] ){
                            
                            finishedOnMyTruckPath = true;
                            
                        }
                        
                    }
                    
                    if (finishedOnMyTruckPath){
                        
                        continue;
                        
                    }
                    
                }
                
                
                // Checking if platoon truck is not from an already chosen company
                int currentCompany = tempPlatoon.truck.company;
                boolean repeatedCompany = false;    
                
                for ( int k = 0 ; k < companiesChosen.length ; k++ ){
                    
                    if ( companiesChosen[k] == currentCompany ){
                        
                        repeatedCompany= true;
                        
                    }
                    
                }
                
                if (repeatedCompany){
                    
                    continue;
                    
                }
                
                
                
//                if ( tempPlatoon.getDepartureDelay(myTruck) < min_time && tempPlatoon.path[0] != myTruck.source ){
                if ( tempPlatoon.getDepartureDelay(myTruck) < min_time ){   
                    
                    
                    min_time =  tempPlatoon.getDepartureDelay(myTruck);
                    min_index = j;
                    
                }
                
            }
            
            if ( min_index < 0 ){
                    
                continue;

            }
            
            
            currentBestPlatoon = possiblePlatoons.get(min_index);
            fastestPlatoons.add(currentBestPlatoon);
            
            for ( int k = 0 ; k < companiesChosen.length ; k++ ){

                if ( companiesChosen[k] == -1 ){

                    companiesChosen[k] = currentBestPlatoon.truck.company;
                    break;

                }

            }
            
            
            
            possiblePlatoons.remove(min_index);
            
            min_time = Double.POSITIVE_INFINITY;
            min_index = -1;
            
        }
        
        
                
        
    }
    
    public void setPlatoonTruckId(int argPlatoonTruckId){
        
        platoonTruckId = argPlatoonTruckId;
        
    }
    
    public int getPlatoonTruckId(){
        
        return platoonTruckId;
        
    }
    
    
    public List<int []> getMyTruckDijkstra(){
        
        int source = 3;
        int destination = 16;
        int departureTime = 0;
        
        
        
        List<int []> truckDijkstraHistory;
        
        swedenDijkstra.setRecordingState(true);
        
        Truck tempTruck = new Truck(0, source, destination, departureTime, swedenDijkstra);
        
        truckDijkstraHistory = swedenDijkstra.getGraphHistory();
        
        swedenDijkstra.setRecordingState(false);
        
        return truckDijkstraHistory;
        
    }
    
    public void definePlatoons(int argSource, int argDestination, double argDepartureTime){
        
        
        
        myTruck = new Truck(0, argSource, argDestination, argDepartureTime, swedenDijkstra);
        
//        System.out.println( " myTruck's path: " + Arrays.toString( myTruck.path ) );
        
        
        // Create the scheduler instance
        scheduler = new PlatoonScheduler( swedenDijkstra );
        
        
        // Create List of possible platoons
        List<PlatoonPlan> possiblePlatoons = new ArrayList<>();
        
        // Get possible platoon opportunities (variable possiblePlatoons changes by reference ??? )
        getPossiblePlatoonPlans(possiblePlatoons);
        
        
        numPossiblePlatoons = possiblePlatoons.size();
//        System.out.println("Found " + possiblePlatoons.size() + " platooning opportunities, out of " + numTrucks + " registered cars.");
        
        //System.out.println("Ratio: " + (double)possiblePlatoons.size() / (double)numTrucks );
             
        double departureDelay;
        double tempFuelSaved, maximumFuelSaved = 0.;
        TimeConverter timeConversion = new TimeConverter();
        
        double[] platoonDelays = new double[possiblePlatoons.size()];
        
        int tempIterator = 0;
        
        for ( PlatoonPlan tempPlatoon : possiblePlatoons ){
            
            //System.out.println("");
            //System.out.println("Shared distance: "+ tempPlatoon.getSharedDistance()/1000. + " km");
            tempFuelSaved = tempPlatoon.getSavedFuel();
            //System.out.println("Saved fuel: "+ tempFuelSaved);
            if ( tempFuelSaved > maximumFuelSaved){
                maximumFuelSaved = tempFuelSaved;
            }
            
            //tempPlatoon.displayPlatoonClockTimes();
            departureDelay = ( tempPlatoon.getDepartureTime() - myTruck.getDepartureTime() );
            //System.out.println( "Departure delayed by " + timeConversion.getClockTime(departureDelay));
            
            platoonDelays[tempIterator] = departureDelay;
            tempIterator ++;
            
        }
        
        PlatoonPlan tempPlatoon;
        
    }
    
     
   
    
    public SwedenPlatoon() throws IOException {
        
        //double[][] adjMatrix = new double[11][11];
        
//        System.out.println("Constructing...");
        
        //int[][] cityLinks = {{1, 4, 5}, {0, 2}, {1, 3}, {2, 4}, {0, 3, 5}, {0, 4}};
        
        
        
        
        
        //String[] cityNames;
        
        // City information (BigSweden)
        String[] cityNames = {"Kiruna","Lulea","Umea","Harnosand","Ostersund","Gavle","Falun","Uppsala",
            "Vasteras","Stockholm","Nykoping","Linkoping","Orebro","Karlstad","Vanersborg","Mariestad","Jonkoping",
        "Gotenorg","Halmstad","Vaxjo","Kalmar","Karlskrona","Kristianstad","Malmo","Malmo1","Malmo2","Malmo3"};

//        THIS IS FOR ALL SWEDEN BEGIN
//        int[][] tempCityPositions = {
//            {631,123},            {710,314},            {650,471},            {564,576},
//            {441,527},            {537,742},            {475,749},            {559,809},
//            {513,834},            {577,853},            {537,903},            {475,934},
//            {455,861},            {385,852},            {326,933},            {394,909},
//            {407,988},            {309,989},            {346, 1080},            {433,1063},
//            {507,1081},            {472,1123},            {404,1134},            {350,1170}};
//        THIS IS FOR ALL SWEDEN END
        
        int[][] tempCityPositions = { {474,    22},    {106,    140},    {386,    164},    {488,    154},
    {124,    440},    {228,    477},    {376,    503},    {495,    360},
    {534,    342},    {626,    413},    {816,    271},    {556,    518},
    {416,    710},    {030,    901},    {642,    748},    {675,    880},
    {1159,    620},   {699,    370},    {758,    443}};
    //{470,-100},{1400,250},{1400,620},{600,1500},{-100,900},{-100,450},{-100,120},{824,430}};
        

//    0.4749    0.0227
//    0.1068    0.1402
//    0.3862    0.1649
//    0.4883    0.1546
//    0.1243    0.4402
//    0.2284    0.4774
//    0.3769    0.5031
//    0.4955    0.3608
//    0.5347    0.3423
//    0.6264    0.4134
//    0.8161    0.2711
//    0.5563    0.5186
//    0.4161    0.7104
//    0.0305    0.9011
//    0.6429    0.7485
//    0.6759    0.8805
//    1.1584    0.6207
        if (bigMode){
        
            for (int i = 0; i < tempCityPositions.length; i++) {

    //            1.527;
    //            double verticalStretch = 1.165;
                tempCityPositions[i][0] = (int) (horizontalStretch * (double) tempCityPositions[i][0]);
                tempCityPositions[i][1] = (int) (verticalStretch * (double) tempCityPositions[i][1]);

            }
    //        
        }
        
        cityPositions = tempCityPositions;
        
//        THIS IS FOR ALL SWEDEN BEGIN
//        numberCities = 24;
//        THIS IS FOR ALL SWEDEN BEGIN    
        numberCities = 19; // before hidden nodes
//        numberCities = tempCityPositions.length; 
           
        cityLinks = new int[numberCities][];
        double[][] doubleCityPositions = new double[numberCities][2];
        // Convert from kilometres to metres
        
        for ( int i = 0 ; i < cityPositions.length ; i++ ){
            doubleCityPositions[i][0] = cityPositions[i][0];
            doubleCityPositions[i][1] = cityPositions[i][1];
        }
        
//        THIS IS FOR ALL SWEDEN BEGIN
//        int[][] handLinks = {{0,1},{0,4},
//            {1,2},            {2,3},            {3,4},            {4,2},
//            {4,6},            {3,5},            {5,6},            {7,9},
//            {7,5},            {7,6},            {7,8},            {8,12},
//            {12,13},            {13,14},            {14,17},            {15,14},
//            {16,11},            {11,16},            {19,16},            {11,20},
//            {20,21},            {21,22},            {22,23},            {23,18},
//            {18,22},            {18,17},            {19,22},            {19,21},
//            {19,20},            {19,18},            {17,16},            {15,12},
//            {9,10},            {12,10},            {6,12},            {6,8},
//            {6,13},            {16,15},            {11,12},     {10,8},   {10,12},  {10,11}};
//        THIS IS FOR ALL SWEDEN END
        
        int[][] handLinks = {{1, 4},        {3, 4},        {2, 3},        {2, 6},
        {5, 6},        {3, 8},        {6, 7}, {7, 8},        {4, 9},
        {9, 10},        {10, 18},        {10, 12},        {7, 12},
        {7, 13},        {13, 14},        {12, 15},        {15, 16},
        {15, 17},        {13, 15},       {8, 9},        {11, 18},   {18, 19}};
//        {1,20},{11,21},{19,27},{17,22},{16,23},{14,24},{5,25},{2,26}};
        for (int i=0;i<handLinks.length;i++){handLinks[i][0]--;handLinks[i][1]--;}
        
       
        int[] cityLinkCounter = new int[numberCities];
        for ( int i = 0 ; i < numberCities ; i++ ){
            cityLinkCounter[i] = 0;
        }
        for ( int i = 0 ; i < handLinks.length ; i++ ){
            cityLinkCounter[handLinks[i][0]] ++;
            cityLinkCounter[handLinks[i][1]] ++;
        }
        for ( int i = 0 ; i < numberCities ; i++ ){
            //System.out.println(cityLinkCounter[i]);
            cityLinks[i] = new int[cityLinkCounter[i]];
            for ( int j = 0 ; j < cityLinkCounter[i] ; j++ ){
                cityLinks[i][j] = -1;
            }
        }
        int cnt = 0;
        for ( int i = 0 ; i < handLinks.length ; i++ ){
            while (true) {
                if ( cityLinks[ handLinks[i][0] ][cnt] == -1 ){
                    break;
                }else{
                    cnt++;
                }
            }
            cityLinks[ handLinks[i][0] ][cnt] = handLinks[i][1];
            cnt = 0;
            while (true) {
                //System.out.println(handLinks[i][1]);
                //System.out.println(cnt);
                if ( cityLinks[ handLinks[i][1] ][cnt] == -1 ){
                    break;
                }else{
                    cnt++;
                }
            }
            cityLinks[ handLinks[i][1] ][cnt] = handLinks[i][0];
            cnt = 0;
        }
        
        adjMatrix = new double[numberCities][numberCities];
        
        for ( int i = 0 ; i < adjMatrix.length ; i++ ){
            
            for ( int j = 0 ; j < adjMatrix.length ; j++ ){
            
                adjMatrix[i][j] = Double.POSITIVE_INFINITY;
                
            }
            
        }
        
        for ( int i = 0 ; i < handLinks.length ; i++ ){
            
            double weight;
            
            weight = sqrt( pow( doubleCityPositions[handLinks[i][0]][0] - doubleCityPositions[handLinks[i][1]][0] ,2) + 
                    pow( doubleCityPositions[handLinks[i][0]][1] - doubleCityPositions[handLinks[i][1]][1] ,2) );
            
//            weight is the euclidean distance in pixels!
            double metresPerPixel = 15.6788;
            weight = weight*metresPerPixel;
//            weight is now the euclidean distance in metres!
            adjMatrix[handLinks[i][0]][handLinks[i][1]] = weight;
            adjMatrix[handLinks[i][1]][handLinks[i][0]] = weight;
            
        } 
        
//        
//        System.out.println("Adjacency Matrix in metres");
//        for ( int i = 0 ; i < adjMatrix.length ; i++ ){
//            
//            System.out.println(Arrays.toString(adjMatrix[i]));
//            
//        }
        
        
        
        //for ( int i = 0 ; i < cityLinks.length ; i++ ){
        //for ( int i = 0 ; i < numberCities ; i++ ){
            //System.out.println( Arrays.toString( cityLinks[i] ) );
        //}
        
        
        // City information (Sweden)
        //int numberCities = 6;
        //String[] cityNames = {"Stockholm" , "Sundsvall" , "Umea" , "Kiruna" , "Gotemburg" , "Malmo"};
        //double[][] cityPositions = {{164.6353, 373.7044}, {151.9674, 281.5739}, {196.8810, 228.5988},
        //    {194.5777, 70.8253}, {67.8983, 426.6795}, {88.6276, 491.1708}};
        //int[][] cityLinks = {{1, 4, 5}, {0, 2}, {1, 3}, {2, 4}, {0, 3, 5}, {0, 4}};
        

        //System.out.println(numberCities);
        //System.out.println(cityNames.length);
        //System.out.println(cityPositions.length);
        //System.out.println(cityLinks.length);
    
        // Create a shortest path finder for Sweden roads
        swedenDijkstra = new DijkstraSolver();
        //swedenDijkstra.setGraph(numberCities, cityNames, doubleCityPositions, cityLinks);
        
        swedenDijkstra.setGraph( numberCities, cityNames, adjMatrix, cityLinks );
        //String filename = "C:\\Users\\rui\\Downloads";
        //swedenDijkstra.getGraphFromFile(filename);
        
        
        
        
        truckList = getRandomTrucks(numTrucks, swedenDijkstra);
        
        // My truck information
//        int argTruckId = -1;
//        // From Kiruna to Malmo
//        int argSource = 1;
//        int argDestination = 17;
//        // From Uppsala to Orebro
//        //int argSource = 7;
//        //int argDestination = 12;
//        double argDepartureTime = 0*3600;
        
//        myTruck = new Truck(argTruckId, argSource, argDestination, argDepartureTime, swedenDijkstra);
//        System.out.println( " number of Cities: " + numberCities );
        
        

//        definePlatoons(argSource, argDestination, argDepartureTime);
        
        
        
        
//        for ( int i = 0 ; i < 5 ; i++ ){
//            
//            tempPlatoon = possiblePlatoons.get(i);
//            //System.out.println("");
//            System.out.println("Shared distance: "+ tempPlatoon.getSharedDistance()/1000. + " km");
//            
//            tempFuelSaved = tempPlatoon.getSavedFuel();
//            //System.out.println("Saved fuel: "+ tempFuelSaved);
//            if ( tempFuelSaved > maximumFuelSaved){
//                maximumFuelSaved = tempFuelSaved;
//            }
//            
//            //tempPlatoon.displayPlatoonClockTimes();
//            departureDelay = ( tempPlatoon.getDepartureTime() - myTruck.getDepartureTime() );
//            System.out.println( "Departure delayed by " + timeConversion.getClockTime(departureDelay));
//            
//        }
        
//        Collections.sort(possiblePlatoons, new DepartureTimeComparator() );
  //      System.out.println("Sorted!");
        
//        for ( int i = 0 ; i < 5 ; i++ ){
//            
//            tempPlatoon = possiblePlatoons.get(i);
//            System.out.println("");
//            //System.out.println("Shared distance: "+ tempPlatoon.getSharedDistance()/1000. + " km");
//            
//            tempFuelSaved = tempPlatoon.getSavedFuel();
//            System.out.println("Saved fuel: "+ tempFuelSaved);
//            if ( tempFuelSaved > maximumFuelSaved){
//                maximumFuelSaved = tempFuelSaved;
//            }
//            
//            //tempPlatoon.displayPlatoonClockTimes();
//            departureDelay = ( tempPlatoon.getDepartureTime() - myTruck.getDepartureTime() );
//            System.out.println( "Departure delayed by " + timeConversion.getClockTime(departureDelay));
//            
//        }
        
        
        
        
        //System.out.println(Arrays.toString(myTruck.getTimes()));
        //myTruck.displayClockTimes();
//        System.out.println("");
//        System.out.println("Default fuel consumption: " + myTruck.getFuelConsumption());
//        System.out.println("Maximum possible fuel savings: " + maximumFuelSaved);
        
    }
    
}
