/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 *
 * @author rui
 */
public class Globals {
    
    public static boolean bigMode = true;
    public static double fontStretch = 1.25;
    public static double horizontalStretch = 1.527;
    public static double verticalStretch = 1.05;
    public static int screenWidth = 1920;
    public static int screenHeight = 1080;
    
    public static double inactivityTimeSeconds = 30;
    
    public static Group root = new Group();
    public static Scene scene = new Scene(root, screenWidth, screenHeight, Color.BLACK);
    public static SwedenPlatoon swedenPlatooning;
    public static Languages language;
    
    public static boolean isSwedishCurrentLanguage = true;
    
    public static String initialHelpEN = "               WELCOME TO THE WORLD OF PLATOONING\n\n"
                + "Here you will be the owner of a truck that has a delivery to do. We will demonstrate how you can do it cheaper and greener by finding other vehicles to platoon with.\n\n"
                + "You must first define the origin and destination of your delivery.\n\n                               TOUCH TO START";
    
    public static String initialHelpSV = "                                   VÄLKOMMEN\n\n" +
                "Här är du ägare till en lastbil som har en leverans att utföra. Vi ska visa hur du kan göra det billigare och grönare genom att hitta andra fordon att köra i kolonn med.\n\n" +
                "Du börjar genom att välja start och mål för din leverans.\n\n                           TRYCK FÖR ATT STARTA";
    
    
//    public static Image truckImgSV = new Image("file:flagTruckSweden.png");
//    public static Image truckImgEN = new Image("file:flagTruckEngland.png");
//    
//    public static Image stockholmMapImage = new Image("file:touchScreenHD.png");
//    
//    public static Image kthTruckImage = new Image("file:kthTruckTransparentLogo.png");
//    public static Image kthTruckRightImage = new Image("file:kthTruckTransparentLogoRight.png");
//    
//    public static Image truckIkeaImage = new Image("file:trailerTruckTransparentIKEALogo.png");
//    public static Image truckIcaImage = new Image("file:trailerTruckTransparentICALogo.png");
//    public static Image truckHmImage = new Image("file:trailerTruckTransparentHMLogo.png");
//    public static Image truckScaniaImage = new Image("file:trailerTruckTransparentScaniaLogo.png");
//    
//    public static Image truckIkeaRightImage = new Image("file:trailerTruckTransparentIKEALogoRight.png");
//    public static Image truckIcaRightImage = new Image("file:trailerTruckTransparentICALogoRight.png");
//    public static Image truckHmRightImage = new Image("file:trailerTruckTransparentHMLogoRight.png");
//    public static Image truckScaniaRightImage = new Image("file:trailerTruckTransparentScaniaLogoRight.png");
//    
//    public static Image bigCompanyImage = new Image("file:companyMediumBigShort.png");
//    public static Image mediumCompanyImage = new Image("file:companyMedium.png");
//    public static Image smallCompanyImage = new Image("file:companySmall.png");
//    
//    public static Image flagSwedenImage = new Image("file:flagSweden.png");
//    public static Image flagEuropeImage = new Image("file:flagEurope.png");
//    
//    public static Image homeButtonImage = new Image("file:home.png");
//    public static Image helpButtonImage = new Image("file:helpImage.png");
    
    public static Image truckImgSV = new Image("http://i.imgur.com/I9kfW1T.png");
    public static Image truckImgEN = new Image("http://i.imgur.com/6CIidN1.png");
    
    public static Image stockholmMapImage = new Image("http://i.imgur.com/7HOZoqt.png");
    
    public static Image kthTruckImage = new Image("http://i.imgur.com/nVdaYxp.png");
    public static Image kthTruckRightImage = new Image("http://i.imgur.com/9jMwlDt.png");
    
    public static Image truckIkeaImage = new Image("http://i.imgur.com/N785NMi.png");
    public static Image truckIcaImage = new Image("http://i.imgur.com/vRnS08i.png");
    public static Image truckHmImage = new Image("http://i.imgur.com/BswFSEr.png");
    public static Image truckScaniaImage = new Image("http://i.imgur.com/Jr6C6Pz.png");
    
    public static Image truckIkeaRightImage = new Image("http://i.imgur.com/ZSiGyqO.png");
    public static Image truckIcaRightImage = new Image("http://i.imgur.com/M0uQ9Hw.png");
    public static Image truckHmRightImage = new Image("http://i.imgur.com/SNQTZoW.png");
    public static Image truckScaniaRightImage = new Image("http://i.imgur.com/GhKbsJw.png");
    
    public static Image bigCompanyImage = new Image("http://i.imgur.com/vXFaPxz.png");
    public static Image mediumCompanyImage = new Image("http://i.imgur.com/1pV83eh.png");
    public static Image smallCompanyImage = new Image("http://i.imgur.com/VkJ2c9u.png");
    
    public static Image flagSwedenImage = new Image("http://i.imgur.com/kJ5UOhQ.png");
    public static Image flagEuropeImage = new Image("http://i.imgur.com/EbG8uSF.png");
    
    public static Image homeButtonImage = new Image("http://i.imgur.com/tRWrqgv.png");
    public static Image helpButtonImage = new Image("http://i.imgur.com/EOEOk68.png");
    
    
    
}
