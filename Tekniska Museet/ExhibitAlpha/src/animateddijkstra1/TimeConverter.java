/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

/**
 *
 * @author rui
 */
public class TimeConverter {
    
    public int[] getHourTimes(double[] times){
        int[] hourTimes = new int[times.length];
        for ( int i = 0 ; i < times.length ; i++ ){
            //System.out.println("getHourTimes");
            //System.out.println("times[i]");
            //System.out.println(times[i]);
            //System.out.println((int)times[i]/(60*60));
            hourTimes[i] = (int)times[i]/(60*60);
        }               
        return hourTimes;
    }
    
    public int[] getMinuteTimes(double[] times){
        int[] minuteTimes = new int[times.length];
        for ( int i = 0 ; i < times.length ; i++ ){
            //System.out.println("getMinuteTimes");
            //System.out.println("times[i]");
            //System.out.println(times[i]);
            //System.out.println((int)times[i]%(3600)/60);
            minuteTimes[i] = (int)times[i]%(3600)/60;
        }               
        return minuteTimes;
    }
    
    public void getClockTimes(double[] times){
        
        int[] hourTimes = getHourTimes(times);
        int[] minuteTimes = getMinuteTimes(times);
        
        for ( int i = 0 ; i < hourTimes.length ; i++ ){
            if ( hourTimes[i] < 10 && minuteTimes[i] < 10){
                System.out.println("Time: 0"+hourTimes[i]+":0"+minuteTimes[i]);
            }
            if ( minuteTimes[i] < 10  && hourTimes[i] >= 10){
                System.out.println("Time: "+hourTimes[i]+":0"+minuteTimes[i]);
            }
            if ( minuteTimes[i] >= 10  && hourTimes[i] < 10){
                System.out.println("Time: 0"+hourTimes[i]+":"+minuteTimes[i]);
            }
            if ( hourTimes[i] >= 10 && minuteTimes[i] >= 10){
                System.out.println("Time: "+hourTimes[i]+":"+minuteTimes[i]);
            }
        }
        
    }
    
    public int getHourTime(double time){
        int hourTime;
        hourTime = (int)time/(60*60);
        return hourTime;
    }
    
    public int getMinuteTime(double time){
        int minuteTime;
            minuteTime = (int)time%(3600)/60;
        return minuteTime;
    }
    
    public String getClockTime(double time){
        
        int hourTime = getHourTime(time);
        int minuteTime = getMinuteTime(time);
        String timeString = new String();
        
        if ( hourTime < 10 && minuteTime < 10){
            timeString = "0"+hourTime+":0"+minuteTime;
        }
        if ( minuteTime < 10  && hourTime >= 10){
            timeString = hourTime+":0"+minuteTime;
        }
        if ( minuteTime >= 10  && hourTime < 10){
            timeString = "0"+hourTime+":"+minuteTime;
        }
        if ( hourTime >= 10 && minuteTime >= 10){
            timeString = hourTime+":"+minuteTime;
        }

        return timeString;
    }
    
    public void displayClockTimes(double[] times){
        
        int[] hourTimes = getHourTimes(times);
        int[] minuteTimes = getMinuteTimes(times);
        
        for ( int i = 0 ; i < hourTimes.length ; i++ ){
            if ( hourTimes[i] < 10 && minuteTimes[i] < 10){
                System.out.println("Time: 0"+hourTimes[i]+":0"+minuteTimes[i]);
            }
            if ( minuteTimes[i] < 10  && hourTimes[i] >= 10){
                System.out.println("Time: "+hourTimes[i]+":0"+minuteTimes[i]);
            }
            if ( minuteTimes[i] >= 10  && hourTimes[i] < 10){
                System.out.println("Time: 0"+hourTimes[i]+":"+minuteTimes[i]);
            }
            if ( hourTimes[i] >= 10 && minuteTimes[i] >= 10){
                System.out.println("Time: "+hourTimes[i]+":"+minuteTimes[i]);
            }
        }
        
    }
    
    
}
