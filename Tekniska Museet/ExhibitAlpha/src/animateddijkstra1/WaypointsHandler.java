/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class RoadWaypoints {
    
    public int srcId, dstId;
    public double[][] waypoints;
    public double[] srcPosition, dstPosition;
    
}


/**
 *
 * @author rui
 */
public class WaypointsHandler {
    
    boolean bigMode = Globals.bigMode;
    double fontStretch = Globals.fontStretch;
    double horizontalStretch = Globals.horizontalStretch;
    double verticalStretch = Globals.verticalStretch;
    
    List<RoadWaypoints> roadWaypoints = new ArrayList<>();
    
    List<RoadWaypoints> linearisedRoadWaypoints = new ArrayList<>();
    
    double numLinearisedPointsRatio = 1/6.;   
    
   
    WaypointsHandler(){
    
        RoadWaypoints tempRoadWaypoints0 = new RoadWaypoints();
        tempRoadWaypoints0.srcId = 0;
        tempRoadWaypoints0.dstId = 3;
    
        double[][] tempWaypoints0 =  { 
                        {478.9850,   23.6982},
                        {474.8609,   36.0706},
                        {477.9540,   46.3809},
                        {484.1401,   52.5671},
                        {490.3263,   59.7843},
                        {497.5435,   71.1256},
                        {499.6056,   83.4980},
                        {500.6366,   98.9635},
                        {501.6677,  112.3669},
                        {498.5746,  121.6461},
                        {494.4505,  131.9565},
                        {491.3574,  143.2978},
                        {481.0470,  155.6702}};
        
        tempRoadWaypoints0.waypoints = tempWaypoints0;
        
        roadWaypoints.add(tempRoadWaypoints0);
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints1 = new RoadWaypoints();
        tempRoadWaypoints1.srcId = 3;
        tempRoadWaypoints1.dstId = 2;
    
        double[][] tempWaypoints1 =  { 
                        {482.0781,  154.6391},
                        {468.6747,  154.6391},
                        {461.4575,  152.5771},
                        {449.0851,  152.5771},
                        {437.7437,  156.7012},
                        {430.5265,  158.7633},
                        {416.0921,  158.7633},
                        {408.8749,  162.8874},
                        {402.6887,  165.9805},
                        {389.2853,  169.0736}};
        
        tempRoadWaypoints1.waypoints = tempWaypoints1;
        
        roadWaypoints.add(tempRoadWaypoints1);
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints2 = new RoadWaypoints();
        tempRoadWaypoints2.srcId = 1;
        tempRoadWaypoints2.dstId = 2;
    
        double[][] tempWaypoints2 =  { 
                        {108.8448,  143.2978},
                        {118.1241,  143.2978},
                        {129.4655,  143.2978},
                        {142.8689,  144.3288},
                        {154.2102,  148.4530},
                        {169.6757,  154.6391},
                        {182.0480,  156.7012},
                        {196.4825,  156.7012},
                        {209.8859,  156.7012},
                        {216.0721,  156.7012},
                        {222.2583,  150.5150},
                        {233.5996,  146.3909},
                        {241.8478,  146.3909},
                        {255.2513,  148.4530},
                        {262.4685,  151.5460},
                        {269.6857,  156.7012},
                        {276.9029,  159.7943},
                        {289.2753,  161.8564},
                        {298.5546,  161.8564},
                        {307.8338,  161.8564},
                        {316.0821,  163.9184},
                        {325.3614,  167.0115},
                        {334.6406,  170.1046},
                        {342.8889,  175.2598},
                        {353.1992,  177.3218},
                        {361.4474,  177.3218},
                        {367.6336,  177.3218},
                        {375.8819,  174.2287},
                        {388.2543,  168.0425}};
        
        tempRoadWaypoints2.waypoints = tempWaypoints2;
        
        roadWaypoints.add(tempRoadWaypoints2);        
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints3 = new RoadWaypoints();
        tempRoadWaypoints3.srcId = 1;
        tempRoadWaypoints3.dstId = 5;
    
        double[][] tempWaypoints3 =  { 
                        {112.5719,  142.6856},
                        {106.9396,  152.6985},
                        {96.9267,  157.7049},
                        {93.1719,  165.2146},
                        {88.7912,  171.4727},
                        {83.1590,  177.7307},
                        {78.1525,  185.2404},
                        {76.9009,  194.0017},
                        {80.6558,  199.0081},
                        {83.7848,  203.3888},
                        {91.9203,  208.3952},
                        {106.9396,  213.4017},
                        {118.8299,  217.7823},
                        {125.0880,  222.7888},
                        {131.9719,  227.7952},
                        {136.9783,  235.9307},
                        {140.7332,  243.4404},
                        {138.8557,  252.2017},
                        {138.8557,  259.0855},
                        {137.6041,  270.3501},
                        {135.7267,  277.8597},
                        {136.9783,  287.2468},
                        {140.1074,  295.3823},
                        {143.2364,  304.7694},
                        {148.2428,  310.4017},
                        {156.3783,  311.6533},
                        {170.7719,  317.2855},
                        {176.4041,  321.6662},
                        {184.5396,  326.6726},
                        {188.2944,  332.3049},
                        {189.5461,  337.3113},
                        {189.5461,  342.3178},
                        {186.4170,  349.2016},
                        {185.7912,  355.4597},
                        {185.1654,  361.0920},
                        {183.9138,  370.4791},
                        {187.6686,  382.3694},
                        {190.7977,  391.1307},
                        {192.6751,  399.2661},
                        {195.1783,  406.1500},
                        {197.6815,  416.1629},
                        {200.8106,  425.5500},
                        {204.5654,  435.5629},
                        {210.1977,  443.6984},
                        {213.9525,  449.9565},
                        {218.9589,  459.9694},
                        {222.0880,  466.8532},
                        {228.9718,  481.2468}};
        
        tempRoadWaypoints3.waypoints = tempWaypoints3;
        
        roadWaypoints.add(tempRoadWaypoints3);             
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints4 = new RoadWaypoints();
        tempRoadWaypoints4.srcId = 4;
        tempRoadWaypoints4.dstId = 5;
    
        double[][] tempWaypoints4 =  { 
                        {121.9590,  443.0726},
                        {140.7332,  449.3306},
                        {155.7525,  453.7113},
                        {168.2686,  458.7177},
                        {180.7848,  463.7242},
                        {196.4299,  466.8532},
                        {202.0622,  472.4855},
                        {211.4493,  474.9887},
                        {227.7202,  479.3693}};
        
        tempRoadWaypoints4.waypoints = tempWaypoints4;
        
        roadWaypoints.add(tempRoadWaypoints4);
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints5 = new RoadWaypoints();
        tempRoadWaypoints5.srcId = 2;
        tempRoadWaypoints5.dstId = 7;
    
        double[][] tempWaypoints5 =  { 
                        {386.6750,  168.3436},
                        {394.1847,  183.3630},
                        {400.4428,  195.8791},
                        {405.4492,  202.7630},
                        {414.8363,  213.4017},
                        {414.8363,  225.2920},
                        {413.5847,  237.1823},
                        {415.4621,  246.5694},
                        {419.2170,  255.9565},
                        {424.8492,  265.3436},
                        {431.1073,  277.8597},
                        {437.3653,  289.7500},
                        {442.9976,  301.0146},
                        {448.6298,  314.1565},
                        {456.1395,  319.1629},
                        {462.3976,  327.9242},
                        {469.2815,  336.0597},
                        {474.2879,  341.6920},
                        {485.5524,  348.5758},
                        {496.8169,  359.8403}};
        
        tempRoadWaypoints5.waypoints = tempWaypoints5;
        
        roadWaypoints.add(tempRoadWaypoints5);              
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints6 = new RoadWaypoints();
        tempRoadWaypoints6.srcId = 5;
        tempRoadWaypoints6.dstId = 6;
    
        double[][] tempWaypoints6 =  { 
                        {228.9718,  481.8726},
                        {246.4944,  482.4984},
                        {260.2622,  482.4984},
                        {271.5267,  486.2532},
                        {285.2944,  487.5048},
                        {299.0621,  488.7564},
                        {311.5783,  487.5048},
                        {322.2170,  485.6274},
                        {332.2299,  485.0016},
                        {340.3654,  488.1306},
                        {352.2557,  496.2661},
                        {363.5202,  496.8919},
                        {376.6621,  498.7693}};
        
        tempRoadWaypoints6.waypoints = tempWaypoints6;
        
        roadWaypoints.add(tempRoadWaypoints6);                      
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints7 = new RoadWaypoints();
        tempRoadWaypoints7.srcId = 6;
        tempRoadWaypoints7.dstId = 7;
    
        double[][] tempWaypoints7 =  { 
                        {376.6621,  500.0210},
                        {377.2879,  480.6210},
                        {377.9137,  464.9758},
                        {383.5460,  454.3371},
                        {382.2944,  439.9436},
                        {379.7912,  429.3048},
                        {376.0363,  416.1629},
                        {379.1654,  408.0274},
                        {384.1718,  403.0210},
                        {394.1847,  398.0145},
                        {403.5718,  403.0210},
                        {413.5847,  407.4016},
                        {421.0944,  410.5307},
                        {430.4815,  411.1565},
                        {437.9911,  409.9049},
                        {446.7524,  402.3952},
                        {451.7589,  392.3823},
                        {454.8879,  384.8726},
                        {462.3976,  377.3629},
                        {474.2879,  368.6016},
                        {485.5524,  364.8468},
                        {496.1911,  358.5887}};
        
        tempRoadWaypoints7.waypoints = tempWaypoints7;
        
        roadWaypoints.add(tempRoadWaypoints7);               
         //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints8 = new RoadWaypoints();
        tempRoadWaypoints8.srcId = 3;
        tempRoadWaypoints8.dstId = 8;
    
        double[][] tempWaypoints8 =  { 
                        {482.4234,  156.4533},
                        {496.8169,  161.4598},
                        {504.9524,  171.4727},
                        {511.8363,  190.2468},
                        {518.7202,  205.8920},
                        {523.1008,  226.5436},
                        {529.3589,  242.1888},
                        {531.2363,  257.8339},
                        {538.1201,  269.7242},
                        {542.5008,  284.1178},
                        {547.5072,  296.6339},
                        {548.1330,  307.8984},
                        {543.7524,  318.5371},
                        {539.3718,  329.8016},
                        {534.3653,  344.8210}};
        
        tempRoadWaypoints8.waypoints = tempWaypoints8;
        
        roadWaypoints.add(tempRoadWaypoints8);                      
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints9 = new RoadWaypoints();
        tempRoadWaypoints9.srcId = 8;
        tempRoadWaypoints9.dstId = 9;
    
        double[][] tempWaypoints9 =  { 
                        {530.6105,  344.8210},
                        {539.9976,  356.0855},
                        {550.6363,  366.0984},
                        {561.2750,  373.6081},
                        {569.4105,  380.4920},
                        {580.6750,  388.6274},
                        {596.9459,  397.3887},
                        {606.3330,  403.0210},
                        {626.9846,  411.7823}};
        
        tempRoadWaypoints9.waypoints = tempWaypoints9;
        
        roadWaypoints.add(tempRoadWaypoints9);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints10 = new RoadWaypoints();
        tempRoadWaypoints10.srcId = 17;
        tempRoadWaypoints10.dstId = 10;
    
        double[][] tempWaypoints10 =  { 
                        {699.6256,  368.0626},
                        {709.9359,  361.8764},
                        {719.2152,  359.8143},
                        {725.4014,  352.5971},
                        {728.4945,  338.1627},
                        {728.4945,  326.8213},
                        {728.4945,  314.4489},
                        {728.4945,  305.1697},
                        {739.8358,  296.9214},
                        {744.9910,  286.6111},
                        {757.3634,  278.3629},
                        {767.6737,  277.3318},
                        {779.0150,  276.3008},
                        {792.4184,  271.1456},
                        {801.6977,  271.1456},
                        {818.1942,  275.2698}};
        
        tempRoadWaypoints10.waypoints = tempWaypoints10;
        
        roadWaypoints.add(tempRoadWaypoints10);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints11 = new RoadWaypoints();
        tempRoadWaypoints11.srcId = 9;
        tempRoadWaypoints11.dstId = 11;
    
        double[][] tempWaypoints11 =  { 
                        {627.8033,  414.4709},
                        {622.1711,  421.9805},
                        {616.5388,  428.8644},
                        {611.5324,  435.1225},
                        {609.6549,  441.3805},
                        {607.1517,  448.8902},
                        {605.9001,  457.0257},
                        {602.1453,  465.7870},
                        {597.1388,  473.9224},
                        {591.5066,  482.6837},
                        {583.3711,  490.8192},
                        {576.4872,  496.4515},
                        {570.8550,  503.9611},
                        {555.2098,  520.2321}};
        
        tempRoadWaypoints11.waypoints = tempWaypoints11;
        
        roadWaypoints.add(tempRoadWaypoints11);               
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints12 = new RoadWaypoints();
        tempRoadWaypoints12.srcId = 6;
        tempRoadWaypoints12.dstId = 11;
    
        double[][] tempWaypoints12 =  { 
                        {374.4583,  498.4211},
                        {393.2325,  496.5437},
                        {407.6260,  496.5437},
                        {417.6389,  501.5502},
                        {423.8970,  506.5566},
                        {436.4131,  514.0663},
                        {450.1809,  520.3244},
                        {460.8196,  524.0792},
                        {475.8389,  527.2082},
                        {487.1034,  529.0856},
                        {502.1228,  530.9631},
                        {519.0195,  530.3373},
                        {533.4131,  530.3373},
                        {546.5550,  527.8340},
                        {559.0711,  517.8211}};
        
        tempRoadWaypoints12.waypoints = tempWaypoints12;
        
        roadWaypoints.add(tempRoadWaypoints12);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints13 = new RoadWaypoints();
        tempRoadWaypoints13.srcId = 6;
        tempRoadWaypoints13.dstId = 12;
    
        double[][] tempWaypoints13 =  { 
                        {376.3357,  498.4211},
                        {376.3357,  510.9373},
                        {376.3357,  524.0792},
                        {371.9551,  532.8405},
                        {360.6906,  540.9760},
                        {351.9293,  546.6082},
                        {344.4196,  551.6147},
                        {334.4067,  556.6211},
                        {330.6519,  567.8856},
                        {330.6519,  581.6534},
                        {333.7809,  587.9114},
                        {342.5422,  598.5501},
                        {346.2970,  610.4405},
                        {354.4325,  620.4534},
                        {360.6906,  633.5953},
                        {364.4454,  640.4792},
                        {368.8260,  653.6211},
                        {374.4583,  660.5050},
                        {381.9680,  672.3953},
                        {386.9744,  676.7759},
                        {393.8583,  681.7824},
                        {401.9938,  688.6662},
                        {408.8776,  693.0469},
                        {417.0131,  695.5501},
                        {418.2647,  706.8146}};
  

        
        tempRoadWaypoints13.waypoints = tempWaypoints13;
        
        roadWaypoints.add(tempRoadWaypoints13);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints14 = new RoadWaypoints();
        tempRoadWaypoints14.srcId = 13;
        tempRoadWaypoints14.dstId = 12;
    
        double[][] tempWaypoints14 =  { 
                        {28.0880,  914.4129},
                        {33.0945,  898.7678},
                        {36.8493,  885.0000},
                        {43.7332,  873.1097},
                        {49.3654,  863.0968},
                        {60.0042,  857.4646},
                        {70.6429,  849.3291},
                        {81.2816,  841.8194},
                        {98.8041,  834.3097},
                        {113.8235,  827.4259},
                        {132.5977,  821.7936},
                        {149.4944,  813.6581},
                        {160.7590,  807.4001},
                        {171.3977,  800.5162},
                        {195.8041,  801.1420},
                        {208.9460,  802.3936},
                        {225.2170,  803.0194},
                        {240.2364,  803.0194},
                        {255.2557,  802.3936},
                        {268.3976,  803.0194},
                        {284.0428,  801.1420},
                        {300.9396,  796.7614},
                        {313.4557,  791.1291},
                        {325.9718,  786.1226},
                        {336.6105,  779.8646},
                        {344.7460,  771.1033},
                        {352.8815,  760.4646},
                        {359.1396,  749.2001},
                        {364.1460,  738.5614},
                        {381.6686,  728.5485},
                        {396.6879,  723.5420},
                        {407.9524,  716.6582},
                        {419.8428,  707.2711}};
  
        
        tempRoadWaypoints14.waypoints = tempWaypoints14;
        
        roadWaypoints.add(tempRoadWaypoints14);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints15 = new RoadWaypoints();
        tempRoadWaypoints15.srcId = 11;
        tempRoadWaypoints15.dstId = 14;
    
        double[][] tempWaypoints15 =  { 
                        {555.8597,  518.2162},
                        {559.6146,  530.7323},
                        {569.0017,  535.7388},
                        {578.3888,  543.8742},
                        {583.3952,  555.1388},
                        {585.8984,  567.6549},
                        {589.6533,  577.0420},
                        {592.7823,  588.3065},
                        {595.2855,  597.6936},
                        {595.9113,  609.5839},
                        {601.5436,  618.9710},
                        {604.0468,  628.9839},
                        {607.1758,  640.8742},
                        {609.0533,  655.8936},
                        {608.4275,  666.5323},
                        {609.6791,  674.6678},
                        {617.1887,  682.1774},
                        {620.9436,  694.0677},
                        {625.9500,  702.8290},
                        {630.3307,  710.9645},
                        {635.3371,  720.3516},
                        {637.2145,  734.1193},
                        {639.0920,  743.5064}};
        
        tempRoadWaypoints15.waypoints = tempWaypoints15;
        
        roadWaypoints.add(tempRoadWaypoints15);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints16 = new RoadWaypoints();
        tempRoadWaypoints16.srcId = 14;
        tempRoadWaypoints16.dstId = 15;
    
        double[][] tempWaypoints16 =  { 
                        {645.3500,  749.7645},
                        {650.3565,  766.6613},
                        {653.4855,  779.1774},
                        {657.2403,  785.4355},
                        {661.6210,  795.4484},
                        {662.8726,  807.3387},
                        {662.8726,  815.4742},
                        {662.8726,  826.1129},
                        {664.1242,  834.8742},
                        {663.4984,  845.5129},
                        {667.2532,  853.0225},
                        {670.3823,  861.7838},
                        {671.6339,  876.1774}};
        
        tempRoadWaypoints16.waypoints = tempWaypoints16;
        
        roadWaypoints.add(tempRoadWaypoints16);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints17 = new RoadWaypoints();
        tempRoadWaypoints17.srcId = 14;
        tempRoadWaypoints17.dstId = 16;
    
        double[][] tempWaypoints17 =  { 
                        {644.2,    751.0},
                        {659.8,    744.1},
                        {678.0,    737.9},
                        {695.5,    732.2},
                        {713.0,    722.2},
                        {728.0,    713.5},
                        {743.0,    707.2},
                        {752.4,    700.3},
                        {757.4,    692.2},
                        {760.6,    681.5},
                        {774.3,    669.6},
                        {786.8,    661.5},
                        {798.1,    656.5},
                        {811.2,    652.1},
                        {822.5,    652.7},
                        {840.0,    645.9},
                        {851.3,    645.9},
                        {865.7,    644.0},
                        {877.0,    638.4},
                        {892.0,    636.5},
                        {906.4,    637.1},
                        {923.9,    638.4},
                        {935.2,    639.0},
                        {952.1,    635.9},
                        {967.1,    632.1},
                        {981.5,    628.3},
                        {1002.7,    622.7},
                        {1019.0,    617.7},
                        {1039.7,    612.1},
                        {1061.6,    609.6},
                        {1077.8,    608.3},
                        {1088.5,    609.6},
                        {1101.6,    612.7},
                        {1117.9,    618.3},
                        {1132.3,    624.0},
                        {1144.2,    624.6},
                        {1154.2,    624.6}};
        
        tempRoadWaypoints17.waypoints = tempWaypoints17;
        
        roadWaypoints.add(tempRoadWaypoints17);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints18 = new RoadWaypoints();
        tempRoadWaypoints18.srcId = 12;
        tempRoadWaypoints18.dstId = 14;
    
        double[][] tempWaypoints18 =  { 
                        {419.9693,  708.5227 },
                        {426.8532,  716.6582},
                        {434.3628,  724.1678},
                        {438.7435,  734.1807},
                        {445.0015,  740.4388},
                        {453.1370,  746.6969},
                        {466.2790,  747.9485},
                        {482.5499,  748.5743},
                        {494.4402,  749.2001},
                        {504.4531,  749.2001},
                        {513.8402,  748.5743},
                        {530.1112,  748.5743},
                        {542.0015,  749.8259},
                        {553.2660,  749.2001},
                        {574.5434,  749.2001},
                        {586.4338,  749.2001},
                        {600.8273,  749.2001},
                        {613.3434,  748.5743},
                        {623.3563,  747.3227},
                        {635.8725,  747.3227}};
        
        tempRoadWaypoints18.waypoints = tempWaypoints18;
        
        roadWaypoints.add(tempRoadWaypoints18);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints19 = new RoadWaypoints();
        tempRoadWaypoints19.srcId = 7;
        tempRoadWaypoints19.dstId = 8;
    
        double[][] tempWaypoints19 =  { 
                    {498.5535,  357.8548},
                    {508.5664,  353.4742},
                    {519.2051,  349.7193},
                    {531.0954,  345.3387}};
  
        
        tempRoadWaypoints19.waypoints = tempWaypoints19;
        
        roadWaypoints.add(tempRoadWaypoints19);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints20 = new RoadWaypoints();
        tempRoadWaypoints20.srcId = 17;
        tempRoadWaypoints20.dstId = 18;
    
        double[][] tempWaypoints20 =  { 
                        {699.2044,  370.2603},
                        {707.4245,  374.9183},
                        {715.6445,  379.3023},
                        {720.5765,  382.5903},
                        {726.8786,  385.3304},
                        {731.5366,  387.5224},
                        {738.9347,  391.3584},
                        {743.5927,  395.1944},
                        {746.6067,  398.7564},
                        {750.1687,  402.5925},
                        {752.9087,  406.9765},
                        {752.6347,  412.4565},
                        {750.7167,  417.9366},
                        {750.4427,  422.8686},
                        {751.5387,  428.3486},
                        {752.0867,  434.1027},
                        {757.8408,  443.4187}};  
        
        tempRoadWaypoints20.waypoints = tempWaypoints20;
        
        roadWaypoints.add(tempRoadWaypoints20);                       
        //////////////////////////////////////////////////////////
        RoadWaypoints tempRoadWaypoints21 = new RoadWaypoints();
        tempRoadWaypoints21.srcId = 9;
        tempRoadWaypoints21.dstId = 17;
    
        double[][] tempWaypoints21 =  { 
                        {627.9640,  413.2785},
                        {636.1840,  404.5105},
                        {644.9521,  398.2084},
                        {657.2822,  390.5364},
                        {669.6122,  384.2344},
                        {680.5723,  379.3023},
                        {689.3404,  374.9183},
                        {698.3824,  368.6163}};
        
        tempRoadWaypoints21.waypoints = tempWaypoints21;
        
        roadWaypoints.add(tempRoadWaypoints21);                       
       
        
        if (bigMode){
        
            for ( int i = 0 ; i < roadWaypoints.size() ; i++ ){

                for ( int j = 0 ; j < roadWaypoints.get(i).waypoints.length ; j++ ){

                    roadWaypoints.get(i).waypoints[j][0] = roadWaypoints.get(i).waypoints[j][0]*horizontalStretch;
                    roadWaypoints.get(i).waypoints[j][1] = roadWaypoints.get(i).waypoints[j][1]*verticalStretch;


                }

            }
        
        }
        
    }
    
    
    
    
    private void setCityPositions(){
        
        int[][] tempCityPositions = { {474,    22},    {106,    140},    {386,    164},    {488,    154},
        {124,    440},    {228,    477},    {376,    503},    {495,    360},
        {534,    342},    {626,    413},    {816,    271},    {556,    518},
        {416,    710},    {030,    901},    {642,    748},    {675,    880},
        {1159,    620},   {699,    370},    {758,    443}};
        
//        cityPositions = tempCityPositions;
        
//        THIS IS FOR ALL SWEDEN BEGIN
//        numberCities = 24;
//        THIS IS FOR ALL SWEDEN BEGIN    
        int numberCities = 19; // before hidden nodes
//        numberCities = tempCityPositions.length; 
           
        
        for ( int i = 0 ; i < roadWaypoints.size() ; i++ ){
            
            RoadWaypoints tempRoadWaypoints = roadWaypoints.get(i);
            
            tempRoadWaypoints.srcPosition = new double[2];
            tempRoadWaypoints.srcPosition[0] = tempCityPositions[tempRoadWaypoints.srcId][0];
            tempRoadWaypoints.srcPosition[1] = tempCityPositions[tempRoadWaypoints.srcId][1];
            
            tempRoadWaypoints.dstPosition = new double[2];
            tempRoadWaypoints.dstPosition[0] = tempCityPositions[tempRoadWaypoints.dstId][0];
            tempRoadWaypoints.dstPosition[1] = tempCityPositions[tempRoadWaypoints.dstId][1];
            
            
        }
        
        if (bigMode){
            
            for ( int i = 0 ; i < roadWaypoints.size() ; i++ ){
            
                RoadWaypoints tempRoadWaypoints = roadWaypoints.get(i);

                tempRoadWaypoints.srcPosition[0] = tempRoadWaypoints.srcPosition[0]*horizontalStretch;
                tempRoadWaypoints.srcPosition[1] = tempRoadWaypoints.srcPosition[1]*verticalStretch;

                tempRoadWaypoints.dstPosition[0] = tempRoadWaypoints.dstPosition[0]*horizontalStretch;
                tempRoadWaypoints.dstPosition[1] = tempRoadWaypoints.dstPosition[1]*verticalStretch;
            
            }
            
            
        }
        
//        cityLinks = new int[numberCities][];
//        cityPositions = new double[numberCities][2];
//        // Convert from kilometres to metres
//        
//        for ( int i = 0 ; i < cityPositions.length ; i++ ){
//            doubleCityPositions[i][0] = cityPositions[i][0];
//            doubleCityPositions[i][1] = cityPositions[i][1];
//        }
//        
        
        
    }
    
    
    public List<RoadWaypoints> getRoadWaypoints(){
    
        return roadWaypoints;
    
    }
    
    public List<RoadWaypoints> getRoadWaypointsLinearised(){
        
        List<RoadWaypoints> listLinearisedRoadWaypoints = new ArrayList<>();
        
        for ( int i = 0 ; i < roadWaypoints.size() ; i++ ){
            
            RoadWaypoints currentWaypoints = roadWaypoints.get(i);
            
            double totalDistance = 0;
            double tempDistance = 0;
            
            double[] waypointsDistance = new double[currentWaypoints.waypoints.length];
            waypointsDistance[0] = 0.;
            
            for ( int j = 1 ; j < currentWaypoints.waypoints.length ; j++ ){
                
                tempDistance = Math.sqrt( Math.pow( currentWaypoints.waypoints[j][0] - currentWaypoints.waypoints[j-1][0] , 2) 
                        + Math.pow( currentWaypoints.waypoints[j][1] - currentWaypoints.waypoints[j-1][1] , 2) );
                
                totalDistance += tempDistance;
                
                waypointsDistance[j] = totalDistance;
                
            }
            
            int numLinearisedPoints = (int) Math.round( numLinearisedPointsRatio * totalDistance );
            
//            System.out.println("numLinearisedPointsRatio");
//            System.out.println(numLinearisedPointsRatio);
//            System.out.println("totalDistance");
//            System.out.println(totalDistance);
//            System.out.println("numLinearisedPoints");
//            System.out.println(numLinearisedPoints);
            
            RoadWaypoints linearisedRoadWaypoints = new RoadWaypoints();
            linearisedRoadWaypoints.srcId = currentWaypoints.srcId;
            linearisedRoadWaypoints.dstId = currentWaypoints.dstId;
            linearisedRoadWaypoints.waypoints = new double[numLinearisedPoints][2];

            
            for ( int j = 0 ; j < numLinearisedPoints ; j++ ) {
                
                if ( j == 0){
                    
                    double tempX = currentWaypoints.waypoints[0][0];
                    double tempY = currentWaypoints.waypoints[0][1];
                
                    linearisedRoadWaypoints.waypoints[j][0] = tempX;
                    linearisedRoadWaypoints.waypoints[j][1] = tempY;
                    
                    continue;
                    
                }
                
                if ( j == numLinearisedPoints - 1){
                    
                    double tempX = currentWaypoints.waypoints[ currentWaypoints.waypoints.length - 1 ][0];
                    double tempY = currentWaypoints.waypoints[ currentWaypoints.waypoints.length - 1 ][1];
                
                    linearisedRoadWaypoints.waypoints[j][0] = tempX;
                    linearisedRoadWaypoints.waypoints[j][1] = tempY;
                    
                    continue;
                    
                }
                
                double fractionPath = ((double)j)/((double) (numLinearisedPoints-1) );
                
//                System.out.println("fractionPath");
//                System.out.println(fractionPath);
                
                fractionPath = fractionPath * waypointsDistance[waypointsDistance.length-1];
                
                int prevIdx = 0;
                
                for ( int k = 1 ; k < waypointsDistance.length ; k++ ){
                    
                    if (fractionPath > waypointsDistance[k-1] && fractionPath < waypointsDistance[k]){
                        
                        prevIdx = k-1;
                        break;
                        
                    }
                   
                }
                
//                System.out.println("");
//                System.out.println("fractionPath");
//                System.out.println(fractionPath);
//                System.out.println("waypointsDistance");
//                System.out.println(Arrays.toString(waypointsDistance));
//                System.out.println("prevIdx");
//                System.out.println(prevIdx);
                
                
                double distStart =  fractionPath -  waypointsDistance[prevIdx];
                double distEnd =  waypointsDistance[prevIdx+1] - fractionPath;
                
//                System.out.println("distStart");
//                System.out.println(distStart);
//                System.out.println("distEnd");
//                System.out.println(distEnd);

                double fractionStart = distStart/(distStart+distEnd);
                double fractionEnd = distEnd/(distStart+distEnd);
                
//                double distToStart = Math.ceil(fractionPath)-fractionPath;
//                double distToStart = Math.sqrt( Math.pow( currentWaypoints.waypoints[ prevIdx ][0] - currentWaypoints.waypoints[ fractionPath ][0] ,2)
//                                                + Math.pow( currentWaypoints.waypoints[ prevIdx ][1] - currentWaypoints.waypoints[ fractionPath ][1] ,2) );
//                
//                double distToEnd = fractionPath - Math.floor(fractionPath);
                
                double tempX = fractionStart * currentWaypoints.waypoints[ prevIdx+1 ][0] + 
                        fractionEnd * currentWaypoints.waypoints[ prevIdx ][0];
                double tempY = fractionStart * currentWaypoints.waypoints[ prevIdx+1 ][1] + 
                        fractionEnd * currentWaypoints.waypoints[ prevIdx ][1];
                
                linearisedRoadWaypoints.waypoints[j][0] = tempX;
                linearisedRoadWaypoints.waypoints[j][1] = tempY;
                
            }
            
            
            
            listLinearisedRoadWaypoints.add(linearisedRoadWaypoints);
            
            
        }
        
                
        return listLinearisedRoadWaypoints;
        
        
    }
    
    
    public List<RoadWaypoints> getRoadWaypointsStraight(){
        
        setCityPositions();
        
        List<RoadWaypoints> listRoadWaypointsStraight = new ArrayList<>();
        
        for ( int i = 0 ; i < roadWaypoints.size() ; i++ ){
            
            RoadWaypoints currentWaypoints = roadWaypoints.get(i);
            
            double totalDistance = 0;
            double tempDistance = 0;
            
            double[] waypointsDistance = new double[currentWaypoints.waypoints.length];
            waypointsDistance[0] = 0.;
            
            for ( int j = 1 ; j < currentWaypoints.waypoints.length ; j++ ){
                
                tempDistance = Math.sqrt( Math.pow( currentWaypoints.waypoints[j][0] - currentWaypoints.waypoints[j-1][0] , 2) 
                        + Math.pow( currentWaypoints.waypoints[j][1] - currentWaypoints.waypoints[j-1][1] , 2) );
                
                totalDistance += tempDistance;
                
                waypointsDistance[j] = totalDistance;
                
            }
            
            int numLinearisedPoints = (int) Math.round( numLinearisedPointsRatio * totalDistance );
            
//            double[] waypointsDistance = new double[currentWaypoints.waypoints.length];
//            waypointsDistance[0] = 0.;
//            
//            for ( int j = 1 ; j < currentWaypoints.waypoints.length ; j++ ){
//                
//                tempDistance = Math.sqrt( Math.pow( currentWaypoints.waypoints[j][0] - currentWaypoints.waypoints[j-1][0] , 2) 
//                        + Math.pow( currentWaypoints.waypoints[j][1] - currentWaypoints.waypoints[j-1][1] , 2) );
//                
//                totalDistance += tempDistance;
//                
//                waypointsDistance[j] = totalDistance;
//                
//            }
            
            RoadWaypoints straightRoadWaypoints = new RoadWaypoints();
            straightRoadWaypoints.srcId = currentWaypoints.srcId;
            straightRoadWaypoints.dstId = currentWaypoints.dstId;
            straightRoadWaypoints.waypoints = new double[numLinearisedPoints][2];

            
            for ( int j = 0 ; j < numLinearisedPoints ; j++ ) {
                
                if ( j == 0){
                    
                    double tempX = currentWaypoints.waypoints[0][0];
                    double tempY = currentWaypoints.waypoints[0][1];
                
                    straightRoadWaypoints.waypoints[j][0] = tempX;
                    straightRoadWaypoints.waypoints[j][1] = tempY;
                    
                    continue;
                    
                }
                
                if ( j == numLinearisedPoints - 1){
                    
                    double tempX = currentWaypoints.waypoints[ currentWaypoints.waypoints.length - 1 ][0];
                    double tempY = currentWaypoints.waypoints[ currentWaypoints.waypoints.length - 1 ][1];
                
                    straightRoadWaypoints.waypoints[j][0] = tempX;
                    straightRoadWaypoints.waypoints[j][1] = tempY;
                    
                    continue;
                    
                }
                
                double fractionPathStart = ((double)j)/((double) (numLinearisedPoints-1) );
                double fractionPathEnd = 1 - fractionPathStart;
                
//                System.out.println("fractionPath");
//                System.out.println(fractionPath);
                
//                fractionPath = fractionPath * waypointsDistance[waypointsDistance.length-1];
                
//                int prevIdx = 0;
//                
//                for ( int k = 1 ; k < waypointsDistance.length ; k++ ){
//                    
//                    if (fractionPath > waypointsDistance[k-1] && fractionPath < waypointsDistance[k]){
//                        
//                        prevIdx = k-1;
//                        break;
//                        
//                    }
//                   
//                }
//                
//                System.out.println("");
//                System.out.println("fractionPath");
//                System.out.println(fractionPath);
//                System.out.println("waypointsDistance");
//                System.out.println(Arrays.toString(waypointsDistance));
//                System.out.println("prevIdx");
//                System.out.println(prevIdx);
//                
//                
//                double distStart =  fractionPath -  waypointsDistance[prevIdx];
//                double distEnd =  waypointsDistance[prevIdx+1] - fractionPath;
//                
//                System.out.println("distStart");
//                System.out.println(distStart);
//                System.out.println("distEnd");
//                System.out.println(distEnd);
//
//                double fractionStart = distStart/(distStart+distEnd);
//                double fractionEnd = distEnd/(distStart+distEnd);
                
//                double distToStart = Math.ceil(fractionPath)-fractionPath;
//                double distToStart = Math.sqrt( Math.pow( currentWaypoints.waypoints[ prevIdx ][0] - currentWaypoints.waypoints[ fractionPath ][0] ,2)
//                                                + Math.pow( currentWaypoints.waypoints[ prevIdx ][1] - currentWaypoints.waypoints[ fractionPath ][1] ,2) );
//                
//                double distToEnd = fractionPath - Math.floor(fractionPath);
                
                
                double tempX = fractionPathStart * currentWaypoints.dstPosition[0] + 
                        fractionPathEnd * currentWaypoints.srcPosition[0];
                double tempY = fractionPathStart * currentWaypoints.dstPosition[1]  + 
                        fractionPathEnd * currentWaypoints.srcPosition[1];
//                double tempX = fractionPathStart * currentWaypoints.waypoints[ currentWaypoints.waypoints.length - 1 ][0] + 
//                        fractionPathEnd * currentWaypoints.waypoints[ 0 ][0];
//                double tempY = fractionPathStart * currentWaypoints.waypoints[ currentWaypoints.waypoints.length - 1 ][1] + 
//                        fractionPathEnd * currentWaypoints.waypoints[ 0 ][1];
                
                straightRoadWaypoints.waypoints[j][0] = tempX;
                straightRoadWaypoints.waypoints[j][1] = tempY;
                
            }
            
            
            
            listRoadWaypointsStraight.add(straightRoadWaypoints);
            
            
        }
        
                
        return listRoadWaypointsStraight;
        
        
    }
    
}
