/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

/**
 *
 * @author rui
 */
class PlatoonScheduler{
    
    double[][] adjMatrix;
       
    public PlatoonScheduler( DijkstraSolver argCityDijkstra ){
        
        adjMatrix = argCityDijkstra.getAdjMatrix();
        
    }
        
    public PlatoonPlan findPlatoonPossibility(Truck followingTruck, Truck followedTruck){
        
        int[] sharedPath;
        PlatoonPlan currentPlatoon = new PlatoonPlan();        
        
        currentPlatoon.path = followedTruck.path;
        currentPlatoon.setPlatoonPossibility(false);
        
        
        sharedPath = findCommonPath(followingTruck.getPath(), followedTruck.getPath());
        currentPlatoon.setSharedPlatoonPath(sharedPath);
        
        if (sharedPath.length == 0){
            
            //System.out.println("No sharedPath found");
            
        }else{
            
            double[] sharedTimes;
            sharedTimes = findCommonTimes(followingTruck, followedTruck , sharedPath);
            
            if ( sharedTimes[0] < followingTruck.getDepartureTime() ){
                //System.out.println("Platoon would require leaving before the current time");
            }else{
                
                double sharedDistance;
                sharedDistance = findSharedDistance(sharedPath);

                currentPlatoon.setPlatoonPossibility(true);
                currentPlatoon.setPlatoonTimes(sharedTimes);
                
                currentPlatoon.setSharedDistance(sharedDistance);
                                
                currentPlatoon.setFollowedTruck(followedTruck);
            }

            
        }
        
        
        
        return currentPlatoon;
    }
    
    public double findSharedDistance(int[] sharedPath) {
        
        double sharedDistance = 0;
        
        for ( int i = 1 ; i < sharedPath.length ; i++ ){
            
            sharedDistance += getWeight(sharedPath[i-1], sharedPath[i]);
            
        }
        
        return sharedDistance;
    
    }
    
    public double[] findCommonTimes(Truck followingTruck, Truck followedTruck, int[] sharedPath  ){
        
        
        int[] myPath = followingTruck.getPath();
        int[] followedPath = followedTruck.getPath();
        
        double[] followedTimes = followedTruck.getTimes();
        double[] sharedTimes = new double[myPath.length];
        
        int myStartIdx, myEndIdx;
        int followedStartIdx, followedEndIdx;
        myStartIdx = followedStartIdx = -1;
        
        int cnt = 0;
        
        for ( int i = 0 ; i < myPath.length ; i++ ){
            if ( myPath[i] == sharedPath[cnt] ){
                myStartIdx = i;
                break;
            }
        }
        
        for ( int i = 0 ; i < followedPath.length ; i++ ){
            if ( followedPath[i] == sharedPath[cnt] ){
                followedStartIdx = i;
                break;
            }
        }
        
        if ( myStartIdx == -1 || followedStartIdx == -1 ){
            
            System.out.println("Code error, review. Indexes should have been assigned.");
            return sharedTimes;
            
        }
        
        for ( int i = 0 ; i < sharedPath.length ; i++ ){
            sharedTimes[myStartIdx + i] = followedTimes[followedStartIdx + i];            
        }
        
        for ( int i = myStartIdx -1 ; i >= 0 ; i-- ){
            sharedTimes[i] = sharedTimes[i+1] - getWeight(i, i+1);
            //sharedTimes[i] = -100.;
        }
        
        for ( int i = myStartIdx + sharedPath.length ; i < sharedTimes.length ; i++ ){
            sharedTimes[i] = sharedTimes[i-1] + getWeight(i, i-1);
            //sharedTimes[i] = 100.;
        }
        
        return sharedTimes;
    }
    
    public double getWeight(int src, int dst){
        
        double weight;
        weight = adjMatrix[src][dst];
        return weight;
        
    }
    
    public int[] findCommonPath(int[] pathA, int[] pathB){
        // WARNING: It returns the last shared path in the paths, not the biggest one!
        int iStart, iStop, jStart, jStop;
        iStart = iStop = -1;
        int increase = 1;
        
        for ( int i = 0 ; i < pathA.length - 1 ; i++ ){
            
                for ( int j = 0 ; j < pathB.length - 1 ; j++ ){
                    
                    if ( pathA[i] == pathB[j] && pathA[i+1] == pathB[j+1] ){
                        
                        iStart = i;
                        iStop = i+1;
                        jStart = j;
                        jStop = j+1;
                        
                        while (true) {
                            
                            if ( iStop + 1 < pathA.length && jStop + 1 < pathB.length ) {
                                
                                if ( pathA[iStop+1] == pathB[jStop+1] ) {
                                    
                                    iStop ++;
                                    jStop ++;
                                    
                                    //System.out.println("Shared path so far");
                                    for ( int iTest = 0 ; iTest < iStop - iStart + 1 ; iTest++ ){
                                        
                                        //System.out.println(pathA[iStart+iTest]);
                                        //System.out.println(pathB[jStart+iTest]);
                                        
                                    }
                                                                       
                                    
                                }else{
                                    // Just found the biggest path!
                                    //System.out.println("Just found the biggest path!");
                                    int[] sharedPath = new int[iStop-iStart+1];
                                    for ( int iTemp = 0 ; iTemp < sharedPath.length ; iTemp++ ){
                                        sharedPath[iTemp] = pathA[iStart+iTemp];
                                    }
                                    return sharedPath;        
                                                                        
                                }
                                
                            }else{
                                // Just found the biggest path!
                                //System.out.println("Just found the biggest path!");
                                int[] sharedPath = new int[iStop-iStart+1];
                                for ( int iTemp = 0 ; iTemp < sharedPath.length ; iTemp++ ){
                                    sharedPath[iTemp] = pathA[iStart+iTemp];
                                }
                                return sharedPath;      
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                }
            
        }
        
        
        if (iStart == -1){
            return new int[0];
        }else{
            int[] sharedPath = new int[iStop-iStart+1];
            for ( int i = 0 ; i < sharedPath.length ; i++ ){
                sharedPath[i] = pathA[iStart+i];
            }
            return sharedPath;            
        }
        
        
    }
    
    
}