/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Translate;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author rui
 */
public class DijkstraVisualisation {
    
    
    Group dijkstraNodes;
    ButtonsBar BB;
    HBox buttonsHB;
    
    Group dijkstraRoot;
    
    Scene mainScene;
    Group mainRoot;
    
    boolean bigMode = Globals.bigMode;
    double fontStretch = Globals.fontStretch;
    double horizontalStretch = Globals.horizontalStretch;
    double verticalStretch = Globals.verticalStretch;
    double inactivityTimeSeconds = Globals.inactivityTimeSeconds;
    
    Timeline inactivityTimeline;
    
    
    double[] zoomStartPos = new double[2];
    double zoomIn = 0;
    Scale scaleTransform;
    Translate translateTransform;
    Group growingLines;
    
    double screenHeight = 1080;
    double screenWidth = 1920;
    
    
    SwedenPlatoon swedenPlatooning;
    
    int textPositionX = 880;
    int textPositionY = 100;
    
    double finalDijkstraTime = 0;
        
    private ArrayList<SwitchListener> switchListenerList = new ArrayList<SwitchListener>();
    
    EventHandler handler;
    
    int[] finalDijkstra;
    
    SequentialTransition sequentialTransition;
    Languages language;
    
    static Text tShpath;
    static final String tShpathEN = "Finding the shortest path\nbetween selected areas";
    static final String tShpathSV = "Söker efter kortaste vägen";
    
    static Text tSkip;
    static final String tSkipEN = "Press here to skip";
    static final String tSkipSV = "Tryck här för att komma vidare";
    
    Group helpMenuGroup;
    HelpMenu helpMenu;
    
    DijkstraVisualisation(Scene argMainScene, Group argMainRoot, SwedenPlatoon argSwedenPlatooning, Languages inheritLang){
        
        mainScene = argMainScene;
        mainRoot = argMainRoot;
        swedenPlatooning = argSwedenPlatooning;
        language = inheritLang;

        if ( Globals.isSwedishCurrentLanguage ){
    
            language.setSwedish();
    
        }else{
            
            language.setEnglish();
            
        }
        
        System.out.println("DIJKSTRA CONSTRUCTOR");
        System.out.println("language.isEnglish() = " + language.isEnglish());
        System.out.println("language.isSwedish() = " + language.isSwedish());
        
    }
    
    
    public void cleanScreen(){
        
        
//        inactivityTimeline.setOnFinished(null);
        inactivityTimeline.stop();
        // Removes all child nodes of mainRoot (and also of dijkstraRoot)
        
        System.out.println("DijkstraVisualisation->cleanScreen!");
        
        int numElements = dijkstraRoot.getChildren().size();
        
        for ( int i = 0 ; i < numElements ; i++ ){
            
            dijkstraRoot.getChildren().remove(0);
            
        }
        
        numElements = growingLines.getChildren().size();
        
        for ( int i = 0 ; i < numElements ; i++ ){
            
            growingLines.getChildren().remove(0);
            
        }        
        
        numElements = mainRoot.getChildren().size();
        
        for ( int i = 0 ; i < numElements ; i++ ){
            
            mainRoot.getChildren().remove(0);
            
        }
        
    }
    
    public void setInitialZoom(double t){
        
        scaleTransform.setX(1 + (t));
        scaleTransform.setY(1 + (t));
        
        translateTransform.setX( -t*( zoomStartPos[0] - screenWidth/2. ) );
        translateTransform.setY( -t*( zoomStartPos[1] - screenHeight/2. ) );
        
        
    }
    
    public void setInitialOffset(double t){
        
        
        translateTransform.setX( -t*( zoomStartPos[0] - screenWidth/2. ) );
        translateTransform.setY( -t*( zoomStartPos[1] - screenHeight/2. ) );
        
    }
    
    public Group dijkstraAnimation(){
        
        String[] args = {"home", "skip"};
        BB = new ButtonsBar(mainScene, args);
        buttonsHB = BB.getHB();
        buttonsEvents(buttonsHB);
        
        mainRoot.getChildren().add(buttonsHB);
        
        Group animation = new Group();
        
        Truck myTruck = swedenPlatooning.getMyTruck();
        
        sequentialTransition = new SequentialTransition();
        //create a timeline for moving the circle
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        
            
        int[][] shortestPathsIdx;
        shortestPathsIdx = myTruck.getTruckDijkstra(swedenPlatooning.swedenDijkstra);
        

        zoomStartPos[0] = swedenPlatooning.cityPositions[myTruck.source][0];
        zoomStartPos[1] = swedenPlatooning.cityPositions[myTruck.source][1];
        zoomIn = 2.0;
        
        
        class InitialZoomInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {

                setInitialZoom(t);

                return t;
                
            }
        }
        
        class InitialOffsetInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {

                setInitialOffset(t);

                return t;
                
            }
        }
        
        class LineGrowerInterpolator extends Interpolator {
            
                int dst = 0;
                Line line;
                
            LineGrowerInterpolator(int argDst, Line argLine){
                
                dst = argDst;
                line = argLine;
                
            }
            
            @Override
            protected double curve(double t) {

            line.setOpacity(1.0);    
                
            line.setEndX( line.getStartX()*(1 - t) + t*( (double) swedenPlatooning.cityPositions[dst][0] ) );
            line.setEndY( line.getStartY()*(1 - t) + t*( (double) swedenPlatooning.cityPositions[dst][1] ) );
                
            return t;
            
            }
        }
        
        
        Rectangle rect = new Rectangle (1, 1, 1, 1);
        rect.setOpacity(0.0); 
        RotateTransition rt = new RotateTransition(Duration.millis(1500), rect);
        rt.setByAngle(180);
        rt.setCycleCount(1);
        rt.setAutoReverse(false);
        InitialZoomInterpolator initialZoomInterpolator = new InitialZoomInterpolator();
        rt.setInterpolator(initialZoomInterpolator);

        translateTransform = new Translate(0, 0);
        scaleTransform = new Scale( 1, 1, zoomStartPos[0] ,  zoomStartPos[1] );

        dijkstraRoot.getTransforms().add(translateTransform);
        dijkstraRoot.getTransforms().add(scaleTransform);
        
        
        dijkstraRoot.getChildren().add(rect);

        sequentialTransition.getChildren().add(rt);
        
        rt.setOnFinished(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                System.out.println("rt.setOnFinished just ran!");
                growingLines.setOpacity(1.);
                
            }
        });
        
       
        ParallelTransition searchAnimation = new ParallelTransition();
        SequentialTransition pathsGrowing = new SequentialTransition();
        
        double dijkstraAnimationDuration = 0;
        
        
        int src, dst;
        int transitionMilliseconds = 250;
        
        int[] currentDijkstra = new int[swedenPlatooning.numberCities]; 
        for ( int i = 0 ; i < swedenPlatooning.numberCities ; i++ ){
            currentDijkstra[i] = -1;
        }
        
        
        finalDijkstra = new int[swedenPlatooning.numberCities];
        for ( int i = 0 ; i < currentDijkstra.length ; i++ ){
            finalDijkstra[i] = currentDijkstra[i];
        }
        for ( int i = 0 ; i < myTruck.dijkstraIterations.size() ; i++ ){
            
            src = myTruck.dijkstraIterations.get(i)[0];
            dst = myTruck.dijkstraIterations.get(i)[1];
            finalDijkstra[src] = dst;
        }
        
        
        int truckSrc = myTruck.path[0];
        int truckDst = myTruck.path[myTruck.path.length - 1];
        
        finalDijkstraTime = 0;
        
        IntegerStringConverter integerStringConverter = new IntegerStringConverter();
        
        growingLines = new Group();
        
        int lineId = -1;
        int[] lineIdTracker = new int[currentDijkstra.length];
        for ( int i = 0 ; i < lineIdTracker.length ; i++ ){
            lineIdTracker[i] = -1;
        }   
                
        double velocityFactor = (20./myTruck.dijkstraIterations.size())*150.;
        
        for ( int i = 0 ; i < myTruck.dijkstraIterations.size() ; i++ ){
            
            lineId++;
            
            transitionMilliseconds = (int)velocityFactor;
            
            src = myTruck.dijkstraIterations.get(i)[0]; // ITS ACTUALLY THE DESTINATION
            dst = myTruck.dijkstraIterations.get(i)[1]; // ITS ACTUALLY THE SOURCE
            
            int prevCurrentDijkstra = currentDijkstra[src];
            currentDijkstra[src] = dst;
            
            FadeTransition ftDisappear = null;
            
//            if (prevIteration == null){
            if( prevCurrentDijkstra == -1 ){
//                System.out.println("Null, it was not found!");
            }else{
                
                Line prevIteration = (Line) growingLines.lookup("#"+lineIdTracker[src]);
                
                if( prevIteration == null ){
                    
                    
                }else{
                
                    transitionMilliseconds = (int)(2.*velocityFactor);
                    
                    ftDisappear = new FadeTransition(Duration.millis(2*transitionMilliseconds), prevIteration);
                    finalDijkstraTime+=2*transitionMilliseconds;
                    ftDisappear.setFromValue(1);
                    ftDisappear.setToValue(0);
                    ftDisappear.setCycleCount(1);
                    dijkstraAnimationDuration += 2*transitionMilliseconds;
                    
                
                }
                
            }
            
            Line line = new Line();
            line.setStartX(swedenPlatooning.cityPositions[dst][0]);
            line.setStartY(swedenPlatooning.cityPositions[dst][1]);
            line.setEndX(swedenPlatooning.cityPositions[dst][0]);
            line.setEndY(swedenPlatooning.cityPositions[dst][1]);
            
            lineIdTracker[src]=lineId;
            line.setId(integerStringConverter.toString(lineId));
            
            
            line.setStrokeWidth(3.0);
            line.setStroke(Color.GREEN);
            line.setOpacity(0.);
            line.setId(""+lineId);
            
            growingLines.getChildren().add(line);
            
            FadeTransition ft = new FadeTransition(Duration.millis(3*transitionMilliseconds), line);
            finalDijkstraTime+=3*transitionMilliseconds;
            ft.setFromValue(1);
            ft.setToValue(1);
            ft.setCycleCount(1);
            
            LineGrowerInterpolator lineGrowerInterolator = new LineGrowerInterpolator(src, line);
            ft.setInterpolator(lineGrowerInterolator);
            
            dijkstraAnimationDuration += 3*transitionMilliseconds;
        
            pathsGrowing.getChildren().add(ft);
            
            double nodeRadius = 5.0;
            double nodeOpacity = 0.0;
//            
            if ( src != truckDst ){
            
                Circle circle = new Circle(swedenPlatooning.cityPositions[src][0],
                    swedenPlatooning.cityPositions[src][1], nodeRadius);
                circle.setFill(Color.GREEN);
                circle.setOpacity(0.0);

                ft = new FadeTransition(Duration.millis(transitionMilliseconds), circle);
                finalDijkstraTime+=transitionMilliseconds;
                ft.setFromValue(0);
                ft.setToValue(1);
                ft.setCycleCount(1);
                
                dijkstraAnimationDuration += transitionMilliseconds;

                growingLines.getChildren().add(circle);

                pathsGrowing.getChildren().add(ft);
                
                if (ftDisappear != null){
                
                    pathsGrowing.getChildren().add(ftDisappear);
                    
                }

            }
                        
            
            if ( src == truckDst && finalDijkstra[src] == dst ){
                 // NEED TO ADD MORE CODE HERE, MIGHT NOT BE THE LAST DIJKSTRA ITERATION!!!
                break;
                
            }else{
                
                
                
            }
            
            
            
            
            
        }
        
        animation.getChildren().add(growingLines);
        
        searchAnimation.getChildren().add(pathsGrowing);
        
        class FinalZoomInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {

                setInitialZoom(1-t);

                return t;
                
            }
        }
        
        
        double zoomOutDuration;
        
        if ( dijkstraAnimationDuration < 2500){
            
            zoomOutDuration = 2500;
            
        }else{
            
            zoomOutDuration = dijkstraAnimationDuration;
            
        }
        
        rt = new RotateTransition(Duration.millis(zoomOutDuration), rect);
        rt.setByAngle(180);
        rt.setCycleCount(1);
        rt.setAutoReverse(false);
        FinalZoomInterpolator finalZoomInterpolator = new FinalZoomInterpolator();
        rt.setInterpolator(finalZoomInterpolator);
        
        searchAnimation.getChildren().add(rt);
        
        sequentialTransition.getChildren().add(searchAnimation);
        
        // Draw circle at truck source
        double truckSrcNodeRadius = 5.0;
        double truckSrcNodeOpacity = 0.8;
        Circle truckSrcCircle = new Circle(swedenPlatooning.cityPositions[truckSrc][0],
                swedenPlatooning.cityPositions[truckSrc][1], truckSrcNodeRadius, Color.web("yellow", truckSrcNodeOpacity));
            truckSrcCircle.setFill(Color.YELLOW);
        
        animation.getChildren().add(truckSrcCircle);    
        
        
        // Draw circle at truck destination
        double truckDstNodeRadius = 5.0;
        double truckDstNodeOpacity = 0.8;
        Circle truckDstCircle = new Circle(swedenPlatooning.cityPositions[truckDst][0],
                swedenPlatooning.cityPositions[truckDst][1], truckDstNodeRadius, Color.web("yellow", truckDstNodeOpacity));
            truckDstCircle.setFill(Color.YELLOW);
        
        animation.getChildren().add(truckDstCircle);  
        
        
//        System.out.println("Final Dijkstra state:");
//        System.out.println( Arrays.toString( currentDijkstra ) );
        
        finalDijkstra = new int[ currentDijkstra.length ];
        
        for ( int i = 0 ; i < currentDijkstra.length ; i++ ){
            finalDijkstra[i] = currentDijkstra[i];
        }
        
        // ADD A HIGHLIGHT ON THE SHORTEST PATH
        
        ParallelTransition parallelTransition = new ParallelTransition();
        
               
        for ( int i = truckDst ; i != truckSrc; i = currentDijkstra[i] ){
            
            Line line = new Line();
            line.setStartX(swedenPlatooning.cityPositions[i][0]);
            line.setStartY(swedenPlatooning.cityPositions[i][1]);
            line.setEndX(swedenPlatooning.cityPositions[currentDijkstra[i]][0]);
            line.setEndY(swedenPlatooning.cityPositions[currentDijkstra[i]][1]);
            
            line.setOpacity(0.0);
            line.setStrokeWidth(3.0);
            line.setStroke(Color.YELLOW);
            
            FadeTransition ft = new FadeTransition(Duration.millis(2*transitionMilliseconds), line);
            ft.setFromValue(0);
            ft.setToValue(1);
  
            ft.setCycleCount(1);
                      
            
            animation.getChildren().add(line);
            
            parallelTransition.getChildren().add(ft);
            
        }
        
        
        
        sequentialTransition.getChildren().add(parallelTransition);
        
       
        sequentialTransition.setCycleCount(1);
        sequentialTransition.setOnFinished(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                BB.changeLabel("skip", "continue");
                BB.addButton("replay");
                buttonsHB = BB.getHB();
                buttonsEvents(buttonsHB);
                if (language.isEnglish())
                    tSkip.setText("Press here to proceed");
                else
                    tSkip.setText("Tryck här för att fortsätta");
            }
        });
//        playDijkstraSearch();
        
        
        return animation;
       
        
    }
    
    private void playDijkstraSearch(){
        
        sequentialTransition.play();       
        
    }
    
    private void setUpHelpStuff(){
        
        helpMenuGroup = new Group();
        int stageOfProgram = 1;
        helpMenu = new HelpMenu(inactivityTimeline, stageOfProgram, language.isSwedish());
        
        // Help menustuff
        helpMenuGroup = helpMenu.createHelpMenu();
        
        helpMenuGroup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {

                playDijkstraSearch();
                
            }

        });
        
        mainRoot.getChildren().add(helpMenuGroup);
        
        helpMenuGroup.setLayoutX( screenWidth/2. - helpMenuGroup.getBoundsInParent().getWidth()/2. );
        helpMenuGroup.setLayoutY( screenHeight/2. - helpMenuGroup.getBoundsInParent().getHeight()/2. );
        
        ImageView helpButtonImageView = helpMenu.createHelpButton();
        mainRoot.getChildren().add(helpButtonImageView);
        
        helpButtonImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {

                System.out.println("Restarted counter because I clicked on the helpLogo");
                restartInactivityAnimation();
                
            }

        });
        
        
    }
    
    private void initializeInactivityAnimation(){
        
        //create a timeline for moving the circle
        inactivityTimeline = new Timeline();
        inactivityTimeline.setCycleCount(1);
        inactivityTimeline.setAutoReverse(false);
        
        //create a keyFrame, the keyValue is reached at time 2s
        Duration duration = Duration.millis(inactivityTimeSeconds*1000);
        //one can add a specific action when the keyframe is reached
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                
                System.out.println("-----------Inactivity from DijsktraVisualisation");
                inactivityTimeline.setOnFinished(null);
                inactivityTimeline.stop();
                processSwitchEvent(new SwitchEvent(DijkstraVisualisation.this, 2,1));
                // go to Original Menu
                
            }
        };
 
        KeyFrame keyFrame = new KeyFrame(duration, onFinished);
 
        //add the keyframe to the timeline
        inactivityTimeline.getKeyFrames().add(keyFrame);
 
        inactivityTimeline.play();
        
        System.out.println("DijkstraVisualisation: Start Inactivity");
        
    }
    
    private void restartInactivityAnimation(){
        
        inactivityTimeline.jumpTo(Duration.ZERO);
        inactivityTimeline.play();
        System.out.println("DijkstraVisualisation: Restart Inactivity");
                
    }
    
    public void stopInactivityAnimation(){
        
        inactivityTimeline.setOnFinished(null);
            inactivityTimeline.stop();
        System.out.println("DijkstraVisualisation: Stop Inactivity");
                
    }
    
    public void showDijkstraSearch(){
        
        // THIS IS THE MAIN OF THIS CLASS
        System.out.println("THIS IS THE MAIN OF THIS CLASS");
        System.out.println("language.isEnglish() = " + language.isEnglish());
        System.out.println("language.isSwedish() = " + language.isSwedish());
        
        initializeInactivityAnimation();
        
        dijkstraRoot = new Group();
        
        Group lines;
        lines = createGraphLinks();
        dijkstraRoot.getChildren().add(lines);

        Group circles;
        circles = createGraphPositions();
        dijkstraRoot.getChildren().add(circles);
        
        // Create the animation group, contaning all the animations
        Group animation;
        animation = dijkstraAnimation();
        dijkstraRoot.getChildren().add(animation);
            
        // Creates the description text and adds it to mainRoot
        createInformationText();

        mainRoot.getChildren().add(dijkstraRoot);
        
        setUpHelpStuff();
        
        

    }
    
    private void createInformationText(){
        
        // Creates the description text and adds it to mainRoot
        
        if (bigMode){
            
            tShpath = new Text(horizontalStretch*textPositionX, verticalStretch*textPositionY, "");
            tShpath.setFont(new Font(fontStretch*30));
            tSkip = new Text(horizontalStretch*textPositionX, verticalStretch*(textPositionY+125), "");
            tSkip.setFont(new Font(fontStretch*30));
            
        }else{
            
            tShpath = new Text(textPositionX, textPositionY, "");
            tShpath.setFont(new Font(30));
            tSkip = new Text(textPositionX, textPositionY+125, "");
            tSkip.setFont(new Font(30));
            
        }
        
        String s = (language.isEnglish())?tShpathEN:tShpathSV;
        tShpath.setText(s);
        tShpath.setFill(Color.WHITE);
        
        
        s = (language.isEnglish())?tSkipEN:tSkipSV;
        tSkip.setText(s);
        tSkip.setFill(Color.WHITE);
        
        Group textPress = new Group(tSkip);
        textPress.setOnMousePressed((MouseEvent me)->{
            if (BB.getButton("skip")!=null)
            {
                
                System.out.println("Skipped by text");
                
                sequentialTransition.jumpTo("end");
                
                growingLines.setOpacity(0.);
                
                BB.changeLabel("skip", "continue");
                BB.addButton("replay");
                buttonsEvents(BB.getHB()); //recursive call to re-set the event handlers of the buttons
                if (language.isEnglish())
                    tSkip.setText("Press here to proceed");
                else
                    tSkip.setText("Tryck här för att fortsätta");
            }
            else
                inactivityTimeline.setOnFinished(null);
                inactivityTimeline.stop();
                processSwitchEvent(new SwitchEvent(DijkstraVisualisation.this, 2));
        });
        
        // Adding the texts
        mainRoot.getChildren().add(tShpath);
        mainRoot.getChildren().add(textPress);
        
    }
    
    private Group createGraphLinks(){
        
        Group graphLinks = new Group();
        
        for (int i = 0 ; i < swedenPlatooning.cityLinks.length ; i++) {
        
            for (int j = 0 ; j < swedenPlatooning.cityLinks[i].length ; j++) {
        
                Line line = new Line();
                line.setStartX(swedenPlatooning.cityPositions[i][0]);
                line.setStartY(swedenPlatooning.cityPositions[i][1]);
                line.setEndX(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][0]);
                line.setEndY(swedenPlatooning.cityPositions[ swedenPlatooning.cityLinks[i][j] ][1]);
                
                //line.setFill(Color.RED);
                line.setStroke(Color.RED);
                
                graphLinks.getChildren().add(line);
            
            }
            
        }
        
        return graphLinks;
        
    }
    
    public Group createGraphPositions(){
        
        dijkstraNodes = new Group();
        
        double nodeOpacity = 0.5;
        double nodeRadius = 10;
        IntegerStringConverter integerStringConverter = new IntegerStringConverter(); 
        
        for (int i = 0 ; i < swedenPlatooning.numberCities ; i++) {           
            
            Circle circle = new Circle(swedenPlatooning.cityPositions[i][0],
                swedenPlatooning.cityPositions[i][1], nodeRadius, Color.web("red", nodeOpacity));
            circle.setStrokeType(StrokeType.OUTSIDE);
            circle.setStroke(Color.web("red", 0.16));
            circle.setStrokeWidth(4);
            circle.setId(integerStringConverter.toString(i));
           
          
            dijkstraNodes.getChildren().add(circle);
           
        }
        
        return dijkstraNodes;
        
    }
    
    public synchronized void addSwitchListener(SwitchListener sl)
    {
        if (!switchListenerList.contains(sl))
        {
            switchListenerList.add(sl);
        }
    }
    
    
    private void processSwitchEvent(SwitchEvent switchEvent) {
        ArrayList<SwitchListener> tempSwitchListenerList;

        synchronized (this) {
                if (switchListenerList.size() == 0)
                        return;
                tempSwitchListenerList = (ArrayList<SwitchListener>) switchListenerList.clone();
        }

        for (SwitchListener listener : tempSwitchListenerList) {
                listener.switchRequested(switchEvent);
        }
    }
    
    private void buttonsEvents(HBox bHB) {
        
        Object[] bArr = bHB.getChildren().toArray();
        
        EventHandler onFinishedFading = (EventHandler<ActionEvent>) (ActionEvent t) -> {
        processSwitchEvent(new SwitchEvent(DijkstraVisualisation.this, 2));
        };
        
        for (Object obj : bArr)
        {
            assert (obj instanceof Button);
            Button b = (Button)obj;
            if (b.getText().equals("")) // home button
            {
//                b.setDefaultButton(true);
                b.setOnMousePressed((MouseEvent me)->{
                    inactivityTimeline.setOnFinished(null);
                    inactivityTimeline.stop();
                    processSwitchEvent(new SwitchEvent(DijkstraVisualisation.this, 2,1));
                });
            }
            if (b.getText().equalsIgnoreCase("skip"))
            {
                b.setOnMousePressed((MouseEvent e)->{
                    
                    restartInactivityAnimation();
                    
                    System.out.println("Skipped by button");

//                    animation.getChildren().remove(growingLines);
                    
//                    for ( int i = 0 ; i < growingLines.getChildren().size() ; i++ ){
//                        
//                        growingLines.getChildren().get(i).setOpacity(0.);
//                        
//                    }
                    
                    growingLines.setOpacity(0.);
                    
//                    sequentialTransition.setOnFinished(onFinishedFading);
                    
//                    sequentialTransition.jumpTo(Duration.millis(finalDijkstraTime));
                    sequentialTransition.jumpTo("end");
                    
                    
                    BB.changeLabel("skip", "continue");
                    BB.addButton("replay");
                    if (language.isEnglish())
                        tSkip.setText("Press here to proceed");
                    else
                        tSkip.setText("Tryck här för att fortsätta");
                    buttonsEvents(bHB); //recursive call to re-set the event handlers of the buttons
                    
                });
            }
            if (b.getText().equalsIgnoreCase("replay"))
            {           
                b.setOnMousePressed((MouseEvent e)->{
                    
                    
                    restartInactivityAnimation();
                    
                    System.out.println("replayed");
                    
//                    growingLines.setOpacity(1.);
                    
                    
                    BB.changeLabel("continue","skip");
                    BB.removeButton("replay");
                    sequentialTransition.jumpTo("start");
                    playDijkstraSearch();
                    buttonsEvents(bHB); //recursive call to re-set the event handlers of the buttons
                    if (language.isEnglish())
                        tSkip.setText("Press here to skip");
                    else
                        tSkip.setText("Tryck här för att komma vidare");
                });                
            }
            if (b.getText().equalsIgnoreCase("continue"))
            {
                b.setOnMousePressed((MouseEvent e)->{
                    System.out.println("continued");
                        inactivityTimeline.setOnFinished(null);
                        inactivityTimeline.stop();
                        processSwitchEvent(new SwitchEvent(DijkstraVisualisation.this, 2));            
                });     
            }
        }
        
    }
    
}
