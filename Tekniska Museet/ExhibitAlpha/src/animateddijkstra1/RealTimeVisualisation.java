/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import static animateddijkstra1.AnimatedDijkstra1.getHourTime;
import static animateddijkstra1.AnimatedDijkstra1.getMinuteTime;
import static animateddijkstra1.AnimatedDijkstra1.getSecondTime;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.util.Duration;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author rui
 */
public class RealTimeVisualisation {
    
    
    
    Scene mainScene;
    Group mainRoot;
    
    SwedenPlatoon swedenPlatooning;
//    
//    Group platoonOptionsRoot = new Group();
//    
//    Group truckPlatooningLines = new Group();
//    
//    Group platooningOptions = new Group();
    
    ImageView truckImgView;
    ImageView truckImgViewRight;
    ImageView platoonTruckImgView;
    ImageView platoonTruckImgViewRight;
    Group convoyGroup;
    Group convoyGroupRight;
    Group truckCartoon;
    Group truckCartoonRight;
    
    boolean bigMode = Globals.bigMode;
    double fontStretch = Globals.fontStretch;
    double horizontalStretch = Globals.horizontalStretch;
    double verticalStretch = Globals.verticalStretch;
    double inactivityTimeSeconds = Globals.inactivityTimeSeconds;

    
    int numFastestPlatoons = 3;
    double barPositionX;
    double barPositionY;
    
    double fuelNoPlat = 0;
    double fuelPlat = 0;
    double fuelSaved = 0;
    double totSF;
    double currentSF = 0;
    
    double incrNoPlat; //5
    double incrBase; //4
    double incrPlat; //= incrNoPlat*.80;
    double maxBarHeight;
    double barWidth;
    
    boolean simOn = false;
    boolean platOn = false;
    boolean tripOn = false;
    boolean prevSimOn = false;
    
    boolean realTimeVisualisationExited = false;
    
    double rectArcHeight = 50;
    double rectArcWidth = 50;
    
    Text text0a = new Text();
    Text text1a = new Text();
    Text text2a = new Text();
    Text text3a = new Text();
    Text text4a = new Text();
    Text text5a = new Text();
    Text text1b = new Text();
    Text text2b = new Text();
    Text text3b = new Text();
    Text text4b = new Text();
    Text text5b = new Text();
    
    int timesPerDay, dayPerWeek, weekPerYear, trucksPerCompany;
    
    
    double baseButtonX;
    double baseButtonY;
    
    double finalFuelNoPlat;
    double finalFuelPlat;
    
    double plTextWidth;
    
    Group finalBox = new Group();
    
    Group longSavingsTextGroup = new Group();
    
    Text diffText;
    Text fuelText;
    Text platooningText;
    
    ButtonsBar BB;
    HBox buttonsHB;
    
    Text selectedAnimalText;
    
    TranslateTransition dummyTranslateTransition;
    SequentialTransition dummyTransition;
    ParallelTransition parallelTransition;
    SequentialTransition sequentialTransition;
    
    Polyline truckTrail;
    int[] prevTruckTrail = {-99,-99};
    Polyline platoonTruckTrail;
    int[] prevPlatoonTruckTrail = {-99,-99};
    Polyline convoyTrail;
    int[] prevConvoyTrail = {-99,-99};
    
    Group truckTrailGroup;
    Group platoonTruckTrailGroup;
    Group convoyTrailGroup;
    
//    int numTrucksToVisualise = 100; This is the number of trucks that we will visualize in the simulation.
    int numTrucksToVisualise = 20;
//    int finalSimulationTime = 1*3600;  This is the time we want the simulation to represent. In this case 3600 seconds = 1 hour of the day.
    int finalSimulationTime = 1*900;
    
    // double simulationTime = 60000; This is the simulation time in milliseconds.
    double simulationTime = 20000;
    
    // double temporalResolution = 1800; The resolution of the particle movement keyframes.
    double temporalResolution = 20;
    
    double platooningStartTime;
    double platooningEndTime;
    
    double screenWidth = 1920;
    double screenHeight = 1080;
    
    double infographicBoxWidth = 0;
    
//    PlatoonPlan currentPlatoonPlan = new PlatoonPlan();
    
    private ArrayList<SwitchListener> switchListenerList = new ArrayList<SwitchListener>();
    
    EventHandler handler;
    
//    XYChart.Series series;
//    XYChart.Series series1;
//    XYChart.Series series2;
     
    Rectangle rectNoPlat;
    Rectangle rectPlat;
    Rectangle rectBG;
    Rectangle rectDiff;
    Rectangle rectDash;
    Line dashed;
//    LineChart<Number,Number> lineChart;    
    private Group rects;
    private Group labels;
    final Languages language;
    
    Group infographics;

    Group companyInfoGraphicGroup;
    Group countryInfoGraphicGroup;
    Group flagButtons;
    Group companyButtons;
    double flagNotSelectedOpacity = 0.5;
    double annualFuelSavings;
    double annualKronorSavings;
    double annualCountryFuelSavings;
    double annualCountryEmissionSavings;
    double rectangleBoxOpacity = .95;
    
    Timeline inactivityTimeline;
    
    double delayTimeAfterSimulationSeconds = 3.5;
    
    double pauseTransitionTimeBeforeStart = 1000;
    
    Group helpMenuGroup;
    HelpMenu helpMenu;
    
    RealTimeVisualisation(Scene argMainScene, Group argMainRoot, SwedenPlatoon argSwedenPlatooning, Languages inheritLang){
        
        mainScene = argMainScene;
        mainRoot = argMainRoot;
        swedenPlatooning = argSwedenPlatooning;
        language = inheritLang;
        
        if ( Globals.isSwedishCurrentLanguage ){
    
            language.setSwedish();
    
        }else{
            
            language.setEnglish();
            
        }
        
//        mainScene.setOnMouseMoved((MouseEvent me)->{
//            System.out.println("mouse event "+me.getX()+" "+me.getY());
//        });
    }
    
    
        
    private Group createMapLinks(){
        
        
        Group mapLinks = new Group();
        List<RoadWaypoints> mapRoadWaypoints;
                
        mapRoadWaypoints = swedenPlatooning.getRoadWayPoints();
        
        RoadWaypoints tempWaypoints;
        double[] polylineWaypoints;
        
//        System.out.println("mapRoadWaypoints.size()");
//        System.out.println(mapRoadWaypoints.size());

        
        for ( int i = 0 ; i < mapRoadWaypoints.size() ; i++ ){
            
            tempWaypoints = mapRoadWaypoints.get(i);
            
            polylineWaypoints = new double[2 * tempWaypoints.waypoints.length ];
            
//            System.out.println("tempWaypoints.waypoints.length");
//            System.out.println(tempWaypoints.waypoints.length);
            
            for ( int j = 0 ; j < tempWaypoints.waypoints.length ; j++ ){
                
                polylineWaypoints[2*j] = tempWaypoints.waypoints[j][0];
                polylineWaypoints[2*j+1] = tempWaypoints.waypoints[j][1];
//                System.out.println(tempWaypoints.waypoints[j][0]);
                
            }
            
            Polyline polyline = new Polyline(polylineWaypoints);
            polyline.setStroke(Color.RED);
            polyline.setStrokeWidth(3.0);
            
            mapLinks.getChildren().add(polyline);
                        
        }
         
        return mapLinks;
        
    }
    
    
    
    public Group createGraphPositions(){
        
//        System.out.println("PlatoonOptionsVisualisation -> createGraphPositions");
        
        Group graphNodes = new Group();
        
        double nodeOpacity = 0.5;
        double nodeRadius = 10;
        IntegerStringConverter integerStringConverter = new IntegerStringConverter(); 
        
        for (int i = 0 ; i < swedenPlatooning.numberCities ; i++) {           
            
            Circle circle = new Circle(swedenPlatooning.cityPositions[i][0],
                swedenPlatooning.cityPositions[i][1], nodeRadius, Color.web("red", nodeOpacity));
            circle.setStrokeType(StrokeType.OUTSIDE);
            circle.setStroke(Color.web("red", 0.16));
            circle.setStrokeWidth(4);
            circle.setId(integerStringConverter.toString(i));
           
            graphNodes.getChildren().add(circle);
           
        }
        
        return graphNodes;
        
    }
    
    
    private void setUpHelpStuff(int argStageOfProgram){
        
        helpMenuGroup = new Group();
        helpMenu = new HelpMenu(inactivityTimeline, argStageOfProgram, language.isSwedish() );
        
        // Help menustuff
        helpMenuGroup = helpMenu.createHelpMenu();
        
        helpMenuGroup.toFront();
        
        
        helpMenuGroup.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                
                System.out.println("PRESSED helpMenuGrouphelpMenuGrouphelpMenuGrouphelpMenuGrouphelpMenuGroup");
                restartInactivityAnimation();
                helpMenuGroup.toBack();
                
                System.out.println("argStageOfProgram = " + argStageOfProgram);
                
                if (argStageOfProgram == 4){
                
                    System.out.println("Running playTrucksMovement()");
                    playTrucksMovement();
                    
                }
                
            }

        });
        
        mainRoot.getChildren().add(helpMenuGroup);
        
        helpMenuGroup.setLayoutX( screenWidth/2. - helpMenuGroup.getBoundsInParent().getWidth()/2. );
        helpMenuGroup.setLayoutY( screenHeight/2. - helpMenuGroup.getBoundsInParent().getHeight()/2. );
        
        ImageView helpButtonImageView = helpMenu.createHelpButton();
        mainRoot.getChildren().add(helpButtonImageView);
        
        helpButtonImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {

                restartInactivityAnimation();
                
            }

        });
        
        
    }
    
    
        
    
    
    public void showTrucksMoving(){
         
        // This is the main of this class

        initializeInactivityAnimation();
        
        Image img;
        img = Globals.stockholmMapImage;

        ImageView imgView = new ImageView(img);
        mainRoot.getChildren().add(imgView);
        
        Group lines;
//        lines = createGraphLinks();
        lines = createMapLinks();
        mainRoot.getChildren().add(lines);
        
        Group circles;
        circles = createGraphPositions();
        mainRoot.getChildren().add(circles);
        
//        Group legendCircles;
//        legendCircles = showLegend();
//        mainRoot.getChildren().add(legendCircles);
        
        FadeTransition ft = new FadeTransition(Duration.millis(1000), mainRoot);
        ft.setFromValue(0);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.play();
        
        int[] truckPosition = new int[2];
        
        int[] firstTruckPosition = new int[2];
        int[] lastTruckPosition = new int[2];
        
        double time = 0;
        
        Group truckDots = new Group();
        double nodeRadius = 3;
        double nodeOpacity = 1;
        Circle truckDot;
        
        rectNoPlat = new Rectangle(100, fuelNoPlat, Color.RED);

        
        SharedPath sP = swedenPlatooning.getSharedPath(swedenPlatooning.platoonTruckId);
        totSF = roundTwoDec(sP.savedFuel); // the format is e.g. 5.76
//        System.out.println("saved fuel" + totSF);
        
        fuelNoPlat = 0;
        fuelPlat = 0;
        fuelSaved = 0;
        
        rectBG = new Rectangle();
        if (bigMode){
            
            double menuWidth =  horizontalStretch*(290);
            rectBG.setX( screenWidth - menuWidth - 50 );
            rectBG.setY( 50 );
            rectBG.setFill(Color.WHITE);
            rectBG.setOpacity(.75);
            rectBG.setWidth( menuWidth );
            rectBG.setHeight( verticalStretch*(530) );
            
        }else{
            
            rectBG.setX(barPositionX-20);
            rectBG.setY(barPositionY-300);
            rectBG.setFill(Color.WHITE);
            rectBG.setOpacity(.75);
            rectBG.setWidth(290);
            rectBG.setHeight(530);
            
        }
        
        rectBG.setArcHeight(rectArcHeight);
        rectBG.setArcWidth(rectArcWidth);
        
        barWidth = rectBG.getBoundsInLocal().getWidth() / 3.;
        
        maxBarHeight = (1./3.)*rectBG.getBoundsInLocal().getHeight() ;
        
        barPositionX = rectBG.getBoundsInLocal().getMinX() + (1./4.)*rectBG.getBoundsInLocal().getWidth() - barWidth/2. ;
        barPositionY = rectBG.getBoundsInLocal().getMinY() + (1./5.)*rectBG.getBoundsInLocal().getHeight() + maxBarHeight;
        
        
        Text fuelC = (language.isEnglish())?new Text("Fuel consumption"):new Text("Bränsleförbrukning");
        rectNoPlat = new Rectangle(barWidth, fuelNoPlat, Color.RED);
        Text rnpText = (language.isEnglish())?new Text("without platooning"):new Text("utan kolonnkörning");
        rectPlat = new Rectangle(barWidth, fuelPlat, Color.GREEN);
        Text rpText = (language.isEnglish())?new Text("with platooning"):new Text("med kolonnkörning");
        
        if (bigMode){
            
            rnpText.setFont( new Font(20));
            rpText.setFont( new Font(20));
            
        }else{
            
//            rnpText.setFont( new Font(15));
//            rpText.setFont( new Font(15));
            
        }
        
        rectDash = new Rectangle(barWidth, fuelPlat, Color.GREEN);
        fuelText = new Text("");
        diffText = new Text("Status");
        
        
        double fuelCBaseX = (language.isEnglish())?barPositionX+50:barPositionX+40;
        double rnpTextBaseX = (language.isEnglish())?barPositionX+150:barPositionX+158;
        double rpTextBaseX = (language.isEnglish())?barPositionX+10:barPositionX+8;
        
     
        fuelC.setFont(new Font(fontStretch*20)); 
        fuelC.setLayoutX( fuelCBaseX );
        fuelC.setLayoutY( barPositionY );
        

        rectNoPlat.setLayoutX( rectBG.getBoundsInLocal().getMinX() + (3./4.)*rectBG.getBoundsInLocal().getWidth() - barWidth/2. );
        rnpText.setLayoutX( rectBG.getBoundsInLocal().getMinX() + (3./4.)*rectBG.getBoundsInLocal().getWidth() - rnpText.getBoundsInLocal().getWidth()/2. );

        rectNoPlat.setLayoutY( barPositionY );
        rnpText.setLayoutY( barPositionY+20 );

        rectPlat.setLayoutX( barPositionX );
        rectPlat.setLayoutY( barPositionY );

        rpText.setLayoutX( barPositionX + barWidth/2. - rpText.getBoundsInLocal().getWidth()/2. );
        rpText.setLayoutY( barPositionY+20 );

        rectDash.setLayoutX( rectBG.getBoundsInLocal().getMinX() + (3./4.)*rectBG.getBoundsInLocal().getWidth() - barWidth/2. );
        rectDash.setLayoutY( barPositionY );

        dashed = new Line( barPositionX, barPositionY, rectBG.getBoundsInLocal().getMinX() + (3./4.)*rectBG.getBoundsInLocal().getWidth() + barWidth/2. , barPositionY );

        fuelText.setFont(new Font(fontStretch*70));
        fuelText.setLayoutX( barPositionX+30 );
        fuelText.setLayoutY( barPositionY+160 );

        diffText.setFont(new Font(fontStretch*20));
        diffText.setLayoutX( rectBG.getBoundsInLocal().getMinX() + (1./2.)*rectBG.getBoundsInLocal().getWidth() - diffText.getBoundsInLocal().getWidth()/2. );
        diffText.setLayoutY( barPositionY+70 );
        
        
                
        double tempTextWidth = fuelC.getBoundsInLocal().getWidth();
        double tempTextHeight = fuelC.getBoundsInLocal().getHeight();      
        double tempRectBGWidth = rectBG.getBoundsInLocal().getWidth();
        double tempRectBGHeight = rectBG.getBoundsInLocal().getHeight();     
        
        fuelC.setLayoutX( rectBG.getBoundsInLocal().getMinX() + tempRectBGWidth/2. - tempTextWidth/2. );
        fuelC.setLayoutY( rectBG.getBoundsInLocal().getMinY() + tempRectBGHeight*(3./20.) - tempTextHeight/2. );
        
        rectNoPlat.toFront();
        
        rectPlat.toFront();        
        
        dashed.setStroke(Color.RED);
        dashed.getStrokeDashArray().addAll(5.0);
        dashed.setVisible(false);

        rects = new Group(rectBG,rectPlat,rectNoPlat,rectDash,dashed);
        labels = new Group(rnpText,rpText,diffText,fuelC,fuelText);
        
        mainRoot.getChildren().add(rects);
        mainRoot.getChildren().add(labels);
           
        String[] types = {"home","skip"};
        BB = new ButtonsBar(mainScene, types);
        buttonsHB = BB.getHB();
        buttonsEvents(buttonsHB);

        mainRoot.getChildren().add(buttonsHB);
        
        DecimalFormat df = new DecimalFormat("#00");
        
        
        parallelTransition = new ParallelTransition();
        sequentialTransition = new SequentialTransition();
        
        PauseTransition pauseTransitionTemp = new PauseTransition( Duration.millis(pauseTransitionTimeBeforeStart) );
        sequentialTransition.getChildren().add(pauseTransitionTemp);
        
        
        PathTransition pathTransition = new PathTransition();
        
        Timeline timeline = new Timeline();
//        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);
        
        Truck testTruck = swedenPlatooning.getMyTruck();
        
        
        
        double[] truckPlatoonTimes = swedenPlatooning.truckList[swedenPlatooning.getPlatoonTruckId()].times;
//        System.out.println("swedenPlatooning.getPlatoonTruckId() = " + swedenPlatooning.getPlatoonTruckId());
        
        int finalOwnTruckTime = (int) (1.1*testTruck.times[ testTruck.times.length -1 ]);
        int finalPlatoonTruckTime = (int) (1.1*truckPlatoonTimes[ truckPlatoonTimes.length -1 ]);
        
        
        finalSimulationTime = Math.max(finalOwnTruckTime, finalPlatoonTruckTime);
        
//        temporalResolution = simulationTime / finalSimulationTime;
        simulationTime = finalSimulationTime*20;
        if ( simulationTime < 5000 ){
            simulationTime = 5000;
        }
        if ( simulationTime > 20000 ){
            simulationTime = 20000;
        }
        
//        System.out.println("Will run simulation for "+simulationTime/1000+" seconds");
        
        
//        System.out.println("testTruck.times");
//        System.out.println( Arrays.toString(testTruck.times) );
        
        double depTime = testTruck.getDepartureTime();
        
//        int depHour = getHourTime(depTime);
//        int depMinute = getMinuteTime(depTime);
        
//        BigDecimal bd = new BigDecimal(testTruck.getDepartureTime(), MathContext.DECIMAL64);
//        int[] ints = splitToComponentTimes(bd);
        
        
        final Animation animation = new Transition() {
            {
                setCycleDuration(Duration.millis(simulationTime));
//                setCycleCount(Timeline.INDEFINITE);
                setCycleCount(1);
            }

            protected void interpolate(double frac) {
                
                double currentTime = frac * (double)finalSimulationTime;
                
                double currentDIff = depTime - currentTime;
                
                String currentTimeString = df.format(getMinuteTime(currentTime))+"'"+df.format(getSecondTime(currentTime))+"''";
                
                int remHour = getHourTime(currentDIff);
                int remMinute = getMinuteTime(currentDIff);
//                
                
                
//                String currentTimeString = df.format(remHour)+"h "+df.format(remMinute)+"m";
                //String currentTimeString = currentDIff+"";
                
//                text.setText( currentTimeString );
//                text.setText(ints[0]+"h "+ints[1]+"' "+ints[2]+"''");

                
            }

        };        
        
        
//        KeyFrame keyFrame = new KeyFrame(duration, onFinished , keyValueX, keyValueY);
        
        
        
        
        
//        truckPosition = swedenPlatooning.getMyTruckPositionInGraph(0);
        truckPosition = swedenPlatooning.getMyTruckPositionInMap(0);

        
        truckCartoon = new Group();
        truckCartoonRight = new Group();
        
        int centerX = truckPosition[0]; int centerY = truckPosition[1];

        Image truckImg = Globals.kthTruckImage;
        truckImgView = new ImageView(truckImg);
//        truckImgView.setOpacity(0.);
        
        int truckWidth = 50;
        int platoonTruckWidth = 80;
        
        if (bigMode){
            
            truckWidth = (int) (truckWidth*horizontalStretch);
            platoonTruckWidth = (int) (platoonTruckWidth*horizontalStretch);
            
        }
        
        truckImgView.setFitWidth(truckWidth);
        truckImgView.setPreserveRatio(true);
        
        int boundWidth = (int)truckImgView.getBoundsInParent().getWidth();
        int boundHeight = (int)truckImgView.getBoundsInParent().getHeight();
        
        truckImgView.setX(centerX - boundWidth/2);
        truckImgView.setY(centerY - boundHeight/2);
                
        
        truckCartoon.getChildren().add(truckImgView);
        
        
        Image truckImgRight = Globals.kthTruckRightImage;
        truckImgViewRight = new ImageView(truckImgRight);
//        truckImgViewRight.setOpacity(0.);
        
        truckImgViewRight.setFitWidth(truckWidth);
        truckImgViewRight.setPreserveRatio(true);
        truckImgViewRight.setFitWidth(truckWidth);
        
        truckImgViewRight.setX(centerX - boundWidth/2);
        truckImgViewRight.setY(centerY - boundHeight/2);
        
        truckCartoonRight.getChildren().add(truckImgViewRight);
                
        Random randGen = new Random(); 
        
        int randInt = randGen.nextInt(4);
        
        int truckPlatoonIdLogo = swedenPlatooning.getPlatoonTruckId();
//        System.out.println("swedenPlatooning.getPlatoonTruckId() = " + swedenPlatooning.getPlatoonTruckId());
               
        int truckCompany = swedenPlatooning.truckList[truckPlatoonIdLogo].company;

        
        
        Image platoonTruckImg = Globals.truckIkeaImage;
        Image platoonTruckImgRight = Globals.truckIkeaRightImage;
        
        if ( truckCompany == 0){
            platoonTruckImg = Globals.truckIkeaImage;
            platoonTruckImgRight = Globals.truckIkeaRightImage;
        }
        if ( truckCompany == 1){
            platoonTruckImg = Globals.truckIcaImage;
            platoonTruckImgRight = Globals.truckIcaRightImage;
        }
        if ( truckCompany == 2){
            platoonTruckImg = Globals.truckHmImage;
            platoonTruckImgRight = Globals.truckHmRightImage;
        }
        if ( truckCompany == 3){
            platoonTruckImg = Globals.truckScaniaImage;
            platoonTruckImgRight = Globals.truckScaniaRightImage;
        }
        
        
                
        platoonTruckImgView = new ImageView(platoonTruckImg);
        platoonTruckImgViewRight = new ImageView(platoonTruckImgRight);
        
//        platoonTruckImgView.setOpacity(0.);
//        platoonTruckImgViewRight.setOpacity(0.);
        
        // Group for the convoy image
        
        convoyGroup = new Group();
        ImageView convoyOwnTruckImgView = new ImageView(truckImg);
        ImageView convoyPlatoonTruckImgView = new ImageView(platoonTruckImg);
                
        convoyOwnTruckImgView.setFitWidth(truckWidth);
        convoyOwnTruckImgView.setPreserveRatio(true);
        
        convoyPlatoonTruckImgView.setFitWidth(platoonTruckWidth);
        convoyPlatoonTruckImgView.setPreserveRatio(true);
        
        int followingTruckOffsetX = -10;
        int followingTruckOffsetY = -15;
        
        int convoyOffsetToRoadX = -50;
        int convoyOffsetToRoadY = -10;
        int convoyOffsetToRoadXRight = -20;
        int convoyOffsetToRoadYRight = -10;
        
        
        
        // Checking first direction
        
        int[] currentTruckPosition;
        int[] previousTruckPosition;
        
        boolean goingRight = false;
        
        for ( int i = 1 ; i < 1000 ; i++ ){
        
            currentTruckPosition = swedenPlatooning.getMyTruckPositionInMap(i*temporalResolution);
            previousTruckPosition = swedenPlatooning.getMyTruckPositionInMap( (i - 1 )*temporalResolution);
            
            double movementMagnitude = Math.abs( currentTruckPosition[0] - previousTruckPosition[0] );
            
            if ( movementMagnitude > 0.1 ){
                
                if ( Math.abs( currentTruckPosition[0] - previousTruckPosition[0] ) > 0){
                    
                    goingRight = true;
                    
                }else{
                    
                    goingRight = false;
                    
                }
                
                break;
                
            }
        
        }
        
//        if (!goingRight){
//            truckImgViewRight.setOpacity(1.0);
//        }else{
//            truckImgView.setOpacity(1.0);            
//        }
        
        int truckPlatoonIdTemp = swedenPlatooning.getPlatoonTruckId();
//        System.out.println("swedenPlatooning.getPlatoonTruckId() = " + swedenPlatooning.getPlatoonTruckId());
        
        for ( int i = 1 ; i < 10000 ; i++ ){
        
            currentTruckPosition = swedenPlatooning.getTruckPositionInMap(truckPlatoonIdTemp, i*temporalResolution);
            previousTruckPosition = swedenPlatooning.getTruckPositionInMap(truckPlatoonIdTemp,  (i - 1 )*temporalResolution);
            
            double movementMagnitude = Math.abs( currentTruckPosition[0] - previousTruckPosition[0] );
            
            if ( movementMagnitude > 0.1 ){
                
//                System.out.println("currentTruckPosition[0] = " + currentTruckPosition[0]);
//                System.out.println("previousTruckPosition[0] = " + previousTruckPosition[0]);
                
                if ( Math.abs( currentTruckPosition[0] - previousTruckPosition[0] ) > 0){
                    
                    goingRight = true;
                    
                }else{
                    
                    goingRight = false;
                    
                }
                
                break;
                
            }
        
        }
        
//        if (!goingRight){
//            platoonTruckImgViewRight.setOpacity(1.0);
//        }else{
//            platoonTruckImgView.setOpacity(1.0);            
//        }
//        
        
        if (bigMode){
            
            followingTruckOffsetX = (int) (followingTruckOffsetX*horizontalStretch);
            followingTruckOffsetY = (int) (followingTruckOffsetY*horizontalStretch); // Horizontal here because preserveRatio = True
            
            convoyOffsetToRoadX = (int) (convoyOffsetToRoadX*horizontalStretch);
            convoyOffsetToRoadY = (int) (convoyOffsetToRoadY*verticalStretch);
            convoyOffsetToRoadXRight = (int) (convoyOffsetToRoadXRight*horizontalStretch);
            convoyOffsetToRoadYRight = (int) (convoyOffsetToRoadYRight*verticalStretch);
            
        }
        
        convoyPlatoonTruckImgView.setX( centerX + convoyOffsetToRoadX );
        convoyPlatoonTruckImgView.setY( centerY + convoyOffsetToRoadY );
        
        convoyOwnTruckImgView.setX( centerX + platoonTruckWidth + followingTruckOffsetX + convoyOffsetToRoadX );
        convoyOwnTruckImgView.setY( centerY + followingTruckOffsetY + convoyOffsetToRoadY );
        
        convoyGroup.getChildren().add(convoyOwnTruckImgView);
        convoyGroup.getChildren().add(convoyPlatoonTruckImgView);
       
        convoyGroup.setOpacity(0.0);
        
        // Group for the convoy image facing right
        
        convoyGroupRight = new Group();
        ImageView convoyOwnTruckImgViewRight = new ImageView(truckImgRight);
        ImageView convoyPlatoonTruckImgViewRight = new ImageView(platoonTruckImgRight);
                
        convoyOwnTruckImgViewRight.setFitWidth(truckWidth);
        convoyOwnTruckImgViewRight.setPreserveRatio(true);
        
        convoyPlatoonTruckImgViewRight.setFitWidth(platoonTruckWidth);
        convoyPlatoonTruckImgViewRight.setPreserveRatio(true);
        
//        int followingTruckOffsetX = -10;
//        int followingTruckOffsetY = -15;
        
        convoyPlatoonTruckImgViewRight.setX( centerX + convoyOffsetToRoadXRight );
        convoyPlatoonTruckImgViewRight.setY( centerY + convoyOffsetToRoadYRight );
        
        convoyOwnTruckImgViewRight.setX( centerX - truckWidth - followingTruckOffsetX + convoyOffsetToRoadXRight );
        convoyOwnTruckImgViewRight.setY( centerY + followingTruckOffsetY + convoyOffsetToRoadYRight );
        
        convoyGroupRight.getChildren().add(convoyOwnTruckImgViewRight);
        convoyGroupRight.getChildren().add(convoyPlatoonTruckImgViewRight);
        
        convoyGroupRight.setOpacity(0.0);
        
//        convoyGroup.setTranslateX(centerX);
//        convoyGroup.setTranslateY(centerY);
        
//        double platooningStartTime = swedenPlatooning.myTruck.times[0]*(simulationTime/finalSimulationTime);               
        platooningStartTime = swedenPlatooning.myTruck.times[0];  
        
        platooningEndTime = 0;
        
        Truck tempMyTruck = swedenPlatooning.getMyTruck();
        Truck tempPlatoonTruck = swedenPlatooning.truckList[truckPlatoonIdLogo];
        
        for ( int i = 0 ; i < tempPlatoonTruck.path.length ; i++ ){
            
            if ( tempPlatoonTruck.path[i] == tempMyTruck.path[0] ){
                
                for ( int j = 0 ; j < 30 ; j++){
                    
                    if ( i + j < tempPlatoonTruck.path.length && j < tempMyTruck.path.length ){
                        
                        if ( tempPlatoonTruck.path[i+j] == tempMyTruck.path[j] ){
                            
                            platooningEndTime = tempPlatoonTruck.times[i+j];
                            
                            
                            
                        }
                        
                    }
                    
                    
                }
                
            }
            
        }
        
        
        

        
        int[] positionPlatooningStart = swedenPlatooning.getTruckPositionInMap(truckPlatoonIdLogo, platooningStartTime);
        
        int convoyBoundWidth = (int)convoyGroup.getBoundsInParent().getWidth();
        int convoyBoundHeight = (int)convoyGroup.getBoundsInParent().getHeight();
        
        convoyGroup.setTranslateX(positionPlatooningStart[0] - convoyBoundWidth);
        convoyGroup.setTranslateY(positionPlatooningStart[1] - convoyBoundHeight);
        
        convoyGroupRight.setTranslateX(positionPlatooningStart[0] - convoyBoundWidth);
        convoyGroupRight.setTranslateY(positionPlatooningStart[1] - convoyBoundHeight);
        
        truckDots.getChildren().add(convoyGroup);
        truckDots.getChildren().add(convoyGroupRight);
        
        mainRoot.getChildren().add(truckCartoon);
        mainRoot.getChildren().add(truckCartoonRight);
        
        
        for ( int timeStep = 1 ; timeStep - 1 < finalSimulationTime/temporalResolution ; timeStep ++ ){

//            truckPosition = swedenPlatooning.getMyTruckPositionInGraph(timeStep*temporalResolution);
            int[] prevTruckPosition = swedenPlatooning.getMyTruckPositionInMap((timeStep-1)*temporalResolution);
            truckPosition = swedenPlatooning.getMyTruckPositionInMap(timeStep*temporalResolution);

            double keyTime = (timeStep) * (simulationTime/(finalSimulationTime/temporalResolution));
            
            KeyValue kvX = new KeyValue(truckCartoon.translateXProperty(), truckPosition[0] - centerX);
            KeyValue kvY = new KeyValue(truckCartoon.translateYProperty(), truckPosition[1] - centerY);
            KeyValue kvXRight = new KeyValue(truckCartoonRight.translateXProperty(), truckPosition[0] - centerX);
            KeyValue kvYRight = new KeyValue(truckCartoonRight.translateYProperty(), truckPosition[1] - centerY);
            KeyValue kvXConvoy = new KeyValue(convoyGroup.translateXProperty(), truckPosition[0] - centerX);
            KeyValue kvYConvoy = new KeyValue(convoyGroup.translateYProperty(), truckPosition[1] - centerY);
            KeyValue kvXConvoyRight = new KeyValue(convoyGroupRight.translateXProperty(), truckPosition[0] - centerX);
            KeyValue kvYConvoyRight = new KeyValue(convoyGroupRight.translateYProperty(), truckPosition[1] - centerY);

            KeyValue kvOpacity;
            KeyValue kvOpacityRight;
            KeyValue kvOpacityConvoy;
            KeyValue kvOpacityConvoyRight;
            if ( (timeStep-1)*temporalResolution > platooningStartTime && (timeStep-1)*temporalResolution < platooningEndTime ){
                
                kvOpacity = new KeyValue(truckCartoon.opacityProperty(), 0.0);
                kvOpacityRight = new KeyValue(truckCartoonRight.opacityProperty(), 0.0);  
                if ( truckPosition[0] - prevTruckPosition[0] > 0){
                    kvOpacityConvoy = new KeyValue(convoyGroup.opacityProperty(), 0.0);  
                    kvOpacityConvoyRight = new KeyValue(convoyGroupRight.opacityProperty(), 1.0); 
                }else{
                    kvOpacityConvoy = new KeyValue(convoyGroup.opacityProperty(), 1.0);  
                    kvOpacityConvoyRight = new KeyValue(convoyGroupRight.opacityProperty(), 0.0); 
                }
                
            }else{
                
                if ( truckPosition[0] - prevTruckPosition[0] > 0){
                    // GOING RIGHT
                    kvOpacity = new KeyValue(truckCartoon.opacityProperty(), 0.0);
                    kvOpacityRight = new KeyValue(truckCartoonRight.opacityProperty(), 1.0);
                }else{
                    // GOING LEFT
                    kvOpacity = new KeyValue(truckCartoon.opacityProperty(), 1.0);
                    kvOpacityRight = new KeyValue(truckCartoonRight.opacityProperty(), 0.0);  
                }
                kvOpacityConvoy = new KeyValue(convoyGroup.opacityProperty(), 0.0);  
                kvOpacityConvoyRight = new KeyValue(convoyGroupRight.opacityProperty(), 0.0);  
            }
            
            KeyFrame kf = new KeyFrame(Duration.millis(keyTime), kvX);
            timeline.getKeyFrames().add(kf);
            kf = new KeyFrame(Duration.millis(keyTime), kvXRight);
            timeline.getKeyFrames().add(kf);
            kf = new KeyFrame(Duration.millis(keyTime), kvXConvoy);
            timeline.getKeyFrames().add(kf);
            kf = new KeyFrame(Duration.millis(keyTime), kvXConvoyRight);
            timeline.getKeyFrames().add(kf);
            kf = new KeyFrame(Duration.millis(keyTime), kvY);
            timeline.getKeyFrames().add(kf);  
            kf = new KeyFrame(Duration.millis(keyTime), kvYRight);
            timeline.getKeyFrames().add(kf);      
            kf = new KeyFrame(Duration.millis(keyTime), kvYConvoy);
            timeline.getKeyFrames().add(kf);   
            kf = new KeyFrame(Duration.millis(keyTime), kvYConvoyRight);
            timeline.getKeyFrames().add(kf);
            kf = new KeyFrame(Duration.millis(keyTime), kvOpacity);
            timeline.getKeyFrames().add(kf);    
            kf = new KeyFrame(Duration.millis(keyTime), kvOpacityRight);
            timeline.getKeyFrames().add(kf);    
            kf = new KeyFrame(Duration.millis(keyTime), kvOpacityConvoy);
            timeline.getKeyFrames().add(kf);    
            kf = new KeyFrame(Duration.millis(keyTime), kvOpacityConvoyRight);
            timeline.getKeyFrames().add(kf);    
            
            firstTruckPosition = truckPosition;

        }      
        
        Truck tempTruck = swedenPlatooning.truckList[0];
        
//        System.out.println(Arrays.toString(tempTruck.path));
                
        int truckPlatoonId = swedenPlatooning.getPlatoonTruckId();
//        System.out.println("swedenPlatooning.getPlatoonTruckId() = " + swedenPlatooning.getPlatoonTruckId());
//        System.out.println("truckPlatoonId");
//        System.out.println(truckPlatoonId);
               
        Truck platoonTruck = swedenPlatooning.truckList[truckPlatoonId];
//        System.out.println("platoonTruck.getPath()");
//        System.out.println(Arrays.toString(platoonTruck.getPath()));
        Truck myTruck = swedenPlatooning.getMyTruck();
//        System.out.println("myTruck.getPath()");
//        System.out.println(Arrays.toString(myTruck.getPath()));
        
        double lastSharedTime = 0;
        
        for ( int i = 0 ; i < platoonTruck.path.length ; i++ ){
            
            if ( platoonTruck.path[i] == myTruck.path[0] ){
                
                for ( int j = 0 ; j < 30 ; j++){
                    
                    if ( i + j < platoonTruck.path.length && j < myTruck.path.length ){
                        
                        if ( platoonTruck.path[i+j] == myTruck.path[j] ){
                            
                            lastSharedTime = platoonTruck.times[i+j];
                            
                            
                            
                        }
                        
                    }
                    
                }
                       
            }
            
        }
        
        double platooningStartKeyFrameTime = myTruck.times[0]*(simulationTime/finalSimulationTime);
        double platooningEndKeyFrameTime = lastSharedTime*(simulationTime/finalSimulationTime);
        double tripEndKeyFrameTime = myTruck.times[ myTruck.times.length - 1 ]*(simulationTime/finalSimulationTime);
        
        double platFrameTime = platooningEndKeyFrameTime - platooningStartKeyFrameTime;
        double platFrames = platFrameTime / (simulationTime/(finalSimulationTime/temporalResolution));
        
        double fuelSavedPerFrame = totSF/platFrames;
        
        double tripFrameTime = tripEndKeyFrameTime - platooningStartKeyFrameTime;
        double tripFrames = tripFrameTime / (simulationTime/(finalSimulationTime/temporalResolution));
        
        incrNoPlat = maxBarHeight / tripFrames;
        incrBase = incrNoPlat;
        incrPlat = .8*incrNoPlat;
        
        finalFuelNoPlat = platFrames*incrNoPlat + (tripFrames-platFrames)*incrBase;
        finalFuelPlat = platFrames*incrPlat + (tripFrames-platFrames)*incrBase;
        
        Line tester = new Line(barPositionX,barPositionY-finalFuelNoPlat,barPositionX+200,barPositionY-finalFuelNoPlat);
        tester.setFill(Color.BLUE);
//        mainRoot.getChildren().addAll(tester);
        
        Line tester2 = new Line(barPositionX,barPositionY-finalFuelPlat,barPositionX+200,barPositionY-finalFuelPlat);
        tester2.setFill(Color.BLUE);
//        mainRoot.getChildren().addAll(tester2);
        
        
        String ensamString = "   Ensam";
        
        if (bigMode){
        
            platooningText = new Text(rectBG.getX()+ horizontalStretch*50, barPositionY+ verticalStretch*140, (language.isEnglish())?"Not\nPlatooning":ensamString);
            platooningText.setFont(new Font(fontStretch*40));
            
        }else{
            
            platooningText = new Text(rectBG.getX()+50, barPositionY+140, (language.isEnglish())?"Not\nPlatooning":ensamString);
            platooningText.setFont(new Font(40));
            
        }
        
        platooningText.setTextAlignment(TextAlignment.CENTER);
        platooningText.setFill(Color.RED);
//        plTextWidth = platooningText.getLayoutBounds().getWidth();
//        platooningText.setX((rectBG.getWidth()-plTextWidth)/2 + barPositionX);
        
        Timeline graphTL = new Timeline();
//        graphTL.setCycleCount(Timeline.INDEFINITE);
        graphTL.setCycleCount(1);
        
        double kt;
        final double finalt = finalSimulationTime/temporalResolution;
        DecimalFormat dfFuel = new DecimalFormat("0.00");        
//        
        for (int t = 1; t-1<finalSimulationTime/temporalResolution;t++)
        {
            kt = t * (simulationTime/(finalSimulationTime/temporalResolution));
            boolean lastit = (t>finalt);
            
            KeyFrame kf = new KeyFrame(Duration.millis(kt), (ActionEvent actionEvent) -> {
                
                    if (!prevSimOn && simOn)
                    {
                        fuelPlat = 0;
                        fuelNoPlat = 0;
                        currentSF = 0;
                        
                    }           
                    if (simOn)
                    {
                        if (tripOn)
                        {
                            if (platOn)
                            {
                                if (fuelNoPlat+incrNoPlat>=maxBarHeight)
                                {
                                    double prevDiff = fuelNoPlat-fuelPlat;
                                    fuelNoPlat = maxBarHeight;
                                    fuelPlat = Math.min(fuelPlat+incrPlat,maxBarHeight-prevDiff);
                                }
                                else
                                {
                                    fuelNoPlat += incrNoPlat;
                                    fuelPlat += incrPlat;
                                    currentSF += fuelSavedPerFrame;
                                }
                            }
                            else
                            {
                                if (fuelNoPlat+incrBase>=maxBarHeight)
                                {
                                    double prevDiff = fuelNoPlat-fuelPlat;
                                    fuelNoPlat = maxBarHeight;
                                    fuelPlat = Math.min(fuelPlat+incrBase,maxBarHeight-prevDiff);
                                }
                                else
                                {
                                    fuelPlat += incrBase;
                                    fuelNoPlat += incrBase;
                                }
                            }
                            
                        }

                    }

                    prevSimOn = simOn;

                rectNoPlat.setLayoutY(barPositionY-fuelNoPlat);
                rectDash.setLayoutY(barPositionY-fuelPlat);
                rectNoPlat.setHeight(fuelNoPlat);
                rectPlat.setLayoutY(barPositionY-fuelPlat);
                rectPlat.setHeight(fuelPlat);
                rectDash.setHeight(fuelPlat);
                dashed.setStartY(barPositionY-fuelPlat);
                dashed.setEndY(barPositionY-fuelPlat);
                if (lastit)
                    currentSF = totSF;
                String fuelString = dfFuel.format(currentSF)+" l";
                fuelText.setText(fuelString);
//                rectDiff.setWidth((fuelNoPlat-fuelPlat)*3);
            });
            graphTL.getKeyFrames().add(kf);
//        
        }
        
        

        
        EventHandler onSimulationStart = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                simOn = true;
                tripOn = false;
                rectDash.setVisible(false);
                dashed.setVisible(false);
                fuelText.setVisible(false);
                platooningText.setText(language.isEnglish()?"Not\nPlatooning":ensamString);
                platooningText.setTextAlignment(TextAlignment.CENTER);
                
                if (bigMode){
                    
                    platooningText.setX(rectBG.getX()+horizontalStretch*70);
                    platooningText.setY(barPositionY+verticalStretch*140);
                    
                }else{
                    
                    platooningText.setX(rectBG.getX()+50);
                    platooningText.setY(barPositionY+140);
                    
                }
                
                
                platooningText.setFill(Color.RED);
                
            }
        };
        EventHandler onPlatoonStarted = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                platOn = true;
                tripOn = true;
                rectDash.setVisible(true);
                dashed.setVisible(true);
//                fuelText.setVisible(true);
                fuelText.setVisible(false);
                
                if (language.isEnglish()){
                    platooningText.setText("Platooning");
                    platooningText.setX(rectBG.getX()+horizontalStretch*70);
                    platooningText.setY(barPositionY+verticalStretch*160);
                }else{
                    platooningText.setText("I fordonskolonn");
                    platooningText.setX(rectBG.getX()+horizontalStretch*40);
                    platooningText.setY(barPositionY+verticalStretch*160);
                }
                
                platooningText.setFill(Color.GREEN);
                
            }
        };
        EventHandler onPlatoonFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                platOn = false;
                platooningText.setText((language.isEnglish())?"Not\nPlatooning":ensamString);
                
                if (bigMode){
                    
                    platooningText.setX(rectBG.getX()+horizontalStretch*70);
                    platooningText.setY(barPositionY+verticalStretch*140);
                    
                }else{
                    
                    platooningText.setX(rectBG.getX()+50);
                    platooningText.setY(barPositionY+140);
                    
                }
                
                
                platooningText.setTextAlignment(TextAlignment.CENTER);
                platooningText.setFill(Color.RED);
            }
        };

        EventHandler onTripFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                tripOn = false;
                simOn = false;
                platooningText.setText((language.isEnglish())?"Trip Finished":"Resan avslutad");
                platooningText.setFill(Color.BLUE);
                
                platooningText.setY(barPositionY+verticalStretch*160);
                
                if (language.isEnglish()){
                    platooningText.setX(rectBG.getX()+horizontalStretch*55);
                }else{
                    platooningText.setX(rectBG.getX()+horizontalStretch*45);
                }
                                
//                BB.changeLabel("skip", "replay");
//                buttonsEvents(buttonsHB);
            }
        };

        
         
       
                
        boolean platooningTruckDrawn = false;
        
                
        Random random = new Random();
        
        int tempNumTrucksToVisualise = numTrucksToVisualise;
        
        int platoonTruckAdvance = 25;
        
        for ( int truckId = 0 ; truckId < tempNumTrucksToVisualise ; truckId++ ){

                int realId = -99;
            
                if ( platooningTruckDrawn == false && truckId == numTrucksToVisualise-1){
                    
                    truckId = truckPlatoonId;
                    realId = truckPlatoonId;
                    
                }else{
                
                    

                    boolean invalidTruck = true;
                    while (invalidTruck){

                        realId = random.nextInt(10000);

                        Truck tempTruckCheck = swedenPlatooning.truckList[realId];
                        if ( tempTruckCheck.times[ tempTruckCheck.times.length - 2 ] > finalSimulationTime ){

//                            System.out.println("Rejecting truck due to being stopped.");

                        }else{
//                            System.out.println("Accepting truck.");
                            invalidTruck = false;


                        }

                    }
                
                }
                
                
                
//                truckPosition = swedenPlatooning.getTruckPositionInGraph(truckId, timeStep*timeJump);
//                truckPosition = swedenPlatooning.getTruckPositionInGraph(truckId, 0);
                if ( realId == truckPlatoonId ){
                    truckPosition = swedenPlatooning.getTruckPositionInMap(realId, 0+platoonTruckAdvance);
                }else{
                    truckPosition = swedenPlatooning.getTruckPositionInMap(realId, 0);        
                }

                
                
                truckDot = new Circle();

//                System.out.println(Arrays.toString(truckPosition));
                if ( realId == truckPlatoonId ){
                    
//                    truckDot = new Circle(truckPosition[0], truckPosition[1], 2.0*nodeRadius, Color.web("blue", nodeOpacity));
                    
                    
                    
                    centerX = truckPosition[0]; centerY = truckPosition[1];
                    
                    platoonTruckImgView.setFitWidth(platoonTruckWidth);
                    platoonTruckImgView.setPreserveRatio(true);
                    platoonTruckImgView.setFitWidth(platoonTruckWidth);
                    
                    int platoonBoundWidth = (int)truckImgView.getBoundsInParent().getWidth();
                    int platoonBoundHeight = (int)truckImgView.getBoundsInParent().getHeight();

                    platoonTruckImgView.setX(centerX - platoonBoundWidth/2);
                    platoonTruckImgView.setY(centerY - platoonBoundHeight/2);

//                    platoonTruckCartoon.getChildren().add(platoonTruckImgView);
                    
                    platoonTruckImgViewRight.setFitWidth(platoonTruckWidth);
                    platoonTruckImgViewRight.setPreserveRatio(true);
                    platoonTruckImgViewRight.setFitWidth(platoonTruckWidth);

                    platoonTruckImgViewRight.setX(centerX - platoonBoundWidth/2);
                    platoonTruckImgViewRight.setY(centerY - platoonBoundHeight/2);
                    
//                    platoonTruckCartoonRight.getChildren().add(platoonTruckImgViewRight);

//                    truckDots.getChildren().add(platoonTruckImgView);
//                    truckDots.getChildren().add(platoonTruckImgViewRight);
                    mainRoot.getChildren().add(platoonTruckImgView);
                    mainRoot.getChildren().add(platoonTruckImgViewRight);
                    
                    platooningTruckDrawn = true;
//                    continue;
                    
                }else{
                
                    truckDot = new Circle(truckPosition[0], truckPosition[1], nodeRadius, Color.web("yellow", nodeOpacity));
                    truckDots.getChildren().add(truckDot);
                }
//                truckDot.setStrokeType(StrokeType.OUTSIDE);
//                truckDot.setStroke(Color.web("red", 0.16));
//                truckDot.setStrokeWidth(10);

                
                
            firstTruckPosition[0] = 0;
            firstTruckPosition[1] = 0;
            lastTruckPosition[0] = 0;
            lastTruckPosition[1] = 0;

            for ( int timeStep = 1 ; timeStep-1 < finalSimulationTime/temporalResolution ; timeStep ++ ){
                
//                truckPosition = swedenPlatooning.getTruckPositionInGraph(truckId, timeStep*temporalResolution);
//                truckPosition = swedenPlatooning.getTruckPositionInMap(realId, timeStep*temporalResolution);
                
                int[] prevTruckPosition = new int[2];
                
                if ( realId == truckPlatoonId ){
                    prevTruckPosition = swedenPlatooning.getTruckPositionInMap(realId, (timeStep-1)*temporalResolution+platoonTruckAdvance);
                    truckPosition = swedenPlatooning.getTruckPositionInMap(realId, timeStep*temporalResolution+platoonTruckAdvance);
                }else{
                    truckPosition = swedenPlatooning.getTruckPositionInMap(realId, timeStep*temporalResolution);   
                }
                
     
                
                double keyTime = timeStep * (simulationTime/(finalSimulationTime/temporalResolution));
                
//                System.out.println(keyTime);
                
                KeyValue kvX, kvXRight;
                KeyValue kvY, kvYRight;
                
                if ( realId == truckPlatoonId ){
                    
                      
                    KeyValue kvOpacity, kvOpacityRight;
                    
                    kvX = new KeyValue(platoonTruckImgView.translateXProperty(), truckPosition[0] - centerX);
                    kvXRight = new KeyValue(platoonTruckImgViewRight.translateXProperty(), truckPosition[0] - centerX);
                    kvY = new KeyValue(platoonTruckImgView.translateYProperty(), truckPosition[1] - centerY);
                    kvYRight = new KeyValue(platoonTruckImgViewRight.translateYProperty(), truckPosition[1] - centerY);
                    
                    
                    if ( (timeStep-1)*temporalResolution > platooningStartTime && (timeStep-1)*temporalResolution < platooningEndTime ){
                
                        kvOpacity = new KeyValue(platoonTruckImgView.opacityProperty(), 0.0);
                        kvOpacityRight = new KeyValue(platoonTruckImgViewRight.opacityProperty(), 0.0);  
                
                    }else{
                        
                        if ( truckPosition[0] - prevTruckPosition[0] > 0){
                            // GOING RIGHT
                            kvOpacity = new KeyValue(platoonTruckImgView.opacityProperty(), 0.0);
                            kvOpacityRight = new KeyValue(platoonTruckImgViewRight.opacityProperty(), 1.0);
                                                       
    //                    timeline.getKeyFrames().add(kf);   
                        }else{
                            // GOING LEFT
                            kvOpacity = new KeyValue(platoonTruckImgView.opacityProperty(), 1.0);
                            kvOpacityRight = new KeyValue(platoonTruckImgViewRight.opacityProperty(), 0.0);
                            
                        }
                        
                    }
                    
                    KeyFrame kf = new KeyFrame(Duration.millis(keyTime), kvOpacity);
                    timeline.getKeyFrames().add(kf);
                    kf = new KeyFrame(Duration.millis(keyTime), kvOpacityRight);
                    timeline.getKeyFrames().add(kf);
                    kf = new KeyFrame(Duration.millis(keyTime), kvXRight);
                    timeline.getKeyFrames().add(kf);
                    kf = new KeyFrame(Duration.millis(keyTime), kvYRight);
                    timeline.getKeyFrames().add(kf);
                    
                }else{
                
                    kvX = new KeyValue(truckDot.centerXProperty(), truckPosition[0]);
                    kvY = new KeyValue(truckDot.centerYProperty(), truckPosition[1]);

                }
                
                KeyFrame kf = new KeyFrame(Duration.millis(keyTime), kvX);
                timeline.getKeyFrames().add(kf);
                kf = new KeyFrame(Duration.millis(keyTime), kvY);
                timeline.getKeyFrames().add(kf);

                
                
                firstTruckPosition = truckPosition;

            }
                        
            
        }
       
        truckTrail = new Polyline();
        truckTrail.setStrokeLineCap(StrokeLineCap.ROUND);
        truckTrail.setStroke(Color.GREEN);
//        truckTrail.setStrokeWidth(0.0);
        truckTrail.setStrokeWidth(8.0);
        truckTrail.setStrokeLineJoin( StrokeLineJoin.ROUND );
        
        platoonTruckTrail = new Polyline();
        platoonTruckTrail.setStrokeLineCap(StrokeLineCap.ROUND);
        platoonTruckTrail.setStroke(Color.BLUE);
//        platoonTruckTrail.setStrokeWidth(0.0);
        platoonTruckTrail.setStrokeWidth(4.0);
        platoonTruckTrail.setStrokeLineJoin( StrokeLineJoin.ROUND );
        
        convoyTrail = new Polyline();
//        convoyTrail.setStrokeLineCap(StrokeLineCap.ROUND);
        convoyTrail.setStroke(Color.YELLOW);
        convoyTrail.setStrokeDashOffset(10.0);
        convoyTrail.getStrokeDashArray().addAll(2.5d, 15d);
//        convoyTrail.setStrokeWidth(1.0);
        convoyTrail.setStrokeWidth(4.0);
        convoyTrail.setStrokeLineJoin( StrokeLineJoin.ROUND );

        truckTrailGroup = new Group();
        platoonTruckTrailGroup = new Group();
        convoyTrailGroup = new Group();       
                
        mainRoot.getChildren().add(truckTrailGroup);
        mainRoot.getChildren().add(platoonTruckTrailGroup);
        mainRoot.getChildren().add(convoyTrailGroup);
        
        mainRoot.getChildren().add(truckTrail);
        mainRoot.getChildren().add(platoonTruckTrail);
        mainRoot.getChildren().add(convoyTrail);
        
        
        platoonTruckImgView.toFront();
        platoonTruckImgViewRight.toFront();
        convoyGroup.toFront();
        convoyGroupRight.toFront();
        truckCartoon.toFront();
        truckCartoonRight.toFront();
        
//        truckDots.getChildren().add(platooningText);
        mainRoot.getChildren().add(platooningText);
        KeyFrame textKeyFrame = new KeyFrame(Duration.millis(platooningStartKeyFrameTime), onPlatoonStarted);
        timeline.getKeyFrames().add(textKeyFrame);
        textKeyFrame = new KeyFrame(Duration.millis(platooningEndKeyFrameTime), onPlatoonFinished);
        timeline.getKeyFrames().add(textKeyFrame);
        textKeyFrame = new KeyFrame(Duration.millis(tripEndKeyFrameTime), onTripFinished);
        timeline.getKeyFrames().add(textKeyFrame);
        textKeyFrame = new KeyFrame(Duration.millis(0), onSimulationStart);
        timeline.getKeyFrames().add(textKeyFrame);
        
        parallelTransition.getChildren().add(timeline);
        parallelTransition.getChildren().add(graphTL);
//        parallelTransition.getChildren().add(animation);
        
        
        sequentialTransition.getChildren().add(parallelTransition);
        
        parallelTransition.setOnFinished((ActionEvent event) -> {
            
            System.out.println("Finished Trucks moving animation");
            
        });
        
        Rectangle dummyRect = new Rectangle();
        dummyRect.setOpacity(0.0);
        
        TranslateTransition waitTransition = new TranslateTransition(Duration.millis(delayTimeAfterSimulationSeconds*1000), dummyRect);
        waitTransition.setFromX( 0 );
        waitTransition.setToX( 1 );
        waitTransition.setCycleCount(1);
        waitTransition.setAutoReverse(false);
        
        sequentialTransition.getChildren().add(waitTransition);
        
//        playTrucksMovement();
        sequentialTransition.setOnFinished((ActionEvent event) -> {
            
            System.out.println("Finished Trucks moving animation and added delay");
            
            if ( realTimeVisualisationExited == true ){
                
                return;
                
            }
            
            
            
//            BB.changeLabel("skip","replay");
//            buttonsEvents(buttonsHB);
            mainRoot.getChildren().removeAll(platooningText);
            showSavedPercentage();
//            showFinalStatsMenu();
            
            System.out.println("must add waiting time here!");
            
            showInfoGraphics();
            helpMenuGroup.toFront();
            
            BB.changeLabel("skip","replay");
            
        });

        mainRoot.getChildren().add(truckDots);
        
        setTrail();
        
        
        int currentStageOfProgram = 4;
        setUpHelpStuff(currentStageOfProgram);

        
    }
    
    private void playTrucksMovement(){
        
        System.out.println("playTrucksMovement()");
        sequentialTransition.play();
        dummyTransition.play();
        realTimeVisualisationExited = false;
        
    }
    
    public void cleanScreen(){
        
//        inactivityTimeline.setOnFinished(null);
        inactivityTimeline.stop();
        
        System.out.println("RealTimeVisualisation->cleanScreen!");
        
        int numElements = longSavingsTextGroup.getChildren().size();
        
        for ( int i = 0 ; i < numElements ; i++ ){
            
            longSavingsTextGroup.getChildren().remove(0);
            
        }     
        
        numElements = finalBox.getChildren().size();
        
        for ( int i = 0 ; i < numElements ; i++ ){
            
            finalBox.getChildren().remove(0);
            
        }      
        
               
        numElements = longSavingsTextGroup.getChildren().size();
        
        for ( int i = 0 ; i < numElements ; i++ ){
            
            longSavingsTextGroup.getChildren().remove(0);
            
        } 
                
        
        numElements = mainRoot.getChildren().size();
        
        for ( int i = 0 ; i < numElements ; i++ ){
            
            mainRoot.getChildren().remove(0);
            
        }
        
//        mainScene.removeEventHandler(KeyEvent.KEY_PRESSED, handler);
        
        
    }
    
    public synchronized void addSwitchListener(SwitchListener sl)
    {
        if (!switchListenerList.contains(sl))
        {
            switchListenerList.add(sl);
        }
    }
    
    
    private void processSwitchEvent(SwitchEvent switchEvent) {
        ArrayList<SwitchListener> tempSwitchListenerList;

        synchronized (this) {
                if (switchListenerList.size() == 0)
                        return;
                tempSwitchListenerList = (ArrayList<SwitchListener>) switchListenerList.clone();
        }

        for (SwitchListener listener : tempSwitchListenerList) {
                listener.switchRequested(switchEvent);
        }
    }
    
    
    private Group showLegend(){
        
        Group legendTrucks = new Group();
        
        int nodeRadius = 10;
        double nodeOpacity = 1.0;
        
        int startX = 1000;
        int startY = 800;
        int offsetY = 50;
        int textOffsetX = 35;
        int textOffsetY = 7;
        
        Rectangle r = new Rectangle();
        r.setX(startX - 25);
        r.setY(startY - 25);
        r.setWidth(220);
        r.setHeight(150);
//        r.setArcWidth(20);
//        r.setArcHeight(20);
        r.setOpacity(0.75);
        r.setFill(Color.WHITE);
        
        legendTrucks.getChildren().add(r);
        
        Circle myTruckDot = new Circle(startX, startY, nodeRadius, Color.web("green", nodeOpacity));
        
        legendTrucks.getChildren().add(myTruckDot);
        
        Circle platoonTruckDot = new Circle(startX, startY + offsetY, nodeRadius, Color.web("blue", nodeOpacity));
        
        legendTrucks.getChildren().add(platoonTruckDot);
        
        Circle randomTruckDot = new Circle(startX, startY + 2*offsetY, nodeRadius, Color.web("yellow", nodeOpacity));
        
        legendTrucks.getChildren().add(randomTruckDot);
        
        Text text = new Text("Own truck");
        text.setX(startX + textOffsetX);
        text.setY(startY + textOffsetY);
        text.setFont(new Font(20));
        text.setFill(Color.BLACK);
        
        legendTrucks.getChildren().add(text);
        
        text = new Text("Platooning truck");
        text.setX(startX + textOffsetX);
        text.setY(startY + offsetY + textOffsetY);
        text.setFont(new Font(20));
        text.setFill(Color.BLACK);
        
        legendTrucks.getChildren().add(text);
        
        text = new Text("Other trucks");
        text.setX(startX + textOffsetX);
        text.setY(startY + 2*offsetY + textOffsetY);
        text.setFont(new Font(20));
        text.setFill(Color.BLACK);
        
        legendTrucks.getChildren().add(text);
        
        return legendTrucks;
        
    }

    private void buttonsEvents(HBox buttonsHB)
    {
        Object[] bArr = buttonsHB.getChildren().toArray();
        
        for (Object obj : bArr)
        {
            assert (obj instanceof Button);
            Button b = (Button)obj;
            if (b.getText().equals("")) // home button
            {
                
//                b.setDefaultButton(true);
                b.setOnMousePressed((MouseEvent me)->{
                    
                    inactivityTimeline.setOnFinished(null);
                    inactivityTimeline.stop();
                    processSwitchEvent(new SwitchEvent(this, 4));
                });
            }
            if (b.getText().equalsIgnoreCase("skip"))
            {
                b.setOnMousePressed((MouseEvent me)->{
                    
                    System.out.println("skipped HERE");
                    restartInactivityAnimation();
                    setBarsFinalValues();
                    
                    sequentialTransition.jumpTo("end");
                    dummyTransition.stop();
                    
                    mainRoot.getChildren().remove(truckTrail);
                    mainRoot.getChildren().remove(platoonTruckTrail);
                    mainRoot.getChildren().remove(convoyTrail);
                    
                    showFinalPolylines();
                    
                    showInfoGraphics();
                    helpMenuGroup.toFront();
                    
                    BB.changeLabel("skip","replay");
                    buttonsEvents(buttonsHB);
                    
                });
            }
            if (b.getText().equalsIgnoreCase("replay"))
            {
                b.setOnMousePressed((MouseEvent me)->{
                    cleanScreen();
                    restartInactivityAnimation();
                    showTrucksMoving();
                });
            }
            if (b.getText().equalsIgnoreCase("continue"))
            {
                // replay the current step inside the same page
            }
        }
    }

    private void setBarsFinalValues()
    {   
        

        
        rectDash.setLayoutX(  rectBG.getBoundsInLocal().getMinX() + (3./4.)*rectBG.getBoundsInLocal().getWidth() - barWidth/2.  );
        rectDash.setLayoutY( barPositionY - finalFuelPlat );
        rectDash.setHeight( finalFuelPlat );

        rectPlat.setLayoutX( barPositionX );
        rectPlat.setLayoutY( barPositionY - finalFuelPlat );
        rectPlat.setHeight( finalFuelPlat );

        rectNoPlat.setLayoutX( rectBG.getBoundsInLocal().getMinX() + (3./4.)*rectBG.getBoundsInLocal().getWidth() - barWidth/2. );
        rectNoPlat.setLayoutY( barPositionY - finalFuelNoPlat );
        rectNoPlat.setHeight( finalFuelNoPlat );

        dashed.setStartY( barPositionY-finalFuelPlat );
        dashed.setEndY( barPositionY-finalFuelPlat );
        
        
        
        
        
        DecimalFormat dfFuel = new DecimalFormat("0.00"); 
        String fuelString = dfFuel.format(totSF)+" l";
        fuelText.setText(fuelString);
        
        for (Node item : rects.getChildren())
            item.setVisible(true);
            
        
    }

    private double roundTwoDec(double orig) {
        orig *= 100;
        long tmp = Math.round(orig);
        return (double) tmp/100;
    }

    private void showSavedPercentage() {
        double TotFuelNoPlat = swedenPlatooning.getMyTruck().fuelConsumption;
        double savedPerc =  (100*totSF/TotFuelNoPlat);
        diffText.setText(language.isEnglish()?"Total saved fuel":"Total bränslebesparing");
        double newDiffTextX = (language.isEnglish())?rectBG.getX() + 70:rectBG.getX() + 55;
        
        DecimalFormat dfFuel = new DecimalFormat("0.00");
        String savedPercString = dfFuel.format(savedPerc);
        fuelText.setText(savedPercString+" %");
        fuelText.setVisible(true);
        
        if (bigMode){
            
//            diffText.setLayoutX( horizontalStretch*(newDiffTextX) );
//            diffText.setLayoutX( 1500 );
            
            if (savedPercString.length()>4){
//                fuelText.setX( horizontalStretch*(fuelText.getX()-15) );
            }
            
        }else{
        
//            diffText.setLayoutX(newDiffTextX);
            
            if (savedPercString.length()>4){
//                fuelText.setX(fuelText.getX()-15);
            }
        }

        double tempTextWidth = diffText.getBoundsInLocal().getWidth();
        double tempTextHeight = diffText.getBoundsInLocal().getHeight();      
        double tempRectBGWidth = rectBG.getBoundsInLocal().getWidth();
        double tempRectBGHeight = rectBG.getBoundsInLocal().getHeight();     
        
        diffText.setLayoutX( rectBG.getBoundsInLocal().getMinX() + tempRectBGWidth/2. - tempTextWidth/2. );
        diffText.setLayoutY( rectBG.getBoundsInLocal().getMinY() + tempRectBGHeight*(15./20.) - tempTextHeight/2. );
        
        tempTextWidth = fuelText.getBoundsInLocal().getWidth();
        tempTextHeight = fuelText.getBoundsInLocal().getHeight();      
        
        fuelText.setLayoutX( rectBG.getBoundsInLocal().getMinX() + tempRectBGWidth/2. - tempTextWidth/2. );
        fuelText.setLayoutY( rectBG.getBoundsInLocal().getMinY() + tempRectBGHeight*(20./20.) - tempTextHeight/2. );
        
        
        
    }
    
    public void setBooleanToFinished(){
        
        realTimeVisualisationExited = true;
        
    }

    private void showFinalStatsMenu() {
        
        
        System.out.println("RealTimeVisualisation->showFinalStatsMenu!");

        double RectW = horizontalStretch*(290);
        double RectH = 300;
        double RectX = screenWidth - RectW - 50;
        double RectY = screenHeight - RectH - 50;
        
        int times = 5;
        int days = 4;
        int weeks = 45;
        int trucks = 3;
        
        ChoiceBox cbTimesPerDay = new ChoiceBox();
        ChoiceBox cbDayPerWeek = new ChoiceBox();
        ChoiceBox cbWeekPerYear = new ChoiceBox();
        ChoiceBox cbTrucksPerCompany = new ChoiceBox();
         
        cbTimesPerDay.getItems().addAll("2", "4", "6", "8", "10");
        cbTimesPerDay.getSelectionModel().select(1);
        
    
        cbDayPerWeek.getItems().addAll("1", "2", "3", "4", "5");
        cbDayPerWeek.getSelectionModel().select(4);
        
        cbWeekPerYear.getItems().addAll("30", "40", "50");
        cbWeekPerYear.getSelectionModel().select(1);
        
        cbTrucksPerCompany.getItems().addAll("5", "10", "20", "50", "100");
        cbTrucksPerCompany.getSelectionModel().select(0);
  
        timesPerDay = 4;
        dayPerWeek = 5;
        weekPerYear = 40;
        trucksPerCompany = 5;
        
        cbTimesPerDay.valueProperty().addListener(new ChangeListener<String>() {
            @Override 
            public void changed(ObservableValue ov, String t, String t1) {                
                  timesPerDay = Integer.parseInt(t1);
                  updateLongSavingsText();
//                                
            }    
        });
        
        cbDayPerWeek.valueProperty().addListener(new ChangeListener<String>() {
            @Override 
            public void changed(ObservableValue ov, String t, String t1) {                
                  dayPerWeek = Integer.parseInt(t1);
                  updateLongSavingsText();
//                                
            }    
        });
        
        cbWeekPerYear.valueProperty().addListener(new ChangeListener<String>() {
            @Override 
            public void changed(ObservableValue ov, String t, String t1) {                
                  weekPerYear = Integer.parseInt(t1);
                  updateLongSavingsText();
//                                
            }    
        });
        
        cbTrucksPerCompany.valueProperty().addListener(new ChangeListener<String>() {
            @Override 
            public void changed(ObservableValue ov, String t, String t1) {                
                  trucksPerCompany = Integer.parseInt(t1);
                  updateLongSavingsText();
//                                
            }    
        });
        
        
        int yearSavings = (int)(totSF*times*days*weeks*trucks);
        
        Rectangle finalRect = new Rectangle(RectX,RectY,RectW,RectH);
        finalRect.setArcHeight(rectArcHeight);
        finalRect.setArcWidth(rectArcWidth);
        finalRect.setFill(Color.web("ffffff", .85));
        
        int textOffsetYFirstLine = 70;
        double textOffsetYLines = 40;
        double textX = (int)(RectX+45);
        double textY = (int)(RectY+30);
        double fontSize = 20;
        int cbOffsetX = -35;
        int cbOffsetY = -20;
        double choiceboxWidth = 50;
        double choiceboxInterval = 10;
        double intervalBetweenLines = 10;
        double choiceBoxWidth1 = 50;
        double choiceBoxWidth2 = 50;
        double choiceBoxWidth3 = 60;
        
                        
        if (bigMode){
            
            textX = (int) (RectX+30);
            textY = (int) (RectY+40);
            textOffsetYFirstLine = (int)(verticalStretch*textOffsetYFirstLine);
            
            fontSize = fontSize * fontStretch;
            cbOffsetX = (int) (cbOffsetX * horizontalStretch)+25;
            cbOffsetY = (int) (cbOffsetY * verticalStretch);
            
        }

        
        Text introText = new Text("");
        
        
        
        
        if (language.isEnglish())
        {

//            introText.setText("You saved about "+totSF+" liters of fuel\nfor this short trip.");
            introText.setText("You saved about "+totSF+" liters\n  of fuel for this short trip.");
            text0a.setText( "If your company saves this much");
            text1b.setText( "times per day,");
            text2b.setText( "days per week,");
            text3b.setText( "weeks per year,");
            text4a.setText( "using " );
            text4b.setText( "trucks..." );
            text5a.setText( "you will save about "+yearSavings+" liters per year!");
            
            introText.setX(textX + 40);
            
        }
        else
        {
            
            introText.setText("Du sparade ungefär "+totSF+" liter under\nden här korta resan.");
            text0a.setText( "Om ditt företag sparade så mycket");
            text1b.setText( "gånger per dag,");
            text2b.setText( "dagar i veckan,");
//            text3.setText( weekPerYear + " veckor om år,\n");
            text3b.setText( "veckor per år,");
            text4a.setText( " med " );
            text4b.setText( "lastbilar..." );
            text5a.setText( "… så skulle du spara "+yearSavings+" liter per år.");
            
            introText.setX(textX + 20);
            
        }
        
        double line1offset = 65;
        double line2offset = 70;
        double line3offset = 70;
        double line4offset = 20;
        
        introText.setFont( new Font(fontSize) );
        
//        introText.setY(textY);
        introText.setLayoutY(textY);
        
        text0a.setFont( new Font(fontSize) );
//        text0a.setX(textX );
//        text0a.setX(RectX + RectW/2. - text0a.getBoundsInLocal().getWidth()/2.);
//        text0a.setY(textY + textOffsetYFirstLine + 0*textOffsetYLines);
        text0a.setLayoutX(RectX + RectW/2. - text0a.getBoundsInLocal().getWidth()/2.);
        text0a.setLayoutY(textY + textOffsetYFirstLine + 0*textOffsetYLines);      
        
        text1b.setFont( new Font(fontSize) );
//        text1b.setX(textX + 1*choiceboxInterval + choiceBoxWidth2 + line1offset);
//        text1b.setY(textY + textOffsetYFirstLine + 1*textOffsetYLines);
        text1b.setLayoutX(textX + 1*choiceboxInterval + choiceBoxWidth2 + line1offset);
        text1b.setLayoutY(textY + textOffsetYFirstLine + 1*textOffsetYLines); 
        
        text2b.setFont( new Font(fontSize) );
//        text2b.setX(textX + 1*choiceboxInterval + choiceBoxWidth1 + line2offset);
//        text2b.setY(textY + textOffsetYFirstLine + 2*textOffsetYLines);
        text2b.setLayoutX(textX + 1*choiceboxInterval + choiceBoxWidth1 + line2offset);
        text2b.setLayoutY(textY + textOffsetYFirstLine + 2*textOffsetYLines);

        text3b.setFont( new Font(fontSize) );
//        text3b.setX(textX + 1*choiceboxInterval + choiceBoxWidth2 + line3offset);
//        text3b.setY(textY + textOffsetYFirstLine + 3*textOffsetYLines);
        text3b.setLayoutX(textX + 1*choiceboxInterval + choiceBoxWidth2 + line3offset);
        text3b.setLayoutY(textY + textOffsetYFirstLine + 3*textOffsetYLines);
        
        text4a.setFont( new Font(fontSize) );
        if (language.isEnglish()){
//            text4a.setX(textX + line4offset + 30);
            line4offset = line4offset + 50;
        }
//        text4a.setX(textX + line4offset);
//        text4a.setY(textY + textOffsetYFirstLine + 4*textOffsetYLines );
        text4a.setLayoutX(textX + line4offset);
        text4a.setLayoutY(textY + textOffsetYFirstLine + 4*textOffsetYLines );
        
        text4b.setFont( new Font(fontSize) );
//        text4b.setX(textX + text4a.getBoundsInLocal().getWidth() + 2*choiceboxInterval + choiceBoxWidth3 + line4offset);
//        text4b.setY(textY + textOffsetYFirstLine + 4*textOffsetYLines);
        text4b.setLayoutX(textX + text4a.getBoundsInLocal().getWidth() + 2*choiceboxInterval + choiceBoxWidth3 + line4offset);
        text4b.setLayoutY(textY + textOffsetYFirstLine + 4*textOffsetYLines);
        
        
        text5a.setFont( new Font(fontSize) );
        if (language.isEnglish()){
//            text4a.setX(textX + line4offset + 30);
//            text5a.setX(textX-20);
            text5a.setLayoutX(textX-20);
        }else{
//            text5a.setX(textX);
            text5a.setLayoutX(textX);
        }
        
        text5a.setY(textY + textOffsetYFirstLine + 5*textOffsetYLines);
                
        longSavingsTextGroup.getChildren().addAll(introText, text0a, text1b, text2b, text3b, text4a, text4b, text5a);
        
        cbTimesPerDay.setTranslateX(textX + line1offset);
        cbTimesPerDay.setTranslateY(textY + textOffsetYFirstLine + 1*textOffsetYLines + cbOffsetY);
        cbTimesPerDay.setPrefWidth(choiceBoxWidth2);
        
        cbDayPerWeek.setTranslateX(textX + line2offset);
        cbDayPerWeek.setTranslateY(textY + textOffsetYFirstLine + 2*textOffsetYLines + cbOffsetY);
        cbDayPerWeek.setPrefWidth(choiceBoxWidth1);
        
        cbWeekPerYear.setTranslateX(textX + line3offset);
        cbWeekPerYear.setTranslateY(textY + textOffsetYFirstLine + 3*textOffsetYLines + cbOffsetY);
        cbWeekPerYear.setPrefWidth(choiceBoxWidth2);
        
//        cbTrucksPerCompany.setTranslateX( text4a.getBoundsInLocal().getMaxX() + choiceboxInterval + line4offset);
        cbTrucksPerCompany.setTranslateX( text4a.getBoundsInParent().getMaxX() );
//        cbTrucksPerCompany.setLayoutX( 100 );
        cbTrucksPerCompany.setTranslateY(text4a.getBoundsInParent().getMinY());
//        cbTrucksPerCompany.setLayoutY(100);
        cbTrucksPerCompany.setPrefWidth(choiceBoxWidth2);
//        cbTrucksPerCompany.setPrefWidth(choiceBoxWidth2);
        
                
        finalRect.setHeight( text5a.getBoundsInLocal().getMaxY() + 10 - RectY );
//        System.out.println(" text5a.getBoundsInLocal().getMaxY() + 30 - RectX: " +  (text5a.getBoundsInLocal().getMaxY() + 30 - RectY) );
//        System.out.println("text5a.getBoundsInLocal().getMaxY()");
//        System.out.println(text5a.getBoundsInLocal().getMaxY());
//        System.out.println("RectX");
//        System.out.println(RectX);
//        System.out.println("");
//        System.out.println();
        
        if ( realTimeVisualisationExited == true ){
            return;
        }
        
        finalBox.getChildren().addAll(finalRect,longSavingsTextGroup, cbTimesPerDay, cbDayPerWeek, cbWeekPerYear, cbTrucksPerCompany);
//        finalBox.getChildren().addAll(finalRect,longSavingsTextGroup, cbTrucksPerCompany);
        
        mainRoot.getChildren().add(finalBox);

    }
        
    public void showFinalPolylines(){

        
        
        truckTrail = new Polyline();
        truckTrail.setStrokeLineCap(StrokeLineCap.ROUND);
        truckTrail.setStroke(Color.GREEN);
        truckTrail.setStrokeWidth(8.0);
        truckTrail.setStrokeLineJoin( StrokeLineJoin.ROUND );
        
        platoonTruckTrail = new Polyline();
        platoonTruckTrail.setStrokeLineCap(StrokeLineCap.ROUND);
        platoonTruckTrail.setStroke(Color.BLUE);
        platoonTruckTrail.setStrokeWidth(4.0);
        platoonTruckTrail.setStrokeLineJoin( StrokeLineJoin.ROUND );
        
        convoyTrail = new Polyline();
//        convoyTrail.setStrokeLineCap(StrokeLineCap.ROUND);
        convoyTrail.setStroke(Color.YELLOW);
        convoyTrail.setStrokeDashOffset(10.0);
        convoyTrail.getStrokeDashArray().addAll(2.5d, 15d);
        convoyTrail.setStrokeWidth(4.0);
        convoyTrail.setStrokeLineJoin( StrokeLineJoin.ROUND );
        
        int[] initialTruckPosition = swedenPlatooning.getMyTruckPositionInMap(0);
        int[] prevTruckPosition = initialTruckPosition;
        
        int truckPlatoonId = swedenPlatooning.getPlatoonTruckId();
//        System.out.println("swedenPlatooning.getPlatoonTruckId() = " + swedenPlatooning.getPlatoonTruckId());
        int[] prevPlatoonTruckPosition = swedenPlatooning.getTruckPositionInMap(truckPlatoonId, 0);
        
        double polylineResolution = 200.;
        
        for ( int i = 1 ; i < polylineResolution ; i++ ){
        
            double currentTime = finalSimulationTime *(i/ (polylineResolution -1) );
            
//            System.out.println("currentTime: " + currentTime);
            
            int[] truckPosition = swedenPlatooning.getMyTruckPositionInMap(currentTime);

            if ( ( initialTruckPosition[0] != truckPosition[0] && initialTruckPosition[1] != truckPosition[1] ) || 
                    ( prevTruckPosition[0] != truckPosition[0] && prevTruckPosition[1] != truckPosition[1] )   ){
            
//                System.out.println("Truck Adding: "+ truckPosition[0] +","+ truckPosition[1] );
                truckTrail.getPoints().addAll(new Double[]{ (double) truckPosition[0], (double) truckPosition[1] });
                
                Circle tempCircle = new Circle();
                tempCircle.setLayoutX( truckPosition[0] );
                tempCircle.setLayoutY( truckPosition[1] );
                mainRoot.getChildren().add(tempCircle);
                
                
                if ( currentTime >= platooningStartTime && currentTime <= platooningEndTime ){

    //                System.out.println("Convoy Adding: "+ truckPosition[0] +","+ truckPosition[1] );
                    convoyTrail.getPoints().addAll(new Double[]{ (double) truckPosition[0], (double) truckPosition[1] });

                }
                        
            }

            


            int[] platoonTruckPosition = swedenPlatooning.getTruckPositionInMap(truckPlatoonId, currentTime);

//            System.out.println("Platoon Adding: "+ platoonTruckPosition[0] +","+ platoonTruckPosition[1] );
            
            if ( ( prevPlatoonTruckPosition[0] != platoonTruckPosition[0] && prevPlatoonTruckPosition[1] != platoonTruckPosition[1] ) ){
            
                platoonTruckTrail.getPoints().addAll(new Double[]{ (double) platoonTruckPosition[0], (double) platoonTruckPosition[1] });
        
            }
            
            prevTruckPosition = truckPosition;
            prevPlatoonTruckPosition = platoonTruckPosition;
            
            
        }
        
        
        mainRoot.getChildren().add(truckTrail);
        mainRoot.getChildren().add(platoonTruckTrail);
        mainRoot.getChildren().add(convoyTrail);
        
        platoonTruckImgView.toFront();
        platoonTruckImgViewRight.toFront();
        convoyGroup.toFront();
        convoyGroupRight.toFront();
        truckCartoon.toFront();
        truckCartoonRight.toFront();
        
        
    }
    
    public void setPolyLines(double currentTime){
        
//        System.out.println("running: setPolyLines");
        int[] truckPosition = swedenPlatooning.getMyTruckPositionInMap(currentTime);
        
//        System.out.println("currentTime: "+currentTime);
        
        if ( truckPosition[0] != prevTruckTrail[0] && truckPosition[1] != prevTruckTrail[1] ){
        
//            System.out.println("Adding: "+ truckPosition[0] + ","+ truckPosition[1] );

            truckTrail.getPoints().addAll(new Double[]{ (double) truckPosition[0], (double) truckPosition[1] });
            
            if ( currentTime >= platooningStartTime && currentTime <= platooningEndTime ){
                
                convoyTrail.getPoints().addAll(new Double[]{ (double) truckPosition[0], (double) truckPosition[1] });
            
            }

        }
        
        int truckPlatoonId = swedenPlatooning.getPlatoonTruckId();        
//        System.out.println("swedenPlatooning.getPlatoonTruckId() = " + swedenPlatooning.getPlatoonTruckId());
        
        int[] platoonTruckPosition = swedenPlatooning.getTruckPositionInMap(truckPlatoonId, currentTime);
        
        if ( platoonTruckPosition[0] != prevPlatoonTruckTrail[0] && platoonTruckPosition[1] != prevTruckTrail[1] ){
        
//            System.out.println("Adding: "+ platoonTruckPosition[0] + ","+ platoonTruckPosition[1] );
//
            platoonTruckTrail.getPoints().addAll(new Double[]{ (double) platoonTruckPosition[0], (double) platoonTruckPosition[1] });

        }
        
        

//        System.out.println("running: setPolyLines");
//        truckPosition = swedenPlatooning.getMyTruckPositionInMap(currentTime);
//        platoonTruckTrail.getPoints().addAll(new Double[]{ (double) truckPosition[0], (double) truckPosition[1] });
        prevTruckTrail = truckPosition;
        prevPlatoonTruckTrail = truckPosition;
        
    }
    
    private void setTrail(){
        
//        truckTrail = new Polyline();
        
        class AnimationPolyLineInterpolator extends Interpolator {
            @Override
            protected double curve(double t) {
                
//                System.out.println("running: AnimationPolyLineInterpolator");
//                System.out.println("t: " + t);
                setPolyLines(t*finalSimulationTime);
//                setTrailLines(t*finalSimulationTime);
                return t ;
                
            }
        }
        
        Rectangle dummyRect = new Rectangle();
        dummyRect.setOpacity(0.0);
        
        dummyTranslateTransition = new TranslateTransition(Duration.millis(simulationTime), dummyRect);
        dummyTranslateTransition.setFromX( 0 );
        dummyTranslateTransition.setToX( 1 );
        dummyTranslateTransition.setCycleCount(1);
        dummyTranslateTransition.setAutoReverse(false);
        
        AnimationPolyLineInterpolator dummyInterpolator = new AnimationPolyLineInterpolator();
        
        dummyTranslateTransition.setInterpolator(dummyInterpolator);
        
        PauseTransition pauseTransitionTemp = new PauseTransition( Duration.millis(pauseTransitionTimeBeforeStart) );
        
        dummyTransition = new SequentialTransition(pauseTransitionTemp, dummyTranslateTransition);
        
        
    }
    
    private void updateLongSavingsText(){
        
        int yearSavings = (int)(totSF*timesPerDay*dayPerWeek*weekPerYear*trucksPerCompany);
       
        if (language.isEnglish())
        {

//            text1.setText( timesPerDay + " times per day,\n");
//            text2.setText( dayPerWeek + " days per week,\n");
//            text3.setText( weekPerYear + " weeks per year,\n");
//            text4.setText( "using " + trucksPerCompany + " trucks...\n\n");
            text5a.setText(  "you will save about "+yearSavings+" liters per year!");
            
        }
        else
        {
            

//            text1.setText( timesPerDay + " gånger om dag,\n");
//            text2.setText( dayPerWeek + " dagar i vecka,\n");
//            text3.setText( weekPerYear + " veckor om år,\n");
//            text4.setText( "och använder "+trucksPerCompany+" lastbilar...\n\n");
            text5a.setText( "du ska spara cirka "+yearSavings+" liter om år!");
            
        }
        
        
        
        
        
        
        
        
        
    }
        
    private void showInfoGraphics(){
        
        System.out.println("showInfoGraphics()");
        
        cleanScreen();
        
        restartInactivityAnimation();
        
        double offsetYToShowButtons = -50;
        
        Group toBlur = new Group();
        
        Image img = Globals.stockholmMapImage;
        ImageView imgView = new ImageView(img);
        toBlur.getChildren().add(imgView);
        
        BoxBlur boxBlur = new BoxBlur();
//        boxBlur.setWidth(10);
//        boxBlur.setHeight(3);
        boxBlur.setWidth(10);
        boxBlur.setHeight(10);
        boxBlur.setIterations(3);
        
        toBlur.setEffect(boxBlur);
        
        boolean isCompany = true;   
        
//        companyInfoGraphicGroup = createCompanyInfoGraphic();
//        companyInfoGraphicGroup = createInfoGraphic(isCompany);
        createInfoGraphic(isCompany);
        
//        System.out.println("companyInfoGraphicGroup.getBoundsInLocal().toString() = " + companyInfoGraphicGroup.getBoundsInLocal().toString());
        
        double offsetFromCenterX = screenWidth*(0.4/20.);
        double infoGraphicsOffsetX = screenWidth*(1.6/10.);
        
        companyInfoGraphicGroup.setLayoutX( screenWidth*(5./10.) + offsetFromCenterX + infoGraphicsOffsetX);
//        companyInfoGraphicGroup.setLayoutX( (companyInfoGraphicGroup.getBoundsInLocal().getWidth()*(1./2.)) );
        companyInfoGraphicGroup.setLayoutY(200 + offsetYToShowButtons);
                       
        updateSliders(10, 175000, 95, 80, 3, isCompany);
        updateSavingsVariablesAndText(isCompany);
        companyButtons.setOpacity(1.);
        companyButtons.getChildren().get(0).setOpacity(1.);
        companyButtons.getChildren().get(1).setOpacity(flagNotSelectedOpacity);
        companyButtons.getChildren().get(2).setOpacity(flagNotSelectedOpacity);
                
        isCompany = false;
//        countryInfoGraphicGroup = createCountryInfoGraphic();
//        countryInfoGraphicGroup = createInfoGraphic(isCompany);
        createInfoGraphic(isCompany);
        
//        countryInfoGraphicGroup.setLayoutX( screenWidth/2 + (countryInfoGraphicGroup.getBoundsInLocal().getWidth()*(1./2.)) );
        countryInfoGraphicGroup.setLayoutX( screenWidth*(5./10.) - offsetFromCenterX +
                - countryInfoGraphicGroup.getBoundsInLocal().getWidth() + infoGraphicsOffsetX);
        
        countryInfoGraphicGroup.setLayoutY(200 + offsetYToShowButtons);
        
        
        updateSliders(85000, 25000, 10, 25, 3, isCompany);
        updateSavingsVariablesAndText(isCompany);
        flagButtons.setOpacity(1.);
        flagButtons.getChildren().get(0).setOpacity(1.);
        flagButtons.getChildren().get(1).setOpacity(flagNotSelectedOpacity);
        
        if ( flagButtons.getChildren().size() == 3){
        
            flagButtons.getChildren().get(2).setOpacity(flagNotSelectedOpacity);
        
        }
        
        showFinalStatsMenu(); // Already adds to mainRoot
        
//        finalBox.setLayoutX( - finalBox.getBoundsInLocal().getMinX() + screenWidth/2. - finalBox.getBoundsInLocal().getWidth()/2 );
//        finalBox.setLayoutY( - finalBox.getBoundsInLocal().getMinY() + screenHeight/2. - finalBox.getBoundsInLocal().getHeight()/2 );
//        finalBox.setLayoutX( - 1425 ); MinX()
//        finalBox.setLayoutX( - 1650 ); CenterX()
        
//        double offsetHandX = - 1650 + screenWidth/2 ;
        double offsetHandX = - 1650 + screenWidth*(3./20.) ;
        finalBox.setLayoutX( offsetHandX );
        
//        finalBox.setLayoutY( - finalBox.getBoundsInLocal().getMinY() + screenHeight/2. - finalBox.getBoundsInLocal().getHeight()/2 );
        rects.setLayoutX( offsetHandX );
        labels.setLayoutX( offsetHandX );
        
        double statusBarsYOffset = 40;
        rects.setLayoutY( statusBarsYOffset  + offsetYToShowButtons);
        labels.setLayoutY( statusBarsYOffset  + offsetYToShowButtons);
        
        double longSavingsYOffset = -20;
        finalBox.setLayoutY(longSavingsYOffset + offsetYToShowButtons);
        
        mainRoot.getChildren().add(toBlur);
        mainRoot.getChildren().addAll(countryInfoGraphicGroup, companyInfoGraphicGroup);
            
        mainRoot.getChildren().add(rects);
        mainRoot.getChildren().add(labels);
        
        mainRoot.getChildren().add(buttonsHB);
        
        finalBox.toFront();
        
        int currentStageOfProgram = 5;
        setUpHelpStuff(currentStageOfProgram);
        
//        System.out.println("finalBox.getBoundsInLocal().toString(): "+ finalBox.getBoundsInLocal().toString());
//        return infoGraphGroup;
        
    }
    
    
    private void initializeInactivityAnimation(){
        
        if ( inactivityTimeline != null ){
            
            System.out.println("INITIALIZING ANIMATION WHEN IT ALREADY EXISTS");
            
            inactivityTimeline.setOnFinished(null);
            inactivityTimeline.stop();
            
            
        }
        
        
        //create a timeline for moving the circle
        inactivityTimeline = new Timeline();
        inactivityTimeline.setCycleCount(1);
        inactivityTimeline.setAutoReverse(false);
        
        //create a keyFrame, the keyValue is reached at time 2s
        Duration duration = Duration.millis(inactivityTimeSeconds*1000);
        //one can add a specific action when the keyframe is reached
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                
                System.out.println("-----------Inactivity from RealTimeVisualisation");
                inactivityTimeline.setOnFinished(null);
                inactivityTimeline.stop();
                processSwitchEvent(new SwitchEvent(this, 4));
                // go to Original Menu
                
            }
        };
 
        KeyFrame keyFrame = new KeyFrame(duration, onFinished);
 
        //add the keyframe to the timeline
        inactivityTimeline.getKeyFrames().add(keyFrame);
 
        inactivityTimeline.play();
        
        System.out.println("RealTimeVisualisation: Start Inactivity");
        
    }
    
    private void restartInactivityAnimation(){
        
        inactivityTimeline.jumpTo(Duration.ZERO);
        inactivityTimeline.play();
        
        System.out.println("RealTimeVisualisation: Restart Inactivity");
                
    }
    
    public void stopInactivityAnimation(){
        
        inactivityTimeline.setOnFinished(null);
        inactivityTimeline.stop();
        
        System.out.println("RealTimeVisualisation: Stop Inactivity");
        
    }
    
    private Group createInfoGraphic(boolean isCompany){
        
        System.out.println("createInfoGraphic");
        
        Group tempGroup = new Group();
        
        if (isCompany){
        
            companyInfoGraphicGroup = new Group();
//            tempGroup = companyInfoGraphicGroup;
            
        
        }else{
        
            countryInfoGraphicGroup = new Group();
//            tempGroup = countryInfoGraphicGroup;
            
        }
            
        boolean isCompanySlider = isCompany;
        
        double sliderSeparationY = 125;
        double sliderWidth = 500;
        
        double sliderTextFontSize = 25;
        double titleFontSize = 50;
        double savingsFontSize = 30;
        
        double borderX = 50;
        double borderY = borderX;
        
        
        Text titleText = new Text();
        
        
        String titleTextString;
        

        
        String numberTrucksString;
        String annualMileageString;
        String percentageEquippedString;
        String percentageMilleageString;
        String consumptionReductionString;
        String totalSavingsString;
        String litersString;
        String reductionEmissionsString;
        String tonCO2String;
        
        
        if (language.isEnglish() ){
            
            if (isCompany){
                titleTextString = "Company savings:";
            }else{
                titleTextString = "Regional savings:";
            }
        
            numberTrucksString = "Number of trucks:";
            annualMileageString = "Annual kilometrage:";
            percentageEquippedString = "Percentage equipped cars:";
            percentageMilleageString = "Percentage of distance spent platooning:";
            consumptionReductionString = "Consumption reduction:";
            totalSavingsString = "Total annual savings : ";
            litersString = " liters";
            reductionEmissionsString = "Reduced emissions : ";
            tonCO2String = " ton CO2";
            
        }else{
            
            if (isCompany){
                titleTextString = "Företagens besparingar:";
            }else{
                titleTextString = "Samhällets besparingar:";
            }
        
            numberTrucksString = "Antal lastbilar:";
            annualMileageString = "Årlig körsträcka:";
            percentageEquippedString = "Andel utrustade bilar:";
            percentageMilleageString = "Del av körsträcka i fordonskolonn:";
            consumptionReductionString = "Minskad förbrukning:";
            totalSavingsString = "Total årlig besparing: ";
            litersString = " liters";
            reductionEmissionsString = "Minskade utsläpp: ";
            tonCO2String = " ton CO2";
            
        }
        
        
        titleText.setBoundsType(TextBoundsType.VISUAL);
        titleText.setText( titleTextString );
	titleText.setFont( new Font(titleFontSize) );
        
        double slidersOffsetY = sliderSeparationY*1.00;
        
        int sliderId = 0;
        
        Group numberTrucksSlider, annualMileageSlider, percentagePlatooningTrucksSlider, percentageMileagePlatooningSlider, percentageFuelSavingSlider;
        
        if (!isCompany){
        
//            numberTrucksSlider = createSlider(numberTrucksString, sliderWidth, sliderTextFontSize, 50000, 10000000, sliderId, "", isCompanySlider);
            numberTrucksSlider = createSlider(numberTrucksString, sliderWidth, sliderTextFontSize, 0, 1000000, sliderId, "", isCompanySlider);

            sliderId = 1;
//            annualMileageSlider = createSlider(annualMileageString, sliderWidth, sliderTextFontSize, 25000, 200000, sliderId, "km", isCompanySlider);
            annualMileageSlider = createSlider(annualMileageString, sliderWidth, sliderTextFontSize, 0, 200000, sliderId, "km", isCompanySlider);
            
            sliderId = 2;
//            percentagePlatooningTrucksSlider = createSlider(percentageEquippedString, sliderWidth, sliderTextFontSize, 1, 100, sliderId, "%", isCompanySlider);
            percentagePlatooningTrucksSlider = createSlider(percentageEquippedString, sliderWidth, sliderTextFontSize, 0, 100, sliderId, "%", isCompanySlider);

            
            sliderId = 3;
//            percentageMileagePlatooningSlider = createSlider(percentageMilleageString, sliderWidth, sliderTextFontSize, 1, 100, sliderId, "%", isCompanySlider);
            percentageMileagePlatooningSlider = createSlider(percentageMilleageString, sliderWidth, sliderTextFontSize, 0, 100, sliderId, "%", isCompanySlider);

            
            sliderId = 4;
//            percentageFuelSavingSlider = createSlider(consumptionReductionString, sliderWidth, sliderTextFontSize, 1, 5, sliderId, "%", isCompanySlider);
            percentageFuelSavingSlider = createSlider(consumptionReductionString, sliderWidth, sliderTextFontSize, 0, 6, sliderId, "liter/km", isCompanySlider);

            
        }else{
            
            sliderId = 0;
//            numberTrucksSlider = createSlider("Number of trucks:", sliderWidth, sliderTextFontSize, 1, 100, sliderId, "", isCompanySlider);
//            numberTrucksSlider = createSlider("Number of trucks:", sliderWidth, sliderTextFontSize, 0, 100, sliderId, "", isCompanySlider);
            numberTrucksSlider = createSlider(numberTrucksString, sliderWidth, sliderTextFontSize, 0, 100, sliderId, "", isCompanySlider);

            
            sliderId = 1;
//            annualMileageSlider = createSlider("Annual mileage:", sliderWidth, sliderTextFontSize, 25000, 200000, sliderId, "km", isCompanySlider);
//            annualMileageSlider = createSlider("Annual kilometrage:", sliderWidth, sliderTextFontSize, 0, 200000, sliderId, "km", isCompanySlider);
            annualMileageSlider = createSlider(annualMileageString, sliderWidth, sliderTextFontSize, 0, 200000, sliderId, "km", isCompanySlider);

            
            sliderId = 2;
//            percentagePlatooningTrucksSlider = createSlider("Percentage equipped cars:", sliderWidth, sliderTextFontSize, 1, 100, sliderId, "%", isCompanySlider);
//            percentagePlatooningTrucksSlider = createSlider("Percentage equipped cars:", sliderWidth, sliderTextFontSize, 0, 100, sliderId, "%", isCompanySlider);
            percentagePlatooningTrucksSlider = createSlider(percentageEquippedString, sliderWidth, sliderTextFontSize, 0, 100, sliderId, "%", isCompanySlider);

           
            sliderId = 3;
//            percentageMileagePlatooningSlider = createSlider("Percentage of mileage spent platooning:", sliderWidth, sliderTextFontSize, 1, 100, sliderId, "%", isCompanySlider);
//            percentageMileagePlatooningSlider = createSlider("Percentage of distance spent platooning:", sliderWidth, sliderTextFontSize, 0, 100, sliderId, "%", isCompanySlider);
            percentageMileagePlatooningSlider = createSlider(percentageMilleageString, sliderWidth, sliderTextFontSize, 0, 100, sliderId, "%", isCompanySlider);

            
            sliderId = 4;
//            percentageFuelSavingSlider = createSlider("Consumption reduction:", sliderWidth, sliderTextFontSize, 1, 5, sliderId, "%", isCompanySlider);            
//            percentageFuelSavingSlider = createSlider("Consumption reduction:", sliderWidth, sliderTextFontSize, 0, 6, sliderId, "liter/km", isCompanySlider);
            percentageFuelSavingSlider = createSlider(consumptionReductionString, sliderWidth, sliderTextFontSize, 0, 6, sliderId, "liter/km", isCompanySlider);

            
        
        }
        
        sliderId = 0;
        numberTrucksSlider.setLayoutY(sliderId*sliderSeparationY + slidersOffsetY + titleText.getBoundsInLocal().getHeight() );
        sliderId = 1;
        annualMileageSlider.setLayoutY(sliderId*sliderSeparationY + slidersOffsetY + titleText.getBoundsInLocal().getHeight() );
        sliderId = 2;
        percentagePlatooningTrucksSlider.setLayoutY(sliderId*sliderSeparationY + slidersOffsetY + titleText.getBoundsInLocal().getHeight() );
        sliderId = 3;
        percentageMileagePlatooningSlider.setLayoutY(sliderId*sliderSeparationY + slidersOffsetY + titleText.getBoundsInLocal().getHeight() );
        sliderId = 4;
        percentageFuelSavingSlider.setLayoutY(sliderId*sliderSeparationY + slidersOffsetY + titleText.getBoundsInLocal().getHeight() );
            
        
        Text fuelSavedText = new Text();
        fuelSavedText.setBoundsType(TextBoundsType.VISUAL);
        fuelSavedText.setFont( new Font(savingsFontSize) );
        fuelSavedText.setId("fuel");
        
        Text kronorSavedText = new Text();
        kronorSavedText.setBoundsType(TextBoundsType.VISUAL);
        kronorSavedText.setFont( new Font(savingsFontSize) );
        
        
        if (!isCompany){
            
            fuelSavedText.setText( totalSavingsString + annualCountryFuelSavings + litersString);
            kronorSavedText.setText( reductionEmissionsString + annualCountryEmissionSavings + tonCO2String);
            kronorSavedText.setId("CO2");
        
        }else{
            
            fuelSavedText.setText( totalSavingsString + annualFuelSavings + " liters");
            kronorSavedText.setText( reductionEmissionsString + annualKronorSavings + " SEK");
            kronorSavedText.setId("kronor");
        
        }
        
        fuelSavedText.setLayoutX(  sliderWidth/2 - fuelSavedText.getBoundsInLocal().getWidth()/2  );
        fuelSavedText.setLayoutY( sliderId*sliderSeparationY + 2.0*slidersOffsetY + titleText.getBoundsInLocal().getHeight() );

        kronorSavedText.setLayoutX( sliderWidth/2 - kronorSavedText.getBoundsInLocal().getWidth()/2  );
        kronorSavedText.setLayoutY( sliderId*sliderSeparationY + 2.5*slidersOffsetY + titleText.getBoundsInLocal().getHeight() );
        
//        Total annual savings : 90,000 liters
//        Reduced costs : 1.2 million SEK
                
        Rectangle rect = new Rectangle();
        
        infographicBoxWidth = sliderWidth + 2*borderX;
        
        rect.setWidth( sliderWidth + 2*borderX );
        rect.setHeight(  5.5*sliderSeparationY + titleText.getBoundsInLocal().getHeight() + 2*borderY +
                3.5*fuelSavedText.getBoundsInLocal().getHeight() + 3.5*kronorSavedText.getBoundsInLocal().getHeight() );
//        rect.setHeight(  2*borderY + kronorSavedText.getBoundsInLocal().getMaxY() - titleText.getBoundsInLocal().getMinY() );
        rect.setLayoutX ( -borderX );
        rect.setLayoutY ( -borderY - sliderSeparationY/2. );
        
	titleText.setLayoutX( sliderWidth/2 - titleText.getBoundsInLocal().getWidth()/2 );
	titleText.setLayoutY( -titleText.getBoundsInLocal().getHeight() );
        
        rect.setArcWidth( 50 );
        rect.setArcHeight( 50 );
        rect.setOpacity(rectangleBoxOpacity);
        rect.setFill(Color.WHITE);
        
        Group tempImageView, tempImageView1, tempImageView2;
        
        double extraWidthRatio;
        
        if (!isCompany){
            
            double flagWidth = sliderWidth/4.0;
            
            flagButtons = createFlagButtons(flagWidth);
            flagButtons.setOpacity(flagNotSelectedOpacity);
            tempImageView = (Group) flagButtons.getChildren().get(0);
//            tempImageView.setFitWidth(sliderWidth/6);
            
            tempImageView1 = (Group) flagButtons.getChildren().get(1);

//            tempImageView2 = (ImageView )flagButtons.getChildren().get(2);
            tempImageView2 = null;
            
            extraWidthRatio = 1.0;
            
        }else{
            
            
            
            
            companyButtons = createCompanyButtons();
            companyButtons.setOpacity(flagNotSelectedOpacity);
            tempImageView = (Group) companyButtons.getChildren().get(0);
//            tempImageView.setFitWidth( 1.375 * ( sliderWidth/6 ) );

            tempImageView1 = (Group) companyButtons.getChildren().get(1);

            tempImageView2 = (Group) companyButtons.getChildren().get(2);
            
            double height0 = tempImageView.getBoundsInLocal().getHeight();
            double height1 = tempImageView1.getBoundsInLocal().getHeight();
            double height2 = tempImageView2.getBoundsInLocal().getHeight();
            
            tempImageView.setLayoutY(slidersOffsetY - height0);
            tempImageView1.setLayoutY(slidersOffsetY - height1);
            tempImageView2.setLayoutY(slidersOffsetY - height2);
            
            extraWidthRatio = 1.5;
        }
        
//        tempImageView.setPreserveRatio(true);
//        if (!isCompany){
//            tempImageView.setFitWidth( extraWidthRatio * sliderWidth/6 );
//        }else{
//            // Extra factor because Scania logo is different!
////            tempImageView.setFitWidth( extraWidthRatio * 1.25 *sliderWidth/6 );
//        }
        
        
        
        
        if ( tempImageView2 != null){
        
            tempImageView.setLayoutX( sliderWidth/6 - tempImageView.getBoundsInLocal().getWidth()/2 );
            
            tempImageView1.setLayoutX( sliderWidth/2 - tempImageView1.getBoundsInLocal().getWidth()/2 ); 
//            tempImageView2.setPreserveRatio(true);
//            tempImageView2.setFitWidth( extraWidthRatio * sliderWidth/6 );
            tempImageView2.setLayoutX( 5*sliderWidth/6 - tempImageView2.getBoundsInLocal().getWidth()/2 );

        }else{
            
            tempImageView.setLayoutX( sliderWidth/4. - tempImageView.getBoundsInLocal().getWidth()/2 );
            tempImageView1.setLayoutX( sliderWidth*(3./4.) - tempImageView1.getBoundsInLocal().getWidth()/2 ); 
            
        }
        
        if (isCompany){
                    
            companyInfoGraphicGroup.getChildren().addAll(rect, companyButtons, titleText, kronorSavedText, fuelSavedText, numberTrucksSlider, annualMileageSlider,
                percentagePlatooningTrucksSlider, percentageMileagePlatooningSlider, percentageFuelSavingSlider);
        
        }else{
                    
            countryInfoGraphicGroup.getChildren().addAll(rect, flagButtons, titleText, kronorSavedText, fuelSavedText, numberTrucksSlider, annualMileageSlider,
                percentagePlatooningTrucksSlider, percentageMileagePlatooningSlider, percentageFuelSavingSlider);
            
        }
        
//        tempGroup.getChildren().addAll(rect, flagButtons, titleText, kronorSavedText, fuelSavedText, numberTrucksSlider, annualMileageSlider,
//                percentagePlatooningTrucksSlider, percentageMileagePlatooningSlider, percentageFuelSavingSlider);
       
        return tempGroup;
        
    }
        
    private Group createFlagButtons(double argSliderWidth){
        
        System.out.println("createFlagButtons");
        
        flagButtons = new Group();
        boolean isCompany = false;
        
        Image flagSwedenImg = Globals.flagSwedenImage;
        ImageView flagSwedenImgView = new ImageView(flagSwedenImg);
        
        flagSwedenImgView.setPreserveRatio(true);
        flagSwedenImgView.setFitWidth(argSliderWidth);
        
        Group leftGroup = new Group();
        leftGroup.getChildren().add(flagSwedenImgView);
        
        Image flagGermanyImg = Globals.flagEuropeImage;
        ImageView flagGermanyImgView = new ImageView(flagGermanyImg);
        
        flagGermanyImgView.setPreserveRatio(true);
        flagGermanyImgView.setFitWidth(argSliderWidth);
        
        Group rightGroup = new Group();
        rightGroup.getChildren().add(flagGermanyImgView);
                
        leftGroup.setOnMousePressed((MouseEvent me)->{

            updateSliders(85000, 25000, 10, 25, 3, isCompany);
            updateSavingsVariablesAndText(isCompany);
            
            flagButtons.setOpacity(1.);
            leftGroup.setOpacity(1.);
            rightGroup.setOpacity(flagNotSelectedOpacity);
//            flagUSAImgView.setOpacity(flagNotSelectedOpacity);
            
            
            
            
        });
        
        
        
        rightGroup.setOnMousePressed((MouseEvent me)->{
            
            updateSliders(150000, 175000, 85, 40, 3, isCompany);
            updateSavingsVariablesAndText(isCompany);
            
            flagButtons.setOpacity(1.);
            leftGroup.setOpacity(flagNotSelectedOpacity);
            rightGroup.setOpacity(1.);
//            flagUSAImgView.setOpacity(flagNotSelectedOpacity);
            
            
            
        });
        
        
        
//        flagUSAImgView.setOnMousePressed((MouseEvent me)->{
//            
//            updateSliders(10000000, 125000, 80, 20, 3, isCompany);
//            updateSavingsVariablesAndText(isCompany);
//            
//            flagButtons.setOpacity(1.);
//            flagSwedenImgView.setOpacity(flagNotSelectedOpacity);
//            flagGermanyImgView.setOpacity(flagNotSelectedOpacity);
//            flagUSAImgView.setOpacity(1.);
//            
//        });
        
//        flagButtons.getChildren().addAll(flagSwedenImgView, flagGermanyImgView, flagUSAImgView);
//        flagButtons.getChildren().addAll(flagSwedenImgView, flagGermanyImgView);
        flagButtons.getChildren().addAll(leftGroup, rightGroup);
        
        return flagButtons;
        
    }
    
    private Group createCompanyButtons(){
        
        System.out.println("createCompanyButtons");
        
        companyButtons = new Group();
        boolean isCompany = true;
        
        double fontSize = 20;
        double originalImageWidth = 0;
        double companyImageScaleRatio = 1.5;
        
        Image flagSwedenImg = Globals.smallCompanyImage;
        ImageView flagSwedenImgView = new ImageView(flagSwedenImg);
        
        originalImageWidth = flagSwedenImg.getWidth();
        flagSwedenImgView.setPreserveRatio(true);
        flagSwedenImgView.setFitWidth(companyImageScaleRatio*originalImageWidth);        
        
        Text leftText = new Text();
        if ( language.isEnglish() ){
            leftText.setText("Small");
        }else{
            leftText.setText("Litet");
        }
        leftText.setFont( new Font(fontSize));
        leftText.setBoundsType(TextBoundsType.VISUAL);
        leftText.setLayoutX( flagSwedenImgView.getBoundsInLocal().getWidth()/2. - leftText.getBoundsInLocal().getWidth()/2. );
        leftText.setLayoutY(flagSwedenImgView.getBoundsInLocal().getHeight() + leftText.getBoundsInLocal().getHeight());
        
        Group leftGroup = new Group();
        leftGroup.getChildren().addAll(flagSwedenImgView, leftText);
        
        
        Image flagGermanyImg = Globals.mediumCompanyImage;
        ImageView flagGermanyImgView = new ImageView(flagGermanyImg);

        originalImageWidth = flagGermanyImg.getWidth();
        flagGermanyImgView.setPreserveRatio(true);
        flagGermanyImgView.setFitWidth(companyImageScaleRatio*originalImageWidth);    
        
        Text centerText = new Text();
        if ( language.isEnglish() ){
            centerText.setText("Medium");
        }else{
            centerText.setText("Medelstort");
        }
        centerText.setFont( new Font(fontSize));
        centerText.setBoundsType(TextBoundsType.VISUAL);
                
        centerText.setLayoutX( flagGermanyImgView.getBoundsInLocal().getWidth()/2. - centerText.getBoundsInLocal().getWidth()/2. );
        centerText.setLayoutY(flagGermanyImgView.getBoundsInLocal().getHeight() + centerText.getBoundsInLocal().getHeight());
        
        Group centerGroup = new Group();
        centerGroup.getChildren().addAll(flagGermanyImgView, centerText);
        
        Image flagUSAImg = Globals.bigCompanyImage;
        ImageView flagUSAImgView = new ImageView(flagUSAImg);
        
        originalImageWidth = flagUSAImg.getWidth();
        flagUSAImgView.setPreserveRatio(true);
        flagUSAImgView.setFitWidth(companyImageScaleRatio*originalImageWidth);    
        
        Text rightText = new Text();
        if ( language.isEnglish() ){
            rightText.setText("Large");
        }else{
            rightText.setText("Stort");
        }
        rightText.setFont( new Font(fontSize));
        rightText.setBoundsType(TextBoundsType.VISUAL);
        
        rightText.setLayoutX( flagUSAImgView.getBoundsInLocal().getWidth()/2. - rightText.getBoundsInLocal().getWidth()/2. );
        rightText.setLayoutY( flagUSAImgView.getBoundsInLocal().getHeight() + rightText.getBoundsInLocal().getHeight());
        
        Group rightGroup = new Group();
        rightGroup.getChildren().addAll(flagUSAImgView, rightText);
        
        
        leftGroup.setOnMousePressed((MouseEvent me)->{

            updateSliders(10, 175000, 95, 80, 3, isCompany);
            updateSavingsVariablesAndText(isCompany);

            companyButtons.setOpacity(1.);
            leftGroup.setOpacity(1.);
            centerGroup.setOpacity(flagNotSelectedOpacity);
            rightGroup.setOpacity(flagNotSelectedOpacity);
            
        });
        
        centerGroup.setOnMousePressed((MouseEvent me)->{
            
            updateSliders(40, 175000, 95, 80, 3, isCompany);
            updateSavingsVariablesAndText(isCompany);
            
            companyButtons.setOpacity(1.);
            leftGroup.setOpacity(flagNotSelectedOpacity);
            centerGroup.setOpacity(1.);
            rightGroup.setOpacity(flagNotSelectedOpacity);
            
            
            
        });
        
        rightGroup.setOnMousePressed((MouseEvent me)->{
            
            updateSliders(100, 175000, 95, 80, 3, isCompany);
            updateSavingsVariablesAndText(isCompany);
            
            companyButtons.setOpacity(1.);
            leftGroup.setOpacity(flagNotSelectedOpacity);
            centerGroup.setOpacity(flagNotSelectedOpacity);
            rightGroup.setOpacity(1.);
            

        });
        
//        companyButtons.getChildren().addAll(flagSwedenImgView, flagGermanyImgView, flagUSAImgView);
        companyButtons.getChildren().addAll(leftGroup, centerGroup, rightGroup);
        
        return companyButtons;
        
    }
    
    private void updateSavingsVariablesAndText(boolean isCompany){
        
        restartInactivityAnimation();
        
//        System.out.println("updateSavingsVariablesAndText");
        
        Group tempGroup;
        
        if (isCompany){
            
            tempGroup = companyInfoGraphicGroup;
            
        }else{
            
            tempGroup = countryInfoGraphicGroup;                        
            
        }
        
        Slider slider0 = (Slider) tempGroup.lookup("#slider0");
        
        double numTrucks = slider0.getValue();
        
        Slider slider1 = (Slider) tempGroup.lookup("#slider1");
        
        double annualMileage = slider1.getValue();
        
        Slider slider2 = (Slider) tempGroup.lookup("#slider2");
        
        double percentageEquipped = slider2.getValue();
        
        Slider slider3 = (Slider) tempGroup.lookup("#slider3");
        
        double percentageMileage = slider3.getValue();
        
        Slider slider4 = (Slider) tempGroup.lookup("#slider4");
        
        double consumptionReduction = slider4.getValue();
        
        if (isCompany){
            
            annualFuelSavings = ( (percentageEquipped/100.) * numTrucks ) * ( (percentageMileage/100.) * annualMileage ) * ( consumptionReduction / 100. );
            annualKronorSavings = 13.3333333333 * annualFuelSavings;
            updateCompanySavingsText();
            
            
        }else{
            
            annualCountryFuelSavings = ( (percentageEquipped/100.) * numTrucks ) * ( (percentageMileage/100.)  * annualMileage ) * (consumptionReduction / 100. );
            annualCountryEmissionSavings = annualCountryFuelSavings / 333.33;   
            updateCountrySavingsText();
            
        }
        
        
        
    }
    
    private void updateCompanySavingsText(){
        
        restartInactivityAnimation();
        
//        System.out.println("updateCompanySavingsText");
        
//        System.out.println("\nupdateCompanySavingsText\n");
        
        double manualCenterXOffset = -50;
        
        double middleGroup = ( companyInfoGraphicGroup.getBoundsInParent().getMaxX() + companyInfoGraphicGroup.getBoundsInParent().getMinX() )/2.;
        
        
        
        Text fuelSavedText = (Text) companyInfoGraphicGroup.lookup("#fuel");
        
//        double prevWidth = fuelSavedText.getBoundsInLocal().getWidth();
        
//        fuelSavedText.setText( "Total annual savings : " + (long)annualFuelSavings + " liters");
        if ( language.isEnglish() ){
        
            fuelSavedText.setText( "Total annual savings: " + transformNumberToString(annualFuelSavings) + " liters");

        }else{
            
            fuelSavedText.setText( "Total årlig besparing: " + transformNumberToString(annualFuelSavings) + " liters");
            
        }
//        double newWidth = fuelSavedText.getBoundsInLocal().getWidth();
        
//        fuelSavedText.setLayoutX( (prevWidth-newWidth)/2. );
        
//        System.out.println("middleGroup -  fuelSavedText.getBoundsInLocal().getWidth()/2.: "+ (middleGroup -  fuelSavedText.getBoundsInLocal().getWidth()/2.) );
        
        fuelSavedText.setLayoutX( middleGroup - companyInfoGraphicGroup.getBoundsInParent().getMinX()  -  fuelSavedText.getBoundsInLocal().getWidth()/2. + manualCenterXOffset );
        
//        fuelSavedText.setTranslateX( 10 );
        
        Text kronorSavedText = (Text) companyInfoGraphicGroup.lookup("#kronor");
        
//        kronorSavedText.setText( "Reduced costs : " + (long)annualKronorSavings + " SEK");
        
        if ( language.isEnglish() ){
        
            kronorSavedText.setText( "Reduced costs: " + transformNumberToString(annualKronorSavings) + " SEK");
            
        }else{
            
            kronorSavedText.setText( "Minskade kostnader: " + transformNumberToString(annualKronorSavings) + " SEK");
            
        }
        
//        String transformNumberToString(double number)
        kronorSavedText.setBoundsType(TextBoundsType.VISUAL);
        
//        System.out.println("middleGroup = " + middleGroup);
//        System.out.println("kronorSavedText.getBoundsInLocal() = " + kronorSavedText.getBoundsInLocal());
//        
//        System.out.println("middleGroup - kronorSavedText.getBoundsInLocal().getWidth()/2. = " + (middleGroup - kronorSavedText.getBoundsInLocal().getWidth()/2.) );
        
        kronorSavedText.setLayoutX( companyInfoGraphicGroup.getBoundsInLocal().getMinX() + infographicBoxWidth/2 - kronorSavedText.getBoundsInLocal().getWidth()/2. );
        
    }
            
    private String transformNumberToString(double number){
        
//        System.out.println("number = " + number);
        
        DecimalFormat df = new DecimalFormat("#000");
        
        String formatedString = new String();
        
        double billions = 0, millions = 0, thousands = 0;
        
        if (number >= 1000000000){
            
//            String firstString = number/1000000.;
            billions = Math.floor(number/1000000000.);
//            System.out.println("billions = " + billions);
            
            if ( billions >= 100 ){
                df = new DecimalFormat("#000");
            }else if(billions >= 10){
                df = new DecimalFormat("#00");
            }else{
                df = new DecimalFormat("#0");
            }
            
            String billionsString = df.format(billions);
            
            formatedString = formatedString + billionsString;
            
        }
        
        if (number >= 1000000){
            
//            String firstString = number/1000000.;
            millions = Math.floor(( number - billions*1000000000 )/1000000.);
            
//            System.out.println("millions = " + millions);
            
            if ( number >= 1000000000 ){
                
                df = new DecimalFormat("#000");
                
            }else{
            
                if ( millions >= 100 ){
                    df = new DecimalFormat("#000");
                }else if(millions >= 10){
                    df = new DecimalFormat("#00");
                }else{
                    df = new DecimalFormat("#0");
                }

            }
            
            String millionsString = df.format(millions);
            
            formatedString = formatedString + " " + millionsString;
            
        }
        
        if (number >= 1000){
            
//            String firstString = number/1000000.;
            thousands = Math.floor(( number - billions*1000000000 - millions*1000000 )/1000.);
            
//            System.out.println("thousands = " + thousands);
            
            if ( number >= 1000000 ){
                
                df = new DecimalFormat("#000");
                
            }else{
            
                if ( thousands >= 100 ){
                    df = new DecimalFormat("#000");
                }else if(thousands >= 10){
                    df = new DecimalFormat("#00");
                }else{
                    df = new DecimalFormat("#0");
                }

            }
            
            String thousandsString = df.format(thousands);
            
            formatedString = formatedString + " " + thousandsString;
            
        }
        
//              String firstString = number/1000000.;
        double units = Math.floor(( number - billions*1000000000 - millions*1000000 - thousands*1000 ));
//        System.out.println("units = " + units);
        df = new DecimalFormat("#000");

        String unitsString = df.format(units);

        formatedString = formatedString + " " + unitsString;
        
        return formatedString;
        
    }
    
    private void updateCountrySavingsText(){
        
        System.out.println("updateCountrySavingsText");
        
//        System.out.println("\nupdateCountrySavingsText\n");
        
        double manualCenterXOffset = -50;
        
        double middleGroup = ( countryInfoGraphicGroup.getBoundsInParent().getMaxX() + countryInfoGraphicGroup.getBoundsInParent().getMinX() )/2.;
        
        Text fuelSavedText = (Text) countryInfoGraphicGroup.lookup("#fuel");
//        fuelSavedText.setText( "Total annual savings : " + (long)annualCountryFuelSavings + " liters");
        fuelSavedText.setText( "Total annual savings : " + transformNumberToString(annualCountryFuelSavings) + " liters");
        
        
        fuelSavedText.setLayoutX( middleGroup - countryInfoGraphicGroup.getBoundsInParent().getMinX()  -  fuelSavedText.getBoundsInLocal().getWidth()/2. + manualCenterXOffset );
        
        Text emmissionsSavedText = (Text) countryInfoGraphicGroup.lookup("#CO2");
//        emmissionsSavedText.setText( "Reduced emissions : " + (long)annualCountryEmissionSavings + " ton CO2");
        emmissionsSavedText.setText( "Reduced emissions : " + transformNumberToString(annualCountryEmissionSavings) + " ton CO2");
        
        
        emmissionsSavedText.setLayoutX( middleGroup - countryInfoGraphicGroup.getBoundsInParent().getMinX()  -  emmissionsSavedText.getBoundsInLocal().getWidth()/2. + manualCenterXOffset );
        
    }
    
    private void updateSliders(int numTrucks, int annualMileage, int percentageEquipped, int percentageMileage, int consumptionReduction, boolean isCompany){
        
        System.out.println("updateSliders");
        
        Group tempGroup;
        
        if (isCompany){
            
            tempGroup = companyInfoGraphicGroup;
            
        }else{
            
            tempGroup = countryInfoGraphicGroup;            
            
        }
        
        Slider slider0 = (Slider) tempGroup.lookup("#slider0");
        
        slider0.setValue(numTrucks);
        
        Slider slider1 = (Slider) tempGroup.lookup("#slider1");
        
        slider1.setValue(annualMileage);
        
        Slider slider2 = (Slider) tempGroup.lookup("#slider2");
        
        slider2.setValue(percentageEquipped);
        
        Slider slider3 = (Slider) tempGroup.lookup("#slider3");
        
        slider3.setValue(percentageMileage);
        
        Slider slider4 = (Slider) tempGroup.lookup("#slider4");
        
        slider4.setValue(consumptionReduction);
        
    }
    
    private Group createSlider(String argTextString, double sliderWidth, double fontSize, double minValue, double maxValue, int sliderId, String extraText, boolean isCompanySlider){

	Group sliderGroup = new Group();
	
//	double minValue = 1;
//	double maxValue = 100;
//	double tempValue = 5;
        double tempValue = .5 * ( maxValue - minValue );

	Slider slider = new Slider(minValue, maxValue, tempValue);
	slider.setPrefWidth(sliderWidth);
        slider.setId("slider"+sliderId);
        
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit( (int) ( (maxValue-minValue)/2) );
        slider.setBlockIncrement( (int) ( (maxValue-minValue)/2) );
        slider.setSnapToTicks(true);
        
//        slider.setShowTickMarks(true);
//        slider.setShowTickLabels(true);
//        slider.setMajorTickUnit(0.25f);
//        slider.setBlockIncrement(0.1f);
//        
//        slider.setMinHeight(sliderHeight);
//        slider.setPrefHeight(sliderHeight);
//        slider.setMaxHeight(sliderHeight);
//        
//        slider.
        
//        System.out.println("slider.getBoundsInLocal().toString() = " + slider.getBoundsInLocal().toString());
//        System.out.println("slider.getBoundsInParent().toString() = " + slider.getBoundsInParent().toString());
        
//	double sliderWidth = slider.getBoundsInLocal().getWidth();
//	double sliderHeight = slider.getBoundsInLocal().getHeight
        double sliderHeight = 25;
	
	Text sliderText = new Text();
	
	sliderText.setText( argTextString + " "+ ( (int) maxValue ) + " " + extraText);
	sliderText.setFont( new Font(fontSize) );
	
        sliderText.setBoundsType(TextBoundsType.VISUAL);
        
        
        	
	double sliderTextWidth = sliderText.getBoundsInLocal().getWidth();
	double sliderTextHeight = sliderText.getBoundsInLocal().getHeight();
	
//        System.out.println("sliderText.getBoundsInLocal().toString() = " + sliderText.getBoundsInLocal().toString());
        
	sliderText.setLayoutX( sliderWidth/2. - sliderTextWidth/2. );
	sliderText.setLayoutY( - 1.0*sliderTextHeight );
	
	// Handle Slider value change events.
	slider.valueProperty().addListener((observable, oldValue, newValue) -> {
	
//		System.out.println("Slider Value Changed (newValue: " + newValue.intValue() + ")");
            int changingVariable = newValue.intValue();
            
            
            
//            updateSliderValues( sliderId, newValue.intValue() );
            sliderText.setText( argTextString + " " + changingVariable + " " + extraText);
            
            if(isCompanySlider){
            
                companyButtons.setOpacity(flagNotSelectedOpacity);
            
            }else{
                
                flagButtons.setOpacity(flagNotSelectedOpacity);
                
            }
            
            updateSavingsVariablesAndText(isCompanySlider);
            if (isCompanySlider){
//                System.out.println("Changing COMPANY values");
            }else{
//                System.out.println("Changing COUNTRY values");
            }
//            System.out.println("Changing variable with id: "+sliderId);
		
	});
        
        sliderGroup.getChildren().add(slider);
        sliderGroup.getChildren().add(sliderText);

	return sliderGroup;

    }
       
    
}
