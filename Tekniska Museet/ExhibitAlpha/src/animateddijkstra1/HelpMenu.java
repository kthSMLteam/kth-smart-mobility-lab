/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animateddijkstra1;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.util.Duration;

/**
 *
 * @author rui
 */
public class HelpMenu {
    
    double screenWidth;
    double screenHeight;
    
    double helpMenuGroupOpacity;
    Group helpMenuGroup;
    Timeline inactivityTimeline;
    int stageOfProgram;
    double fontSize;
    public String infoTextString0, infoTextString1, infoTextString2, infoTextString3, infoTextString4, infoTextString5; 
    
    HelpMenu(Timeline argInactivityTimeline, int argStageOfProgram, boolean isSwedish){
        
        screenWidth = Globals.screenWidth;
        screenHeight = Globals.screenHeight;
        helpMenuGroupOpacity = 0.9;
        fontSize = 50;
        helpMenuGroup = new Group();
        inactivityTimeline = argInactivityTimeline;
        stageOfProgram = argStageOfProgram;
        

        if (isSwedish){
        
            infoTextString0 = Globals.initialHelpSV;     
            infoTextString1 = "En sökalgoritm hittar den kortaste vägen mellan start och mål.\n\n"
                    + "Du får nu se hur sökningen går till.";
            infoTextString2 = "När kortaste vägen hittats söker vi efter andra fordon som kör samma väg.\n\n"
                    + "Detta gör vi för att hitta fordon att köra i kolonn med.";
            infoTextString3 = "Flera möjligheter att bilda fordonskolonner hittades. Varje alternativ motsvarar en lastbil som vi kan ansluta till.\n\n"
                    + "Vissa alternativ är bättre än andra.";
            infoTextString4 = "Här får du se en uppsnabbad simulering av trafiken i staden, tillsammans med ditt eget fordon och det fordon du kommer köra i kolonn med.\n\n"
                    + "På slutet får du se hur mycket bränsle du sparade genom att köra i kolonn.";
            infoTextString5 = "Vinsterna med kolonnkörning blir tydligare om vi tittar på vad man kan spara på ett helt år.\n\n"
                    + "Du kan också se hur mycket kolonnkörning påverkar miljön samt åkeriföretagens besparingar. Justera parametrarna för att se effekten.";
            
        }else{
            
            infoTextString0 = Globals.initialHelpEN;
            infoTextString1 = "Once the origin and destination locations are defined the shortest path between them must be found.\n\n"
                    + "In this section you will see this shortest path being found.";
            infoTextString2 = "Now that we know our path, we will compare it with paths of other trucks driving in the city.\n\n"
                    + "We do this to find potential trucks that we can share a path with.";
            infoTextString3 = "Several platooning opportunities were found. These correspond to possible trucks that we can share a path with.\n\n"
                    + "Some of these opportunities might be better than others, so you should choose wisely.";
            infoTextString4 = "Here you will see a speeded up simulation of the traffic in the city, including our truck and the truck we will platoon with. \n\n"
                    + "At the end you will see how much fuel you saved, compared to the case when you do not platoon.";
            infoTextString5 = "The benefits of platooning are more evident in the long time, so here we show how much you can save on a yearly basis.\n\n"
                    + "You can also check how much the platooning can help sparing money and the environment, by checking company/country statistics.";
            
        }
        
    }
    
    public ImageView createHelpButton(){
        
        Image helpImage = Globals.helpButtonImage;
        ImageView helpImageView = new ImageView(helpImage);
        
        helpImageView.setOnMousePressed((MouseEvent me) -> {
            
            inactivityTimeline.jumpTo(Duration.ZERO);
            setHelpMenuVisible(true);
            
        });
        
        helpImageView.setPreserveRatio(true);
        helpImageView.setFitWidth(50);
        
        helpImageView.setLayoutX( helpImageView.getBoundsInLocal().getWidth()/2. );
        helpImageView.setLayoutY( screenHeight - helpImageView.getBoundsInLocal().getHeight()*(3./2.) );
        
//        RotateTransition rt = new RotateTransition(Duration.millis(7500), helpImageView);
        RotateTransition rt = new RotateTransition(Duration.millis(4000), helpImageView);
        Point3D rotationAxis = new Point3D(0.,1.,0.);
        rt.setAxis(rotationAxis);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setByAngle(10*180);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.setAutoReverse(false);
//        rt.play();
        
        ScaleTransition st = new ScaleTransition(Duration.millis(2000), helpImageView);
        st.setByX(0.5f);
        st.setByY(0.5f);
        st.setCycleCount(Animation.INDEFINITE);
        st.setAutoReverse(true);
        
        
//        ParallelTransition parallelTransition = new ParallelTransition();
//        
//        parallelTransition.getChildren().addAll(rt, parallelTransition);
        ParallelTransition pt = new ParallelTransition(rt, st);
        pt.play();
        
        return helpImageView;
//        helpImageView.toFront();
        
        
    }
        
    public Group createHelpMenu(){
        
        
        Rectangle ghostBox = new Rectangle(screenWidth, screenHeight);
        ghostBox.setOpacity(0.3);        
        
        
        Rectangle infoBox = new Rectangle();
        
        double rectangleWidth = screenWidth*0.8;
        double rectangleHeight = screenHeight*0.6;
        double strokeWidth = 10;
        double arcSize = 150;
        double wrappingWidth = 0.8*rectangleWidth;
        
        infoBox.setWidth(rectangleWidth);
        infoBox.setHeight(rectangleHeight);
        infoBox.setFill(Color.BLANCHEDALMOND);
        infoBox.setStrokeWidth(strokeWidth);
        infoBox.setStroke(Color.BLACK);
        infoBox.setOpacity(helpMenuGroupOpacity);
        infoBox.setArcWidth(arcSize);
        infoBox.setArcHeight(arcSize);
        infoBox.setId("infoBox");
        
        
        Text infoText = new Text();
        
        String infoTextString;
                
        switch (stageOfProgram) {
            case 0:  infoTextString = infoTextString0;
                     rectangleWidth= 1.05*rectangleWidth;
                     rectangleHeight = 1.2*rectangleHeight;
                     wrappingWidth = 1.15*wrappingWidth;
                     break;
            case 1:  infoTextString = infoTextString1;
                     break;
            case 2:  infoTextString = infoTextString2;
                     break;
            case 3:  infoTextString = infoTextString3;
                     break;
            case 4:  infoTextString = infoTextString4;
                 break;
            case 5:  infoTextString = infoTextString5;
                 break;
            default: infoTextString = "INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID INVALID ";
                     break;
        }
        
        infoBox.setWidth(rectangleWidth);
        infoBox.setHeight(rectangleHeight);
        infoBox.setLayoutX( screenWidth/2. - rectangleWidth/2. );
        infoBox.setLayoutY( screenHeight/2. - rectangleHeight/2. );
        
        
        infoText.setText(infoTextString);
        infoText.setWrappingWidth(wrappingWidth);
        infoText.setBoundsType(TextBoundsType.VISUAL);
        infoText.setFont(new Font(fontSize));
        infoText.setId("text");
        
        double boxCenterX = infoBox.getBoundsInParent().getMinX() + infoBox.getBoundsInParent().getWidth()/2.;
        double boxCenterY = infoBox.getBoundsInParent().getMinY() + infoBox.getBoundsInParent().getHeight()/2.;
        
        infoText.setLayoutX( boxCenterX - infoText.getBoundsInLocal().getWidth()/2. );
        infoText.setLayoutY( boxCenterY - infoText.getBoundsInLocal().getHeight()/2. + 0.5*infoText.getBaselineOffset() );
        
        helpMenuGroup.getChildren().addAll(ghostBox, infoBox, infoText);
        
        helpMenuGroup.setOnMousePressed((MouseEvent me) -> {
            
            setHelpMenuVisible(false);
            inactivityTimeline.jumpTo(Duration.ZERO);
            
        });
        
        return helpMenuGroup;        
        
    }
        
    public void setHelpMenuVisible(boolean visibility){
        
        if ( visibility ){
            helpMenuGroup.setOpacity(1.);
            helpMenuGroup.toFront();
        }else{
            helpMenuGroup.setOpacity(0.);
            helpMenuGroup.toBack();
        }
        
    }
    
}
