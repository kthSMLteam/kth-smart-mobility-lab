/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animateddijkstra1;

import java.util.Arrays;

/**
 *
 * @author Matteo
 */
public class Languages {
    
    final String[] languages = {"svenska","english"};
    String currLang;
    
    public Languages()
    {
        currLang = "svenska";
    }
    
    public Languages(String argLang)
    {
        if (Arrays.asList(languages).contains(argLang))
            currLang = argLang;
        else
            throw new IllegalArgumentException("language not supported");
    }
    
    public void setEnglish()
    {
        currLang = "english";
    }
    
    public void setSwedish()
    {
        currLang = "svenska";
    }
    
    public String getLang()
    {
        currLang = "english";
        return currLang;
//        return currLang;
    }
    
    public boolean isSwedish()
    {
        
        boolean isSwedish;
        
        if ( currLang.equals(languages[0]) ){
            isSwedish = true;
        }else{
        
            isSwedish = false;
        }

        return isSwedish;
        
    }
    
    public boolean isEnglish()
    {
        boolean isEnglish;
        
        if ( currLang.equals(languages[1]) ){
            isEnglish = true;
        }else{
        
            isEnglish = false;
        }

        return isEnglish;
        
    }
}
