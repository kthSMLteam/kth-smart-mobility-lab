# -*- coding: utf-8 -*-
from math import sqrt, atan2, cos, sin
from numpy import clip
from serial import Serial
from time import sleep

import numpy as np

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG)

###########################
# serial communication to nexus
def mk_expectation_exception(wanted, got):
    return Exception(
        "Expected {0} but got {1}".format(
            str(wanted),
            str(got)))

class Wlink(object):
    def __init__(self, port):
        self.serial = Serial(port, 9600, timeout=5)
        log.info("Giving serial port 2 seconds to wake up")
        sleep(2)
        log.info("Serial ready")
        log.info("Reading motor set points")
        l, r = self.read_state()
        log.info("motor set to {0:d} {1:d}".format(l, r))
        log.info("Robot ready")

    def __del__(self):
        self.serial.close()
        print 'Serial port closed'

    def _writemsg(self, msg):
        b = msg.encode("ascii")
        log.info("sending {0}".format(str(b)))
        self.serial.write(b)

    def _readmsg(self):
        while True:
            b = b""
            while b != b":":
                b = self.serial.read()
            b = self.serial.read()
            msg = b""
            while b != b";":
                msg += b
                b = self.serial.read()
            log.info("read {0}".format(str(msg)))
            if msg.startswith(b"!"):
                log.debug("robot says: {0!s}".format(msg))
            else:
                break
        return msg

    def read_state(self):
        self._writemsg(":state?;")
        #sleep(1) ##########################
        resp = self._readmsg()
        if not resp.startswith(b"state"):
            self.control(0,0)
            raise mk_expectation_exception(b"state", resp)
        _, left, right = resp.decode("ascii").split(" ")
        return int(left), int(right)

    def buffer_input(self):
        self.serial.read

    def close_connection(self):
        self.serial.close()


    #=================================================
    #manual control mode
    
    def manual_forward(self, power):
        assert power >= -999 and power <= 999
        self.control(power, power)

    def manual_backward(self, power):
        assert power >= -999 and power <= 999
        self.control(-power, -power)

    def manual_rotateR(self, power):
        assert power >= -999 and power <= 999
        self.control(power, -power)

    def manual_rotateL(self, power):
        assert power >= -999 and power <= 999
        self.control(-power, power)

    def control(self, uleft, uright):
        assert uleft >= -999 and uleft <= 999
        assert uright >= -999 and uright <= 999
        # NOTE: a constant base voltage may be needed. 
        uleft = int(uleft)
        uright = int(uright)
        msg = ":manual {0:=+04d} {1:=+04d};".format(uleft, uright)
        print msg
        self._writemsg(msg)

    #===============================================
    #send state
    
    def transmit_state(self, x, y, theta):
        # transform x, y, theta to int
        # assume x,y are in meters, send them in cm
        x = int(x*100)
        y = int(y*100)
        # assume theta is given in degree
        theta = int(theta)
        msg = ":pose {0:=+04d} {1:=+04d} {2:=+04d};".format(x, y, theta)
        print msg
        self._writemsg(msg)

    #===============================================
    #send start and goal point
    
    def transmit_startgoal(self, x0, y0, xg, yg):
        # transform x0, y0, xg, yg, to int
        # assume x,y are in meters, send them in cm
        x0 = int(x0*100)
        y0 = int(y0*100)
        xg = int(xg*100)
        yg = int(yg*100)
        msg = ":startgoal {0:=+04d} {1:=+04d} {2:=+04d} {3:=+04d};".format(x0, y0, xg, yg)
        print msg
        self._writemsg(msg)
