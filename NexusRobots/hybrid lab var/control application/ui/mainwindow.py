# -*- coding: utf-8 -*-

"""
Module implementing MainWindow.
"""

from PyQt4.QtGui import QMainWindow
from PyQt4.QtGui import QFileDialog
from PyQt4.QtCore import pyqtSignature
from PyQt4.QtCore import QTimer
import math

from Ui_mainwindow import Ui_MainWindow

import corridor_projection, mocap, wlink, time

class MainWindow(QMainWindow, Ui_MainWindow):
    """
    Class documentation goes here.
    """
    def __init__(self, parent = None):
        """
        Constructor
        """
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
        
        self.controllooptimer = QTimer()
        self.controllooptimer.timeout.connect(self.controlupdate)
        self.mocaplooptimer = QTimer()
        self.mocaplooptimer.timeout.connect(self.mocapupdate)
        
        self.animation = corridor_projection.corridor_animation()
        
        # conversion factor from mocap measurements to screen pixels
        self.xoffset = -1.1
        self.yoffset = 2.4
        self.rotoffset = 0
        self.xscale = 164.214847759152
        self.yscale = 165.860400829302
        self.rotscale = math.pi/180
        
        self.mocaptime = 0
        self.pose = (0, 0, 0)
        
        nodes = self.animation.get_node_index()
        nodes = [str(node) for node in nodes]
        self.startnodecombobox.addItems(nodes)
        self.goalnodecombobox.addItems(nodes)
    
    @pyqtSignature("bool")
    def on_enablecontrolbutton_clicked(self, checked):
        """
        Slot documentation goes here.
        """
        
        if checked:
            try:
                self.wlinkconnection = wlink.Wlink(str(self.portlineedit.text()))
                self.controllooptimer.start(int(1./self.controlspinbox.value()*1000.))
                self.last_controlupdate = time.time()
            except:
                self.enablecontrolbutton.setChecked(False)
                self.logbrowser.append('Could not connect to Robot! Wrong serial port?')
                return
        else:
            self.controllooptimer.stop()
            self.autocontrolbutton.setChecked(False)
            self.wlinkconnection = None
            
        self.autocontrolbutton.setEnabled(checked)
        self.forwardbutton.setEnabled(checked)
        self.backwardbutton.setEnabled(checked)
        self.rightbutton.setEnabled(checked)
        self.leftbutton.setEnabled(checked)
        
    def controlupdate(self):
        current_time = time.time()
        control_frequency = 1./(current_time-self.last_controlupdate)
        self.controlflabel.setText('{0:.2f} Hz'.format(control_frequency))
        self.last_controlupdate = current_time
        controlpowerstraight = self.controlpowsspinbox.value()
        controlpowerrot = self.controlpowrspinbox.value()
        if self.forwardbutton.isDown(): # send control values
            self.wlinkconnection.manual_forward(controlpowerstraight)
            return
        elif self.backwardbutton.isDown():
            self.wlinkconnection.manual_backward(controlpowerstraight)
            return
        elif self.rightbutton.isDown():
            self.wlinkconnection.manual_rotateR(controlpowerrot)
            return
        elif self.leftbutton.isDown():
            self.wlinkconnection.manual_rotateL(controlpowerrot)
            return
        elif self.autocontrolbutton.isChecked():
            self.wlinkconnection.transmit_state(*self.pose)
            return
        else:
            self.wlinkconnection.manual_forward(0.)
#            send control update
        
    def mocapupdate(self):
        current_time = time.time()
        mocap_frequency = 1./(current_time-self.last_mocapupdate)
        self.mocapflabel.setText('{0:.2f} Hz'.format(mocap_frequency))
        self.last_mocapupdate = current_time
        (mocaptime, pose) = self.mocapconnection.execute()
        if pose[0] == pose[0]:
            self.mocaptime = mocaptime
            self.pose = pose
            self.xposlabel.setText('{0:.3f}'.format(self.pose[0]))
            self.yposlabel.setText('{0:.3f}'.format(self.pose[1]))
            self.rotposlabel.setText('{0:.3f}'.format(self.pose[2]))
            (xs, ys, rots) = self.get_mapposition(*self.pose)
            self.animation.draw_robot(xs, ys, rots)
            if self.logbutton.isChecked():
                self.logfile.write('%f; %.2f; %.2f; %.2f' %(self.mocaptime, self.pose[0],  self.pose[1], self.pose[2]) + '\n')
                # update display
                if self.autocontrolbutton.isChecked():
                    # send state to robot
                    print 'send current state'
        return
    
    @pyqtSignature("bool")
    def on_enablemocapbutton_clicked(self, checked):
        """
        Slot documentation goes here.
        """
        
        if checked:
            try:
                self.mocapconnection = mocap.Mocap(self.bodynumberspinbox.value())
                self.mocaplooptimer.start(int(1./self.mocapspinbox.value()*1000.))
                self.last_mocapupdate = time.time()
            except:
                self.enablemocapbutton.setChecked(False)
                self.logbrowser.append('Could not connect to Mocap!')
                return
        else:
            self.mocapconnection.close()
            self.mocaplooptimer.stop()
         
        self.logbutton.setEnabled(checked)
        self.usecurposbutton.setEnabled(checked)
    
    @pyqtSignature("")
    def on_loggingbutton_clicked(self):
        """
        Slot documentation goes here.
        """
       
        logfilename = QFileDialog.getSaveFileName(self, 'Open logfile', self.logfilelineedit.text())
        if logfilename != '':
            self.logfilelineedit.setText(logfilename)
    
    @pyqtSignature("bool")
    def on_autocontrolbutton_clicked(self, checked):
        """
        Slot documentation goes here.
        """
        
#        if checked:
#            command = self.commandlineedit.text()
#            print command
#            self.logbrowser.append(command)
#            try:
#                commandsplit = command.split(',')
#                x = float(commandsplit[0])
#                y = float(commandsplit[1])
#                theta = float(commandsplit[2])
#            except:
#                self.logbrowser.append('Could not parse command!')
#                self.autocontrolbutton.setChecked(False)
#                return
                
#            self.wlinkconnection.transmit_goal(x, y, theta)

        self.sendgoalbutton.setEnabled(checked)
        # New version
        if checked:
            xs = self.xstartspinbox.value()
            ys = self.ystartspinbox.value()
            xg = self.xgoalspinbox.value()
            yg = self.ygoalspinbox.value()
            self.logbrowser.append('Send robot from x:{0:.2f}, y:{1:.2f} to x:{2:.2f}, y:{3:.2f}'.format(xs,ys,xg,yg))
            self.wlinkconnection.transmit_startgoal(xs,ys,xg,yg)
        
        
    def get_mapposition(self, realx, realy, realrot):
        # converts a position on the displayed map into a position in the mocap frame
        mapx = (realx-self.xoffset)*self.xscale
        mapy = (realy-self.yoffset)*self.yscale*-1.
        maprot = (-1*realrot-self.rotoffset)*self.rotscale
        return int(mapx), int(mapy), maprot
        
    def get_realposition(self, mapx, mapy):
        # converts a position in the mocap frame  into a position on the displayed map
        realx = mapx/self.xscale + self.xoffset
        realy = -1*mapy/self.yscale + self.yoffset
        return (realx, realy)
    
    @pyqtSignature("bool")
    def on_logbutton_clicked(self, checked):
        """
        Slot documentation goes here.
        """
        # TODO: not implemented yet
        if checked:
            # open file for logging
            try:
                self.logbrowser.append('start logging')
                self.logfile=open(self.logfilelineedit.text(), 'a')
    #            self.logfile.write('=============\n')
                self.logfile.write('t; x; y; theta\n')
            except:
                self.logbrowser.append('Could not open file!')
                self.logbutton.setChecked(False)
        else:
            # close file
            self.logbrowser.append('stop logging')
            self.logfile.close()
    
    @pyqtSignature("")
    def on_usecurposbutton_clicked(self):
        """
        Slot documentation goes here.
        """
        self.xstartspinbox.setValue(self.pose[0])
        self.ystartspinbox.setValue(self.pose[1])
    
    @pyqtSignature("QString")
    def on_startnodecombobox_activated(self, node):
        """
        Slot documentation goes here.
        """
        pos = self.get_realposition(*self.animation.get_node_position(int(node)))
        self.xstartspinbox.setValue(pos[0])
        self.ystartspinbox.setValue(pos[1])
        print self.animation.get_node_position(int(node))
        
    
    @pyqtSignature("QString")
    def on_goalnodecombobox_activated(self, node):
        """
        Slot documentation goes here.
        """
        pos = self.get_realposition(*self.animation.get_node_position(int(node)))
        self.xgoalspinbox.setValue(pos[0])
        self.ygoalspinbox.setValue(pos[1])
    
    @pyqtSignature("")
    def on_sendgoalbutton_clicked(self):
        """
        Slot documentation goes here.
        """
        xs = self.xstartspinbox.value()
        ys = self.ystartspinbox.value()
        xg = self.xgoalspinbox.value()
        yg = self.ygoalspinbox.value()
        self.logbrowser.append('Send robot from x:{0:.2f}, y:{1:.2f} to x:{2:.2f}, y:{3:.2f}'.format(xs,ys,xg,yg))
        self.wlinkconnection.transmit_startgoal(xs,ys,xg,yg)
