const int buffer_size = 100;
char buffer[buffer_size];
char junk[buffer_size];
boolean flag;

int printer(){
  int thisByte = 33; 
  while(thisByte < 126){
  Serial.write(thisByte);  
  Serial.print(", dec: "); 
  Serial.print(thisByte);     
  Serial.print(", hex: "); 
  Serial.print(thisByte, HEX);     
  Serial.print(", oct: "); 
  Serial.print(thisByte, OCT);     
  Serial.print(", bin: "); 
  Serial.println(thisByte, BIN);   
  thisByte++;
  }
  return 0;
}

void setup() { 
  Serial.begin(9600); 
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  Serial.println("ASCII Table ~ Character Map"); 
} 

void loop() { 
  Serial.println("Waiting for user to input 'PRINT'");

   flag = false;

  while(!flag) {
    Serial.readBytesUntil('P',junk,buffer_size);
    int bytes_read = Serial.readBytesUntil('T', buffer, buffer_size);
    if (strncmp(buffer, "RINT", 1) == 0) {
      flag = true;
    }
  }

  printer();
  
  while (true){
    continue;
  }
    
} 
