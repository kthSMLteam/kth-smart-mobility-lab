from serial import Serial
from time import sleep

import numpy as np
import math

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG)

def mk_expectation_exception(wanted, got):
	return Exception(
		"Expected {0} but got {1}".format(
			str(wanted),
			str(got)))

class Ubot:
	def __init__(self, port):
		self.serial = Serial(port, 9600, timeout=7)
		log.info("Giving serial port 2 seconds to wake up")
		sleep(2)
		log.info("Serial ready")
		log.info("Reading motor set points")
		l, r = self.read_state()
		log.info("motor set to {0:d} {1:d}".format(l, r))
		log.info("Robot ready")

	def _writemsg(self, msg):
		b = msg.encode("ascii")
		log.info("sending {0}".format(str(b)))
		self.serial.write(b)

	def _readmsg(self):
		while True:
			b = b""
			while b != b":":
				b = self.serial.read()
			b = self.serial.read()
			msg = b""
			while b != b";":
				msg += b
				b = self.serial.read()
			log.info("read {0}".format(str(msg)))
			if msg.startswith(b"!"):
				log.debug("robot says: {0!s}".format(msg))
			else:
				break
		return msg

	def control(self, uleft, uright):
		assert uleft >= -999 and uleft <= 999
		assert uright >= -999 and uright <= 999
		uleft = int(uleft)
		uright = int(uright)
		msg = ":u {0:=+04d} {1:=+04d};".format(uleft, uright)
		self._writemsg(msg)

	def read_state(self):
		self._writemsg(":state?;")
		resp = self._readmsg()
		if not resp.startswith(b"state"):
			self.control(0,0)
			raise mk_expectation_exception(b"state", resp)
		_, left, right = resp.decode("ascii").split(" ")
		return int(left), int(right)

	def buffer_input(self):
		self.serial.read


if __name__ == "__main__":
	r = Ubot("COM6")
	t = np.linspace(0,2 * math.pi)
	sint = np.sin(t) * 400;
	for v in sint:
		r.control(v,v)
		left, right = r.read_state()
		sleep(100e-3)
		print
	r.control(0, 0)
    