/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author rui
 */
public class ProcessXml {
    
    List<Node> nodeList;
    List<Way> wayList;
    
    public ProcessXml() {
        
    }
    
    public static Document loadXMLFromString(String xmlString) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xmlString));
        return builder.parse(is);
    }
    
    public void processIncomingLanelets(String xmlString) throws Exception{
        
//        parseXmlFile();
        
        System.out.println("processIncomingLanelets(1)");
        
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        
        try {

                //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();

            //parse using builder to get DOM representation of the XML file
            dom = loadXMLFromString(xmlString);


        }catch(ParserConfigurationException pce) {
                pce.printStackTrace();
        }catch(SAXException se) {
                se.printStackTrace();
        }catch(IOException ioe) {
                ioe.printStackTrace();
        }

        System.out.println("processIncomingLanelets(2)");
        
            //get each employee element and create a Employee object
//        parseDocument();
        
        //get the root elememt
        Element docEle = dom.getDocumentElement();

        System.out.println("processIncomingLanelets(3)");
        
        //get a nodelist of <employee> elements
        NodeList nl = docEle.getElementsByTagName("node");
        System.out.println("There are " + nl.getLength() + " nodes in the xml.");
        
        nodeList = new ArrayList();
        
        System.out.println("processIncomingLanelets(4)");
        
        if(nl != null && nl.getLength() > 0) {
        
            System.out.println("NodeOut");
            System.out.println("nl.getLength() = " + nl.getLength());
            
            for(int i = 0 ; i < nl.getLength();i++) {
                
//                System.out.println("NodeIn");
//                //get the employee element
//                System.out.println("A");
                Element el = (Element)nl.item(i);
                
                int nodeId = Integer.parseInt( el.getAttribute("id") );
                double pixelX = Double.parseDouble( el.getAttribute("pixel_x") );
                double pixelY = Double.parseDouble( el.getAttribute("pixel_y") );
                
                nodeList.add( new Node(nodeId, pixelX, pixelY) );
                
//                System.out.println("B");
//                //get the Employee object
                Node node = getNode(el);
//                System.out.println("C");
//                nodeList.add(node);
//                System.out.println("D");
//                //add it to list
//
                        
                        
                        
            }
        }
        
        //get a nodelist of <employee> elements
        nl = docEle.getElementsByTagName("way");
        
        wayList = new ArrayList();
        
        if(nl != null && nl.getLength() > 0) {
        
            for(int i = 0 ; i < nl.getLength();i++) {

                        //get the employee element
                        Element el = (Element)nl.item(i);

                        //get the Employee object
                        
                        
                        
                        int wayId = Integer.parseInt( el.getAttribute("id") );
                        
                        Way newWay = new Way( wayId );
                        
                        NodeList nodeIds = el.getElementsByTagName("nd");
                        
                        if(nodeIds != null && nodeIds.getLength() > 0) {
        
                            for(int temp = 0 ; temp < nodeIds.getLength();temp++) {
                        
                                Element nodeEl = (Element)nodeIds.item(temp);
                                
//                                System.out.println("nodeEl.getAttribute(id) = " + nodeEl.getAttribute("id"));
                                
                                int tempNodeId = Integer.parseInt( nodeEl.getAttribute("id") );
                                
                                newWay.addNodeId(tempNodeId);
                                
                            }
                            
                        }
//                        Way way = getWay(el);
                        
                        wayList.add(newWay);
                        //add it to list
//                        myEmpls.add(e);
                        
                        
                }
        }

        
//        System.out.println("XML String processed. Results:");
//        System.out.println("nodeList.size() = " + nodeList.size());
//        System.out.println("wayList.size() = " + wayList.size());
        for (int i = 0; i < wayList.size(); i++){
            
//            System.out.println( wayList.get(i).nodeIds.size() );
//            System.out.println( wayList.get(i).getNodeIds().size() );
//            System.out.println( wayList.get(i).id );
            
        }
        
                        
        //Iterate through the list and print the data
//        printData();
        
        
        
        
    }
    
    
    public List<Node> getNodeList(){
        
        return nodeList;
        
    }
    public List<Way> getWayList(){
        
        return wayList;
        
    }
    
    private Node getNode(Element nodeEl) {
        
//        System.out.println("getNode");
            //for each <employee> element get text or int values of 
            //name ,id, age and name
//            String name = getTextValue(nodeEl,"Name");
//        System.out.println("getNodeA");
        
        
        String idString = getTextValue(nodeEl,"id");
//        System.out.println("idString = " + idString);
//        nodeEl.
//        int id = getIntValue(nodeEl,"id");
        int id = -1;
//        System.out.println("getNodeB");
//        double pixelX = getDoubleValue(nodeEl,"pixel_x");
        double pixelX = 1000.;
//        System.out.println("getNodeC");
//        double pixelY = getDoubleValue(nodeEl,"pixel_y");
        double pixelY = 1000.;
//        System.out.println("getNodeD");
        Node node = new Node(id, pixelX, pixelY);
//        System.out.println("getNodeE");

        return node;
        
    }
    
    private Way getWay(Element wayEl) {
                        
            //for each <employee> element get text or int values of 
            //name ,id, age and name
//            String name = getTextValue(nodeEl,"Name");
        int id = getIntValue(wayEl,"id");
//        double pixelX = getDoubleValue(nodeEl,"pixelX");
//        double pixelY = getDoubleValue(nodeEl,"pixelY");
        
        Way way = new Way(id);

        return way;
        
    }
    
    public void processIncomingTrajectory(String xmlString){
        
        
    }
    
    
    
    /**
     * I take a xml element and the tag name, look for the tag and get
     * the text content 
     * i.e for <employee><name>John</name></employee> xml snippet if
     * the Element points to employee node and tagName is name I will return John  
     * @param ele
     * @param tagName
     * @return
     */
    private String getTextValue(Element ele, String tagName) {
//        System.out.println("getTextValueA");
        String textVal = null;
//        System.out.println("getTextValueB");
        NodeList nl = ele.getElementsByTagName(tagName);
//        System.out.println("getTextValueC");
        if(nl != null && nl.getLength() > 0) {
//            System.out.println("getTextValueD");
            Element el = (Element)nl.item(0);
            textVal = el.getFirstChild().getNodeValue();
        }
//        System.out.println("getTextValueE");
        return textVal;
    }


    /**
     * Calls getTextValue and returns a int value
     * @param ele
     * @param tagName
     * @return
     */
    private int getIntValue(Element ele, String tagName) {
            //in production application you would catch the exception
        return Integer.parseInt(getTextValue(ele,tagName));
    }
    
    private double getDoubleValue(Element ele, String tagName) {
            //in production application you would catch the exception
//        System.out.println("getDoubleValue");
        return Double.parseDouble(getTextValue(ele,tagName));
    }
    
    
}
