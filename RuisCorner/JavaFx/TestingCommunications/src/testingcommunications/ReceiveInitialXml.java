/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import javafx.concurrent.Task;

/**
 *
 * @author rui
 */
public class ReceiveInitialXml extends Task<Void>{
    
    private final long SLEEP_TIME = 200;
    private int connection_tries = 0;
//    private String hostName = "130.237.50.246";
    private String hostName = "130.237.43.173";
    private int portNumber = 5555; 
    private int socketTimeOut = 500; // in milliseconds
    
    public ProcessXml xmlProcesser;
    
    public ReceiveInitialXml(ProcessXml argXmlProcesser){
        
        xmlProcesser = argXmlProcesser;
        
    }
    
    @Override protected Void call() throws Exception {
        
        updateMessage("waiting");
        
        System.out.println("WILL CREATE SOCKET");
//        Socket echoSocket = new Socket(hostName, portNumber);
        Socket socket = new Socket();
        
//        SocketAddress sockaddr = new InetSocketAddress(hostName, portNumber);
//        System.out.println("WILL CREATE SOCKET");
        
//        while (true) {
//            
//            System.out.println("TRYING TO CONNECT");
//            
//            try{
//                
//                socket.connect(sockaddr, socketTimeOut);
//                break;
//
//            }catch (SocketTimeoutException e){
//            
//                updateMessage("SERVER NOT FOUND");
//                System.out.println("SERVER NOT FOUND");
//                socket = new Socket();
//                System.err.println("SocketTimeoutException: " + e.getMessage());
//            
//            }
//            
//        }
        
//        updateMessage("SERVER FOUND");
//        
        
        System.out.println("Creating server socket");
        ServerSocket serverSocket = new ServerSocket(portNumber);
        System.out.println("Acepting server socket");
        Socket clientSocket = serverSocket.accept();
        System.out.println("Acepted server socket");
        PrintWriter out =
            new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(
            new InputStreamReader(clientSocket.getInputStream()));
//        
//        out.println("CONNECT");
        
        String response = in.readLine();
        
//        System.out.println("response = " + response);
        
        System.out.println("Will process XML String");
//        ProcessXml xmlProcesser = new ProcessXml();
        xmlProcesser.processIncomingLanelets(response);
        System.out.println("XML String processed");
        
        updateMessage("RECEIVED1");
        
        if ( response.equals("OK") ){
            
            System.out.println("ConnectToSML Thread Message: SML accepted connection request.");
            updateMessage("ACCEPTED");
        }
        
        String input_state;
        
        while( (input_state = in.readLine()) != null ){
            updateMessage("RECEIVING STATES");
            System.out.println("ConnectToSML Thread Message: Received state.");
            
        }
        
//        while(true)
//        {
//            if (isCancelled()) {
//
//                System.out.println("ConnectToSML Thread Message: Task cancelled.");
//                break;
//
//            }
//
//            connection_tries++;
//            System.out.println("ConnectToSML Thread Message: Will try to connect to SML. [connection attempt" + connection_tries + "]");
//
//            if (connection_tries > 20){
//                break;
//            }
//            
//            updateMessage("Connection attempts " + connection_tries);
//            updateProgress(connection_tries, 100000);
//
//            // Now block the thread for a short time, but be sure
//            // to check the interrupted exception for cancellation!
//            try {
//                Thread.sleep(SLEEP_TIME);
//            } catch (InterruptedException interrupted) {
//                if (isCancelled()) {
//                    updateMessage("Cancelled");
//                    break;
//                }
//            }
//
//        }        
        
        // It is required to return null with Void  
        return null;
        
    }
    
}
