/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 *
 * @author rui
 */
public class Node {

    int id;
    double pixelX;
    double pixelY;
            
    public Node(int argId, double argPixelX, double argPixelY){
        
        id = argId;
        pixelX = argPixelX;
        pixelY = argPixelY;
        
    }
            
    @Override public String toString() {
    
        String toString = "Node with id=" + id + " pixelX=" + pixelX + " pixelY=" + pixelY;

        return toString;
    }
    
}
