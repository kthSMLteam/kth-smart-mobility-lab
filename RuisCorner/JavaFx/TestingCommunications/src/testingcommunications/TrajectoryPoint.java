/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 *
 * @author rui
 */
public class TrajectoryPoint {
    
    double time;
    double pixelX;
    double pixelY;
            
    public TrajectoryPoint(double argTime, double argPixelX, double argPixelY){
        
        time = argTime;
        pixelX = argPixelX;
        pixelY = argPixelY;
        
    }
            
    @Override public String toString() {
    
        String toString = "TrajecoryPoint with time=" + time + " pixelX=" + pixelX + " pixelY=" + pixelY;

        return toString;
    }
    
}
