/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import javafx.concurrent.Task;
//import java.io.InetSocketAddress;


/**
 *
 * @author rui
 */
public class ConnectToSML extends Task<Void>{
    
    private final long SLEEP_TIME = 200;
    private int connection_tries = 0;
//    private String hostName = "130.237.50.246";
    private String hostName = "130.237.43.173";
    private int portNumber = 5555; 
    private int socketTimeOut = 500; // in milliseconds
    
    
    @Override protected Void call() throws Exception {
        
        updateMessage("CONNECTING");
        
        System.out.println("WILL CREATE SOCKET");
//        Socket echoSocket = new Socket(hostName, portNumber);
        Socket socket = new Socket();
        
        SocketAddress sockaddr = new InetSocketAddress(hostName, portNumber);
        System.out.println("WILL CREATE SOCKET");
        
        while (true) {
            
            System.out.println("TRYING TO CONNECT");
            
            try{
                
                socket.connect(sockaddr, socketTimeOut);
                break;

            }catch (SocketTimeoutException e){
            
                updateMessage("SERVER NOT FOUND");
                System.out.println("SERVER NOT FOUND");
                socket = new Socket();
    //            System.err.println("SocketTimeoutException: " + e.getMessage());
            
            }
            
        }
        
        updateMessage("SERVER FOUND");
        
        System.out.println("SOCKET CREATED");
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        
        out.println("CONNECT");
        
        String response = in.readLine();
        
        if ( response.equals("OK") ){
            
            System.out.println("ConnectToSML Thread Message: SML accepted connection request.");
            updateMessage("ACCEPTED");
        }
        
        String input_state;
        
        while( (input_state = in.readLine()) != null ){
            updateMessage("RECEIVING STATES");
            System.out.println("ConnectToSML Thread Message: Received state.");
            
        }
        
//        while(true)
//        {
//            if (isCancelled()) {
//
//                System.out.println("ConnectToSML Thread Message: Task cancelled.");
//                break;
//
//            }
//
//            connection_tries++;
//            System.out.println("ConnectToSML Thread Message: Will try to connect to SML. [connection attempt" + connection_tries + "]");
//
//            if (connection_tries > 20){
//                break;
//            }
//            
//            updateMessage("Connection attempts " + connection_tries);
//            updateProgress(connection_tries, 100000);
//
//            // Now block the thread for a short time, but be sure
//            // to check the interrupted exception for cancellation!
//            try {
//                Thread.sleep(SLEEP_TIME);
//            } catch (InterruptedException interrupted) {
//                if (isCancelled()) {
//                    updateMessage("Cancelled");
//                    break;
//                }
//            }
//
//        }        
        
        // It is required to return null with Void  
        return null;
        
    }
    
}