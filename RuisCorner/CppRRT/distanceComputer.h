/* 
 * File:   distanceComputer.h
 * Author: rui
 *
 * Created on den 15 december 2014, 12:23
 */

#ifndef DISTANCECOMPUTER_H
#define	DISTANCECOMPUTER_H

extern int const STATE_DIMENSION;
extern float const TRUCK_LENGTH;
extern float const TRAILER_LENGTH;
extern float GOAL_TOLERANCE;

float get_metric_distance(float *state_a, float *state_b);

float get_xy_distance(float *state_a, float *state_b);

float get_car_front_distance(float *state_truck, float *xy);

float get_distance(float *state_a, float *state_b);

bool goal_check(float *state_a, float *state_b);

#endif	/* DISTANCECOMPUTER_H */

