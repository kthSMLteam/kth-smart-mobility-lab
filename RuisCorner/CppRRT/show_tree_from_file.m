clear

disp('Opening connection to Qualisys...')
qtm=QMC('QMC_conf.txt');
disp('Opened connection to Qualisys...')

fileId = fopen('obstacles.txt', 'w');

close all
figure(); hold on;
set(gcf, 'position', [1921,433,1024,692])
% axis(2*[-1 1 -1 1])
width=6.18;
x_offset=-2.36;
height=4.4;
y_offset=-2.4;
axis([x_offset x_offset+width y_offset y_offset+height])

% qualisys_obstacle_id = 1;
write_obstacle_to_file(qtm, 10, fileId );
write_obstacle_to_file(qtm, 11, fileId );
write_obstacle_to_file(qtm, 12, fileId );
write_obstacle_to_file(qtm, 13, fileId );
% write_obstacle_to_file(qtm, qualisys_obstacle_id, fileId );

fclose(fileId);


init_theta = 0.5*pi;
% init_state =[-1.1 -1.1 0*pi 0*pi];

[x,y,theta,t_stamp] = get_pose_qualisys_id(qtm,3);
% init_state =[-1.5 -1.5 init_theta init_theta];
init_state =[x y theta theta];
quiver(init_state(1), init_state(2), cos(init_state(3)), sin(init_state(3)) ,'r')

init_state_string = [];
for i=1:4
    init_state_string = [init_state_string, ' ', num2str( init_state(i) ) ];
end

% % num1
% goal_xy = [-0.09 -0.99];
% % num3
% goal_xy = [-0.00 -0];
% % num0
% goal_xy = [-1 0.8];
% % num2
[x,y,theta,t_stamp] = get_pose_qualisys_id(qtm,5);
goal_xy = [x y];

init_state_string = [init_state_string, ' ',  num2str(goal_xy(1)), ' ',  num2str(goal_xy(2)) ];

space_limits =1.1*[-2 2 -2 2];
for i=1:4
    init_state_string = [init_state_string, ' ', num2str( space_limits(i) ) ];
end

goal_tolerance = 0.1;
init_state_string = [init_state_string, ' ', num2str( goal_tolerance ) ];

euler_simulation_time = 0.10;
init_state_string = [init_state_string, ' ', num2str( euler_simulation_time ) ];

% maximum_steering_angle = pi/6;
maximum_steering_angle = deg2rad(20);
init_state_string = [init_state_string, ' ', num2str( maximum_steering_angle ) ];

plot(goal_xy(1), goal_xy(2), 'kx');
goal_tolerance_x = goal_tolerance*cos(0:0.1:2*pi);
goal_tolerance_y = goal_tolerance*sin(0:0.1:2*pi);
plot(goal_xy(1)+goal_tolerance_x, goal_xy(2)+goal_tolerance_y, 'g');


pause()


init_state_string = strcat('rrt_1.exe', init_state_string);
tic
system(init_state_string);
toc

% return

fid = fopen('example.txt');

% close all;
% figure; hold on;
% % show_square_obstacles();

% return

tline = fgets(fid);
while ischar(tline)
%     disp(tline)
    output = regexp(tline, ' ', 'split');
    
    plot([str2num(output{1}) str2num(output{3})], [str2num(output{2}) str2num(output{4})])
    plot(str2num(output{1}), str2num(output{2}), 'rx') 
%     break
    tline = fgets(fid);
end

fclose(fid);