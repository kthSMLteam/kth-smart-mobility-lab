/*
 * Copyright (c) 2009-2010, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of Oracle nor the names of its contributors
 *   may be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>     /* atof */
#include <cmath>        // std::abs
#include <math.h>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "collision.h"
#include "BoxObstacle.h"

int const STATE_DIMENSION = 4;
#include "TreeNode.h"
#include "SquareObstacle.h"
#include "systemSimulator.h"
#include "distanceComputer.h"
#include "systemSimulator.h"
#include "treeManager.h"


float EULER_SIMULATION_TIME = 0.1;
int const EULER_NUMBER_STEPS = 10;

float const TRUCK_LENGTH = 0.115;
float const TRAILER_LENGTH = 0.166;

float const SPACE_WIDTH = 2.;
float GLOBAL_MIN_X = 2.;
float GLOBAL_MAX_X = 2.;
float GLOBAL_MIN_Y = 2.;
float GLOBAL_MAX_Y = 2.;
float GOAL_TOLERANCE = 0.2;
float MAX_STEERING_ANGLE = M_PI/6;


std::vector<SquareObstacle> obstacles_list_global;
std::vector<BoxObstacle>  box_obstacles_vector_global;


void initialize_obstacles_vector(){
    
    SquareObstacle temp_obstacle;
    
    std::ofstream myFile;
    myFile.open ("square_obstacles.txt");
    
    for ( int i = 0; i < obstacles_list_global.size(); i++ ){
        
        temp_obstacle = obstacles_list_global.at(i);
        myFile << temp_obstacle.x << " " << temp_obstacle.y << " "
                << temp_obstacle.w << " " << temp_obstacle.h  << "\n";
        
    }
    
    myFile.close();
    
}

void read_obstacles_from_file(std::string file_name){
    
    std::string current_line;
    
//    std::ifstream myfile (file_name);
//    std::ifstream myfile ("obstacles.txt");
    std::ifstream myfile ( file_name.c_str() );
    
    
    int num_obstacle_lines = 0;
    
    
    
    
    if (myfile.is_open()){
        
        BoxObstacle current_box_obstacle;
        
        while ( getline (myfile, current_line) ){
            
//            std::cout << "Start of line read" << std::endl; 
            
            if ( current_line.compare("Obstacle") == 0 ){
            
                if ( num_obstacle_lines == 4 ){
                    num_obstacle_lines = 0;
                    box_obstacles_vector_global.push_back(current_box_obstacle);
                }
                
            }else{
                
                std::string delimiter = " ";
                size_t pos = 0;
                std::string token;
                
                pos = current_line.find(delimiter);
                // FIRST NUMBER
                token = current_line.substr(0, pos);
                current_box_obstacle.bigger[num_obstacle_lines] = atoi(token.c_str());
                current_line.erase(0, pos + delimiter.length());
                
                pos = current_line.find(delimiter);
                // SECOND NUMBER
                token = current_line.substr(0, pos);
                current_box_obstacle.m[num_obstacle_lines] = atof(token.c_str());
                current_line.erase(0, pos + delimiter.length());
                
                pos = current_line.find(delimiter);
                // THIRD NUMBER
                token = current_line.substr(0, pos);
                current_box_obstacle.b[num_obstacle_lines] = atof(token.c_str());
                
                num_obstacle_lines++;
                
                if ( ( num_obstacle_lines == 4 ) && ( box_obstacles_vector_global.size() == 3 ) ){
                    
                    box_obstacles_vector_global.push_back(current_box_obstacle);
                    break;
                    
                }
                
                
                
            }
          
        }
                
        myfile.close();
        
    }else{
        
        
    }
        
    
}



int main(int argc, char**argv) {
    
    std::cout << "PLEASE DEBUG: get_metric_distance(float *state_a, float *state_b), declaration of global STATE_DIMENSION" << std::endl;
    
    clock_t t;
    int f;
    std::cout << "Starting timer." << std::endl;
    t = clock();
    
    std::cout << "Starting RRT search ..." << std::endl;
    
    std::vector<TreeNode> rrt_tree_vector;     
    
    std::vector<SquareObstacle> obstacles_list;     
    
    read_obstacles_from_file("obstacles.txt");
    
        
    float goal_xy[2];
    initialize_tree_list(argc, argv, rrt_tree_vector, goal_xy);
    
        
    float goal_state[STATE_DIMENSION];
//    goal_state[0] = SPACE_WIDTH/4.; goal_state[1] = SPACE_WIDTH/4.; goal_state[2] = 0;
    goal_state[0] = goal_xy[0];
    goal_state[1] = goal_xy[1];
    
    int closest_node_index;
    
    float new_state[STATE_DIMENSION];
    new_state[0] = 0; new_state[1] = 0; new_state[2] = 0;
        
    float random_state[STATE_DIMENSION];
    
    srand ( time(NULL) );
    bool goal_found = false;
    
    for ( int i = 0; i < 100000; i++){
        
        get_random_state(random_state, goal_state);
        
        closest_node_index = find_closest_node(rrt_tree_vector, random_state);
        
        float best_input[2];
        bool possible_movement = find_best_input( rrt_tree_vector.at(closest_node_index).state_configuration, random_state, best_input); // This should actually be FIND_BEST_FINAL_CONFIGURATION
                
        if (!possible_movement){
            
            continue;
            
        }        
        
        float final_state[STATE_DIMENSION];
        euler_integration( rrt_tree_vector.at(closest_node_index).state_configuration, final_state, best_input);     
        
        add_tree_node(rrt_tree_vector, final_state, closest_node_index);
        
        if ( goal_check(final_state, goal_state) ){
            
            std::cout << "GOAL FOUND" << std::endl;
            goal_found = true;
            break;
            
        }
                
    }
    
    if (!goal_found){
        std::cout << "GOAL NOT FOUND" << std::endl;
        closest_node_index = find_closest_node(rrt_tree_vector, goal_state);
    }
    
    std::cout << "Searched " << rrt_tree_vector.size() << " nodes in the tree." << std::endl;
    
    t = clock() - t;
    printf ("It took me %d clicks (%f seconds).\n",t,((float)t)/CLOCKS_PER_SEC);
    
  
    std::ofstream myFile;
    myFile.open ("example.txt");
//    myFile << "Writing this to a file.\n";
    
    for ( int i = 1; i < rrt_tree_vector.size(); i++ ){
        
        TreeNode current_tree_node = rrt_tree_vector.at(i);
        TreeNode parent_tree_node = rrt_tree_vector.at(current_tree_node.parent_node_id);
        
        myFile << current_tree_node.state_configuration[0] << " " << current_tree_node.state_configuration[1] << " "
                << parent_tree_node.state_configuration[0] << " " << parent_tree_node.state_configuration[1]  << "\n";
        
    }
    
    myFile.close();
           
    
    return 0;
    
}


