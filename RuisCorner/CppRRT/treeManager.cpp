#include "treeManager.h"
#include "distanceComputer.h"
#include <iostream>
#include <cmath>        // M_PI constant
#include <stdlib.h>     /* atof */

// TREE MANAGING
void initialize_tree_list(int argc, char**argv, std::vector<TreeNode> &rrt_tree_vector, float *goal_xy){
    
    TreeNode initial_tree_node;
    
    // Prints arguments...
    
    
//    std::cout << "argc: " << argc << std::endl;
//    if (argc > 1) {
////        std::cout << std::endl << "Reading arguments and assigning to initial state:" << std::endl;
//        for (int i = 1; i < argc; i++) {
//            std::cout << "Argument " << i << ": " << argv[i] << std::endl;
////            initial_tree_node.state_configuration[i-1] = atof( argv[i] );
//            //            std::cout << i << ": " << atof( argv[i] )* << std::endl;
//        }
//    }
    
    
//    if ( argc == 5 ){
//        std::cout << "Four arguments provided, will use them as initial configuration" << std::endl;
//        initial_tree_node.state_configuration[0] = atof( argv[1] );
//        initial_tree_node.state_configuration[1] = atof( argv[2] );
//        initial_tree_node.state_configuration[2] = atof( argv[3] );
//        initial_tree_node.state_configuration[3] = atof( argv[4] );
//    }else{
//    // INITIAL CONFIGURATION STARTING NODE
//        std::cout << "Invalid number of arguments" << std::endl;
//        initial_tree_node.state_configuration[0] = -SPACE_WIDTH/4.;
//        initial_tree_node.state_configuration[1] = -SPACE_WIDTH/4.;
//        initial_tree_node.state_configuration[2] = M_PI;
//        initial_tree_node.state_configuration[3] = M_PI;
//    }   
    
    // Argc is always number of arguments provided + 1
    if ( argc == 14 ){
        
        std::cout << "14 arguments provided, will use them as initial" <<
                " configuration and goal point and environment limits," <<
                " goal tolerance, and simulation time (1 meter per second)," <<
                " and max steering" << std::endl;
        initial_tree_node.state_configuration[0] = atof( argv[1] );
        initial_tree_node.state_configuration[1] = atof( argv[2] );
        initial_tree_node.state_configuration[2] = atof( argv[3] );
        initial_tree_node.state_configuration[3] = atof( argv[4] );
        goal_xy[0] = atof( argv[5] );
        goal_xy[1] = atof( argv[6] );
        GLOBAL_MIN_X = atof( argv[7] );
        GLOBAL_MAX_X = atof( argv[8] );
        GLOBAL_MIN_Y = atof( argv[9] );
        GLOBAL_MAX_Y = atof( argv[10] );
        GOAL_TOLERANCE = atof( argv[11] );
        EULER_SIMULATION_TIME = atof( argv[12] );
        MAX_STEERING_ANGLE = atof( argv[13] );
        
    }else{
        
    // INITIAL CONFIGURATION STARTING NODE
        std::cout << "Invalid number of arguments" << std::endl;
        initial_tree_node.state_configuration[0] = -SPACE_WIDTH/4.;
        initial_tree_node.state_configuration[1] = -SPACE_WIDTH/4.;
        initial_tree_node.state_configuration[2] = M_PI;
        initial_tree_node.state_configuration[3] = M_PI;
        goal_xy[0] = 0;
        goal_xy[1] = 0;
        GLOBAL_MIN_X = -2;
        GLOBAL_MAX_X = 2;
        GLOBAL_MIN_Y = -2;
        GLOBAL_MAX_Y = 2;
        GOAL_TOLERANCE = 0.1;
        EULER_SIMULATION_TIME = 0.1;
        MAX_STEERING_ANGLE = M_PI/6;
        
    }    
    
    std::cout << "Initial configuration: " << "(" << initial_tree_node.state_configuration[0] << ", " 
                << initial_tree_node.state_configuration[1] << ", " 
                << initial_tree_node.state_configuration[2] << ", " 
                << initial_tree_node.state_configuration[3] << ")" << std::endl;
    std::cout << "Goal: " << "(" << goal_xy[0] << ", " << goal_xy[1] << ")" << std::endl;
    std::cout << "Goal tolerance: " << GOAL_TOLERANCE << std::endl;
    std::cout << "Environment limits: " << "[" << GLOBAL_MIN_X << ", " << GLOBAL_MAX_X << "]x[" << GLOBAL_MIN_Y << ", " << GLOBAL_MAX_Y << "]" << std::endl;
    std::cout << "Maximum steering angle: " << MAX_STEERING_ANGLE << std::endl;

    
    initial_tree_node.parent_node_id = -1;
    
    rrt_tree_vector.push_back(initial_tree_node);
    
}

int find_closest_node(std::vector<TreeNode> &rrt_tree_vector, float *goal_state){
    
    float minimal_distance = 1000;
    int minimal_distance_index = -1;
        
    for (int i = 0; i < rrt_tree_vector.size(); i++){
        
        float * state_configuration;
        state_configuration = rrt_tree_vector.at(i).state_configuration;
        
        float current_distance;
        current_distance = get_distance(state_configuration, goal_state);
                
        if ( current_distance < minimal_distance ){
            
            minimal_distance = current_distance;
            minimal_distance_index = i;
            
        }
        
    }
    
    return minimal_distance_index;
    
}

void add_tree_node(std::vector<TreeNode> &rrt_tree_vector, float *new_state,int parent_index){
    
    TreeNode new_tree_node;
    
//    for (int i = 0; i < STATE_DIMENSION; i++){
    for (int i = 0; i < 4; i++){
    
        new_tree_node.state_configuration[i] = new_state[i];

    }
    
    new_tree_node.parent_node_id = parent_index;
    
    rrt_tree_vector.push_back(new_tree_node);
    
}

void print_tree_nodes(std::vector<TreeNode> &rrt_tree_vector){
    
    std::cout << "Printing tree" << std::endl;
    
    for (int i = 0; i < rrt_tree_vector.size(); i++){
    
        TreeNode current_tree_node = rrt_tree_vector.at(i);
        
        std::cout << "Node number: " << i  << " with parent " << current_tree_node.parent_node_id << std::endl;
        
//        for (int j = 0; j < STATE_DIMENSION; j++){
        for (int j = 0; j < 4; j++){
            
            std::cout << current_tree_node.state_configuration[j] << " ";
            
        }
        
        std::cout << std::endl;

    }
    
    
}

