/* 
 * File:   collision.h
 * Author: rui
 *
 * Created on den 15 december 2014, 11:36
 */

#ifndef COLLISION_H
#define	COLLISION_H

#include <stdlib.h> 
#include <vector>
#include "SquareObstacle.h"
#include "BoxObstacle.h"

extern float MAX_STEERING_ANGLE;
extern float const TRUCK_LENGTH;
extern float const TRAILER_LENGTH;

extern std::vector<SquareObstacle> obstacles_list_global;
extern std::vector<BoxObstacle> box_obstacles_vector_global;

bool is_inside_square_check(float *current_state, float x, float y, float w, float h);

bool is_in_collision_1_square(float *current_state);

bool is_in_collision_4_squares(float *current_state);

bool is_in_collision_square_obstacles_list(float *current_state);

bool is_in_collision_boxes_vector(float *current_state);

bool is_in_collision_square_obstacles_list_truck_and_trailer(float *current_state);

bool is_in_collision(float *current_state);

#endif	/* COLLISION_H */

