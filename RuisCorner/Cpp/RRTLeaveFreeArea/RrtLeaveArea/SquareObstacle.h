/* 
 * File:   SquareObstacle.h
 * Author: rui
 *
 * Created on den 15 december 2014, 11:55
 */

#ifndef SQUAREOBSTACLE_H
#define	SQUAREOBSTACLE_H

struct SquareObstacle {
    float x;
    float y;
    float w;
    float h;
} ;


#endif	/* SQUAREOBSTACLE_H */

