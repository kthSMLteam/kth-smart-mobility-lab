/* 
 * File:   BoxObstacle.h
 * Author: rui
 *
 * Created on den 15 december 2014, 12:11
 */

#ifndef BOXOBSTACLE_H
#define	BOXOBSTACLE_H

struct BoxObstacle{
    
    float m[4];
    float b[4];
    bool bigger[4];
        
} ;

#endif	/* BOXOBSTACLE_H */

