/* 
 * File:   TreeNode.h
 * Author: rui
 *
 * Created on den 15 december 2014, 11:53
 */

#ifndef TREENODE_H
#define	TREENODE_H

extern int const STATE_DIMENSION;

struct TreeNode {
//    float state_configuration[STATE_DIMENSION];
    float state_configuration[4];
    int parent_node_id;
} ;

#endif	/* TREENODE_H */

