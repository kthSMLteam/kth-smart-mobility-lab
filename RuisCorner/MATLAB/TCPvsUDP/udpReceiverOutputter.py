# Echo server program
import socket
import time
import random

def count_characters_in_string(string, character):

	num_occurences = 0

	for i in xrange(len(string)):

		if string[i] == character:

			num_occurences = num_occurences + 1

	return num_occurences


UDP_IP = 'localhost'
UDP_PORT = 8000              # Arbitrary non-privileged port
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = (UDP_IP, UDP_PORT)

#sock.bind((UDP_IP, UDP_PORT))

print "Started! now"

data = 'real_vehicle_output'

sock.sendto(data, server_address)

print "data = " + str(data)

initial_time = time.time()

desired_read_rate = 50.

sock.settimeout( (1./desired_read_rate)/100. )

num_reads = 0
num_iterations = 0
num_packets = 0
failed_loops = 0

while 1:

	# print "Cycle"
	start_cycle_time = time.time()

	num_iterations = num_iterations + 1

	current_loop_packets = 0

	while 1:

		data = []

		try:

			data, addr = sock.recvfrom(1024)

		except:
			
			#print "Exception"
			pass

		if data:

			#print "data = " + str(data)

			num_reads = num_reads + 1

			packets = count_characters_in_string(data, '\n')

			num_packets = num_packets + packets

			current_loop_packets = current_loop_packets + packets

		else:

			#print "No data"
			break


	if current_loop_packets == 0:

		failed_loops = failed_loops + 1


	if num_reads != 0:
		print "Average packets per read = " + str( float(num_packets)/num_reads )
	if num_iterations != 0:
		print "Average reads per iteration = " + str( float(num_reads)/num_iterations )

	print "Loops without read available = " + str( failed_loops )

	data = str( time.time() - initial_time ) + '\n'

	cycle_time = time.time() - start_cycle_time

	if cycle_time == 0.0: 
		time.sleep(1./desired_read_rate)
		continue

	if cycle_time < 1./desired_read_rate:
		time.sleep(1./desired_read_rate - cycle_time)
		continue

	print "Overtime"
	

sock.close()
print "Socket closed"