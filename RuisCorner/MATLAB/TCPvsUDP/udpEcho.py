import socket
import sys
import time

print "Started!"

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind the socket to the port
server_address = ('', 10000)
# server_address = ('localhost', 10000)
# print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)
# Messages are read from the socket using recvfrom(), which returns the data as well as the address of the client from which it was sent.

initial_time = time.time()
packets_received = 0

while True:
	
	# print >>sys.stderr, '\nwaiting to receive message'
	# print "Waiting for message to arrive"
	data, address = sock.recvfrom(4096)

	# print "Received " + str(len(data)) + " bytes."
	# print >>sys.stderr, 'received %s bytes from %s' % (len(data), address)
	# print >>sys.stderr, data

	# Assuming each cycle corresponds to one packet? Is this correct?




	if data:

		packets_received = packets_received + 1

		frequency = packets_received/(time.time()-initial_time)

		print "frequency = " + str(frequency)

		# print "packets_received = " + str(packets_received)
		
		print "data = " + str(data)

		sent = sock.sendto(data, address)
		# print >>sys.stderr, 'sent %s bytes back to %s' % (sent, address)
		# print "Echoed back!"