function [ commands, num_lines ] = getAllLines( line )
%GETMOSTRECENTCOMMANDS Summary of this function goes here
%   Detailed explanation goes here

    line_str = char(line)';

    num_lines = sum(line==10);
    
    commands = zeros(num_lines, 1);

    for i = 1:num_lines

        [rem, line_str]=strtok(line_str, char(10));
        commands(i, 1) = str2double(rem);
            
    end
    
end

