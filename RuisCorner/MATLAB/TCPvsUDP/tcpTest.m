clear; close all;

my_ip = '130.237.50.246';
host_port = 50007;

desired_rate = 200;


t = tcpip(my_ip, host_port);

t.Timeout = 0.005;

global exitButtonPressed
exitButtonPressed = false;
% exitGui

fopen(t);

time_stamp_timer = tic;

iterations = 0;
total_time = 0;

packets_received = 0;

while 1

    timer = tic;
    
%     fprintf(t, strcat(num2str(toc(time_stamp_timer)), '\n' ));
    
%     try
    available_bytes = t.BytesAvailable;

%     disp(available_bytes)
    
    if ( available_bytes ~= 0 )
%         fscanf(t);
%         line = fgetl(t);

        line = fread(t, available_bytes);
%         disp(char(line)')
        [ speed, phi, num_lines ] = getMostRecentCommands( line );
        
        packets_received = packets_received + num_lines;
%         line_num = str2num(char(line)');
%         delay = toc(time_stamp_timer) - line_num;
%         disp(available_bytes)
%         disp(delay)
        
%         disp(line_num)
%         disp('line_num')
%         
%         disp(delay)
%         total_time = total_time + delay;
%         iterations = iterations + 1;
%         disp('line_num = ')
%         disp(line_num)
%         disp('toc(time_stamp_timer) = ')
%         disp(toc(time_stamp_timer))
    end
       
%     catch 
%        disp('Nothing to read')
%     end

    if exitButtonPressed
       
        break
        
    end
    
%     disp('Cycle')

    if rem(packets_received, 1000) == 0
        
        disp( ['Received ', num2str(packets_received), ' packets.'])
        
    end
    
    time_elapsed = toc(timer) - tic;
    
    if (time_elapsed < desired_rate)    
        
        pause(1/desired_rate - time_elapsed)
        
    else
        
        disp('Cycle overtime')
        
    end
    
end

fclose(t);