import socket
import sys
import time

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


server_address = ('130.237.50.246', 10000)
# server_address = ('localhost', 10000)
message = 'This is the message.  It will be repeated.'

start_time = time.time()

try:

	while True:

		# print "Cycle"

		# Send data
		# print >>sys.stderr, 'sending "%s"' % message

		message = str(time.time())
		sent = sock.sendto(message, server_address)

		# Receive response
		# print >>sys.stderr, 'waiting to receive'
		data, server = sock.recvfrom(4096)

		delay = float(data) - time.time()

		print "delay = " + str(delay)

		# print >>sys.stderr, 'received "%s"' % data

finally:
	# print >>sys.stderr, 'closing socket'
	sock.close()