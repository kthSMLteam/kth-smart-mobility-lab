function [u] = linear_controller(error,v_r,w_r)
%LINEAR_CONTROLLER [u] = linear_controller(error,v_r,w_r)
%   Detailed explanation goes here

u_F=[v_r*cos(error(3));w_r];

csi=0.7; g=60;

k1=2*csi*sqrt(w_r^2 + g*(v_r^2));
k3=k1;
k2=g*abs(v_r);

k1=.5*k1;

K_s=[k1 0 0;...
    0 sign(v_r)*k2 k3];

u_B=K_s*error;

u=[u_B+u_F]';

end

