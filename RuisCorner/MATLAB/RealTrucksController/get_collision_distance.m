function [ collision_distance ] = get_collision_distance( states, x_curr, traj, real_i )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%   collision_distance = get_collision_distance(states, x_curr, [x_r, y_r], real_i);

    ahead_distance = 0.4;
    safety_distance = 0.05;
    
    
    ahead_traj = traj(real_i:end, :);
    
    delta_x = diff( ahead_traj(:, 1) );
    delta_y = diff( ahead_traj(:, 2) );
    
    delta_s = ( delta_x.^2 + delta_y.^2 ).^.5;
    
    distance_moved = [0; cumsum(delta_s)];
    
    try
    
        danger_points = ahead_traj( distance_moved < ahead_distance, 1:2 );
        
    catch
       
        disp('BEING DUMB, CORRECT THIS CATCH STATEMENT!!!')
        danger_points = ahead_traj(:, 1:2);
        
    end
    
    for i = 1:size(states, 1)
       
        distances = danger_points - repmat( states(i,1:2), [size(danger_points,1) 1] );
        distances = ( distances(:,1).^2 + distances(:,2).^2 ).^.5;
        
        if sum(distances < safety_distance) > 0
            
%             collision_distance = 1;
            
            collision_distance = min( distance_moved ( distances < safety_distance ) );
            
            return;
            
        end

%         colliding_indexes = ;
        
    end
    
    collision_distance = 0;

end

