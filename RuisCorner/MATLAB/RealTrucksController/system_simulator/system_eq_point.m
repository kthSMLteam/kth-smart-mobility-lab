function [ x_dot ] = system_eq_point( x , u )
% SYSTEM_EQ 

if length(u)~=2
    error('Input command with wrong dimmention')
end
if length(x)~=2
    error('Input state with wrong dimmention')
end

x_dot=x;

x_dot(1)=u(1);
x_dot(2)=u(2);


end