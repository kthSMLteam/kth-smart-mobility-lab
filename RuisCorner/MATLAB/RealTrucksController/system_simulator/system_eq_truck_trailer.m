function [ x_dot ] = system_eq_truck_trailer( x , u )
% SYSTEM_EQ Simulates the truck+trailer system as seen in 
% Conversion of the Kinematics of a Car with n Trailers into a 
% Chained Form
% with fixed parameters L and d_1
% Inputs:
% State x =[x,y,theta_0,theta_1]
% Command =[v,phi]
% Output:
% State x_dot;


if length(u)~=2
    error('Input command with wrong dimmention')
end
if length(x)~=4
    error('Input state with wrong dimmention')
end

d=0.166;
L=0.115;

x_dot=x;

v_0=u(1);
phi=u(2);

v_1=cos(x(4)-x(3))*v_0;

x_dot(1)=cos(x(3))*v_1;
x_dot(2)=sin(x(3))*v_1;
x_dot(3)=(1/d)*sin(x(4)-x(3))*v_0;
x_dot(4)=tan(phi)*(v_0/L);

end

