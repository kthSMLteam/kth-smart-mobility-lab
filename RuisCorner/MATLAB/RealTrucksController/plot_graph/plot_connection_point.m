function [ ] = plot_connection_point( new_node , graph)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

%     x_i=new_node.x;
parent=graph.get(new_node.parent);
%     x_f=parent.x;

%     plot([car_pos_i(1) car_pos_f(1)],[car_pos_i(2) car_pos_f(2)])
x_continuous=continuous_sim_point( parent.x , new_node.u , 1 );

x_position=zeros(size(x_continuous,1),2);
for j=1:size(x_continuous,1)
    x_position(j,:)=x_continuous(j,1:2);
end

plot(x_position(:,1),x_position(:,2))


end

