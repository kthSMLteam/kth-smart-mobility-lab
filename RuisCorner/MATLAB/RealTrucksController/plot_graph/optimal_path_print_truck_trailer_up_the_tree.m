function [path,coord_path] = optimal_path_print_truck_trailer_up_the_tree( graph , node_id , t)
%OPTIMAL_PATH Summary of this function goes here
%   Detailed explanation goes here

path=[];
coord_path=[];
coord_path_car=[];

while 1
    
    path=[path node_id];
    node=graph.get(node_id);
    
    if node.prev==0
        break
    else
        node_id=node.prev;
    end

    car_temp=car_position_axle_truck_trailer_model(node.x);
    plot(car_temp(1),car_temp(2),'rx')
    coord_path_car=[coord_path_car;...
                car_temp(1) car_temp(2) node.x(4)];

    
end

coord_path_car=flipud(coord_path_car);

path=fliplr(path);

node=graph.get(path(1));

hold on

x_continuous=node.x;
d=0.166;
for i=2:length(path)
    
    node=graph.get(path(i));
    
    x_continuous=continuous_sim_truck_trailer( x_continuous(end,:) , node.u , t );
    
    x_position=zeros(size(x_continuous,1),2);
    for j=1:size(x_continuous,1)
%         x_position(j,:)=car_position_front_truck_trailer_model(x_continuous(j,:));
        x_position(j,:)=car_position_axle_truck_trailer_model(x_continuous(j,:));
    end
    coord_path=[coord_path;x_position(:,1) x_position(:,2)];
%     coord_path_car=[coord_path_car;...
%                     x_continuous(end,1)+d*cos(x_continuous(end,3))...
%                     x_continuous(end,2)+d*sin(x_continuous(end,3))...
%                     x_continuous(end,4)];
    
    %        plot(x_position(:,1),x_position(:,2),'r--',...
    %            'LineWidth',2)
%     plot(x_position(:,1),x_position(:,2)) % Plot car trajectory
%     plot(x_continuous(:,1),x_continuous(:,2),'b','LineWidth',1)
    
end

% coord_path=coord_path_car

end

