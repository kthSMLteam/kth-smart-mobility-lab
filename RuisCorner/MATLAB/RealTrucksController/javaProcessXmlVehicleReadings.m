function [ vehicle_readings ] = javaProcessXmlVehicleReadings( statesString )
%CREATEXML Summary of this function goes here
%   Detailed explanation goes here

    import java.lang.String;
    import java.lang.Double;
    import java.lang.Integer;
    import java.io.File;
    import java.io.IOException;
    import java.io.StringReader;
    import java.io.StringWriter;
    import java.net.*;
    import java.net.InetAddress;
    import java.net.UnknownHostException;
    import java.util.ArrayList;
    import java.util.Collections;
    import java.util.Enumeration;
    import java.util.List;
    import javax.xml.parsers.DocumentBuilder;
    import javax.xml.parsers.DocumentBuilderFactory;
    import javax.xml.parsers.ParserConfigurationException;
    import javax.xml.transform.Transformer;
    import javax.xml.transform.TransformerException;
    import javax.xml.transform.TransformerFactory;
    import javax.xml.transform.dom.DOMSource;
    import javax.xml.transform.stream.StreamResult;
    import org.w3c.dom.Attr;
    import org.w3c.dom.Document;
    import org.w3c.dom.Element;
    import org.w3c.dom.NodeList;
    import org.xml.sax.InputSource;
    import org.xml.sax.SAXException;


    dbf = DocumentBuilderFactory.newInstance();
%     dom = null;

%     try {

        %Using factory get an instance of document builder
    db = dbf.newDocumentBuilder();
    
    factory = DocumentBuilderFactory.newInstance();
    builder = factory.newDocumentBuilder();
    is = InputSource(StringReader(statesString));
    dom = builder.parse(is);

    %get the root element
    docEle = dom.getDocumentElement();
    
    messageElement = dom.getElementsByTagName('message');
    
    messageType = messageElement.item(0).getAttribute('type');

    %get a nodelist of <employee> elements
    stateElements = docEle.getElementsByTagName('vehicle_readings');
%     messageElements = docEle.getElementsByTagName('message');
    
    
%        System.out.println("There are " + nl.getLength() + " nodes in the xml.");

%     trajectoryPointList = ArrayList();
% 
%     if(nl != null && nl.getLength() > 0) {
% 

    vehicle_readings = cell(stateElements.getLength(), 1);

    states = zeros(stateElements.getLength(), 4);

    for i = 0:stateElements.getLength()-1
%     for( i = 0 ; i < nl.getLength();i++) {

        el = stateElements.item(i);

        vehicle_readings{i+1, 1}.id = Integer.parseInt( el.getAttribute('id') );
        
        otherVehicles = el.getElementsByTagName('other_body_info');
        
        vehicle_readings{i+1, 1}.readings = zeros(otherVehicles.getLength(), 2);
        
        for j = 0:otherVehicles.getLength()-1
            
            otherEl = otherVehicles.item(j);
            
            vehicle_readings{i+1, 1}.readings(j+1,1) = Double.parseDouble( otherEl.getAttribute('x') );
            vehicle_readings{i+1, 1}.readings(j+1,2) = Double.parseDouble( otherEl.getAttribute('y') );
            
        end
        
%         el = stateElements.item(i);
% %         el.getAttribute('time')
% %         Double.parseDouble( el.getAttribute('time') )
%         
%         xPos = Double.parseDouble( el.getAttribute('x') );
%         yPos = Double.parseDouble( el.getAttribute('y') );
%         theta = Double.parseDouble( el.getAttribute('theta') );
%         id = Integer.parseInt( el.getAttribute('id') );

        
%         states(i+1,:) = [xPos, yPos, theta, id];

    end


end

