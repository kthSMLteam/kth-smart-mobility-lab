function drawObstacles(obstacles, obstaclesColor)
color = 'k';
if nargin >= 2
    color = obstaclesColor;
end
nrObstacles = size(obstacles,2)/2;
for i = 1: nrObstacles
obsIndex = 2*i-1;
fill(obstacles(:,obsIndex),obstacles(:,obsIndex+1),color)
end


