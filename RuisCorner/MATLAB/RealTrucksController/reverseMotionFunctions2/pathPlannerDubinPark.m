function [path, nrPathPoints] = pathPlannerDubinPark(startPose, goalPose, minTurningRadius, stepSize, parkingSpaceLength)

trailerIndex = [1 2 4];
startPoseTrailer = startPose(trailerIndex);
goalPoseTrailer = goalPose(trailerIndex);

xcGoal = goalPoseTrailer(1);
ycGoal = goalPoseTrailer(2);
theta2Goal = goalPoseTrailer(3);

nrParkingSpacePoints = parkingSpaceLength/stepSize;
parkingSpaceStartPoint = [xcGoal+parkingSpaceLength*cos(theta2Goal),ycGoal+parkingSpaceLength*sin(theta2Goal)];
pathParkingSpace = [linspace(parkingSpaceStartPoint(1),xcGoal,nrParkingSpacePoints);...
    linspace(parkingSpaceStartPoint(2),ycGoal,nrParkingSpacePoints);...
    theta2Goal*ones(1,nrParkingSpacePoints)]';

dubinCarStart = startPoseTrailer + [0,0,pi];
dubinCarGoal = [parkingSpaceStartPoint theta2Goal+pi];
dubinPath= dubins(dubinCarStart, dubinCarGoal, minTurningRadius,stepSize)';
path2ParkingSpace = dubinPath - repmat([0 0 pi],size(dubinPath,1),1);
path = [path2ParkingSpace; pathParkingSpace];

% --- adding curvature information to path
nrPathPoints = size(path,1);
curvatureVector = zeros(nrPathPoints,1);

for i= 2:nrPathPoints
   if  path(i,3) == path(i-1,3)
       curvatureVector(i-1) = 0;       
   else       
       thetaDelta = path(i,3) - path(i-1,3);      
       if(abs(thetaDelta)>pi)
           thetaDelta = thetaDelta -sign(thetaDelta)*2*pi;
       end
       curvatureVector(i-1) = -sign(thetaDelta)*1/minTurningRadius;       
   end             
end

curvatureVector(end) = curvatureVector(end-1);
path = [path, curvatureVector];
end
 