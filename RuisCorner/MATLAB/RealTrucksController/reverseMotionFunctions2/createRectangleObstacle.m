function obstacle = createRectangleObstacle(lowerLeftCorner, length, width, angle)

if nargin<4
    gamma = 0;
else    
    gamma = angle;
end

A = lowerLeftCorner;
B = A + [-width*sin(gamma) width*cos(gamma)];
C = B + [length*cos(gamma) length*sin(gamma)];
D = A + [length*cos(gamma) length*sin(gamma)];
obstacle = [A; B; C; D];

