function close2Nodes = checkCloseness2Nodes(node, otherNodes, minDist)
positionIndex=1:2;
nodePos = node(positionIndex);

nrOtherNodes = size(otherNodes,1);
for i = 1:nrOtherNodes
    node2Pos = otherNodes(i,positionIndex);
    nodeDist = pdist([nodePos; node2Pos],'euclidean');
    if nodeDist < minDist
        close2Nodes = 1;
        return
    end
end
close2Nodes = 0;

