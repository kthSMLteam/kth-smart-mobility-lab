% Function for drawing tractor-trailer. It requires the position of
% the midpoint of the trailers backside and the orientations of the tractor
% and trailer. A path consisting of coordinate points can also be given and
% drawn.

function drawTractorTrailer(pose,space,path,searchPoint,colorNr,margin,shadow)

if nargin >=6 && margin > 0
    [tractorCorners, trailerCorners, hitchPoint]  = tractorTrailerPoints(pose, margin);
else
    [tractorCorners, trailerCorners, hitchPoint]  = tractorTrailerPoints(pose);
end

A = trailerCorners(1,1:2);
B = trailerCorners(2,1:2);
C = trailerCorners(3,1:2);
D = trailerCorners(4,1:2);

A2 = tractorCorners(1,1:2);
B2 = tractorCorners(2,1:2);
C2 = tractorCorners(3,1:2);
D2 = tractorCorners(4,1:2);

% draw  path
if(nargin>=3 && ~isempty(path))
    plot(path(1:end-1,1),path(1:end-1,2),'g.'), hold on;
    plot(path(end,1),path(end,2),'r*');
    
    if(nargin>=4&& ~isempty(searchPoint))
        plot(searchPoint(1),searchPoint(2),'b.');
    end
end
if(nargin>=5 && colorNr>0)
    switch colorNr
        case 1
            Color1 = [204 229 255]/255;
            Color2 = [204 229 255]/255;
        case 2
            Color1 = [255 140 0]/255;
            Color2 = [255 140 0]/255;
        otherwise
            disp('Choose a valid color')
    end
else
    Color1 = [.1 .1 .44];
    Color2 = [.28 .24 .55];
end

hold on;
h1 = fill([A2(1),B2(1),C2(1),D2(1)], [A2(2),B2(2),C2(2),D2(2)], Color1);
h2 = fill([A(1),B(1),C(1),D(1)], [A(2),B(2),C(2),D(2)], Color2);
if nargin >= 7 && shadow == 1
    set(h1,'EdgeColor','none'); set(h2,'EdgeColor','none');
else
    plot(hitchPoint(1),hitchPoint(2),'r.');
end
xlabel('[m]'), ylabel('[m]');
axis(space);
figure(gcf) 
end