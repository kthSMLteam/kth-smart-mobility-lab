%% Experiment Initialization
clear all, clc, close all

global qtm s
% --- Open qualisys connection
qtm = QMC('QMC_conf.txt');

% --- Open controlboard connection
s = NI_initialization();
NI_voltage_stop(s);

% --- Tractor and trailer identification numbers
qualisys_truck_id = 22;
qualisys_trailer_id = 26;

% Boxes identication numbers
global qualisys_B1_id qualisys_B2_id qualisys_B3_id qualisys_B4_id qualisys_BM_id
qualisys_B1_id = 8;
qualisys_B2_id = 9;
qualisys_B3_id = 10;
qualisys_B4_id = 11;
qualisys_BM_id = 17;

 %% Checking reading from Qualisys: tractor-trailer
clc
[xt,yt,theta1,tInit] = get_pose_qualisys_id(qtm,qualisys_truck_id)
[xc,yc,theta2,~] = get_pose_qualisys_id(qtm,qualisys_trailer_id)
sq = 2.5;
figure, drawTractorTrailer([xc yc theta1 theta2], [-sq sq -sq sq])

%% Checking reading from Qualisys: box
[xB,yB,thetaB,~] = get_pose_qualisys_id(qtm,qualisys_BM_id)
hold on,
plot(xB,yB,'y*')
 %% Moving tractor
 NI_voltage_output(s,1.85,1.5,0);
 
%% Stopping tractor1
 NI_voltage_output(s,1.75,1.85,0); 
