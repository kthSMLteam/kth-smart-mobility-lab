function [path, nrPathPoints] = pathPlannerDubin(startPose, goalPose, minTurningRadius, stepSize)

trailerIndex = [1 2 4];
startPoseTrailer = startPose(trailerIndex);
goalPoseTrailer = goalPose(trailerIndex);

dubinCarStart = startPoseTrailer + [0,0,pi];
dubinCarGoal = goalPoseTrailer + [0,0,pi];

dubinPath = dubins(dubinCarStart, dubinCarGoal, minTurningRadius,stepSize)';
path = dubinPath - repmat([0 0 pi],size(dubinPath,1),1);

% --- adding curvature information to path
nrPathPoints = size(path,1);
curvatureVector = zeros(nrPathPoints,1);

for i= 2:nrPathPoints
   if  path(i,3) == path(i-1,3)
       curvatureVector(i-1) = 0;       
   else       
       thetaDelta = path(i,3) - path(i-1,3);      
       if(abs(thetaDelta)>pi)
           thetaDelta = thetaDelta -sign(thetaDelta)*2*pi;
       end
       curvatureVector(i-1) = -sign(thetaDelta)*1/minTurningRadius;       
   end             
end

curvatureVector(end) = curvatureVector(end-1);
path = [path, curvatureVector];
end
 