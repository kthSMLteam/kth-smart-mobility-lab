function varargout = driveTractorTrailer(varargin)
% DRIVETRACTORTRAILER MATLAB code for driveTractorTrailer.fig
%      DRIVETRACTORTRAILER, by itself, creates a new DRIVETRACTORTRAILER or raises the existing
%      singleton*.
%
%      H = DRIVETRACTORTRAILER returns the handle to a new DRIVETRACTORTRAILER or the handle to
%      the existing singleton*.
%
%      DRIVETRACTORTRAILER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DRIVETRACTORTRAILER.M with the given input arguments.
%
%      DRIVETRACTORTRAILER('Property','Value',...) creates a new DRIVETRACTORTRAILER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before driveTractorTrailer_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to driveTractorTrailer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help driveTractorTrailer

% Last Modified by GUIDE v2.5 08-Apr-2015 16:17:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @driveTractorTrailer_OpeningFcn, ...
                   'gui_OutputFcn',  @driveTractorTrailer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before driveTractorTrailer is made visible.
function driveTractorTrailer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to driveTractorTrailer (see VARARGIN)

% Choose default command line output for driveTractorTrailer
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes driveTractorTrailer wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = driveTractorTrailer_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global s
NI_voltage_output(s,1.75,2.4,0);  % Straight

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global s
 NI_voltage_output(s,3,2.4,0); % Right

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global s
 NI_voltage_output(s,1.75,1.75,0); % Stop

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global s
NI_voltage_output(s,0.4,2.4,0); % Left

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global s
NI_voltage_output(s,1.75,1.5,0); % Reverse

% --- Executes during object creation, after setting all properties.
function pushbutton3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
