function orientationVector = pathOrientation(path)

nrPoints = size(path,1);
orientationVector = zeros(nrPoints,1);

for i = 1:nrPoints-1
    p1 = path(i,1:2);
    p2 = path(i+1,1:2);  % p2 can be seen as origo in a unit circle
    deltaX = p1(1) - p2(1);
    deltaY = p1(2) - p2(2);
    theta = atan2(deltaY,deltaX);
    orientationVector(1+i) = theta;    
end

orientationVector(1) = orientationVector(2);