function [newNode, path2NewNode] = growTreeNode(growingNode, indexGrowingNode, destinationNode, obstacles, minTurningRadius, stepSize, maxGrowth, space, margin)

if isempty(growingNode)
    newNode = [];
    path2NewNode = [];
    return
end

path2NewNode = pathPlannerDubin(growingNode, destinationNode, minTurningRadius, stepSize);
pathLength = calculatePathLength(path2NewNode);

if pathLength > maxGrowth
    maxPoints = round(maxGrowth/stepSize);
    path2NewNode = path2NewNode(1:maxPoints,:);
end

p1 = path2NewNode(end,:);
p2 = path2NewNode(end-1,:);

deltaX = p2(1) - p1(1);
deltaY = p2(2) - p1(2);
theta = atan2(deltaY,deltaX);

newNode = [p1(1) p1(2) theta theta indexGrowingNode];

collision = collisionCheck(newNode,obstacles,0);
withinBoundaries = boundaryCheck(path2NewNode,space,margin);
obstacleIntersection = pathObstacleIntersection(path2NewNode,obstacles);

if collision || ~withinBoundaries || ~isempty(obstacleIntersection)
    newNode = [];
end