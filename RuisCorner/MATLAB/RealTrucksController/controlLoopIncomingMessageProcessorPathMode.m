function [ new_vehicle_readings, dangerTrucks, trajectoryToPerform, bodyToPerformTrajectoryId, stop_command_ids ] = ...
    controlLoopIncomingMessageProcessorPathMode( socket, t_step )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    dangerTrucks = [];
    new_states = [];
    new_vehicle_readings = {};
    timeOutMillis = 10;
    trajectoryToPerform = [];
    bodyToPerformTrajectoryId = [];
    stop_command_ids = [];
    
    global fakeTrajectory

    receiving_messages_timeout = t_step/2;

    receiving_messages_timer = tic;

    while true

        if toc(receiving_messages_timer) > receiving_messages_timeout

            disp('Warning: Spending too much time to receive messages.')
            break

        end

        receivedLine = javaReceiveLineOverSocket( socket, timeOutMillis );

        if receivedLine == 0

            break

        end
        
        
        
 
        messageType = javaGetMessageType( receivedLine );
        
        if strcmp(messageType,'collision_warning') == 1
%                 disp('Warning received')
            dangerTrucks = javaProcessXmlCollisionWarning( receivedLine );
        end

        if strcmp(messageType,'vehicle_states_reply') == 1
            disp('Vehicle states received - NOT EXPECTED ANYMORE!!!!!!!!!!!')
            new_states = javaProcessXmlStates( receivedLine );
        end
        
        if strcmp(messageType,'vehicle_readings_reply') == 1
%             disp('Vehicle readings received')
            new_vehicle_readings = javaProcessXmlVehicleReadings( receivedLine );
        end
        
        if strcmp(messageType,'vehicle_stop_command') == 1
            disp('Vehicle stop command received')
            disp(receivedLine)
            stop_command_ids = javaProcessXmlVehicleStopCommand( receivedLine );
        end
        
        
        if ~fakeTrajectory && rand()<0.05
            
            disp('Faking a Trajectory receival')
            
            fakeTrajectory = true;
            
            
            load trajectoryToPerformOriginal
            trajectoryToPerform = trajectoryToPerform *32.;
                        
            trajectoryToPerform = interpolateTrajectory(trajectoryToPerform);
            
            velocity_profile = compute_velocity_profile(trajectoryToPerform);
            
            trajectoryToPerform = [trajectoryToPerform velocity_profile];
            
            bodyToPerformTrajectoryId = 12;

            disp( strcat( 'Will make trajectory on body  ' , num2str(bodyToPerformTrajectoryId) ) )
            
            
            
        end
        
        if strcmp(messageType, 'body_trajectory_command' ) == 1

            disp('Received a trajectory command, will break the cycle')
            
            disp('Process XML Trajectory')
            [receivedInfo, bodyToPerformTrajectoryId] = javaProcessXmlTrajectory( receivedLine );
            disp('XML Trajectory Processed')

%             trajectoryToPerform = receivedInfo(:,1:2)/32.;
            trajectoryToPerform = receivedInfo(:,1:2);
                        
%             disp('----------trajectoryToPerform:')
            
%             save('trajectoryToPerformOriginal','trajectoryToPerform')
%             disp(trajectoryToPerform)
                        
            trajectoryToPerform = interpolateTrajectory(trajectoryToPerform);
            
            velocity_profile = compute_velocity_profile(trajectoryToPerform);
            
            trajectoryToPerform = [trajectoryToPerform velocity_profile];
            
%             disp('------------trajectoryToPerform:')
%             disp(trajectoryToPerform)

%             save('trajectoryToPerformInterpolated','trajectoryToPerform')
            
            % Must interpolate my trajectory

            disp( strcat( 'Will make trajectory on body  ' , num2str(bodyToPerformTrajectoryId) ) )
            
%             disp( 'Trajecoty:' )
%             disp(trajectoryToPerform)
            
            
            break
                
        end

    end


end

