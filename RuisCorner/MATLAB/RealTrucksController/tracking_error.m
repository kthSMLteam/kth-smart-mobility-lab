function [error] = tracking_error(x_r,x)
%TRACKING_ERROR [error] = tracking_error(x_r,x)

R=[cos(x(3)) sin(x(3)) 0;...
    -sin(x(3)) cos(x(3)) 0;...
    0 0 1];

error=R*[x_r-x]';

% Limit error to range [-pi,pi]
while abs(error(3))>pi
    if error(3)>0
        error(3)=error(3)-2*pi;
        continue
    end
    if error(3)<0
        error(3)=error(3)+2*pi;
        continue
    end
end

end

