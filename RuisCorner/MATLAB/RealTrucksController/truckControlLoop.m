function [ PID_struct_longitudinal_vector, PID_struct_lateral_vector, ...
    t_stopped_vector, voltage_vector, isPerformingVector] =...
    truckControlLoop...
    ( truckNumber, t_stopped_vector, t_init_vector, t_stamp, x_curr_vector,...
    x_r_vector, y_r_vector, PID_struct_longitudinal_vector, PID_struct_lateral_vector, isPerformingVector,...
    controlledBodyIds, d_s, mean_velocity, t_step, vehicle_readings, velocity2voltage_handle, socket)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

    if isPerformingVector(truckNumber)
       
        t_stopped = t_stopped_vector(truckNumber);
        t_init = t_init_vector(truckNumber);
        x_curr = x_curr_vector(truckNumber, :);
        x_r = x_r_vector{truckNumber};
        y_r = y_r_vector{truckNumber};
        PID_struct_longitudinal = PID_struct_longitudinal_vector{truckNumber};
        PID_struct_lateral = PID_struct_lateral_vector{truckNumber};


        real_i=round(((t_stamp-t_init-t_stopped)+t_step)/t_step);

        if real_i==0
            real_i=1; disp('Zero index, rounded to one!')
        end
        
        try

    % Save current theta for unwrap purposes
    %         theta_hist_vector(iteration_counter)=x_curr(3);
    %         theta_temp=unwrap(theta_hist_vector(1:iteration_counter));
    %         x_curr(3)=theta_temp(end);
    %         theta_hist_vector(iteration_counter, truckNumber)=x_curr(3);
    %         time_hist_vector(iteration_counter, truckNumber)=t_stamp-t_init;
            states = [];
            
            for i = 1:length( vehicle_readings )
                
                if controlledBodyIds( truckNumber ) == vehicle_readings{i, 1}.id
                    
                    states = vehicle_readings{i, 1}.readings;
                    
                end
                
            end
            
            cars_being_seen = size(states, 1);
%             if cars_being_seen ~= 0
%                
%                 disp( strcat( 'Truck number ' , num2str(truckNumber),...
%                     ' is seeing ' , num2str(cars_being_seen), ' cars.') )
%                 
%             end
    
    
            collision_distance = get_collision_distance(states, x_curr, [x_r, y_r], real_i);

            [x_r_rot,y_r_rot]=rotate_trajectory(x_curr,x_r,y_r);
            [x_car_rot,y_car_rot]=rotate_trajectory(x_curr,x_curr(1),x_curr(2)); % Simply rotates the car coordinates
            longitudinal_error=x_r_rot(real_i)-x_car_rot;
            [u_longitudinal,PID_struct_longitudinal]=...
                PID_control(PID_struct_longitudinal,longitudinal_error,t_step);

            e_1=y_r_rot(real_i)-y_car_rot;
            x_front=0; y_front=y_car_rot;
            i_front=round(d_s/(t_step*mean_velocity));

            if real_i+i_front<=length(y_r_rot)
                e_2=y_r_rot(real_i+i_front)-y_front;
            else
                e_2=0;
            end

            lateral_error=e_1+e_2;
            [u_lateral,PID_struct_lateral]=PID_control(PID_struct_lateral,lateral_error,t_step);

            disp('Control iteration')
            disp( strcat( 'Lateral error: ', num2str(lateral_error) ) )
            disp( strcat( 'Longitudinal error: ', num2str(longitudinal_error) ) )
            disp( strcat( 'x_curr: ', num2str( x_curr ) ) )
            disp( strcat( 'x_ref: ', num2str( [x_r(real_i) y_r(real_i)] ) ) )
                        
            u=[u_longitudinal u_lateral];

            [u, t_stopped] = adjustOutputToCollisionDistance( u, collision_distance, t_stopped, t_step );

            u=pioneer_command_to_truck_SML_command(u); % Command conversion from unicycle to car
            V_volt=velocity2voltage_handle(u(1)); w_volt=steering2voltage(u(2));

            if u(1)<=0
                V_volt = 1.7;
                disp('No movement forward')
            end

            terminate = check_termination_condition(x_curr, x_r, y_r);

            PID_struct_longitudinal_vector{truckNumber} = PID_struct_longitudinal;
            PID_struct_lateral_vector{truckNumber} = PID_struct_lateral;
            t_stopped_vector(truckNumber) = t_stopped;
                        
        catch
            
%             Bad programming, ignore
            disp ('Attempted to access x_r_rot(380); index out of bounds because numel(x_r_rot)=379.');
            disp('BEING DUMB, CORRECT CATCH STATEMENT')
            
            terminate = true;
            V_volt = 1.70; w_volt = 1.70;
            
        end
        
%         When the control algorithm terminates, change the isperforming 
% property and send a completion message to the command central
        if terminate
            
           disp('Terminate condition = true') 
           isPerformingVector(truckNumber) = false;
           sendCompletionMessageToSocket( socket, controlledBodyIds(truckNumber) );
%            disp('Will create completion xml string ')
%            outputString = javaCreateCompletionMessage( controlledBodyIds(truckNumber) );
%            disp('Will send output string: ')
%            disp(outputString)
%            javaSendStringOverSocket(socket, outputString);           
            
        end
        
        
        
    else
        
        doNotTurnOffVoltage = 0.2;
        
        if rand() > 0.98
            
            doNotTurnOffVoltage = -doNotTurnOffVoltage;
            
        end
        
        V_volt = 1.70; w_volt = 1.70 + doNotTurnOffVoltage;
        
    end
        
    bodyId = controlledBodyIds(truckNumber);
    
    % Voltage redirecter summation places the current control loop outputs 
    % in the correct position of the final voltages vector that is sent to 
    % the NI daq
    voltage_vector = voltage_redirecter_summation( V_volt, w_volt, bodyId, controlledBodyIds );
    
end

