function [ current_gen ] = ps_fitness_sort(current_gen,waypoints)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

score=zeros(size(current_gen,1),1);

for i=1:size(current_gen,1)
    score(i)=ps_fitness_function(waypoints,current_gen(i,:));
end

[score,I_sorted]=sort(score,'descend');
current_gen=current_gen(I_sorted,:);

end