function [terminate] = check_termination_condition(x_curr, x_r, y_r)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%  [terminate, memory] = check_termination_condition(memory, x_curr, x_r, y_r)

    goal_tolerance = 0.05;

    dist_to_end = ( ( x_curr(1)-x_r(end) )^2 + ( x_curr(2)-y_r(end) )^2 )^.5;
    
    if dist_to_end < goal_tolerance
        
        terminate = true;
%         memory = []
        return
        
    end
    
    terminate = false;
 

end

