function [u] = non_linear_controller(error,v_r,w_r)
%LINEAR_CONTROLLER [u] = linear_controller(error,v_r,w_r)
%   Detailed explanation goes here

u_F=[v_r*cos(error(3));w_r];

csi=0.7; g=60;

k1=2*csi*sqrt(w_r^2 + g*(v_r^2));
k3=k1;
k2=g;

if error(3)~=0
    K_s=[k1 0 0;...
        0 k2*(sin(error(3))/error(3)) k3];
else
    K_s=[k1 0 0;...
        0 k2 k3];
end

u_B=K_s*error;

u=[u_B+u_F]';

end

