function [u] = non_linear_controller_params(error,v_r,w_r,K)
%LINEAR_CONTROLLER [u] = linear_controller(error,v_r,w_r)
%   Detailed explanation goes here

u_F=[v_r*cos(error(3));w_r];

k1=K(1); k2=K(2); k3=K(3);

if error(3)~=0
    K_s=[k1 0 0;...
        0 k2*(sin(error(3))/error(3)) k3];
else
    K_s=[k1 0 0;...
        0 k2 k3];
end

u_B=K_s*error;

u=[u_B+u_F]';

end

