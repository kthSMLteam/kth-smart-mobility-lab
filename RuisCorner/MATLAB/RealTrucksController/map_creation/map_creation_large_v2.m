function [ Obs ] = map_creation_large_v2( space_width , d )
%MAP_CREATION Summary of this function goes here
%   Detailed explanation goes here

Obs={};
point=[-(1/3)*(space_width/2) -(1/3)*(space_width/2)];
obstacle=[point;point;point;point]+...
    [0 0;d 0;d -(2/3)*(space_width/2);0 -(2/3)*(space_width/2)];
Obs{1}=obstacle;
point=[-(1/3)*(space_width/2) -(1/3)*(space_width/2)];
obstacle=[point;point;point;point]+...
    [0 0;0 -d;-(1/3)*(space_width/2) -d;-(1/3)*(space_width/2) 0];
Obs{2}=obstacle;
point=[-(1/3)*(space_width/2) (1/3)*(space_width/2)];
obstacle=[point;point;point;point]+...
    5*[0 0;0 -d;-d -d;-d 0];
Obs{3}=obstacle;
point=[-(1/4)*(space_width/2) (3/4)*(space_width/2)];
obstacle=[point;point;point;point]+...
    [0 0;(1/1.5)*(space_width/2) 0;(1/1.5)*(space_width/2) -d;0 -d];
Obs{4}=obstacle;
point=[(space_width/2) (space_width/2)];
obstacle=[point;point;point;point]+...
    5*[0 0;0 -d;-d -d;-d 0];
Obs{5}=obstacle;
point=[(1/4)*(space_width/2) (1/4)*(space_width/2)];
obstacle=[point;point;point;point]+...
    [0 0;0 -(5/4)*(space_width/2);-d -(5/4)*(space_width/2);-d 0];
Obs{6}=obstacle;
point=[(space_width/2) 0];
obstacle=[point;point;point;point]+...
    5*[0 0;0 -d;-d -d;-d 0];
Obs{7}=obstacle;

end

