function [ Obs ] = map_creation_parking( space_width , d )
%MAP_CREATION Summary of this function goes here
%   Detailed explanation goes here

Obs={};
point=[-(space_width/2) (space_width/2)];
obstacle=[point;point;point;point]+...
    [0 0;space_width/d 0;space_width/d -space_width;0 -space_width];
Obs{1}=obstacle;
point=[(space_width/2) (space_width/2)];
obstacle=[point;point;point;point]+...
    [0 0;0 -space_width;-space_width/d -space_width;-space_width/d 0];
Obs{2}=obstacle;
point=[-(space_width/2) (space_width/2)];
obstacle=[point;point;point;point]+...
    [0 0;space_width 0;space_width -space_width/d;0 -space_width/d];
Obs{3}=obstacle;

point=[-(space_width/2) -(space_width/2)+space_width/d];
obstacle=[point;point;point;point]+...
    [0 0;space_width 0;space_width -space_width/d;0 -space_width/d];
Obs{4}=obstacle;
end

