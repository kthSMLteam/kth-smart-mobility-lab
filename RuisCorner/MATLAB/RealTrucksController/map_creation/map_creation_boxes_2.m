function [ Obs ] = map_creation_boxes_2( qtm )
%MAP_CREATION Summary of this function goes here
%   Detailed explanation goes here




id_box_1=2;
id_box_2=7;
id_box_3=9;


[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box_1);

sf_mg=0.125; % safe margin 
box_length=0.6+2*sf_mg; box_width=0.4+2*sf_mg;

Obs={};
obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
   [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
Obs{1}=obstacle;

[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box_2);
obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
   [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
Obs{2}=obstacle;

[x,y,theta,~]=get_pose_qualisys_id(qtm,id_box_3);
obstacle=repmat([x-sf_mg*cos(theta)-sf_mg*sin(-theta) y-sf_mg*sin(theta)-sf_mg*cos(theta)],[4 1])+...    
   [[cos(theta) -sin(theta);sin(theta) cos(theta)]*[0 0;0 box_width; box_length box_width; box_length 0]']';     
Obs{3}=obstacle;


end