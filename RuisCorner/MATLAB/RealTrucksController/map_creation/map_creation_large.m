function [ Obs ] = map_creation_large( space_width , d )
%MAP_CREATION Summary of this function goes here
%   Detailed explanation goes here

Obs={};
point=[(1/8)*(space_width/2) (6/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{1}=obstacle;
point=[-(5/8)*(space_width/2) (3/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{2}=obstacle;
point=[-(4/8)*(space_width/2) -(4/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{3}=obstacle;
point=[(6/8)*(space_width/2) (0/8)*(space_width/2)];
obstacle=[point;point;point;point]+[0 0;-d 0;-d d;0 d];
Obs{4}=obstacle;
point=[0 0];
obstacle=[point;point;point;point]+[0 0;0 -(space_width/2);(space_width/20) -(space_width/2);(space_width/20) 0];
Obs{5}=obstacle;
point=[(space_width/8) 0];
obstacle=[point;point;point;point]+[0 0;(space_width/4) 0;(space_width/4) -(space_width/20);0 -(space_width/20)];
Obs{6}=obstacle;
point=[(space_width/4) 2*(space_width/5)];
obstacle=[point;point;point;point]+[0 0;0 -(space_width/2);(space_width/20) -(space_width/2);(space_width/20) 0];
Obs{7}=obstacle;



end

