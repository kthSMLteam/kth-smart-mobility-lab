function [dist]=metric_distance_truck_trailer_car_front(x,alpha)
%METRIC_DISTANCE Summary of this function goes here
%   Detailed explanation goes here

car_position_front=car_position_front_truck_trailer_model(x);
% car_position_alpha=car_position_front_truck_trailer_model(alpha);
car_position_alpha=alpha(1:2);

dist=car_position_front-repmat(car_position_alpha,[size(car_position_front,1) 1]);

dist=(dist(:,1).^2+dist(:,2).^2).^.5;

end

