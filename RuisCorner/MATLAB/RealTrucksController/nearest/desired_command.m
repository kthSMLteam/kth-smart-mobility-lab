function [ u , best_i , best_j] = desired_command( alpha , nearest_node , t , U_space, ...
    system_sim_handle,metric_distance_handle)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%     u=[(alpha(1)-nearest_node.x(1))/t (alpha(2)-nearest_node.x(2))/t];

best_dist=inf;
best_u=[0 0];

for i=1:size(U_space,1)
    
    for j=1:size(U_space,2)
        
%         x=system_sim_handle(nearest_node.x,U_space(i,j,:),t); It causes
%         trapping in  point models. It only moves in straight lines with
%         previous orientation, updating only orientation at the end. This
%         happens because we are not using continuous sim.
        x=system_sim_handle(nearest_node.x,U_space(i,j,:),t);
        x_f=x(end,:);
        final_node.x=x_f;
        dist=metric_distance_handle(final_node,alpha);
        if dist<best_dist
            best_dist=dist;
            best_u=[U_space(i,j,1) U_space(i,j,2)];
            best_i=i;
            best_j=j;
        end
        
    end
    
end

u=best_u;

end

