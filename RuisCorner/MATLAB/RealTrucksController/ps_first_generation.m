function [first_gen] = ps_first_generation(string_individual,generation_size)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

first_gen=repmat(string_individual,[generation_size 1]);

first_gen(1,:)=0;

for individual=2:size(first_gen,1)
    
    for i=1:length(string_individual)
        if rand<0.1
            first_gen(individual,i)=1;
        end
    end
    
end

end

