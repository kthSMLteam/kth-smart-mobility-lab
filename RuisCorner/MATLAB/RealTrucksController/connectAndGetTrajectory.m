function [ receivedInfo, bodyId, socket ] = connectAndGetTrajectory( ipAddress, port , s, controlledBodyIds, realRun)
%CONNECTANDGETTRAJECTORY Summary of this function goes here
%   Detailed explanation goes here

%     ipAddress = '130.237.50.246';
%     port = 8000;

    get_trajectory_timer = tic;
    
    minimum_safety_socket_period = 0.025;

    numberTries = 0;

    direction = true;
    neutralVoltage = 1.75;
    currentVoltage = [neutralVoltage neutralVoltage neutralVoltage neutralVoltage neutralVoltage neutralVoltage 0];
    
    
    while 1

        numberTries = numberTries + 1;
        
        if toc(get_trajectory_timer)/numberTries < minimum_safety_socket_period && numberTries > 1000
           
            error('Socket is going crazy!')
            
        end
        
        socket = javaStartSocket( ipAddress , port );

        if realRun
            [currentVoltage ,direction] = increaseSteering( currentVoltage, s , direction);
        end
        
        if socket ~= 0

            break

        end

        disp( strcat( 'Trying to connect. Tries:' , num2str(numberTries) ) )

    end
    
    disp('Connection estabilished')
    
    try
       
    %     cleanupObj = onCleanup( @() javaCloseSocket(socket) );
    %     reallyCloseSocket = true;

    %     controlledBodyIds = [2, 3, 12];

        outputString = javaCreateXmlBodyInfo( controlledBodyIds );

        %  pause(4.2)

        javaSendStringOverSocket(socket, outputString);


        numberTries = 0;

        disp('Waiting for trajectory')

    %     verbosePeriod = 1.0;
        get_trajectory_timer = tic;

        while 1

            if toc(get_trajectory_timer)/numberTries < minimum_safety_socket_period && numberTries > 1000

                javaCloseSocket(socket);
                error('Socket is going crazy!')

            end

            numberTries = numberTries + 1;
            % Trying to receive a string over the socket
    %         receivedInfo = javaReceiveTrajectoryOverSocket(socket);
            timeOutMillis = 1000;
            receivedInfo = javaReceiveLineOverSocket( socket , timeOutMillis);

    %         remainder =  rem( (verbosePeriod*1000)/(timeOutMillis), 1 );

    %         if remainder == 0

            disp( strcat('Trying to read a line in the socket. Attempt number ',num2str(numberTries)) )

    %         end


            if receivedInfo == 0

                disp('Nothing received')
                continue

            end

            disp('Received something')

            if strcmp( javaGetMessageType( receivedInfo ) , 'body_trajectory_command' ) == 1

                disp('Received a trajectory command, will break the cycle')
                break

            else

                if strcmp( javaGetMessageType( receivedInfo ) , 'collision_warning' ) == 1

                    disp('Received a collision warning')

                end

                if strcmp( javaGetMessageType( receivedInfo ) , 'vehicle_states_reply' ) == 1

                    disp('Received vehicle_states_reply')

                end

                receivedInfo = 0;

            end
            if realRun
                [currentVoltage ,direction] = increaseSteering( currentVoltage, s , direction);
            end

            if receivedInfo ~= 0

                break

            end

            disp( strcat( 'Waiting for trajectory. Tries:' , num2str(numberTries) ) )

        end


    %     javaSendStringOverSocket(socket, 'close');
    %     socket.close();

        disp('No more error handling')
    %     reallyCloseSocket = false;
    %     cleanupObj = onCleanup(@() dummyFunction());
    %     clear cleanupObj;
    %     cleanupObj = onCleanup(@() javaCloseSocket(socket));

        disp('Process XML Trajectory')
        [receivedInfo, bodyId] = javaProcessXmlTrajectory( receivedInfo );
        disp('XML Trajectory Processed')
        
        receivedInfo = receivedInfo(:,1:2)/32.;
        
        disp( strcat( 'Will make trajectory on body  ' , num2str(bodyId) ) )
        
    catch
        
        javaCloseSocket( socket );
        
    end
%     
%     
%     trajectory.x = [1:100]
%     trajectory.y = [2:2:00]

end

