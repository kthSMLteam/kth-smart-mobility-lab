function [ ] = javaCloseSocketError( socket , reallyClose)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    if reallyClose
        disp('Running onCleanUp Function: javaCloseSocketError with reallyClose = True')
    else
        disp('Running onCleanUp Function: javaCloseSocketError with reallyClose = False')
    end

    if reallyClose
    
        disp('Closing socket due to error!')

        javaSendStringOverSocket(socket, 'close');
        socket.close();
        
    end


end

