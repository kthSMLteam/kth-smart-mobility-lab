function [u]=SMC_controller(error,error_d,w_d_d,w_d,v_r,v_d_d,v_r_d,s1,s2,K,P,Q,t_step)
%SMC_CONTROLLER Summary of this function goes here
%   Detailed explanation goes here

k0=K(1);
k1=K(2);
k2=K(3);

q1=Q(1);
q2=Q(2);

p1=P(1);
p2=P(2);

l=0.115;  

v_c_d=(1/cos(error(3)))*...
    (-q1*s1-p1*signum(s1)-k1*error_d(1)-w_d_d*error(2)-...
    w_d*error_d(2)+v_r*error_d(3)*sin(error(3))+v_d_d);

phi_c=atan( ((l/v_r)*w_d) + ( l/(v_r*(v_r*cos(error(3))+k0*signum(error(2)))))*...
    (-q2*s2-p2*signum(s2))-k2*error_d(2)-v_r_d*sin(error(3))+w_d_d*error(1)+w_d*error_d(1));

v_c=v_r+t_step*v_c_d;

u=[v_c phi_c];

end

