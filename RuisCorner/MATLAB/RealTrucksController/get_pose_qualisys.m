function [x,y,theta,t_stamp] = get_pose_qualisys(qtm)
%GET_POSE_QUALISYS Get x,y,theta from qualisys, they come in meters and
%radians
%   Detailed explanation goes here

%     truck_id=14; % Mini truck #1
    truck_id=10; % Mini truck #2

    the6DOF=QMC(qtm);
    frameinfo=QMC(qtm,'frameinfo');
    t_stamp=frameinfo(2)/(10^6);
%     frameinfo(2)/(10^6)
%     disp(frameinfo(2)/(10^8))
    x=the6DOF(1,truck_id)/1000;
    y=the6DOF(2,truck_id)/1000;
    theta=deg2rad(the6DOF(6,truck_id));
    
    
end

