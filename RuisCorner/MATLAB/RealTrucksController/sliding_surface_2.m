function [s2] = sliding_surface_2(error,error_d,K)
%SLIDING_SURFACE_1 Summary of this function goes here
%   Detailed explanation goes here

    k0=K(1);
    k2=K(3);

    s2=error_d(2)+k2*error(2)+k0*signum(error(2))*error(3);

end

