function [ h1,h2 ] = draw_perm_state_truck_trailer( x , color )
%DRAW_STATE Summary of this function goes here
%   Detailed explanation goes here



if length(x)~=4
    error('Input state with wrong dimmention')
end

L=1;
d=10;

trailer_pos=[x(1);x(2)];

trailer_bounds=[-0.5 8 8 -0.5; 0.5 0.5 -0.5 -0.5];

R=[cos(x(3)) -sin(x(3));...
    sin(x(3)) cos(x(3))];

trailer_bounds=R*trailer_bounds;

trailer_bounds=trailer_bounds+repmat(trailer_pos,[1 4]);

h1=fill(trailer_bounds(1,:),trailer_bounds(2,:),color);

car_pos=[x(1)+d*cos(x(3));x(2)+d*sin(x(3))];

car_bounds=[-0.5 0.5+L 0.5+L -0.5; 0.5 0.5 -0.5 -0.5];

R=[cos(x(4)) -sin(x(4));...
    sin(x(4)) cos(x(4))];

car_bounds=R*car_bounds;

car_bounds=car_bounds+repmat(car_pos,[1 4]);

h2=fill(car_bounds(1,:),car_bounds(2,:),color);

end

