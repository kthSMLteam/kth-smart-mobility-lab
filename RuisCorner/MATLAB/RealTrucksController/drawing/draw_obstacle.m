function [  ] = draw_obstacle( Obstacle )
%DRAW_STATE Summary of this function goes here
%   Detailed explanation goes here

for obstacle_id=1:length(Obstacle)
    
    fill(Obstacle{obstacle_id}(:,1),Obstacle{obstacle_id}(:,2),'k',...
        'HandleVisibility','on')
    
end
    
end