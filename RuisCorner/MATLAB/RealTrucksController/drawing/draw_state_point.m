function [  ] = draw_state_point( x )
%DRAW_STATE Summary of this function goes here
%   Detailed explanation goes here



if length(x)~=2
    error('Input state with wrong dimmention')
end

plot(x(1),x(2),'ko','LineWidth',5)

end

