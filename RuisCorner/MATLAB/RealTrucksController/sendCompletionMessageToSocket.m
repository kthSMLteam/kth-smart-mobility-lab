function [ success ] = sendCompletionMessageToSocket( socket, bodyId )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    success = true;

    try

        outputString = javaCreateCompletionMessage( bodyId );
        javaSendStringOverSocket( socket, outputString );
        
    catch
       
        success = false;
        
    end


end

