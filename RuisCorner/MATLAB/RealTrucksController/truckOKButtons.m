function varargout = truckOKButtons(varargin)
% TRUCKOKBUTTONS MATLAB code for truckOKButtons.fig
%      TRUCKOKBUTTONS, by itself, creates a new TRUCKOKBUTTONS or raises the existing
%      singleton*.
%
%      H = TRUCKOKBUTTONS returns the handle to a new TRUCKOKBUTTONS or the handle to
%      the existing singleton*.
%
%      TRUCKOKBUTTONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRUCKOKBUTTONS.M with the given input arguments.
%
%      TRUCKOKBUTTONS('Property','Value',...) creates a new TRUCKOKBUTTONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before truckOKButtons_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to truckOKButtons_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help truckOKButtons

% Last Modified by GUIDE v2.5 09-May-2015 23:36:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @truckOKButtons_OpeningFcn, ...
                   'gui_OutputFcn',  @truckOKButtons_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before truckOKButtons is made visible.
function truckOKButtons_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to truckOKButtons (see VARARGIN)

% Choose default command line output for truckOKButtons
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes truckOKButtons wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = truckOKButtons_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global truck1Ok
    truck1Ok = true;


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global truck2Ok
    truck2Ok = true;


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global truck3Ok
    truck3Ok = true;

% --- Executes on button press in stopbutton.
function stopbutton_Callback(hObject, eventdata, handles)
% hObject    handle to stopbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global closedButtonPushed
    closedButtonPushed = true;
    close truckOKButtons;



% --- Executes on button press in pushbuttonReverseRequest.
function pushbuttonReverseRequest_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonReverseRequest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global buttonReverseRequest
    buttonReverseRequest = true;


% --- Executes on button press in pushbuttonReverseCommand.
function pushbuttonReverseCommand_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonReverseCommand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global buttonReverseCommand
    buttonReverseCommand = true;
