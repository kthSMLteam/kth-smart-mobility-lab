function [ continue_var ] = fancy_gui(v_r,w_r,phi)
%FANCY_GUI Summary of this function goes here
%   Detailed explanation goes here
choice = questdlg({strcat('Maximum linear speed: ',num2str(v_r),' m/s');...
    strcat('Maximum angular speed: ',num2str(w_r),' o/s');...
    strcat('Maximum steering: ',num2str(phi), ' o');}, ...
	'Maximum velocities', ...
	'Yes','No','No');
% Handle response
switch choice
    case 'Yes'
        continue_var = 1;
    case 'No'
        continue_var = 0;
end

end

