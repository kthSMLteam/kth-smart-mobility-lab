function [ collision ] = collision_checker_truck_trailer( x , Obs )
%COLLISION_CHECKER Summary of this function goes here
%   Detailed explanation goes here

theta_0=x(3);
theta_1=x(4);

% delta_theta=theta_0-theta_1;
delta_theta=unwrap([theta_0 theta_1]);
delta_theta=abs(delta_theta(1)-delta_theta(2));

if delta_theta>pi/2 || delta_theta<-pi/2
    collision=1;
    return
end


Vehicle=vehicle_boundary_truck_trailer(x);
collision=objects_colliding(Vehicle,Obs);

end

