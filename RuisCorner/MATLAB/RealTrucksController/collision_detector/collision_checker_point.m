function [ collision ] = collision_checker_point( x , Obstacle )
%COLLISION_CHECKER Summary of this function goes here
%   Detailed explanation goes here

% Vehicle=vehicle_boundary_point(x);
% collision=objects_colliding(Vehicle,Obs);
collision=0;

for obstacle_id=1:length(Obstacle)
    
    points_obstacle=Obstacle{obstacle_id};
    
    if inpolygon(x(1),x(2),...
        points_obstacle(:,1),points_obstacle(:,2))
        collision=1;
        return
    end
    
end

end



