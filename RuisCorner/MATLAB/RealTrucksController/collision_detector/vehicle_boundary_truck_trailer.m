function [ Vehicle ] = vehicle_boundary_truck_trailer( x )
%VEHICLE_BOUNDARY Summary of this function goes here
%   Detailed explanation goes here

Vehicle={};

d=0.15;
L=0.115;

trailer_pos=[x(1);x(2)];

E=0.166-0.025;
axle=0.045;

trailer_bounds=[-0.025 E E -0.025; axle axle -axle -axle];

R=[cos(x(3)) -sin(x(3));...
    sin(x(3)) cos(x(3))];

trailer_bounds=R*trailer_bounds;

trailer_bounds=trailer_bounds+repmat(trailer_pos,[1 4]);

Vehicle{1}=trailer_bounds;

car_pos=[x(1)+d*cos(x(3));x(2)+d*sin(x(3))];

car_bounds=[-0.025 L+0.025 L+0.025 -0.025; axle axle -axle -axle];

R=[cos(x(4)) -sin(x(4));...
    sin(x(4)) cos(x(4))];

car_bounds=R*car_bounds;

car_bounds=car_bounds+repmat(car_pos,[1 4]);

Vehicle{2}=car_bounds;

end

