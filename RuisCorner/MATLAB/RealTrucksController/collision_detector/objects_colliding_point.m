function [ collision ] = objects_colliding_point( x , Obstacle )
%OBJECTS_COLLIDING Summary of this function goes here
%   Detailed explanation goes here

collision=0;

for obstacle_id=1:length(Obstacle)
    
        points_obstacle=Obstacle{obstacle_id};
        
%         in=inpolygon(points_vehicle(:,1),points_vehicle(:,2),...
%             points_obstacle(:,1),points_obstacle(:,2));
        [xa, ~] = polybool('intersection',...
            x(1), x(2),...
            points_obstacle(:,1), points_obstacle(:,2));
        
        if ~isempty(xa)
            collision=1;
            break
        
        end
        
   
end

end