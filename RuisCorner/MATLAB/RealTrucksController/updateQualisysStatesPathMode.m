function [ x_curr, t_stamp ] = updateQualisysStatesPathMode( qtm, controlledBodyIds, x_curr_old, qualisysVerbose )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

        disp('Qualisys Update')

        disp(x_curr_old{1})
        
        x_curr = x_curr_old;
        
        for i = 1:length(x_curr)
            
            prev_readings = x_curr{1};
            prev_readings(2:end, :) = prev_readings(1:end-1, :);
            x_curr{1} = prev_readings;
            
        end
        
        % Get current position
        [x_q,y_q,theta_q,t_stamp]=get_pose_qualisys_by_ids(qtm,controlledBodyIds);

        for i = 1:3

            if ~isnan( x_q(i) ) % Handle possible invalid values
                if theta_q(i)<0 % Put theta in range [0,2*pi]
                    theta_q(i)=theta_q(i)+2*pi;
                end
                x_curr{i}(1,:)=[x_q(i) y_q(i) theta_q(i) t_stamp/1000];
            else % If invalid reading, use previous reading
                x_curr{i}(1,:)=x_curr{i}(2,:);
                if qualisysVerbose
                    disp( strcat('Invalid Qualisys, for id:', num2str(controlledBodyIds(i) ) ) ) % Issue warning to the user
                end
            end

        end
        
        disp(x_curr{1})


end

