function [ redirected_voltage ] = voltage_redirecter_summation( V_volt,w_volt,bodyId , controlled_body_ids)
%UNTITLED Redirects the voltages to its correct place in the voltage to be
%sent to the NI Daq
%   Detailed explanation goes here

    redirected_voltage = zeros(1, 2*length(controlled_body_ids) );

    body_num = find(controlled_body_ids == bodyId);
     
    if ~isempty(body_num)
    
        redirected_voltage((body_num-1)*2 + 1) = V_volt;
        redirected_voltage((body_num-1)*2 + 2) = w_volt;

    else

        disp('in voltage_redirecter_summation function: ')
        disp('a bodyId for which I am not ready appeared! Please FIX! ')

    end



    
    
    
    
end

