function [ isPerformingVector ] = processVehicleStop( stop_command_ids, controlledBodyIds, isPerformingVector )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    if ~isempty(stop_command_ids)   

        for i = 1:length(stop_command_ids)

            for j = 1:length(controlledBodyIds)

                if stop_command_ids(i) == controlledBodyIds(j)

                    disp(strcat('Should kill truck number ',num2str(j)))
                    isPerformingVector(j) = 0;

                end

            end

        end

    end


end

