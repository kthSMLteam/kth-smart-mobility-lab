function [solution_path,best_path] = RRT(space_width,space_limits,Obs,u_1,u_2,x_i,tolerance,goal,filename,...
    model_load_handle,time_allowed_min)

[system_sim_handle,collision_checker_handle,inside_workspace_handle,...
    ~,optimal_path_print_handle,metric_distance_handle,...
    ~,draw_state_handle,get_random_point_handle,...
    nearest_handle]...
    =model_load_handle();

tic

% figure();
hold on
% axis(space_limits)
draw_state_handle(x_i);
draw_obstacle(Obs);
% draw_goal(goal,tolerance)
drawnow

U_space=zeros(length(u_1),length(u_2),2);
for i=1:length(u_1)
    for j=1:length(u_2)
        U_space(i,j,:)=[u_1(i) u_2(j)];
    end
end

x=x_i;

node.x=x;
node.u=[0 0];
node.prev=0;

graph=tree(node);
nodes_max=100; nodes_max_delta=100;
graph_pos=x_i;  % Graph postions are the state positions
graph_pos=inf(nodes_max,size(graph_pos,2));
graph_pos(1,:)=x_i;  % Graph postions are the state positions

t=1;

hold on
goal_found=0;

if collision_checker_handle(x_i,Obs)
    error('Initial state in collision! Unsolvable RRT.')
end

while 1
    
    x_rand=get_random_point_handle(goal,space_width,collision_checker_handle,Obs);
    
    nearest_id=nearest_handle(graph_pos,x_rand,1,metric_distance_handle);
    nearest_node=graph.get(nearest_id);
    
    u=best_command(x_rand,nearest_node,t,U_space,...
    system_sim_handle,metric_distance_handle);
    
    x=system_sim_handle(nearest_node.x,u,t);
    x_new=x(end,:);
    
    if ~collision_checker_handle(x_new,Obs) && ...
            inside_workspace_handle(x_new,space_limits)
        
        new_node.x=x_new;
%         plot(x_new(1),x_new(2),'x');
        drawnow;
        new_node.u=u;
        new_node.prev=nearest_id;
        
        [graph,new_id]=graph.addnode(nearest_id,new_node);
        
%         Plotting the car position
        car_old=car_position_axle_truck_trailer_model(graph_pos(nearest_id,:));
        car_new=car_position_axle_truck_trailer_model(x_new);
        plot([car_old(1) car_new(1)],[car_old(2) car_new(2)])
%         Plotting the truck position
%         plot([graph_pos(nearest_id,1) x_new(1)],[graph_pos(nearest_id,2) x_new(2)])
        graph_pos(new_id,:)=x_new;
        
        if(goal_check(x_new,goal,tolerance,metric_distance_handle))
            disp('Goal found')
            pause(.5)
            draw_state_handle(x_new);

            [best_path,coord_path]=optimal_path_print_handle(graph,new_id,t);
            best_distance=(length(best_path)-1)*u_1;
            goal_found=1;
            break
        end
        
    else
        %          disp('In collision')
    end
    %     h=waitbar(i/max_it);
    if length(graph.Node)==nodes_max
        disp('Allowed more nodes')
        nodes_max=nodes_max+nodes_max_delta;
        graph_pos=[graph_pos;inf*ones(nodes_max_delta,size(graph_pos,2))];
        
    end
    
    if toc>60*time_allowed_min
        disp('Too much time')
        time_warning='Too much time'; %#ok<NASGU>
        break
    end
    
end

if ~goal_found
    nearest_id=nearest_handle(graph_pos,goal,1);
%     optimal_path_print_handle(graph,nearest_id);
    [best_path,coord_path]=optimal_path_print_handle(graph,nearest_id,t);
    best_distance=(length(best_path)-1)*u_1;
    final_node=graph.get(nearest_id);
    draw_state_handle(final_node.x);
end

best_path=graph_pos(best_path,:);

disp(best_distance)
num_nodes_expanded=length(graph.Node);
disp(num_nodes_expanded)
time_elapsed=toc;
disp(time_elapsed)

% save(filename)
% saveFigure(filename)
% print('-dpng',strcat(filename,'.png'))

solution_path=coord_path;

% close all

end