function [ x_prev, x_curr, t_stamp ] = updateQualisysStates( qtm, controlledBodyIds, x_curr, qualisysVerbose , qualisysStructure)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

        x_prev = x_curr;
        
        % Get current position        
        [x_q,y_q,theta_q,t_stamp]=get_pose_qualisys_by_ids(controlledBodyIds, qualisysStructure);

        for i = 1:3

            if ~isnan( x_q(i) ) % Handle possible invalid values
                if theta_q(i)<0 % Put theta in range [0,2*pi]
                    theta_q(i)=theta_q(i)+2*pi;
                end
                x_curr(i,:)=[x_q(i) y_q(i) theta_q(i)];
            else % If invalid reading, use previous reading
                x_curr(i,:)=x_prev(i,:);
                if qualisysVerbose
                    disp( strcat('Invalid Qualisys, for id:', num2str(controlledBodyIds(i) ) ) ) % Issue warning to the user
                end
            end

        end


end

