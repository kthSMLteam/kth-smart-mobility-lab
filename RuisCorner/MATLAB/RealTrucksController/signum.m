function [x]=signum(x)
%SIGNUM Summary of this function goes here
%   Detailed explanation goes here

%Ideally it would be the sign function:

% x=sign(x);

% However to remove chattering:

if abs(x)<0.5
    x=0;
else
    x=sign(x);
end

end

