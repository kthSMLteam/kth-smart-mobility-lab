/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import javafx.geometry.VPos;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

/**
 *
 * @author rui
 */
public class DrawingBaseFunctions {
 
    static double windowWidth;
    
    public DrawingBaseFunctions(double argWindowWidth){
                
        windowWidth = argWindowWidth;
        
    }
    
    public static void setWindowWidth(double argWindowWidth){
        
        windowWidth = argWindowWidth;
        
    }
    
    public static double convertToPixelX(double argX){
        
        double xPixel = argX*Globals.smlWorldInfo.pixelPerMeterX*32. + Globals.smlWorldInfo.centerPixelX;
        xPixel = xPixel*(windowWidth/1920.0);    
        
        return xPixel;
        
    }
    
    public static double convertToPixelY(double argY){
        
        double yPixel = -argY*Globals.smlWorldInfo.pixelPerMeterY*32. + Globals.smlWorldInfo.centerPixelY;
    
        return yPixel;
        
    }
    
    public static double convertToX(double argPixelX){
                
        double x = argPixelX*(1920.0/windowWidth);
        x = x - Globals.smlWorldInfo.centerPixelX;
        x = x / ( Globals.smlWorldInfo.pixelPerMeterX*32. );
        
        return x;
        
    }
    
    public static double convertToY(double argPixelY){
            
        double y = argPixelY - Globals.smlWorldInfo.centerPixelY;
        y = y / ( -Globals.smlWorldInfo.pixelPerMeterY*32. );
        
        return y;
        
    }
    
    public static double getMaximumFittingFontSize(Font desiredFont, String desiredText, double maxWidth, double maxHeight){
//            
        String fontName = desiredFont.getName();
        double currentFontSize = desiredFont.getSize();
        double lastFontSize = 0;
        
        double delta = 1.0;
                
        Font currentFont = new Font(fontName, currentFontSize);   
        
        Text currentText = new Text(desiredText);
        currentText.setFont(currentFont);
        
        if ( currentText.getBoundsInLocal().getWidth() > maxWidth || currentText.getBoundsInLocal().getHeight() > maxHeight ){
            
            delta = -delta;
            
            while ( true ){
                
//                System.out.println("Decreasing Loop = " + currentFontSize);
                
                currentFontSize = currentFontSize + delta;
                currentFont = new Font(fontName, currentFontSize);   
                currentText.setFont(currentFont);
                
                if ( currentText.getBoundsInLocal().getWidth() < maxWidth && currentText.getBoundsInLocal().getHeight() < maxHeight ){
                    
                    return currentFontSize;
                    
                }
                                   
            }  
            
        }
                
        while ( true ){
            
//            System.out.println("Increasing Loop = " + currentFontSize);
            
            if ( currentText.getBoundsInLocal().getWidth() > maxWidth || currentText.getBoundsInLocal().getHeight() > maxHeight ){
                                
                return lastFontSize;
                
            }
            
            lastFontSize = currentFontSize;
            
            currentFontSize = currentFontSize + delta;
            currentFont = new Font(fontName, currentFontSize);   
            currentText.setFont(currentFont);
                        
        }
        
//        double y = argPixelY - Globals.smlWorldInfo.centerPixelY;
//        y = y / ( -Globals.smlWorldInfo.pixelPerMeterY*32. );
        
//        return y;
        
    }
        
    public static Text centerTextAroundOffset(double x, double y, Text text){
        
        text.setBoundsType(TextBoundsType.VISUAL);
        
        text.setTextOrigin(VPos.TOP);
        
        double textWidth = text.getBoundsInLocal().getWidth();
        
        String originalText = text.getText();
        text.setText("A!");
        
        double textHeight = text.getBoundsInLocal().getHeight();
        
        text.setText(originalText);
        
        text.setLayoutX( x - textWidth/2.0);
        text.setLayoutY( y - textHeight/2.0);
                
        return text;
        
    }
    
}
