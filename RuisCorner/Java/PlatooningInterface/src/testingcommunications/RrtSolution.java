/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import javafx.scene.image.Image;

/**
 *
 * @author rui
 */



public class RrtSolution {
    
    private ArrayList<RrtNode> treeNodes;
    private ArrayList<Integer> solutionNodes;
//    private int truckId;
//    private double truckFuel;
//    private Image truckDriverPhoto;
//    private Image truckPhoto;
//    private Image truckAndDriverPhoto;
//    private String truckDriverContact;
//    private String truckModel;
    
    public RrtSolution(){
          
        treeNodes = new ArrayList<>();
        solutionNodes = new ArrayList<>();
        
    }
    
    public void setTreeNodes(ArrayList<RrtNode> newTreeNodes){
        
        treeNodes.clear();
        
        for ( RrtNode currentNode : newTreeNodes  ){
            
            treeNodes.add(currentNode);
            
        }
                
    }
    
    public void setSolutionNodes(ArrayList<Integer> newSolutionNodes){
        
        solutionNodes.clear();
        
        for ( int currentSolutionNode : newSolutionNodes  ){
            
            solutionNodes.add(currentSolutionNode);
            
        }
                
    }
    
    public ArrayList<RrtNode> getTreeNodes(){
        
        return treeNodes;
                
    }
    
    public ArrayList<Integer> getSolutionNodes(){
        
        return solutionNodes;    
        
    }
    
}


