/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import javafx.scene.image.Image;

/**
 *
 * @author rui
 */
public class TruckInformation {
    
    private String truckDriverName;
    private int truckId;
    private double truckFuel;
    private Image truckDriverPhoto;
    private Image truckPhoto;
    private Image truckAndDriverPhoto;
    private String truckDriverContact;
    private String truckModel;
    
    public TruckInformation(String argTruckDriverName, int argTruckId, String driverPhotoFilename, String truckPhotoFilename, String truckAndDriverPhotoFilename, String argTruckDriverContact){
        
        truckDriverName = argTruckDriverName;
        truckId = argTruckId;
        truckDriverPhoto = new Image("file:"+driverPhotoFilename);
        truckPhoto = new Image("file:"+truckPhotoFilename);
        truckAndDriverPhoto = new Image("file:"+truckAndDriverPhotoFilename);
        truckDriverContact = argTruckDriverContact;
        
        truckFuel = -1;
        truckModel = "";
                
    }
    
    public String getTruckDriverName(){
        
        return truckDriverName;
        
    }
    
    public double getTruckFuel(){
        
        return truckFuel;
        
    }
    
    public String getTruckModel(){
        
        return truckModel;
        
    }
    
    public int getTruckId(){
        
        return truckId;
        
    }
    
    public Image getTruckDriverImage(){
        
        return truckDriverPhoto;
        
    }
    
    public Image getTruckImage(){
        
        return truckPhoto;
        
    }
    
    public Image getTruckAndDriverImage(){
        
        return truckAndDriverPhoto;
        
    }
    
    public String getTruckDriverContact(){
        
        return truckDriverContact;
        
    }
    
    public void setTruckFuel(double argFuel){
        
        truckFuel = argFuel;
        
    }
    
    public void setTruckModel(String argTruckModel){
        
        truckModel = argTruckModel;
        
    }
    
}
