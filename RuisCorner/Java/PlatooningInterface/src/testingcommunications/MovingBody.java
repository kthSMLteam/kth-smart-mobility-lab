/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 *
 * @author rui
 */
public class MovingBody extends Body{
    
    public double xSpeed;
    public double ySpeed;
    public double zSpeed;
    
    public double yawSpeed;
    public double pitchSpeed;
    public double rollSpeed;
    
    public MovingBody(){
        
        super();
        
        xSpeed = 0;
        ySpeed = 0;
        zSpeed = 0;

        yawSpeed = 0;
        pitchSpeed = 0;
        rollSpeed = 0;
        
    }
    
}
