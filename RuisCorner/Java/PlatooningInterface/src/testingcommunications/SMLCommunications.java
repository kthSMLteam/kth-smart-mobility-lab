/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import javafx.concurrent.Task;
//import java.io.InetSocketAddress;


/**
 *
 * @author rui
 */
public class SMLCommunications extends Task<Void>{
    
    private final long SLEEP_TIME = 200;
    private int connection_tries = 0;
    
    public String hostName = Globals.hostName;
    
    private final int portNumber = 8000; 
    private int socketTimeOut = 500; // in milliseconds
    
    private String inputMessage;
    private ArrayList<String> inputMessagesQueue = new ArrayList<String>();
    private PrintWriter outputStream;
    
    Socket socket;
    
    
    @Override protected Void call() throws Exception {
        
        updateMessage("CONNECTING");
        
        System.out.println("Will create socket");
        
        socket = new Socket();
        SocketAddress sockaddr = new InetSocketAddress(hostName, portNumber);
                
        while (true) {
            
            System.out.println("Trying to connect");
            
            try{
                
                socket.connect(sockaddr, socketTimeOut);
                break;

            }catch (SocketTimeoutException e){
            
                updateMessage("SERVER NOT FOUND");
                socket = new Socket();
                
            }
            
        }
        
        
        socket.setSendBufferSize(262144);
        System.out.println("socket.getSendBufferSize() = " + socket.getSendBufferSize());
        
        updateMessage("SERVER FOUND");
        
        System.out.println("Connected");
        outputStream = new PrintWriter(socket.getOutputStream(), true);
                
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
       
        updateMessage("WAITING");
        
        System.out.println("Listening for messages");
       
        while( true ){
            
            String receivedMessage = in.readLine();
            
            if (receivedMessage == null){
                continue;
            }
            
            inputMessage = receivedMessage;
            
            inputMessagesQueue.add(receivedMessage);
            
            updateMessage("MESSAGE TO PROCESS");
//            System.out.println("SMLCommunications Thread Message: Received a message.");
//            System.out.println("SMLCommunications Thread Message: inputMessage = " + inputMessage);
                        
        }
                
    }
    
    
    public String getInputMessage(){
        
//        updateMessage("WAITING");
//        return inputMessage;
        
//        System.out.println("OnThread: getInputMessage()");
        
        if ( inputMessagesQueue.isEmpty() ){
            
            updateMessage("WAITING");
            
//            System.out.println("OnThread: UpdateTo: WAITING");
            
            return null;
            
        }
        
        String oldestInputMessage = inputMessagesQueue.remove(0);
                
        if ( inputMessagesQueue.size() > 10 ){
            
            System.err.println("inputMessagesQueue.size() = " + inputMessagesQueue.size());
            
        }
        
        if ( inputMessagesQueue.isEmpty() ){
            
            updateMessage("WAITING");
            
        }
        
        return oldestInputMessage;
        
    }
    
    public void sendOutputMessage(String outputMessage){
//        System.out.println("\n");
//        System.out.println("outputMessage sent = " + outputMessage);
        if (outputMessage == null){
            System.out.println("Tried to send a null output message.");
        }else{
            outputStream.println(outputMessage);
        }
        outputStream.flush();
                
    }
    
    public void receiveImage(String filename) throws FileNotFoundException, IOException{
        
        ServerSocket serverSocket = new ServerSocket(8090);
        
        System.out.println("########################################\nWill start image receival through socket\n##############################################");
        
        System.out.println("serverSocket.getInetAddress().toString() = " + serverSocket.getInetAddress().toString());
            
            // Creates a socket accepting incoming connections
            
        
//        System.out.println("serverSocket.accept()");
//        Socket clientSocket = serverSocket.accept();     
        Socket clientSocket;
        
        while (true) {
            
            System.out.println("Accepting again");
            
            try{
                
//                socket.connect(sockaddr, socketTimeOut);
                System.out.println("serverSocket.accept()");
                System.out.println("serverSocket.getInetAddress().toString() = " + serverSocket.getInetAddress().toString());
                serverSocket.setSoTimeout(100);
                clientSocket = serverSocket.accept();     
                break;

            }catch (SocketTimeoutException e){
                
                updateMessage("serverSocket.accept() timeout");
                    
//                updateMessage("SERVER NOT FOUND");
//                clientSocket = new Socket();
    //            System.err.println("SocketTimeoutException: " + e.getMessage());
            
            }
            
        }

        // Creates a printer to send information through the socket
        PrintWriter out =
            new PrintWriter(clientSocket.getOutputStream(), true);    
        
               
        
        InputStream is = clientSocket.getInputStream();
        
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;

        fos = new FileOutputStream(filename);
        bos = new BufferedOutputStream(fos);

        int bufferSize = clientSocket.getReceiveBufferSize();
        byte[] bytes = new byte[bufferSize];

        int count = 0;
        int totalcount = 0;

        System.out.println("Before while");
        
        // Is this timeout doing anything?
        clientSocket.setSoTimeout(500);
                
        try{
            
        
            while ((count = is.read(bytes)) != -1) {

                    bos.write(bytes, 0, count);
                    totalcount += count;
    //                System.out.println("totalcount = " + totalcount);


            }
        
        }catch (SocketTimeoutException e){
            
//            System.out.println("Unable to receive whole image due to time out");
            System.err.println("Unable to receive whole image due to time out");
            
        }
        
        System.out.println("After while");

        bos.flush();
        bos.close();          
                
    }
    
    public void closeSocket() throws IOException{
        
        socket.close();
        
    }
   
}