/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 *
 * @author rui
 */
public class TrajectoryPoint {
    
    double time;
    double X;
    double Y;
            
    public TrajectoryPoint(double argTime, double argX, double argY){
        
        time = argTime;
        X = argX;
        Y = argY;
        
    }
            
    @Override public String toString() {
    
        String toString = "TrajectoryPoint with time=" + time + " X=" + X + " Y=" + Y;

        return toString;
    }
    
}
