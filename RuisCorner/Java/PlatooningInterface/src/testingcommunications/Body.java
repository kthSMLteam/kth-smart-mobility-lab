/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 *
 * @author rui
 */
public class Body {
    /**
    * The current x position of the body.
    * (Note: Expected to be in Real World dimensions: somewhere between 
    * hundreds of meters)
    */
    public double x;
    public double y;
    public double z;
    
    /**
    * The current theta angle of the body.
    * (Note: Expected to be in Real World dimensions: somewhere between
    * -180 and 180 degrees)
    */
    public double yaw;   
    public double pitch;   
    public double roll;    
    
    public double networkConnectivity;
    
    public int id;
    public boolean controllable;
    public boolean obstacle;
    public String obstacleType;
    public String bodyType;
    
    public double width;
    public double height;
    public double radius;
    
    public boolean isTrailer;
    
    /**
     * Creates a QualisysBody assuming that x = 0, y = 0, theta = 0, id = 0, and
     * controllable = false.
     * Note: QualisysBody is expected to be in Smart Mobility Lab dimensions:
     * x and y somewhere between the -4 and 4 meters and the angle in degrees 
     * (probably between -180 and 180).
     */
    public Body(){
        
        x = 0;
        y = 0;
        z = 0;
        
        yaw = 0;
        pitch = 0;
        roll = 0;
        
        id = 0;
        controllable = false;
        networkConnectivity = 0;
        isTrailer = false;
        
    }
    
    /**
     * 
     * Creates a QualisysBody with the given parameters.
     * Note: QualisysBody is expected to be in Smart Mobility Lab dimensions:
     * x and y somewhere between the -4 and 4 meters and the angle in degrees 
     * (probably between -180 and 180).
     * 
     * @param argX
     * @param argY
     * @param argTheta
     * @param argId
     * @param argControllable
     */
    public Body(double argX, double argY, double argTheta, int argId, boolean argControllable, double argNetworkConnectivity){
        
        x = argX;
        y = argY;
        z = 0;
        
        yaw = argTheta;
        pitch = 0;
        roll = 0;
        
        id = argId;
        controllable = argControllable;
        obstacle = false;
        obstacleType = "";
        bodyType = "";
        networkConnectivity = argNetworkConnectivity;
        isTrailer = false;
        
    }
}
