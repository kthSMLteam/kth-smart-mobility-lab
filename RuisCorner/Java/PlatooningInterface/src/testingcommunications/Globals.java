/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 *
 * @author rui
 */
public class Globals {
    
    public static SMLWorldInformation smlWorldInfo;
    public static XmlProcesser xmlProcesser;
    public static SMLCommunications smlCommunications; 
    public static CommunicationStateMachine commsStateMachine;
    public static RoutePlanner routePlanner;
    
    public static DrawingManager drawingManager;
    
    public static RrtSolution currentRrtSolution = new RrtSolution();
    public static MissionManager missionManager;
    
    public static double mapXRatio = 0.8;
    public static double screenWidth;
    public static double screenHeight;
    
    public static boolean passwordRequired = false;
    public static String passwordString = "pratoori";
    public static boolean receiveImageOverSocket = true;
    
    public static boolean touchScreenMode = false;
    
    public static boolean networkVisualisationSwitchIsAvailable = false;
    
//    public static String hostName = "130.237.43.135"; // Pedro's computer
    public static String hostName = "130.237.50.246"; // My computer
//    public static String hostName = "130.237.43.200"; // My internet
    
    public static int loadPileId = -45;
    public static int unloadPileId = -46;
    
    
    public static int body1QualisysId = 16;
    public static int body2QualisysId = 17;
//    public static int body3QualisysId = 12;
    public static int body3QualisysId = 10;
        
    public static int trailer1QualisysId = 14;
    public static int trailer2QualisysId = 15;
    public static int trailer3QualisysId = 13;
    
    public static int rrtMiniBoxId = 5;
    
    
}
