/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

import java.io.*;
import java.net.*;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
//import parsingosm.CoordinateConversion;
//import parsingosm.OSMNode;

public class ReceiveXMLOverSocket {
    
    static boolean connection_request = false;
    static List myEmpls;
    
    
    public static Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }
    
    public static void main(String[] args) throws IOException, InterruptedException, Exception {
        
        System.out.println("Starting server...");
        
        if (args.length != 1) {
            System.err.println("Usage: java ReceiveXMLOverSocket <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        
        System.out.println("Server is now online and waiting to accept a connetion...");
        try (
            // Creates a server socket listening on the port given in the command line
            ServerSocket serverSocket =
                new ServerSocket(Integer.parseInt(args[0]));
            
            // Creates a socket accepting incoming connections
            
            Socket clientSocket = serverSocket.accept();     
                
            // Creates a printer to send information through the socket
            PrintWriter out =
                new PrintWriter(clientSocket.getOutputStream(), true);    
                
            // Creates a buffer to receive information through the socket
            BufferedReader in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
        ) {
            String inputLine;
            String outputLine;
            
            System.out.println("Connection estabilished, waiting for messages...");
            
            inputLine = in.readLine();
            
            System.out.println("inputLine = " + inputLine);
            
//            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//            try {
//                //Using factory get an instance of document builder
//                DocumentBuilder db = dbf.newDocumentBuilder();
//                //parse using builder to get DOM representation of the XML file
//                Document dom = db.parse("scaniaNewLayer.xml");
//
//            }catch(ParserConfigurationException pce) {
//                    pce.printStackTrace();
//            }catch(SAXException se) {
//                    se.printStackTrace();
//            }catch(IOException ioe) {
//                    ioe.printStackTrace();
//            }
        
            Document dom = loadXMLFromString(inputLine);
            
            Element docEle = dom.getDocumentElement();

            //get a nodelist of <employee> elements
            NodeList nl = docEle.getElementsByTagName("node");
            if(nl != null && nl.getLength() > 0) {
                    for(int i = 0 ; i < nl.getLength();i++) {

                            //get the employee element
                            Element el = (Element)nl.item(i);

                            //get the Employee object
//                            OSMNode e = getOSMNode(el);

                            //add it to list
//                            myEmpls.add(e);
                    }
            }
            
            
//            System.out.println("No of nodes '" + myEmpls.size() + "'.");
//
//            Iterator it = myEmpls.iterator();
//            while(it.hasNext()) {
//                
//                    OSMNode tempOSMNode = (OSMNode) it.next();
//                
//                    System.out.println("tempOSMNode = " + tempOSMNode);
//                    
////                    String utmString = coordinateConversion.latLon2UTMRui(tempOSMNode.getLat(), tempOSMNode.getLon());
//                                        
////                    String[] parsedStrings = utmString.split(" ");
////                    double easting = Double.parseDouble(parsedStrings[2]);
////                    double northing = Double.parseDouble(parsedStrings[3]);
//                    
////                    System.out.println(tempOSMNode.toString());
////                    System.out.println(utmString);
////                    System.out.println("easting = " + easting + " " + "northing = " + northing);
////                    System.out.println(easting + " " + northing);
//                    
//            }
            
            
            

        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
    
//    private static OSMNode getOSMNode(Element empEl) {
//
//            
//                        
//            //for each <employee> element get text or int values of 
//            //name ,id, age and name
////        id='-3835' action='modify' visible='true' lat='59.17009609462' lon='17.62777597908' />
//            int id = -1;
//            
//            if ( empEl.hasAttribute("id") ){
//        
////                System.out.println( getTextValue(empEl,"id") );
////                System.out.println( empEl.getAttribute("id") );
////                Integer.parseInt(getTextValue(empEl,"id"));
//                
//                id = getIntValue(empEl,"id");
//             
//            }
//            
//            boolean visible = false;
//            if ( empEl.hasAttribute("visible") ){
//        
//                visible = getBooleanValue(empEl,"visible");
//             
//            }
//            
//            double lat = -1;
//            if ( empEl.hasAttribute("lat") ){
//        
//                lat = getDoubleValue(empEl,"lat");
//             
//            }
//            
//            double lon = -1;
//            if ( empEl.hasAttribute("lon") ){
//        
//                lon = getDoubleValue(empEl,"lon");
//             
//            }
//            
////            int age;
//////            if ( empEl.hasAttribute("Age") ){
//////            System.out.println("empEl.getAttribute(\"Age\") = " + empEl.getAttribute("Age") );
//////            
//////            System.out.println("empEl.getElementsByTagName(\"Age\").getLength() = " + empEl.getElementsByTagName("Age").getLength() );
////            
////            if ( empEl.getElementsByTagName("Age").getLength() == 1 ){
////                age = getIntValue(empEl,"Age");
////            }else{
////                age = -1;
////            }
//            
//                        //Create a new Employee with the value read from the xml nodes
//            OSMNode e = new OSMNode(id, visible, lat, lon);
//
//            return e;
//    }
//
//
//    /**
//     * I take a xml element and the tag name, look for the tag and get
//     * the text content 
//     * i.e for <employee><name>John</name></employee> xml snippet if
//     * the Element points to employee node and tagName is name I will return John  
//     * @param ele
//     * @param tagName
//     * @return
//     */
//    private static String getTextValue(Element ele, String tagName) {
//            String textVal = null;
//            NodeList nl = ele.getElementsByTagName(tagName);
//            if(nl != null && nl.getLength() > 0) {
//                    Element el = (Element)nl.item(0);
//                    textVal = el.getFirstChild().getNodeValue();
//            }
//
//            return textVal;
//    }
//
//
//    /**
//     * Calls getTextValue and returns a int value
//     * @param ele
//     * @param tagName
//     * @return
//     */
//    private static int getIntValue(Element ele, String tagName) {
//            //in production application you would catch the exception
////            return Integer.parseInt(getTextValue(ele,tagName));
//            return Integer.parseInt( ele.getAttribute(tagName) );
//
//    }
//    
//    private static double getDoubleValue(Element ele, String tagName) {
//            //in production application you would catch the exception
////            return Double.parseDouble(getTextValue(ele,tagName));
//            return Double.parseDouble( ele.getAttribute(tagName) );
//    }
//    
//    private static Boolean getBooleanValue(Element ele, String tagName) {
//            //in production application you would catch the exception
////            return Boolean.parseBoolean(getTextValue(ele,tagName));
//            return Boolean.parseBoolean( ele.getAttribute(tagName) );
//    }
//
//    /**
//     * Iterate through the list and print the 
//     * content to console
//     */
//    private static void printData(){
//
//            CoordinateConversion coordinateConversion = new CoordinateConversion();
//        
//            System.out.println("No of nodes '" + myEmpls.size() + "'.");
//
//            Iterator it = myEmpls.iterator();
//            while(it.hasNext()) {
//                
//                    OSMNode tempOSMNode = (OSMNode) it.next();
//                
//                    
//                    
//                    String utmString = coordinateConversion.latLon2UTMRui(tempOSMNode.getLat(), tempOSMNode.getLon());
//                                        
//                    String[] parsedStrings = utmString.split(" ");
//                    double easting = Double.parseDouble(parsedStrings[2]);
//                    double northing = Double.parseDouble(parsedStrings[3]);
//                    
////                    System.out.println(tempOSMNode.toString());
////                    System.out.println(utmString);
////                    System.out.println("easting = " + easting + " " + "northing = " + northing);
//                    System.out.println(easting + " " + northing);
//                    
//            }
//    }
//    
//    static private String parseSocketInput(String socketInput){
//        
//        if ( socketInput.equals("CONNECT") ){
//            
//            if ( connection_request == false ){
//                
//                // We are receiving a connection request for the first time
//                return "OK";
//                
//            }
//            
//        }
//        
//        
//        return null;
//    }
}