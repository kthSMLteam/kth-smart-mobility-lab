/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rui
 */
public class OsmWay {

    int id;
    List<Integer> nodeIds = new ArrayList();
    public String wayType = "";
            
    public OsmWay(int argId){
        
        id = argId;
//        nodeIds = new ArrayList();
                        
    }
        
    public void addNodeId(int argId){
        
        nodeIds.add(argId);
        
    }
    
    public void setWayType(String argWayType){
        
        wayType = argWayType;
        
    }
    
//    public List<Integer> getNodeIds(){
//        
//        return this.nodeIds;
//        
//    }
    
    @Override public String toString() {
    
        String toString = "Way with id=" + id + " and " + nodeIds.size() + " nodes";

        return toString;
    }    
    
}
