/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author rui
 */
public class CommunicationStateMachine extends Task<Void>{
    
//    SMLCommunications task;
//    XmlProcesser xmlProcesser;
//    SMLWorldInformation smlWorldInfo;
    
    String currentState;

    private boolean newTrajectory;
    private boolean newRoadInfo;
    
    Group screenRoot, backgroundRoot, statesRoot, trajectoryRoot, truckMenuRoot;
    
    int numberMessagesReceived = 0;
    
    double initialTime;
    
//    States: Connecting, Handshaking, RoadInfo, Image, ImageProperties, Looping
    
    @Override protected Void call() throws Exception {
        
        currentState = "Handshaking";
        
        newTrajectory = false;
        newRoadInfo = false;
        
        initialTime = System.currentTimeMillis();
        
        while( true ){
            
            step();
//            System.out.println("SMLCommunications Thread Message: Received a message.");
//            System.out.println("SMLCommunications Thread Message: inputMessage = " + inputMessage);
            
            Thread.sleep(100);
            
        }
        
        
//        return null;
        
    }
    
    
    public CommunicationStateMachine(Group argScreenRoot,  Group argBackgroundRoot, Group argStatesRoot, Group argTrajectoryRoot, Group argTruckMenuRoot){
        
//        task = argTask;
//        xmlProcesser = argXmlProcesser;
//        smlWorldInfo = argSmlWorldInfo;
        
        screenRoot = argScreenRoot;
        backgroundRoot = argBackgroundRoot;
        statesRoot = argStatesRoot;
        trajectoryRoot = argTrajectoryRoot;
        truckMenuRoot = argTruckMenuRoot;
        
        currentState = "Handshaking";
        
        newTrajectory = false;
        newRoadInfo = false;
        
        initialTime = System.currentTimeMillis();
        
    }
    

    
    public void step() throws SocketException, Exception{
        
        while ( true ){
            
            if ( !Globals.smlCommunications.getMessage().equals("MESSAGE TO PROCESS") ){
                
                break;
                
            }
            
            
//                    
//                    System.out.println("Incoming message needs to be processed");
                    
            String incomingMessage = Globals.smlCommunications.getInputMessage();
            
            if ( incomingMessage == null ){
                
                System.err.println("CommunicationsStateMaching.step(): Weird thread error, should not happen.");
                break;
                
            }
   
//            System.out.println("incomingMessage = " + incomingMessage);
            
            processReceivedMessage(incomingMessage);
 
            numberMessagesReceived++;
  
            double rate = (numberMessagesReceived*1.0) / ( ((System.currentTimeMillis() - initialTime)/1000.) );
//                    System.out.println("rate = " + rate);

        }

        if (getCurrentState().equals("Looping")){
            
//            if ( Globals.drawingManager != null ){
//                if ( Globals.drawingManager.actionsManager != null ){
//                    if ( Globals.drawingManager.actionsManager.actionsManagerGroup != null ){
//                        System.out.println(" Globals.drawingManager.inspectNumberNodesInGroup( Globals.drawingManager.actionsManager.actionsManagerGroup ) = " + Globals.drawingManager.inspectNumberNodesInGroup( Globals.drawingManager.actionsManager.actionsManagerGroup ) );
//                    }
//                }
//            }
//            
//            if ( Globals.drawingManager != null ){
//                if ( Globals.drawingManager.worldViewManager != null ){
//                    if ( Globals.drawingManager.worldViewManager.worldViewGroup != null ){
//                        System.out.println(" Globals.drawingManager.inspectNumberNodesInGroup( Globals.drawingManager.worldViewManager.worldViewGroup ) = " + Globals.drawingManager.inspectNumberNodesInGroup( Globals.drawingManager.worldViewManager.worldViewGroup ) );
//                    }
//                }
//            }
            
            // If drawEverything is set to true, then it will also draw quadrotors, boxes, etc.
            boolean drawEverything = false;
            Globals.drawingManager.drawStates(drawEverything);
                        
            if ( Globals.missionManager != null ){
                            
                boolean newTrucksConnected = Globals.missionManager.checkTruckControllability();
                
                if ( newTrucksConnected ){

                    ArrayList<String> logMessages = Globals.missionManager.getControllabilityChangesLogMessages();

                    for ( String logMessage : logMessages ){

                        System.out.println("logMessage = " + logMessage);
                        
                        if ( logMessage.contains("disco") ){
                        
                            Globals.drawingManager.addLogEntry(logMessage, Color.RED);
                        
                        }else{
                            
                            Globals.drawingManager.addLogEntry(logMessage, Color.GREEN);
                            
                        }

                    }
                    
                    Globals.missionManager.resetControllabilityChange();
                    
                }            

            }
            
            Globals.drawingManager.updateTrucksMenu();

            if ( isRoadInfoNew() ){

                setNewRoadInfo(false);
                System.out.println("Draw map");
//                Globals.drawingManager.drawImage(backgroundRoot, statesRoot);
                Globals.drawingManager.clearLoadingScreen();
                Globals.drawingManager.drawWorldView();
//                Globals.drawingManager.drawMap(backgroundRoot, screenRoot);

            }

            if ( isTrajectoryNew() ){

                setNewTrajectory(false);
                System.out.println("Draw trajectory");
                Globals.drawingManager.drawTrajectory();

            }

            Globals.smlCommunications.sendOutputMessage( Globals.xmlProcesser.getVehicleStatesRequestMessageString() );
            
        }
        
    }
    
    public void processReceivedMessage(String receivedMessage) throws SocketException, Exception{
        
        String monthString = null;
        
        switch (currentState) {
//            case "Connecting":  
//                processReceivedMessageStateConnecting(receivedMessage);
//                break;
            case "Handshaking":
                processReceivedMessageStateHandshaking(receivedMessage);
                break;
            case "RoadInfo":  
                processReceivedMessageStateRoadInfo(receivedMessage);
                break;
            case "Image":  
                processReceivedMessageStateImage(receivedMessage);
                break;
            case "ImageProperties":  
                processReceivedMessageStateImageProperties(receivedMessage);
                break;
            case "Looping":  
                processReceivedMessageStateLooping(receivedMessage);
                break;
            case "WaitingForTrajectory":  
                System.out.println("Case: WaitingForTrajectory");
                processReceivedMessageStateWaitingForTrajectory(receivedMessage);
                break;
            default: 
//                processReceivedMessageStateConnecting(receivedMessage);
                break;
        }
        
    }
   
    
    private void processReceivedMessageStateHandshaking(String receivedMessage){
        
        if ( receivedMessage.equals("OK") ){
                
            System.out.println("Received an OK");
            Globals.smlCommunications.sendOutputMessage("command"+Globals.passwordString);
            System.out.println("Sent command");
            
            transitionToStateRoadInfo();
            
            return;
                
        }
        
    }
    
    private void transitionToStateRoadInfo(){
            
        currentState = "RoadInfo";
    
    }
            
    private void processReceivedMessageStateRoadInfo(String receivedMessage) throws SocketException{
        
        String receivedProtocol = null;
         
        try {
            receivedProtocol = Globals.xmlProcesser.processIncomingMessage(receivedMessage);
        } catch (Exception ex) {
            Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        if ( receivedProtocol.equals("road_info") ){
            
            Globals.xmlProcesser.saveRoadInfo(Globals.smlWorldInfo);
            transitionToStateImage();     
            
        }
        
        
    }
    
    private void transitionToStateImage() throws SocketException{
            
        newRoadInfo = true;
        
        String imageRequestString = null;
        
        boolean receiveImage = true;
        receiveImage = Globals.receiveImageOverSocket;
        
        imageRequestString = Globals.xmlProcesser.getImageRequestMessageString(receiveImage);
                           
        System.out.println("sendOutputMessage: " + imageRequestString);
        
        Globals.smlCommunications.sendOutputMessage(imageRequestString);     
                
        currentState = "Image";
        
        String filename = "receive_image.bmp";
                            
        System.out.println("Receiving Image");
        
        
        
        if ( receiveImage ){

            try {
                Globals.smlCommunications.receiveImage(filename);
            } catch (IOException ex) {
                Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.out.println("Received Image");
        
        transitionToStateImageProperties();
            
    }
    
    private void transitionToStateImageProperties(){
        
        String imagePropertiesRequestString = null;
            
        imagePropertiesRequestString = Globals.xmlProcesser.getImagePropertiesRequestMessageString();
        
        
        System.out.println("sendOutputMessage: " + imagePropertiesRequestString);
        Globals.smlCommunications.sendOutputMessage(imagePropertiesRequestString); 
        
        currentState = "ImageProperties";
        
    }
    
    private void processReceivedMessageStateImage(String receivedMessage){
                
    }
    
    private void processReceivedMessageStateImageProperties(String receivedMessage){
        
        String receivedProtocol = null;
                
        try {
            receivedProtocol = Globals.xmlProcesser.processIncomingMessage(receivedMessage);
        } catch (Exception ex) {
            Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if ( receivedProtocol.equals("image_properties_reply") ){
            
            Globals.xmlProcesser.saveImageProperties(Globals.smlWorldInfo);
            transitionToStateLooping();     
            
        }
                       
    }
    
    private void transitionToStateLooping(){
        
        
        System.out.println("TRANSITION TO STATE LOOPING");
        currentState = "Looping";
        
    }
    
    public void transitionToTrajectoryWaiting(){
        
        
        System.out.println("TRANSITION TO TRAJECTORY WAITING");
        currentState = "WaitingForTrajectory";
        
    }
    
    
    private void processReceivedMessageStateLooping(String receivedMessage) throws Exception{
        
        String receivedProtocol = null;
                    
        try {
            receivedProtocol = Globals.xmlProcesser.processIncomingMessage(receivedMessage);
        } catch (Exception ex) {
            System.err.println("Error while trying to preocess message string :");
            System.out.println(receivedMessage);
            Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if ( receivedProtocol == null ){
        
            System.out.println("Unknown protocol received");
            System.out.println("receivedMessage = " + receivedMessage);
            System.err.println("Unknown protocol received, message:");
            System.out.println(receivedMessage);
            
        }else if ( receivedProtocol.equals("trajectory_reply") ){
                                                                        
            System.out.println("Trajectory_reply received");
            Globals.xmlProcesser.saveTrajectory(Globals.smlWorldInfo);
            
            newTrajectory = true;
                        
        }else if ( receivedProtocol.equals("vehicle_states_reply") ){
                                                                        
//            System.out.println("vehicle_states_reply received");
            Globals.xmlProcesser.saveBodiesList(Globals.smlWorldInfo);
                       
                        
        }else if ( receivedProtocol.equals("task_completed") ){
                                                                        
            System.out.println("task_completed received");
            Globals.xmlProcesser.processTaskCompletedMessage(receivedMessage);
            
            Globals.drawingManager.popUpBodyCompletion();
                               
        
        }else if ( receivedProtocol.equals("rrt_solution") ){
                                                                        
            System.out.println("rrt_solution received");
            Globals.xmlProcesser.processRrtSolutionMessage(receivedMessage);
            Globals.drawingManager.drawRrtSolution();
            
            
        
        }else if ( receivedProtocol.equals("lanelet_obstruction") ){
                                                                        
            //System.out.println("lanelet_obstruction received");
            
//            double startTime = System.currentTimeMillis();
            
            Globals.xmlProcesser.processLaneletObstructionMessage(receivedMessage);
            
//            double middleTime = System.currentTimeMillis();
            
            Globals.missionManager.updateLaneletObstructions();
            
//            double endTime = System.currentTimeMillis();
            
//            System.out.println("middleTime - startTime = " + (middleTime - startTime) );
//            System.out.println("endTime - middleTime = " + (endTime - middleTime) );
            
//            Globals.drawingManager.updateLaneletObstructions();
        
        }else if ( receivedProtocol.equals("reverse_trajectory_solution") ){
                                                                        
            Globals.xmlProcesser.processReverseTrajectorySolutionMessage(receivedMessage);
            
            System.out.println("Received a reverse_trajectory_solution message");
            
        
        }else if ( receivedProtocol.equals("construction_finished") ){
                                                                        
            Globals.xmlProcesser.processConstructionFinishedMessage(receivedMessage);
            
            System.out.println("Received a construction_finished message");
            
        
        }else{
            
            System.err.print("processReceivedMessageStateLooping() : unknown protocol");
            
        }
    
    }
    
    private void processReceivedMessageStateWaitingForTrajectory(String receivedMessage){
                        
        String receivedProtocol = null;
                    
        try {
            receivedProtocol = Globals.xmlProcesser.processIncomingMessage(receivedMessage);
        } catch (Exception ex) {
            Logger.getLogger(TestingCommunications.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("receivedProtocol = " + receivedProtocol);
        
        if ( receivedProtocol.equals("trajectory_reply") ){
                                                                        
            System.out.println("Trajectory_reply received");
            Globals.xmlProcesser.saveTrajectory(Globals.smlWorldInfo);
            
            newTrajectory = true;
            transitionToStateLooping();
                        
        }
    
        
    }
    
    public boolean isRoadInfoNew(){
        
        return newRoadInfo;
        
    }
    
    public boolean isTrajectoryNew(){
        
        return newTrajectory;
        
    }
    
    public void setNewRoadInfo(boolean argNewRoadInfo){
        
        newRoadInfo = argNewRoadInfo;
        
    }
    
    public void setNewTrajectory(boolean argNewTrajectory){
        
        newTrajectory = argNewTrajectory;
        
    }
    
    public String getCurrentState(){
        
        return currentState;
        
    }

}