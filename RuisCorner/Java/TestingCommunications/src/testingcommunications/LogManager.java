/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.time.LocalDateTime;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author rui
 */
public class LogManager {
    
    Group logGroup;
    
    Group logEntryGroup;
    ScrollPane scrollPane;
    
    public LogManager(double desiredWidth, double desiredHeight){
        
        logGroup = new Group();
        
        scrollPane = new ScrollPane();
        scrollPane.setPrefSize(desiredWidth, desiredHeight);
//        scrollPane.setStyle("-fx-background: rgb(80,80,80);");
        scrollPane.setStyle("-fx-background: rgb(0,0,0);");
//        Text text = new Text("1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19");
//        text.setFont( Font.font( 20 ) );
        
        logEntryGroup = new Group();
//        logEntryGroup.getChildren().add(text);
        
        scrollPane.setContent( logEntryGroup );
        
        
        logGroup.getChildren().add(scrollPane);
        
    }
    
    public void addLogEntry( String logEntry){
        
        addLogEntry( logEntry , Color.WHITE );
        
    }
    
    public void addLogEntry( String logEntry , Paint entryColor){
        
        LocalDateTime currentTime = LocalDateTime.now();
        
        String hourString;
        int currentHour = currentTime.getHour();        
        if ( currentHour < 10 ){
            hourString = "0" + currentHour;
        }else{
            hourString = "" + currentHour;
        }
        
        String minuteString;
        int currentMinute = currentTime.getMinute();        
        if ( currentMinute < 10 ){
            minuteString = "0" + currentMinute;
        }else{
            minuteString = "" + currentMinute;
        }
        
        String secondString;
        int currentSecond = currentTime.getSecond();        
        if ( currentSecond < 10 ){
            secondString = "0" + currentSecond;
        }else{
            secondString = "" + currentSecond;
        }
        
        String timeString = hourString + ":" + minuteString + ":" + secondString + " \t ";
        
        double currentMaxY = logEntryGroup.getBoundsInLocal().getMaxY();
        
        Text newLogEntryTimeText = new Text();
        newLogEntryTimeText.setText( timeString );
        newLogEntryTimeText.setFont( Font.font( 20 ) );
        newLogEntryTimeText.setFill( Color.WHITE );
        
        newLogEntryTimeText.setLayoutY(currentMaxY + newLogEntryTimeText.getBoundsInLocal().getHeight() );
        
        Text newLogEntryText = new Text();
        newLogEntryText.setText( logEntry );
        newLogEntryText.setFont( Font.font( 20 ) );
        newLogEntryText.setFill(entryColor);
        
        newLogEntryText.setLayoutX(newLogEntryTimeText.getBoundsInLocal().getMaxX() );
        newLogEntryText.setLayoutY(currentMaxY + newLogEntryText.getBoundsInLocal().getHeight() );
        
        Group newLogEntryGroup = new Group();
        newLogEntryGroup.getChildren().addAll(newLogEntryTimeText, newLogEntryText);
                
        logEntryGroup.getChildren().add(newLogEntryGroup);
        
        pullScrollPaneDown();
        
    }
    
    private void pullScrollPaneDown(){
        
//        Maybe use this if we are having log going down when we are scrolling up
//        scrollPane.isPressed()
                
        scrollPane.setVvalue( scrollPane.getVmax() );
        
    }
    
}
