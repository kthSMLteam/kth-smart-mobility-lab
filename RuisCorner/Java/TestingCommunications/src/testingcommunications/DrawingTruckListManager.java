/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author rui
 */
public class DrawingTruckListManager {
    
    public List<TruckInformation> truckInformationList;
    
    double windowWidth;
    double windowHeight;
    double mapWidth;
    int highlightedId = -1000;
    
    public DrawingTruckListManager(double argWindowWidth, double argWindowHeight, double argMapWidth){
        
        windowWidth = argWindowWidth;
        windowHeight = argWindowHeight;
        mapWidth = argMapWidth;
        
        DrawingBaseFunctions.setWindowWidth(mapWidth);
        
    }
    
    
    public void createTruckInformationList(){
        
        System.err.println("DrawingTruckListManager.createTruckInformationList() Not being used!");
        
        int[] truckIds = new int[5];
        
        truckInformationList = new ArrayList<>();
        
        TruckInformation truckInfoRui = new TruckInformation("Rui Oliveira", Globals.body1QualisysId, "rui.png", "matteoTruck.png", "ruiAndTruck.png", "+46720752943");
        truckInfoRui.setTruckFuel(180);
        truckInfoRui.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoRui);
        truckIds[0] = Globals.body1QualisysId;
        
        TruckInformation truckInfoAlvito = new TruckInformation("Pedro Alvito", Globals.body2QualisysId, "alvito.png", "alvitoTruck.png", "alvitoAndTruck.png", "+46720842651");
        truckInfoAlvito.setTruckFuel(220);
        truckInfoAlvito.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoAlvito);
        truckIds[1] = Globals.body2QualisysId;
        
        TruckInformation truckInfoMatteo = new TruckInformation("Matteo Vanin", Globals.body3QualisysId, "matteo.png", "matteoTruck.png", "matteoAndTruck.png", "+46720574526");
        truckInfoMatteo.setTruckFuel(240);
        truckInfoMatteo.setTruckModel("Scania R260");
        truckInformationList.add(truckInfoMatteo);
        truckIds[2] = Globals.body3QualisysId;
        
        TruckInformation truckInfoLima = new TruckInformation("Pedro Lima", 98, "lima.png", "limaTruck.png", "limaAndTruck.png", "+46720775142");
        truckInfoLima.setTruckFuel(120);
        truckInfoLima.setTruckModel("Scania G440");
        truckInformationList.add(truckInfoLima);
        truckIds[3] = 98;
        
        TruckInformation truckInfoJonas = new TruckInformation("Jonas Mårtensson", 99, "jonas.png", "jonasTruck.png", "jonasAndTruck.png", "+46720985635");
        truckInfoJonas.setTruckFuel(60);
        truckInfoJonas.setTruckModel("Scania R560 V8");
        truckInformationList.add(truckInfoJonas);
        truckIds[4] = 99;
        
        Globals.missionManager = new MissionManager(truckIds);
        
        
    }
    
    public void makeRipple(Group someRoot, int selectedId){
        
        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
                        
        Group ripplesGroup = (Group) someRoot.lookup("#ripplesGroup");
        
        if ( ripplesGroup == null ){
            
            ripplesGroup = new Group();
            ripplesGroup.setId("ripplesGroup");
            someRoot.getChildren().add(ripplesGroup);
            
        }else{
            
            ripplesGroup.getChildren().clear();
            
        }
        
        for (QualisysBody body : currentQualisysBodiesList){
            
            if ( body.getId() != selectedId ){
                
                continue;
                
            }
            
            double truckLength = 4.0;
                        
            double xPixel = DrawingBaseFunctions.convertToPixelX( body.getX() + ( (truckLength/2.0) /32.0)*Math.cos( Math.toRadians( body.getTheta()) ) );
            double yPixel = DrawingBaseFunctions.convertToPixelY( body.getY() + ( (truckLength/2.0) /32.0)*Math.sin( Math.toRadians( body.getTheta()) ) );
            
            int maxCircles = 5;
            double initialRadius = 10.0;
            double endZoom = 2.0;
            
            ParallelTransition pt = new ParallelTransition();
            
//            double baseDurationAnimation = 2000;
            double baseDurationAnimation = 5000;
            
            for (int i = 0; i < maxCircles; i++){
            
                Circle rippleCircle = new Circle();
                rippleCircle.setRadius(initialRadius);

                rippleCircle.setFill(Color.TRANSPARENT);

                rippleCircle.setLayoutX(xPixel);
                rippleCircle.setLayoutY(yPixel);

                rippleCircle.setStroke(Color.WHITE);
                rippleCircle.setStrokeWidth(1.0);
                
                rippleCircle.setOpacity(0.);

                ripplesGroup.getChildren().add(rippleCircle);
                
                
                
                double durationAnimation = baseDurationAnimation*(((double)(i+1) )/((double)maxCircles) );
                
                FadeTransition ft = new FadeTransition(Duration.millis(durationAnimation), rippleCircle);
                ft.setFromValue(1.0);
                ft.setToValue(0.0);
                ft.setCycleCount(1);
                ft.setAutoReverse(false);
               
                ScaleTransition st = new ScaleTransition(Duration.millis(durationAnimation), rippleCircle);
                st.setByX(endZoom);
                st.setByY(endZoom);
                st.setCycleCount(1);
                st.setAutoReverse(false);
                
                PauseTransition pauseTransition = new PauseTransition( Duration.millis( baseDurationAnimation - durationAnimation ));
                
                SequentialTransition seqTransitionA = new SequentialTransition (pauseTransition, ft);
                SequentialTransition seqTransitionB = new SequentialTransition (pauseTransition, st);

                pt.getChildren().addAll(ft, st);
            
            }
            
            pt.play();
        
        
        }
        
    }
    
    public void clearRipple(Group someRoot){
        
        Group ripplesGroup = (Group) someRoot.lookup("#ripplesGroup");
        
        if ( ripplesGroup != null ){
            
            ripplesGroup.getChildren().clear();
            
        }
        
    }
    
    public Group getTruckMenuElement(Group truckMenuRoot, Group trajectoryRoot, TruckInformation truckInformation, double elementWidth, double elementHeight){
        
        Group truckMenuElementGroup = new Group();
        truckMenuElementGroup.setId( "truckId"+truckInformation.getTruckId() );
        
        Rectangle backgroundRectangle = new Rectangle();
        backgroundRectangle.setWidth(elementWidth);
        backgroundRectangle.setHeight(elementHeight);
        backgroundRectangle.setArcWidth(20);
        backgroundRectangle.setArcHeight(20);
        backgroundRectangle.setFill(Color.SLATEGRAY);
        
        truckMenuElementGroup.getChildren().add(backgroundRectangle);
        
        Image truckDriverImage = truckInformation.getTruckDriverImage();
        ImageView truckDriverImageView = new ImageView(truckDriverImage);
        
        double photoWidthRatio = 0.2;
        
        truckDriverImageView.setPreserveRatio(true);
        truckDriverImageView.setFitWidth(elementWidth*photoWidthRatio);
        
        Rectangle statusRectangle = new Rectangle();
        statusRectangle.setWidth(elementWidth*photoWidthRatio);
        statusRectangle.setHeight(elementWidth*photoWidthRatio);
        statusRectangle.setFill(Color.RED);
        statusRectangle.setOpacity(0.6);
        
        statusRectangle.setId("statusRectangle");
        
        double imageLayoutX = 0.25*photoWidthRatio*elementWidth;
        double imageLayoutY = elementHeight/2.0 - truckDriverImageView.getFitWidth()/2.;
        
        truckDriverImageView.setLayoutX( imageLayoutX );
        truckDriverImageView.setLayoutY( imageLayoutY );
        
        statusRectangle.setLayoutX( imageLayoutX );
        statusRectangle.setLayoutY( imageLayoutY );
               
        truckMenuElementGroup.getChildren().add(statusRectangle);
        truckMenuElementGroup.getChildren().add(truckDriverImageView);
        
        Image truckImage = truckInformation.getTruckImage();
        double truckImageRatio = truckImage.getHeight()/truckImage.getWidth();
        
        ImageView truckImageView = new ImageView(truckImage);
        
        double truckWidthRatio = 1.0 - photoWidthRatio - 3.0*(photoWidthRatio/4.0);
        
        truckImageView.setPreserveRatio(true);
        truckImageView.setFitWidth(elementWidth*truckWidthRatio);
        
        truckImageView.setLayoutX( photoWidthRatio*elementWidth + 0.5*photoWidthRatio*elementWidth );
        truckImageView.setLayoutY( elementHeight/2.0 - truckImageView.getFitWidth()*truckImageRatio/2.0 );
           
        truckMenuElementGroup.getChildren().add(truckImageView);
        
        EventHandler stateClickEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                MouseButton pressedButton = arg0.getButton();
                
                if(pressedButton == MouseButton.PRIMARY){
                    
                    
                    highlightedId = truckInformation.getTruckId();   
                    makeRipple(truckMenuRoot, highlightedId);
                }
                if(pressedButton == MouseButton.SECONDARY){                
                
                    clearRipple(truckMenuRoot);
                    highlightedId = truckInformation.getTruckId();   
                    boolean highlight = true;
                    String messageToSend = null;
                    
                    messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(truckInformation.getTruckId(), highlight);
                                        
                    if (messageToSend != null){

                        Globals.smlCommunications.sendOutputMessage(messageToSend);     
                        System.out.println("Sent highlight request = " + messageToSend);
                    }

//                    showTruckOrdersMenu(truckMenuRoot, trajectoryRoot, truckInformation);
                    
                }

            }
        };
        
        truckMenuElementGroup.setOnMouseClicked(stateClickEvent);
        
        EventHandler swipeEventHandler = new EventHandler<SwipeEvent>() {
            @Override public void handle(SwipeEvent event) {

                System.out.println("SWIPPEED!!!");
                
                clearRipple(truckMenuRoot);
                highlightedId = truckInformation.getTruckId();   
                boolean highlight = true;
                String messageToSend = null;
                
                messageToSend = Globals.xmlProcesser.getHighlightRequestMessageString(truckInformation.getTruckId(), highlight);
                
                if (messageToSend != null){

                    Globals.smlCommunications.sendOutputMessage(messageToSend);     
                    System.out.println("Sent highlight request = " + messageToSend);
                }

//                showTruckOrdersMenu(truckMenuRoot, trajectoryRoot, truckInformation);

                event.consume();
            }
        };
        
        truckMenuElementGroup.setOnSwipeRight(stateClickEvent); 
        truckMenuElementGroup.setOnSwipeLeft(stateClickEvent); 
         
        EventHandler enterTruckerEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                highlightedId = truckInformation.getTruckId();   
//                makeRipple(truckMenuRoot, highlightedId);
                
            }
        };
                
        truckMenuElementGroup.setOnMouseEntered(enterTruckerEvent);    
        
        EventHandler exitTruckerEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent arg0) {

                highlightedId = -1000;                    

            }
        };
        
        truckMenuElementGroup.setOnMouseExited(exitTruckerEvent);        
        
        return truckMenuElementGroup;
        
    }
        
//    public void createTrucksMenu(Group truckMenuRoot, Group trajectoryRoot){
//        
//        createTruckInformationList();
//        
//        Rectangle menuBackground = new Rectangle();
//        menuBackground.setX(windowWidth*Globals.mapXRatio);
//        menuBackground.setY(0);
//        menuBackground.setWidth(windowWidth*(1.0-Globals.mapXRatio));
//        menuBackground.setHeight(windowHeight);
//        menuBackground.setFill(Color.DARKSLATEGREY);
//
//        truckMenuRoot.getChildren().add(menuBackground);
//        
//        Text text = new Text();
//
//        text.setFont( Font.font("Verdana", FontWeight.BOLD, 30) );
//        text.setText("VEHICLES");
//        
//        truckMenuRoot.getChildren().add(text);
//        
//        System.out.println("text.getBoundsInLocal().getWidth()/2.0 = " + text.getBoundsInLocal().getWidth()/2.0);
//                
//        text.setLayoutX( windowWidth*Globals.mapXRatio + windowWidth*(1-Globals.mapXRatio)*0.5 - 0.5*text.getBoundsInParent().getWidth() );
//        text.setLayoutY( text.getBoundsInParent().getHeight()*(3.0/2.0) );
//        
//        text.setFill(Color.ALICEBLUE);
//        text.setId("availableText");
//        
//        double elementWidth = windowWidth*(1.0-Globals.mapXRatio);
//        double elementHeight = (windowHeight - 2*text.getBoundsInLocal().getHeight() )/truckInformationList.size();
//        int truckCounter = 1;
//        
//        for (TruckInformation truckInformation : truckInformationList){
//        
//            Group currentGroup = getTruckMenuElement(truckMenuRoot, trajectoryRoot, truckInformation, 0.9*elementWidth, 0.9*elementHeight);
//            currentGroup.setLayoutX( windowWidth*Globals.mapXRatio + windowWidth*(1.0-Globals.mapXRatio)*0.5 - currentGroup.getBoundsInLocal().getWidth()/2.0 );
//            currentGroup.setLayoutY( ((double)(truckCounter-1))*elementHeight + 2*text.getBoundsInLocal().getHeight() );
////            truckMenuRoot.getChildren().add(currentGroup);
//            truckCounter++;           
//            
//        }
//        
//        System.out.println("updateTrucksMenu(truckMenuRoot)");
//        updateTrucksMenu(truckMenuRoot);
//        
//    }
////        
//    public void updateTrucksMenu(Group truckMenuRoot){
//        
//        List<QualisysBody> currentQualisysBodiesList = Globals.smlWorldInfo.getQualisysBodiesList();
//        List<QualisysBody> availableTruckBodiesList = new ArrayList<>();
//       
//        for ( int i = 0 ; i < truckMenuRoot.getChildren().size() ; i++ ){
//            
//            Node currentNode = truckMenuRoot.getChildren().get(i);
//           
//            if ( currentNode.getId() == null ){
//                continue;
//            }
//            
//            if ( !currentNode.getId().contains("truckId") ){
//                continue;
//            }
//            
//            Group currentGroup = (Group) currentNode;
//            
//            Rectangle statusRectangle = (Rectangle) currentGroup.lookup("#statusRectangle");
//                        
//            if ( statusRectangle != null ){
//                
//                for (QualisysBody body : currentQualisysBodiesList){
//                   
//                    if ( currentGroup.getId().equals("truckId"+body.getId()) ){
//                        
//                        boolean isTruckAvailable = Globals.missionManager.isTruckAvailable(body.getId());
//                        
//                        System.out.println("body.isControllable() = " + body.isControllable() );
//                        System.out.println("isTruckAvailable = " + isTruckAvailable );
//                        
//                        if ( body.isControllable() ){
//                                           
//                            if ( isTruckAvailable ){
//
//                                if ( body.getX() < -9000.0 ){
//                                    statusRectangle.setFill(Color.YELLOW);
//                                }else{
//                                    statusRectangle.setFill(Color.GREEN);
//                                }
//
//                            }else{
//
//                                statusRectangle.setFill(Color.CYAN);
//
//                            }
//                        
//                        }else{
//                            
//                            statusRectangle.setFill(Color.RED);
//                            
//                        }
//                        
//                        break;
//                                                        
//                    }else{
//                        
//                        // If the group corresponds to a ID not detected:
//                                                
//                        statusRectangle.setFill(Color.RED);
//                        
//                    }
//                                        
//                }    
//                
//            }
//            
//        }
//        
//                
//        List<Integer> onlineBodiesList = new ArrayList<Integer>();
//        List<Integer> offlineBodiesList = new ArrayList<>();
//        
//        
//        for (QualisysBody body : availableTruckBodiesList){
//            if ( body.isControllable() ){
//                
//                onlineBodiesList.add(body.getId());
//                                
//            }else{
//                
//                offlineBodiesList.add(body.getId());
//                
//            }
//        }
//        
//        Collections.sort( onlineBodiesList );
//        Collections.sort( offlineBodiesList );
//        
//        
//        int availableTrucksCounter = 0;
//        
//        for (QualisysBody body : availableTruckBodiesList){
//                   
//            
//            Group currentTruckGroup = (Group) truckMenuRoot.lookup( "#truck"+body.getId() );
//            
//            int truckCounter = 1;
//            
//            if ( !body.isControllable() ){
//                truckCounter = onlineBodiesList.size();
//
//                truckCounter = truckCounter + offlineBodiesList.indexOf(body.getId());
//            }else{
//                
//                truckCounter = onlineBodiesList.indexOf(body.getId());
//            }
//            
//            System.out.println("truckCounter = " + truckCounter);
//            
//            Text tempText = (Text) currentTruckGroup.lookup("#availableText");
//        
//            double elementHeight = (windowHeight - 2*tempText.getBoundsInLocal().getHeight() )/truckInformationList.size();
//            
//            currentTruckGroup.setLayoutY( ((double)(truckCounter-1))*elementHeight + 2*tempText.getBoundsInLocal().getHeight() );
//            
//            if (currentTruckGroup == null){
//                
//                System.out.println("CREATING");
//            
//                currentTruckGroup = new Group();
//                
//                currentTruckGroup.setId( "#truck"+body.getId() );
//                
//                Image image = new Image("file:truck.png");
//
//                if ( image.getWidth() < 1.0 ){
//
//                    System.err.println("Image not found! (truck.png)");
//
//                }
//                
//                ImageView imageView = new ImageView(image);
//
//                double imageRatio = image.getWidth()/image.getHeight();
//
//                imageView.setPreserveRatio(true);
//
//                double truckMargin = 0.1*windowWidth*(1.0 - Globals.mapXRatio);
//                imageView.setFitWidth( windowWidth*(1.0 - Globals.mapXRatio) - 2*truckMargin);
//
//                double imageHeight = imageView.getFitWidth()/imageRatio;
//
//                imageView.setLayoutX( windowWidth*Globals.mapXRatio + truckMargin);
//                imageView.setLayoutY( ( (double)(availableTrucksCounter+1) )*(1.2)*imageHeight );
//                availableTrucksCounter++;
//                
//                currentTruckGroup.getChildren().add(imageView);
//                
//                truckMenuRoot.getChildren().add(currentTruckGroup);
//                
//            }
//            
//        }
//        
//        
//        
//    }
//    
//    
    
    
    
}
