package testingcommunications;

import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rui
 */
public class SMLWorldInformation {
    
    List<OsmNode> nodeList;
    List<OsmWay> wayList;
    List<TrajectoryPoint> trajectoryPointList;
    List<QualisysBody> qualisysBodiesList;
    
    double centerPixelX;
    double centerPixelY;
    double pixelPerMeterX;
    double pixelPerMeterY;
    
    public SMLWorldInformation(){
        
        nodeList = new ArrayList();
        wayList = new ArrayList();
        trajectoryPointList = new ArrayList();
        qualisysBodiesList = new ArrayList();
        
        centerPixelX = 0;
        centerPixelY = 0;
        pixelPerMeterX = 0;
        pixelPerMeterY = 0;
        
    }
    
    public List<QualisysBody> getQualisysBodiesList(){
        
        return qualisysBodiesList;
        
    }
    
    
    
    
}
