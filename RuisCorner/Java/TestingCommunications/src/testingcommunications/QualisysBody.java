/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testingcommunications;

/**
 *
 * Container for QualisysBodies.
 * Note: QualisysBody is expected to be in Smart Mobility Lab dimensions:
 * x and y somewhere between the -4 and 4 meters and the angle in degrees 
 * (probably between -180 and 180).
 * 
 * @author rui
 */
public class QualisysBody {
    
    /**
    * The current x position of the body.
    * (Note: Expected to be in Smart Mobility Lab dimensions: somewhere between
    * the -4 and 4 meters)
    */
    private double x;
    
    /**
    * The current y position of the body.
    * (Note: Expected to be in Smart Mobility Lab dimensions: somewhere between
    * the -4 and 4 meters)
    */
    private double y;
    
    /**
    * The current theta angle of the body.
    * (Note: Expected to be in Smart Mobility Lab dimensions: somewhere between
    * -180 and 180 degrees)
    */
    private double theta;    
    
    private double networkConnectivity;
    
    private int id;
    private boolean controllable;
    private boolean obstacle;
    private String obstacleType;
    private String bodyType;
    
    public double width;
    public double height;
    public double radius;
    
    public boolean isTrailer;
    
    /**
     * Creates a QualisysBody assuming that x = 0, y = 0, theta = 0, id = 0, and
     * controllable = false.
     * Note: QualisysBody is expected to be in Smart Mobility Lab dimensions:
     * x and y somewhere between the -4 and 4 meters and the angle in degrees 
     * (probably between -180 and 180).
     */
    public QualisysBody(){
        
        x = 0;
        y = 0;
        theta = 0;
        id = 0;
        controllable = false;
        networkConnectivity = 0;
        isTrailer = false;
        
    }
    
    /**
     * 
     * Creates a QualisysBody with the given parameters.
     * Note: QualisysBody is expected to be in Smart Mobility Lab dimensions:
     * x and y somewhere between the -4 and 4 meters and the angle in degrees 
     * (probably between -180 and 180).
     * 
     * @param argX
     * @param argY
     * @param argTheta
     * @param argId
     * @param argControllable
     */
    public QualisysBody(double argX, double argY, double argTheta, int argId, boolean argControllable, double argNetworkConnectivity){
        
        x = argX;
        y = argY;
        theta = argTheta;
        id = argId;
        controllable = argControllable;
        obstacle = false;
        obstacleType = "";
        bodyType = "";
        networkConnectivity = argNetworkConnectivity;
        isTrailer = false;
        
    }
    
    /**
     *
     * @param argX
     */
    public void setX(double argX){
        x = argX;
    }

    /**
     *
     * @param argY
     */
    public void setY(double argY){
        y = argY;
    }

    /**
     *
     * @param argTheta
     */
    public void setTheta(double argTheta){
        theta = argTheta;
    }

    /**
     *
     * @param argId
     */
    public void setId(int argId){
        id = argId;
    }

    /**
     * 
     * Gets the X position of this QualisysBody.
     * 
     * (Note: Expected to be in Smart Mobility Lab dimensions: somewhere between
     * the -4 and 4 meters)
     * 
     * @return
     */
    public double getX(){
        return x;
    }

    /**
     * 
     * Gets the Y position of this QualisysBody.
     * 
     * (Note: Expected to be in Smart Mobility Lab dimensions: somewhere between
     * the -4 and 4 meters)
     * 
     * @return
     */
    public double getY(){
        return y;
    }

    /**
     * 
     * Gets the Theta angle of this QualisysBody.
     * 
     * (Note: Expected to be in Smart Mobility Lab dimensions: somewhere between
     * the -180 and 180 degrees)
     * 
     * @return
     */
    public double getTheta(){
        return theta;
    }

    /**
     *
     * @return
     */
    public int getId(){
        return id;
    }
    
    /**
     * 
     * Gets the Network Connectivity this QualisysBody.
     * 
     * (Note: Expected to be between 0 and 1)
     * 
     * @return
     */
    public double getNetworkConnectivity(){
        return networkConnectivity;
    }

    /**
     *
     * @return
     */
    public boolean isControllable(){
        return controllable;
    }
    
    /**
     *
     * @param argObstacleType
     */
    public void setObstacleType(String argObstacleType){
        
        obstacle = true;
        obstacleType = argObstacleType;
        
    }
    
    
     /**
     *
     *  Sets the width (should be used with obstacles of type rectangle or rrt)
     * 
     * @param argWidth
     */
    public void setWidth(double argWidth){
        
        width = argWidth;
        
    }
    
    /**
     *
     *  Sets the height (should be used with obstacles of type rectangle or rrt)
     * 
     * @param argHeight
     */
    public void setHeight(double argHeight){
        
        height = argHeight;
        
    }
    
    /**
     *
     *  Sets the radius (should be used with obstacles of type gun or circle)
     * 
     * @param argRadius
     */
    public void setRadius(double argRadius){
        
        radius = argRadius;
        
    }
    
    /**
     *
     *  The body type can be: antenna
     * 
     * @param argBodyType
     */
    public void setBodyType(String argBodyType){
        
        bodyType = argBodyType;
        
    }
    
   
    
    /**
     *
     * @return
     */
    public boolean isObstacle(){
        
        return obstacle;
        
    }
    
    /**
     *
     * @return
     */
    public String getObstacleType(){
        
        return obstacleType;
        
    }
    
    /**
     *
     * The body type can be antenna.
     * 
     * @return
     */
    public String getBodyType(){
        
        return bodyType;
        
    }
    
    /**
     *
     * Sets the boolean isTrailer property
     * 
     * @return
     */
    public void setTrailer(boolean argIsTrailer){
        
        isTrailer = argIsTrailer;
        
    }
       
    
}
