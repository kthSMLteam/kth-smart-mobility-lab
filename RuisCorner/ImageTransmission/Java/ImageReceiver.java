/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

import java.io.*;
import java.net.*;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

//import parsingosm.CoordinateConversion;
//import parsingosm.OSMNode;

public class ImageReceiver {
    
    static boolean connection_request = false;
    
    public static void main(String[] args) throws IOException, InterruptedException, Exception {
        
        System.out.println("Starting server...");
        
        if (args.length != 1) {
            System.err.println("Usage: java ReceiveXMLOverSocket <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        
        System.out.println("Server is now online and waiting to accept a connetion...");
        try (
            // Creates a server socket listening on the port given in the command line
            ServerSocket serverSocket =
                new ServerSocket(Integer.parseInt(args[0]));
            
            // Creates a socket accepting incoming connections
            
            Socket clientSocket = serverSocket.accept();     
                
            // Creates a printer to send information through the socket
            PrintWriter out =
                new PrintWriter(clientSocket.getOutputStream(), true);    
                
            // Creates a buffer to receive information through the socket
//            BufferedReader in = new BufferedReader(
//                new InputStreamReader(clientSocket.getInputStream()));
            
                
        ) {
            String inputLine;
            String outputLine;
            
            System.out.println("Connection estabilished, waiting for messages...");
            
//            inputLine = in.readLine();
            
//            System.out.println("inputLine = " + inputLine);
//            System.out.println("inputLine.length() = " + inputLine.length());
           
            String filename = "receivedImage.bmp";
            
            receiveImage(clientSocket, filename);
          

        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
    
    
    public static void receiveImage(Socket receivingSocket, String filename) throws FileNotFoundException, IOException{
        
        InputStream is = receivingSocket.getInputStream();
        
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;

        fos = new FileOutputStream(filename);
        bos = new BufferedOutputStream(fos);

        int bufferSize = receivingSocket.getReceiveBufferSize();
        byte[] bytes = new byte[bufferSize];

        int count;

        while ((count = is.read(bytes)) > 0) {
            bos.write(bytes, 0, count);
        }

        bos.flush();
        bos.close();          
        
        
    }
    

}