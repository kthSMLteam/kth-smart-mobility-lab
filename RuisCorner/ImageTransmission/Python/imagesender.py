# import socket
# from sendfile import sendfile

# print "Loading file"
# file = open("world_surface.bmp", "rb")

# blocksize = os.path.getsize("somefile")

# print "Connecting to receiver socket"
# sock = socket.socket()
# sock.connect(("130.237.50.246", 8001))

# offset = 0

# while True:
#     sent = sendfile(sock.fileno(), file.fileno(), offset, blocksize)
#     if sent == 0:
#         break  # EOF
#     offset += sent


import socket


def send_image_file_over_socket(socket, filename):

	file = open(filename, "rb")

	while True:
		chunk = file.read(65536)
		if not chunk:
			break  # EOF
		socket.sendall(chunk)


print "Connecting to receiver socket"
sock = socket.socket()
sock.connect(("130.237.50.246", 8001))

filename = "world_surface.bmp"

send_image_file_over_socket(sock, filename)