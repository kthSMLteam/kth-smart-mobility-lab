import utm
import xml.etree.ElementTree as ET
import customclasses as cc
from laneletlibrary import *
import time
import math
import aiddrawings
import xmlcomms
from Tkinter import *

XML_file = 'myFirstCity.xml'

tree = ET.parse(XML_file)
xml_root = tree.getroot()

print xml_root.tag
print xml_root.attrib

osm_node_list = create_osm_node_list(xml_root)
print len(osm_node_list)

osm_way_list = create_osm_way_list(xml_root)

osm_lanelet_list = create_osm_lanelet_list(osm_way_list, xml_root)

adjacency_matrix = create_lanelet_adjacency_matrix(osm_lanelet_list, osm_way_list, osm_node_list)

source_id = 34
destination_id = 42

distances, previous_node = dijkstra_algorithm(adjacency_matrix, source_id)

shortest_path = get_shortest_path(previous_node, destination_id)
lanelet_path = convert_to_lanelet_id(shortest_path, osm_lanelet_list) 

[traj_x, traj_y] = get_trajectory(lanelet_path, osm_lanelet_list, osm_node_list)

aiddrawings.visualise_map_shortest_path_and_trajectory(osm_node_list, osm_way_list, osm_lanelet_list, shortest_path, traj_x, traj_y, source_id, destination_id)
# [click_one_position, click_two_position] = aiddrawings.draw_lanelets_and_get_input(osm_node_list, osm_way_list, osm_lanelet_list)


# temp_cnt = 0osm_lanelet_list
# for node in osm_node_list:osm_lanelet_list

# 	print "node.pixel_x = " + str(node.pixel_x) + " node.pixel_y = " + str(node.pixel_y)
# 	temp_cnt = temp_cnt + 1
# 	if temp_cnt > 10:
# 		breakSystem.out.println("in.read() = " + in.read());

# way_id = 10;

# print "osm_way_list[way_id].id"
# print osm_way_list[way_id].id
# for node_id in osm_way_list[way_id].node_ids:

# 	temp_node = get_osm_node_by_id(osm_node_list, node_id)
# 	print "temp_node.pixel_x = " + str(temp_node.pixel_x) + " temp_node.pixel_y = " + str(temp_node.pixel_y)

xmlcomms.create_server(osm_lanelet_list, osm_node_list, osm_way_list)

# # xmlcomms.send_initial_xml(osm_node_list, osm_way_list)

# # print click_one_position
# # print click_two_position

# [closest_lanelets_start, distance] = find_closest_lanelets(click_one_position, osm_lanelet_list, osm_node_list)
# print "closest_lanelets_start: " + str(closest_lanelets_start) + " distance: " + str(distance)

# [closest_lanelets_end, distance] = find_closest_lanelets(click_two_position, osm_lanelet_list, osm_node_list)
# print "closest_lanelets_end: " + str(closest_lanelets_end) + " distance: " + str(distance)

# shortest_path = find_possible_paths_between_lanelets(closest_lanelets_start, closest_lanelets_end, adjacency_matrix)

# print "shortest_path: " + str(shortest_path)

# lanelet_path = convert_to_lanelet_id(shortest_path, osm_lanelet_list) 
# [traj_x, traj_y] = get_trajectory(lanelet_path, osm_lanelet_list, osm_node_list)
# # aiddrawings.visualise_map_shortest_path_and_trajectory(osm_node_list, osm_way_list, osm_lanelet_list, shortest_path, traj_x, traj_y, source_id, destination_id)