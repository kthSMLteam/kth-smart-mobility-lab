import utm
import xml.etree.ElementTree as ET
import customClasses as cc
from laneletLibrary import *
from Tkinter import *
import time
import math

XML_file = 'myFirstCityBi.xml'
# XML_file = 'ScaniaLaneletsLayer2.xml'
# XML_file = 'twoScaniaLanelets.xml'

tree = ET.parse(XML_file)
xml_root = tree.getroot()

print xml_root.tag
print xml_root.attrib

OSMNodeList = create_OSMNodeList(xml_root)

OSMWayList = create_OSMWayList(xml_root)

OSMLaneletList = create_OSMLaneletListBi(OSMWayList, xml_root)

adjacency_matrix = create_Lanelet_Adjacency_Matrix(OSMLaneletList, OSMWayList, OSMNodeList)

# adjacency_matrix = [ [0, 14, 50], [0, 0, 13], [0, 0, 0] ]
# source_id = 0

# Unidirectional
# adjacency_matrix = [ [0, 29, 11, 27, 0], [0, 0, 0, 1, 1], [0, 0, 0, 12, 0], [0, 0, 0, 0, 13], [0, 0, 0, 0, 0] ]
# Bidirectional
# adjacency_matrix = [ [0, 29, 11, 27, 0], [29, 0, 0, 1, 1], [11, 0, 0, 12, 0], [27, 1, 12, 0, 13], [0, 1, 0, 13, 0] ]
source_id = 34
# 5
destination_id = 42

distances, previous_node = dijkstra_algorithm(adjacency_matrix, source_id)

shortest_path = get_shortest_path(previous_node, destination_id)
# shortest_path = [32]
# shortest_path = [30]
lanelet_path = convert_to_lanelet_id(shortest_path, OSMLaneletList) 

# print 'distances:'
# print distances
# print 'previous_node:'
# print previous_node
# print 'shortest_path:'
# print shortest_path
# print 'lanelet_path:'
# print lanelet_path

[traj_x, traj_y] = get_trajectory(lanelet_path, OSMLaneletList, OSMNodeList)

# print traj_x
# print traj_y

# print 'OSMLaneletList[-1].id = ' + str(OSMLaneletList[-1].id)
# print 'OSMLaneletList[-1].left_OSMWay.id = ' + str(OSMLaneletList[-1].left_OSMWay.id)
# print 'OSMLaneletList[-1].left_OSMWay.node_ids = ' + str(OSMLaneletList[-1].left_OSMWay.node_ids)



def draw_circle(x, y, min_x, min_y, canvas_handler, canvas_height, shrinking_ratio, fill_color = "black"):
	"function used to draw a line between two OSMNodes"

	x1 = x
	y1 = y

	# x2 = OSMNode2.x
	# y2 = OSMNode2.y

	# print "Traj draw"
	circle_radius = .01
	top_left_x = x1 - circle_radius
	top_left_y = y1 + circle_radius
	bottom_right_x = x1 + circle_radius
	bottom_right_y = y1 - circle_radius

	canvas_handler.create_oval( (top_left_x - min_x)*shrinking_ratio, canvas_height - (top_left_y - min_y)*shrinking_ratio, (bottom_right_x - min_x)*shrinking_ratio, canvas_height - (bottom_right_y - min_y)*shrinking_ratio, outline = "red", fill = "red", width = 2)

	# canvas_handler.create_line( (x1 - min_x)*shrinking_ratio, canvas_height - (y1 - min_y)*shrinking_ratio, (x2 - min_x)*shrinking_ratio, canvas_height - (y2 - min_y)*shrinking_ratio , fill = fill_color)

	return

def draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, canvas_handler, canvas_height, shrinking_ratio, fill_color = "black"):
	"function used to draw a line between two OSMNodes"

	x1 = OSMNode1.x
	y1 = OSMNode1.y

	x2 = OSMNode2.x
	y2 = OSMNode2.y

	canvas_handler.create_line( (x1 - min_x)*shrinking_ratio, canvas_height - (y1 - min_y)*shrinking_ratio, (x2 - min_x)*shrinking_ratio, canvas_height - (y2 - min_y)*shrinking_ratio , fill = fill_color)

	return

min_x = 10e+10; max_x = - 10e+10;
min_y = 10e+10; max_y = - 10e+10;

for node in OSMNodeList:

	if ( min_x > node.x ):
		min_x = node.x
	if ( max_x < node.x ):
		max_x = node.x
	if ( min_y > node.y ):
		min_y = node.y
	if ( max_y < node.y ):
		max_y = node.y

viewing_gap = 0
min_x = min_x - viewing_gap
min_y = min_y - viewing_gap
max_x = max_x + viewing_gap
max_y = max_y + viewing_gap

world_width = max_x - min_x;
world_height = max_y - min_y;

canvas_width = 1800;
canvas_height = 1000;

if (world_width > world_height):

	shrinking_ratio = canvas_width/world_width

else:

	shrinking_ratio = canvas_height/world_height

# shrinking_ratio = shrinking_ratio*

draw = False
if draw:

	master = Tk()
	w = Canvas(master, width=canvas_width, height=canvas_height)
	w.pack()

	# coordinate system origin: upper-left corner



	for way in OSMWayList:

		for idx in xrange(0, len(way.node_ids)-1):

			# print 'way.node_ids[idx]' , way.node_ids[idx]
			# print 'type(way.node_ids[idx])' , type(way.node_ids[idx])

			OSMNode1 = get_OSMNode_by_id(OSMNodeList, way.node_ids[idx])
			OSMNode2 = get_OSMNode_by_id(OSMNodeList, way.node_ids[idx+1])


			draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w, canvas_height, shrinking_ratio*(8/6))


	color_list = ['red', 'yellow', 'blue', 'green', 'pink', 'yellow']

	color_id = 0
	lanelet_id = 0

	for traj_index in range( len( traj_x ) ):


		print "Traj draw"
		circle_radius = 1.0
		top_left_x = traj_x[traj_index] - circle_radius
		top_left_y = traj_y[traj_index] + circle_radius
		bottom_right_x = traj_x[traj_index] + circle_radius
		bottom_right_y = traj_y[traj_index] - circle_radius
		draw_circle(traj_x[traj_index], traj_y[traj_index], min_x, min_y, w, canvas_height, shrinking_ratio*(8/6))
 		# w.create_oval(traj_x[traj_index], top_left_y, bottom_right_x, bottom_right_y, outline = "red", fill = "red", width = 2)



	for lanelet in OSMLaneletList:

		current_color = 'black'
		if lanelet_id in shortest_path:
			current_color = 'blue'
		if lanelet_id == source_id:
			current_color = 'red'
		if lanelet_id == destination_id:
			current_color = 'green'

		# if lanelet_id != source_id:
		# 	lanelet_id = lanelet_id + 1
		# 	continue

		if current_color == 'black':
			lanelet_id = lanelet_id + 1
			continue

		lanelet_id = lanelet_id + 1	

		print "Drawing a lanelet"

		

		

		for idx in xrange(0, len(lanelet.left_OSMWay.node_ids)-1):

			OSMNode1 = get_OSMNode_by_id(OSMNodeList, lanelet.left_OSMWay.node_ids[idx])
			OSMNode2 = get_OSMNode_by_id(OSMNodeList, lanelet.left_OSMWay.node_ids[idx+1])

			draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w, canvas_height, shrinking_ratio*(8/6), current_color)

		for idx in xrange(0, len(lanelet.right_OSMWay.node_ids)-1):

			OSMNode1 = get_OSMNode_by_id(OSMNodeList, lanelet.right_OSMWay.node_ids[idx])
			OSMNode2 = get_OSMNode_by_id(OSMNodeList, lanelet.right_OSMWay.node_ids[idx+1])

			draw_line_between_nodes(OSMNode1, OSMNode2, min_x, min_y, w, canvas_height, shrinking_ratio*(8/6), current_color)

		color_id = color_id + 1


	# w.create_line( (x1 - min_x)*shrinking_ratio, canvas_height - (y1 - min_y)*shrinking_ratio, 500, 500)

	# w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
	# w.create_rectangle(50, 25, 150, 75, fill="blue")

	# w.create_line(min_x-min_x, min_)

	mainloop()

