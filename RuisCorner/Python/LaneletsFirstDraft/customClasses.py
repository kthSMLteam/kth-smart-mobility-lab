import utm

# "This class is the OSM Node container"
class OSMNode:
	def __init__(self, id, lat, lon):

		if not isinstance(id, int):
			raise NameError('In class OSMNode: constructor: id must be an integer')

		self.id = id

		if not isinstance(lat, float):
			raise NameError('In class OSMNode: constructor: lat must be a float')

		if not isinstance(lon, float):
			raise NameError('In class OSMNode: constructor: lon must be a float')

		# WARNING: There is a small loss of precision when the string is converted to a float
		self.lat = lat
		self.lon = lon

		# self.x and self.y are variables of type float
		(self.x, self.y, unused, unused) = utm.from_latlon(self.lat, self.lon)


# "This class is the OSM Way container"
class OSMWay:
	def __init__(self, id):

		if not isinstance(id, int):
			raise NameError('In class OSMWay: constructor: id must be an integer')

		self.id = id
		self.node_ids = []

	def add_node_id(self, id):

		if not isinstance(id, int):
			raise NameError('In class OSMWay: add_node_id: id must be an integer')

		self.node_ids.append( id )

class OSMLanelet:
	def __init__(self, id, left_OSMWay, right_OSMWay):

		if not isinstance(id, int):
			raise NameError('In class OSMLanelet: constructor: id must be an integer')

		if not isinstance(left_OSMWay, OSMWay):
			raise NameError('In class OSMLanelet: constructor: left_OSMWay must be of type OSMWay')

		if not isinstance(right_OSMWay, OSMWay):
			raise NameError('In class OSMLanelet: constructor: right_OSMWay must be of type OSMWay')

		self.id = id
		self.left_OSMWay = left_OSMWay
		self.right_OSMWay = right_OSMWay

class RoadTrajectory:
	def __init__(self, x_road, y_road, t_road):

		for num in x_road:
			if not isinstance(num, float):
				raise NameError('In class RoadTrajectory: constructor: x_road is not a list of floats')

		for num in y_road:
			if not isinstance(num, float):
				raise NameError('In class RoadTrajectory: constructor: y_road is not a list of floats')

		for num in t_road:
			if not isinstance(num, float):
				raise NameError('In class RoadTrajectory: constructor: t_road is not a list of floats')

		self.x = x_road
		self.y = y_road
		self.t = t_road

