 #!/bin/usr/python

import pygame
import sys, math, os
import threading
import time, datetime
from xml.etree import ElementTree as ET

import projector
sys.path.append('../')
from mocap import Mocap, Body
from socket import *

def get_current_time():
	return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")

def conv_mm2px(area,image_size):

	x = float(image_size[0])/float(area[1]-area[0])
	y = float(image_size[1])/float(area[3]-area[2])

	return (x,y)

def prettify(elem):
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def update_fps(previous_time,window,font):
	current_time = time.time()
	time_diff = current_time - previous_time
	
	fps = 1.0 / time_diff
	
	draw_fps(fps,window,font)

	return current_time

def draw_origin(window,origin,start_info):
	if start_info.has_key('origin'):	
		if start_info['origin'] == 2:
			pygame.draw.circle(window,(255,255,255),origin,10,4)

def draw_fps(fps,window,font):
   scoretext = font.render("FPS:"+"{:2.2f}".format(fps),True,(255,255,255),(0,0,0))
   window.blit(scoretext, (400, 400))

def draw_bodies(window,bodies_list,start_info):

	global conv
	global origin
	global vehicle_selected

	for body in bodies_list:
		if vehicle_selected != 0:
			if vehicle_selected != body['id']:
				print body['id']
				break
		# 	print vehicle_selected
		# 	if vehicle_selected != body['id']:
		# 		break
		#print body['id']


		try:
			radius = 30
			linewidth = 4
			x = int(origin[0] + body['x']*conv[0])
			y = int(origin[1] - body['y']*conv[1])
			pygame.draw.circle(window,(255,100,255),(x,y),radius,linewidth)
			theta = math.radians(body['a3'])
			pygame.draw.circle(window,(0,0,0),(int(x+radius*math.cos(theta)),int(y-radius*math.sin(theta))),int(radius/4),0)

		except:
			pass
			#print 'Value is NAN'

		#print 'x:'+str(x)+' y:'+str(y)
		#print 'x1:'+str(x+radius*math.cos(body['a3']))+' y1:'+str(y+radius*math.cos(body['a3']))

	draw_origin(window,origin,start_info)

def parse_bodies_xml(data):

	bodies_list = []

	f = open('states.xml','wb')
	f.write(data)
	f.close()


	root = ET.fromstring(data)

	for body_info in root.findall('body_info'):
		body = dict()
		body['x'] = float(body_info.get('x'))
		body['y'] = float(body_info.get('y'))
		body['a3'] = float(body_info.get('theta'))
		body['id'] = int(body_info.get('id'))
		bodies_list.append(body)

	return bodies_list

def start_projector_client(info,client_type):

	HOST = info['projector_host']
	PORT = info['projector_port']

	ADDR = (HOST,PORT)
	BUFSIZE = 4096

	try:
		cli = socket( AF_INET,SOCK_STREAM)
		cli.connect((ADDR))

		data = cli.recv(BUFSIZE)

		if data == 'OK\n':

			cli.send(client_type)

			data = cli.recv(BUFSIZE)

			if data == 'OK':
				return cli
	except:
		return 0

def send_request(cli,request):

	global bodies_list

	BUFSIZE = 4096

	cli.send(request)
 	
 	if request == 'BACKGROUND':
 		
 		with open('background.bmp','wb') as img:
 			while True:
 				cli.settimeout(0.1)
 				try:
 					data = cli .recv(BUFSIZE);
				except:
					break

				img.write(data)
		img.close()

	elif request == 'STATES':
		data = ''
		while True:
			cli.settimeout(0.1)
 			try:
 				data = data + cli.recv(BUFSIZE);
			except:
				break
		
		bodies_list = parse_bodies_xml(data)
		
def states_handler(states_cli):

	thread_count = 1

	thread_vector = []

	while 1:

		for thread_number in xrange(thread_count):
			t = threading.Thread(target=send_request,args=(states_cli,'STATES'))
			t.daemon = True
			t.start()
			thread_vector.append(t)

		for thread in thread_vector:
			thread.join()

		# send_request(states_cli,'STATES')

def start_projector(start_info):
	global origin
	global conv
	global bodies_list
	global vehicle_selected 

	vehicle_selected = 5

	activate_fps = True

	hostname = None 
	port = None

	print get_current_time() + ' Starting Projector Manager'
	cli = start_projector_client(info,'projector-main')
	print get_current_time() + ' Starting adquiring states from SML World'
	states_cli = start_projector_client(info,'projector-states') 

	t = threading.Thread(target=states_handler,args=[states_cli])
	t.daemon = True
	t.start()


	if cli == 0:
		print get_current_time() + ' Problem connecting to SML World'
		return

	pygame.init()
	pygame.font.init()

	font = pygame.font.SysFont('monospace',40)

	print get_current_time() + ' Request for Background Image'
	send_request(cli,'BACKGROUND')
	print get_current_time() + ' Background Image Received'

	bg = pygame.image.load("background.bmp")

	bg_scaled = bg

	initial_size = (640,480)

	area = [-3000,3000,-3000,3000]

	if start_info.has_key('min_x'):
		area[0] = start_info['min_x']
	if start_info.has_key('max_x'):
		area[1] = start_info['max_x']
	if start_info.has_key('min_y'):
		area[2] = start_info['min_y']
	if start_info.has_key('max_y'):
		area[3] = start_info['max_y']

	conv = conv_mm2px(area,initial_size)

	origin = (int(conv[0]*float(area[1])),int(conv[1]*float(area[3])))

	window = pygame.display.set_mode(initial_size,pygame.RESIZABLE)

	pygame.display.set_caption('Projector')

	draw_origin(window,origin,start_info)

	#Qs = Mocap(info=0)

	current_time = time.time()


	while True: 
		
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				#print event
				send_request(cli,'CLOSE')
				cli.close()
				pygame.quit()
				return 0
				#sys.exit(0)
			elif event.type == pygame.VIDEORESIZE:
				window = pygame.display.set_mode(event.size,pygame.RESIZABLE)
				bg_scaled = pygame.transform.scale(bg,event.size)
				conv = conv_mm2px(area,event.size)
				origin = (int(conv[0]*float(area[1])),int(conv[1]*float(area[3])))

				window.blit(bg_scaled,(0,0))

			else:
				pass
				#print event

		
		#bodies_list = send_request(cli,'STATES')
		#bodies_list = Qs.get_updated_bodies()

		window.blit(bg_scaled,(0,0))

		draw_bodies(window,bodies_list,start_info)	

		if activate_fps:
			current_time = update_fps(current_time,window,font)

		pygame.display.flip()


if __name__ == "__main__":

	info = dict()
	info['projector_host'] = '130.237.43.135'
	info['projector_port'] = 8000
	info['origin'] = 2

	bodies_list = []

	start_projector(info)