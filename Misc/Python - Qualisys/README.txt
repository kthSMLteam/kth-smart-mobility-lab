# README

mocap.py is a library containing the functions for getting data from the Qualisys server

Running the library as a function returns a test that
- creates a Mocap object
- finds and display valid bodies of the workspace
- creates a Body object of one of the valid bodies