function [ obj ] = createsf( ref, port)
%CREATESF Summary of this function goes here
%   Detailed explanation goes here

obj.ref=ref;
obj.port=port;

[status cmout] = system('C:\cygwin\bin\motelist');

if ~status
    cmout=textscan(cmout, '%s', 'delimiter', '\n');
    cmout=cmout{:};
    references=cell(1,size(cmout,1)-2);
    coms=cell(1,size(cmout,1)-2);
    for i=3:size(cmout,1)
        [references{i-2}, coms{i-2}]=remove_spaces(textscan(cmout{i}, '%s', 'delimiter', ' '));
    end
    
    for i=1:size(references,2)
        if references{i}==ref
            obj.com=coms{i};
            break
        end
    end
    arg=[num2str(obj.port),' ',num2str(obj.com),' 115200'];
    
    [status cmout] = system(['C:\cygwin\bin\sf.exe ' arg ' &']);
    if ~status
        disp('SF creation successfull')
    end
    
else
   disp('SF creation unsuccessfull') 
end

end

function [reference,com]=remove_spaces(line)
line=line{:};
for j=1:size(line,1)
   if ~isempty(line{j}) && j==1
       reference=line{j};
   elseif ~isempty(line{j}) && j>1 && j<length(line)
       com=line{j};
   end
       
end

end

