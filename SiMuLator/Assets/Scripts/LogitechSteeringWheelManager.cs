using UnityEngine;
using System.Collections;

public class LogitechSteeringWheelManager : MonoBehaviour {

    LogitechGSDK.LogiControllerPropertiesData properties;

	string[] activeForceAndEffect;

	public int cameraView = 0;
	public bool newCameraView = true;

	public int magnitudePercentage;

	public bool steeringFeedback;
	public float steeringDesired = 0.0f;

	public float currentRPM;
	public float rpmFirstLedTurnsOn = 0.0f; 
	public float rpmRedLine = 6000.0f; 
		
	public float pGain = 50.0f;
	public float iGain = 50.0f;
	float iError = 0.0f;
	public float maxWindup = 2.0f;

	public bool useJoystick = true;

	public float debugCurrentSteeringWheelReading;

	float lastTime = -1.0f;

	float maxValue = 32767.0f;

	float initializationTime;
	float calibrationTime = 10.0f;

	public int currentVehicle;
	public int lastPressedPOV;

	public int lastPressedGearShifter;

	public float steeringRatio;
	public float throttleRatio;
	public float brakeRatio;
	public float clutchRatio;
	public int currentGear;

	// Use this for initialization
	void Start () {
       
        activeForceAndEffect = new string[9];
		LogitechGSDK.LogiSteeringInitialize(false);

		initializationTime = Time.time;

		currentVehicle = 0;
		lastPressedPOV = -1;
		lastPressedGearShifter = -1;

	}


	// Update is called once per frame
	void Update () {

		//All the test functions are called on the first device plugged in(index = 0)
		if(LogitechGSDK.LogiUpdate() && LogitechGSDK.LogiIsConnected(0)){
            
			if ( currentGear == 5 ){

				Application.Quit();

			}

            //CONTROLLER PROPERTIES

            LogitechGSDK.LogiControllerPropertiesData actualProperties = new LogitechGSDK.LogiControllerPropertiesData();
            LogitechGSDK.LogiGetCurrentControllerProperties(0, ref actualProperties);
                           
            //CONTROLLER STATE
            LogitechGSDK.DIJOYSTATE2ENGINES rec;
            rec = LogitechGSDK.LogiGetStateUnity(0);
            



			//actualState += "x-axis position :" + rec.lX + "\n";



			//LogiPlayLeds(const int index, const float currentRPM, const float rpmFirstLedTurnsOn, const float rpmRedLine);

//			float LimitsToLimitsConverter(float value, float minValue, float maxValue, float minOutput, float maxOutput)

			//				rec.lX Steering
			debugCurrentSteeringWheelReading = (float)rec.lX;
			steeringRatio = (float)rec.lX;
			steeringRatio = steeringRatio/32000.0f;
			steeringRatio = Mathf.Clamp(steeringRatio, -1.0f, 1.0f);
			//steeringRatio = 0.3f;
			
			//				rec.lY Throttle
			throttleRatio = (float) -rec.lY;
			throttleRatio = (throttleRatio + 32000.0f)/64000.0f;
			throttleRatio = Mathf.Clamp(throttleRatio, 0.0f, 1.0f);
			//throttleRatio = 0.3f;
			
			//				rec.lRz Braking
			brakeRatio = (float) -rec.lRz;
			brakeRatio = (brakeRatio + 32000.0f)/64000.0f;
			brakeRatio = Mathf.Clamp(brakeRatio, 0.0f, 1.0f);
			
			//				rec.rglSlider[1] Clutch
			clutchRatio = (float) -rec.rglSlider[1];
			clutchRatio = (clutchRatio + 32000.0f)/64000.0f;
			clutchRatio = Mathf.Clamp(clutchRatio, 0.0f, 1.0f);


			if ( useJoystick ){

				// Overwriting with Logitech Dual Controller
				throttleRatio = Input.GetAxis("Vertical");
				brakeRatio = throttleRatio;
				throttleRatio = Mathf.Clamp(throttleRatio, 0.0f, 1.0f);


				brakeRatio = Mathf.Clamp(brakeRatio, -1.0f, 0.0f);
				brakeRatio = Mathf.Abs(brakeRatio);

				clutchRatio = 1.0f;

			}

			//actualState += "extra axes positions 2 :" + rec.rglSlider[1] + "\n";

//			Debug.Log("steeringRatio = " + steeringRatio + " throttleRatio = " + throttleRatio + " clutchRatio = " + clutchRatio);

//			actualState += "x-axis position :" + rec.lX + "\n";
//			actualState += "y-axis position :" + rec.lY + "\n";
//			actualState += "z-axis position :" + rec.lZ + "\n";
//			actualState += "x-axis rotation :" + rec.lRx + "\n";
//			actualState += "y-axis rotation :" + rec.lRy + "\n";
//			actualState += "z-axis rotation :" + rec.lRz + "\n";
//			actualState += "extra axes positions 1 :" + rec.rglSlider[0] + "\n";
//			actualState += "extra axes positions 2 :" + rec.rglSlider[1] + "\n";

			int offsetPercentage;
			
			float steeringError = steeringDesired - steeringRatio;
			//float pGain = 75.0f;
			
			float actionCommand = steeringPID(-steeringError);

			//offsetPercentage = (int) (- pGain*steeringError );
			offsetPercentage = (int) (actionCommand);
			
			//Debug.Log("offsetPercentage = " + offsetPercentage);

			//offsetPercentage = -50;


			//LogitechGSDK.LogiPlaySpringForce(0, offsetPercentage, 50, 50);

			if ( Time.time - initializationTime < calibrationTime/2.0f){

				//Debug.Log("Calibrating to the RIGHT");
				updateRpm( (Time.time - initializationTime)/(calibrationTime/2.0f) );

				LogitechGSDK.LogiPlayConstantForce(0, 100);
				
			}else if( Time.time - initializationTime < calibrationTime){


				//Debug.Log("Calibrating to the LEFT");
				updateRpm( (Time.time - initializationTime)/calibrationTime );
				LogitechGSDK.LogiPlayConstantForce(0, -100);
				
			}else{

				steeringFeedback = false;

				if (steeringFeedback){

					//Debug.Log ("YES steering feedback");
					LogitechGSDK.LogiPlayConstantForce(0, offsetPercentage);
					
				}else{

					//Debug.Log ("NO steering feedback");
					LogitechGSDK.LogiPlayConstantForce(0, 0);

				}
								
			}



			//int magnitudePercentage = -20; // between -100 and 100
			//LogitechGSDK.LogiPlayConstantForce(0, -100);

//			if (LogitechGSDK.LogiIsPlaying(0, LogitechGSDK.LOGI_FORCE_CONSTANT))
//			{
//				LogitechGSDK.LogiStopConstantForce(0);
//				activeForceAndEffect[1] = "";
//			}
//			else
//			{
//				LogitechGSDK.LogiPlayConstantForce(0, 50);
//				activeForceAndEffect[1] = "Constant Force\n ";
//			}


			int tempCurrentGear = 0;

			int oldCameraView = cameraView;

			int pressedGearShifter = 0;

			for (int i = 0; i < 128; i++)
			{
				if (rec.rgbButtons[i] == 128)
				{

					if ( i >= 0 && i <= 3){
						cameraView = i;
					}

					if ( i == 5 ){ // LEFT GEAR SHIFTER
						pressedGearShifter = -1;
					}
					if ( i == 4 ){ // RIGHT GEAR SHIFTER
						pressedGearShifter = 1;
					}

					if ( i == 7 ){ // TOP LEFT INNER BUTTON
						cameraView = 0;
					}
					if ( i == 6 ){ // TOP RIGHT INNER BUTTON
						cameraView = 1;
					}

					if ( i == 20 ){ // MIDDLE LEFT INNER BUTTON
//						cameraView = i;
					}
					if ( i == 19 ){ // MIDDLE RIGHT INNER BUTTON
//						cameraView = i;
					}

					if ( i == 22 ){ // BOTTOM LEFT INNER BUTTON
						cameraView = 2;
					}
					if ( i == 21 ){ // BOTTOM RIGHT INNER BUTTON
						cameraView = 3;
					}

					if ( i >= 8 && i<= 12 ){
						
						tempCurrentGear = i - 7;
						
					}
					if ( i == 13 ){
						
						tempCurrentGear = -1;
						
					}
					
				}
				
			}

			if ( lastPressedGearShifter != pressedGearShifter){
				
				currentVehicle = currentVehicle + pressedGearShifter;
				
			}

			lastPressedGearShifter = pressedGearShifter;

			//Debug.Log ("Time: " + Time.time + " pressedGearShifter = " + pressedGearShifter + " lastPressedGearShifter = " + lastPressedGearShifter);
			
			currentGear = tempCurrentGear;

			if ( useJoystick ){
			
				currentGear = 1;

			}
			if ( cameraView != oldCameraView ){

				oldCameraView = cameraView;
				newCameraView = true;

			}

			int pressedPOV;

			switch (rec.rgdwPOV[0])
			{
			case (0): pressedPOV = 1; break; // "POV : UP\n"
			case (4500): pressedPOV = 1; break; // "POV : UP-RIGHT\n"
			case (9000): pressedPOV = 0; break;//  "POV : RIGHT\n"
			case (13500): pressedPOV = -1; break;//  "POV : DOWN-RIGHT\n";
			case (18000): pressedPOV = -1; break;//  "POV : DOWN\n"
			case (22500): pressedPOV = -1; break;// "POV : DOWN-LEFT\n"
			case (27000): pressedPOV = 0; break;// "POV : LEFT\n"
			case (31500): pressedPOV = 1; break;// "POV : UP-LEFT\n"
			default: pressedPOV = 0; break;// "POV : CENTER\n"
			}

//			switch (rec.rgdwPOV[0])
//			{
//			case (0): actualState += "POV : UP\n"; break;
//			case (4500): actualState += "POV : UP-RIGHT\n"; break;
//			case (9000): actualState += "POV : RIGHT\n"; break;
//			case (13500): actualState += "POV : DOWN-RIGHT\n"; break;
//			case (18000): actualState += "POV : DOWN\n"; break;
//			case (22500): actualState += "POV : DOWN-LEFT\n"; break;
//			case (27000): actualState += "POV : LEFT\n"; break;
//			case (31500): actualState += "POV : UP-LEFT\n"; break;
//			default: actualState += "POV : CENTER\n"; break;
//			}

			if ( lastPressedPOV != pressedPOV){

				currentVehicle = currentVehicle + pressedPOV;

			}

			lastPressedPOV = pressedPOV;
//			int forceMagnitude = 50;
//			
//			float error = steeringRatio - steeringDesired;
//			float pGain = 10.0f;
//			forceMagnitude = (int) ( pGain*error* ((float)forceMagnitude) );
//			
//			Debug.Log("forceMagnitude");
//			Debug.Log(forceMagnitude);
//			
//			//bool success = LogitechGSDK.LogiPlayConstantForce(0, 50);
//			activeForceAndEffect[1] = "Constant Force\n ";
//			
//
//			if (LogitechGSDK.LogiIsPlaying(0, LogitechGSDK.LOGI_FORCE_CONSTANT))
//			{
//				LogitechGSDK.LogiStopConstantForce(0);
//				activeForceAndEffect[1] = "";
//			}
//			else
//			{
////					LogitechGSDK.LogiPlayConstantForce(0, 50);
//				bool success = LogitechGSDK.LogiPlayConstantForce(0, 50);
//				activeForceAndEffect[1] = "Constant Force\n ";
//				Debug.Log ("Constant force Success = " + success);
//			}



            /* THIS AXIS ARE NEVER REPORTED BY LOGITECH CONTROLLERS 
             * 
             * actualState += "x-axis velocity :" + rec.lVX + "\n";
             * actualState += "y-axis velocity :" + rec.lVY + "\n";
             * actualState += "z-axis velocity :" + rec.lVZ + "\n";
             * actualState += "x-axis angular velocity :" + rec.lVRx + "\n";
             * actualState += "y-axis angular velocity :" + rec.lVRy + "\n";
             * actualState += "z-axis angular velocity :" + rec.lVRz + "\n";
             * actualState += "extra axes velocities 1 :" + rec.rglVSlider[0] + "\n";
             * actualState += "extra axes velocities 2 :" + rec.rglVSlider[1] + "\n";
             * actualState += "x-axis acceleration :" + rec.lAX + "\n";
             * actualState += "y-axis acceleration :" + rec.lAY + "\n";
             * actualState += "z-axis acceleration :" + rec.lAZ + "\n";
             * actualState += "x-axis angular acceleration :" + rec.lARx + "\n";
             * actualState += "y-axis angular acceleration :" + rec.lARy + "\n";
             * actualState += "z-axis angular acceleration :" + rec.lARz + "\n";
             * actualState += "extra axes accelerations 1 :" + rec.rglASlider[0] + "\n";
             * actualState += "extra axes accelerations 2 :" + rec.rglASlider[1] + "\n";
             * actualState += "x-axis force :" + rec.lFX + "\n";
             * actualState += "y-axis force :" + rec.lFY + "\n";
             * actualState += "z-axis force :" + rec.lFZ + "\n";
             * actualState += "x-axis torque :" + rec.lFRx + "\n";
             * actualState += "y-axis torque :" + rec.lFRy + "\n";
             * actualState += "z-axis torque :" + rec.lFRz + "\n";
             * actualState += "extra axes forces 1 :" + rec.rglFSlider[0] + "\n";
             * actualState += "extra axes forces 2 :" + rec.rglFSlider[1] + "\n";
             */

            int shifterTipe = LogitechGSDK.LogiGetShifterMode(0);
            string shifterString = "";
            if (shifterTipe == 1) shifterString = "Gated";
            else if (shifterTipe == 0) shifterString = "Sequential";
            else  shifterString = "Unknown";
       


            // FORCES AND EFFECTS 
     
                  
    
          
            
          

            

           

           

           

           

		}
		else if(!LogitechGSDK.LogiIsConnected(0))
		{
			 Debug.Log ( "PLEASE PLUG IN A STEERING WHEEL OR A FORCE FEEDBACK CONTROLLER");
		}
		else{
			Debug.Log (  "THIS WINDOW NEEDS TO BE IN FOREGROUND IN ORDER FOR THE SDK TO WORK PROPERLY");
		}
	}


	public float LimitsToLimitsConverter(float value, float minValue, float maxValue, float minOutput, float maxOutput){

		float output;
		float inputRatio = (value - minValue) / (maxValue - minValue);

		output = minOutput + inputRatio * (maxOutput - minOutput);

		return output;

	}

	public float steeringPID(float error){

		float currentTime = Time.time;

		if (lastTime < 0) {

			lastTime = currentTime;
			return 0.0f;

		}

		iError = iError + error * (currentTime - lastTime);

		iError = Mathf.Clamp (iError, -maxWindup, maxWindup);

		float actionCommand = pGain * error + iGain * iError;

		lastTime = currentTime;

		return actionCommand;

	}

	public void updateRpm(float rpmFraction){

		if (Time.time - initializationTime < calibrationTime) {

			return;

		}

		currentRPM = rpmFirstLedTurnsOn + rpmFraction * (rpmRedLine - rpmFirstLedTurnsOn);

		LogitechGSDK.LogiPlayLeds(0, currentRPM, rpmFirstLedTurnsOn, rpmRedLine);
		
	}
    

}
