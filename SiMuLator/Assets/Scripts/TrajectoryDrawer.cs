﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TrajectoryDrawer : MonoBehaviour {

	public GameObject goalCylinderPrefab;

	LineRenderer lineRenderer;
	ArrayList lastTrajectoryPoints;
		
	ArrayList allTrajectories;
	ArrayList allIds;

	// Use this for initialization
	void Start () {

		lineRenderer = GetComponent<LineRenderer> ();


		//Material whiteDiffuseMat = new Material(Shader.Find("Unlit/Texture"));
		//lineRenderer.material = whiteDiffuseMat;

		//lineRenderer.SetColors (Color.blue, Color.blue);

		//lineRenderer.material.color = Color.blue;
		//lineRenderer.receiveShadows = false;
		//lineRenderer.castShadows = false;

		//Vector3 cero = new Vector3(0,0,0);
		//lineRenderer.SetPosition(0, cero);

		lastTrajectoryPoints = new ArrayList ();
		lastTrajectoryPoints.Add (new Vector3 (0.0f, 0.0f, 0.0f));
		lastTrajectoryPoints.Add (new Vector3 (10.0f, 20.0f, 10.0f));
		lastTrajectoryPoints.Add (new Vector3 (20.0f, 0.0f, 20.0f));

		allTrajectories = new ArrayList ();
		allIds = new ArrayList ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ClearTrajectory(){

		lineRenderer.SetVertexCount(0);

		GameObject goalCylinder = GameObject.FindGameObjectWithTag("GoalCylinder");
		
		if (goalCylinder != null) {

			Destroy(goalCylinder);
			
		}

	}

//	public void ReshowTrajectory(){
//
//		if (lastTrajectoryPoints != null) {
//
//			DrawTrajectory (lastTrajectoryPoints);
//
//		}
//		//lineRenderer.SetColors (Color.blue, Color.blue);
//		//lineRenderer.material.color = Color.blue;
//		//lineRenderer.receiveShadows = false;
//		//lineRenderer.castShadows = false;
//
//	}

	public void SetTrajectory(int bodyId, ArrayList trajectoryPoints){

		Debug.Log ("New trajectory for body: " + bodyId);

		if (!allIds.Contains (bodyId)) {

			allIds.Add (bodyId);
			allTrajectories.Add (trajectoryPoints);

		} else {

			allTrajectories [allIds.IndexOf (bodyId)] = trajectoryPoints;

		}


	}

	public void ShowTrajectory(int bodyId){

		//Debug.Log ("Show Trajectory for vehicle: " + bodyId);

		GameObject monitoringText = GameObject.FindGameObjectWithTag("MonitoringText");




		if (!allIds.Contains (bodyId)) {

			//Debug.Log ("No trajectory found, clearing trajectory");

			ClearTrajectory();


			//Debug.Log ("monitoringText.GetComponent<Text> () != null = ");
			//Debug.Log ( "" + ( monitoringText.GetComponent<Text>() != null) );

			if (monitoringText != null) {
				
				Text monitoringTextText = monitoringText.GetComponent<Text> ();
				monitoringTextText.text = "Automatic Driving OFF";
				monitoringTextText.color = Color.red;
				
			}


			//monitoringText.GetComponent<Text> ().color = Color.red;


			return;
			
		}

		if (monitoringText != null) {
			
			monitoringText.GetComponent<Text> ().text = "Automatic Driving ON";
			monitoringText.GetComponent<Text> ().color = Color.green;
			
		}
		

		
		Debug.Log ("Will draw trajectory");

		ArrayList trajectoryPoints = (ArrayList) allTrajectories [allIds.IndexOf (bodyId)];

		lastTrajectoryPoints = trajectoryPoints;

		int numTrajectoryPoints = trajectoryPoints.Count;

		lineRenderer.SetVertexCount(numTrajectoryPoints);

		Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);

		for (int i = 0; i < numTrajectoryPoints; i++) {

			//Vector3 pos = trajectoryPoints[i];
			pos = (Vector3) trajectoryPoints[i];
			//Vector3 pos = Vector3(i * 0.5, Mathf.Sin(i + Time.time), 0);
			lineRenderer.SetPosition(i, pos);

		}

		GameObject goalCylinder = GameObject.FindGameObjectWithTag("GoalCylinder");

		if (goalCylinder == null) {

			Vector3 spawnPosition = pos;
			Quaternion spawnRotation = Quaternion.Euler (0.0f, 90.0f, 0.0f);

			goalCylinder = Instantiate (goalCylinderPrefab, spawnPosition, spawnRotation) as GameObject;

		} else {

			goalCylinder.GetComponent<Transform>().position = pos;
			goalCylinder.GetComponent<Transform>().rotation = Quaternion.Euler (0.0f, 90.0f, 0.0f);

		}

	}

//	public void DrawTrajectory(int bodyId, ArrayList trajectoryPoints){
//		
//		lastTrajectoryPoints = trajectoryPoints;
//		
//		int numTrajectoryPoints = trajectoryPoints.Count;
//		
//		lineRenderer.SetVertexCount(numTrajectoryPoints);
//		
//		for (int i = 0; i < numTrajectoryPoints; i++) {
//			
//			//Vector3 pos = trajectoryPoints[i];
//			Vector3 pos = (Vector3) trajectoryPoints[i];
//			//Vector3 pos = Vector3(i * 0.5, Mathf.Sin(i + Time.time), 0);
//			lineRenderer.SetPosition(i, pos);
//			
//		}
//		//		
//		
//	}


}
