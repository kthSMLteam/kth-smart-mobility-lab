﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private float currentSteering;
	
	public float minSpeed = 0.0f;
	public float maxSpeed = 10.0f;
	public float maxSteering;
	//	private float axisLength = 1.8f;

	public float minPitch;
	public float maxPitch;

	public float minVolume;
	public float maxVolume;




	public float steeringRatio;
	public float throttleRatio;
	public float brakeRatio;
	public float clutchRatio;

	float currentVelocity;
	
	public int currentGear;
	
	public float showRPM;
	public float showMaxTorque;
	public float showWheelTorque;
	public float showAirDragForce;
	public float showTractionForce;
	public float showVelocity;

	public float axisLength = 1.8f;
	
	float wheelRadius = 0.34f;
	
	float carMass = 1500.0f;
	
	float prevTime;
	
	float airDragCoefficient = .4257f;
	
	float differentialRatio = 3.42f;

	float makeItSlower = 1.0f;

	float gearRatio1 = 2.66f;
	float gearRatio2 = 1.78f;
	float gearRatio3 = 1.30f;
	float gearRatio4 = 1.00f;
	float gearRatio5 = 0.74f;
	float gearRatioR = -2.90f;




	AudioSource engineSound;

	RawImage logoRawImage;

	float startOfTime = 0.0f;

	LogitechSteeringWheelManager steeringWheelManager;

	CameraManager cameraManager;


	// Use this for initialization
	void Start () {
	
		startOfTime = Time.time;

		currentSteering = 0.0f;

		engineSound = GetComponentInChildren<AudioSource> ();

		GameObject tempGameObject = GameObject.FindGameObjectWithTag ("Logo");
		logoRawImage = tempGameObject.GetComponent<RawImage> ();

		tempGameObject = GameObject.FindGameObjectWithTag ("SteeringWheelManager");
		steeringWheelManager = tempGameObject.GetComponent<LogitechSteeringWheelManager> ();

		tempGameObject = GameObject.FindGameObjectWithTag ("CameraManager");
		cameraManager = tempGameObject.GetComponent<CameraManager> ();

		currentVelocity = 0.0f;
		currentGear = 0;
		prevTime = Time.time;

		
		gearRatio1 = gearRatio1 / makeItSlower;
		gearRatio2 = gearRatio2 / makeItSlower;
		gearRatio3 = gearRatio3 / makeItSlower;
		gearRatio4 = gearRatio4 / makeItSlower;
		gearRatio5 = gearRatio5 / makeItSlower;

	}
	
	// Update is called once per frame
	void Update () {

		if (Time.time - startOfTime > 20.0f) {

			Destroy(logoRawImage);

		}
	
	}

	void FixedUpdate() {

//		float moveHorizontal = Input.GetAxis ("Steering");
//		float moveVertical = Input.GetAxis ("Throttle");

		steeringRatio = steeringWheelManager.steeringRatio;
		throttleRatio = steeringWheelManager.throttleRatio;
		brakeRatio = steeringWheelManager.brakeRatio;
		clutchRatio = steeringWheelManager.clutchRatio;
		transmissionUpdate ();
		float currentRPM = getEngineTurnOverRate (currentVelocity);
		showRPM = currentRPM;
		float maxTorque = getMaxTorquePorsche (currentRPM);
		float engineTorque = maxTorque * throttleRatio;
		float wheelTorque = getWheelTorque (engineTorque);
		float tractionForce = getTractionForce(wheelTorque);
		float airDragForce = airDragCoefficient*Mathf.Pow(currentVelocity, 2.0f);
		float allForces = tractionForce - airDragForce;
		float acceleration = allForces / carMass;
		float newVelocity = getNewVelocity (currentVelocity, acceleration);

		if (Mathf.Abs (newVelocity) < 0.3f && newVelocity < 0.0f && currentGear > 0) {

			newVelocity = 0.0f;

		}

		newVelocity = Mathf.Clamp (newVelocity, -5.0f, 5.0f);

		showVelocity = newVelocity;




		updateEngineSound (currentRPM/4000.0f);

		if (cameraManager.currentId == 0) {
			updateRpm (currentRPM/4000.0f);
		}

		currentSteering = maxSteering * steeringRatio;
		currentSteering = Mathf.Clamp (currentSteering, -maxSteering, maxSteering);

		//Debug.Log ("currentSteering = " + currentSteering);

		float currentSteeringRadians = Mathf.Deg2Rad * currentSteering;

		float angularVelocity = newVelocity / axisLength;
		angularVelocity = angularVelocity * Mathf.Tan (currentSteeringRadians);

		// Debug.Log ("Angular velocity = " + angularVelocity);

		float currentTheta = GetComponent<Transform> ().eulerAngles.y;

		// Debug.Log ("currentTheta = " + currentTheta);

		currentTheta = Mathf.Deg2Rad * currentTheta;

		// Debug.Log ("Radians: currentTheta = " + currentTheta);

		float xSpeed = newVelocity * Mathf.Cos (currentTheta);

		float ySpeed = newVelocity * Mathf.Sin (currentTheta);

		currentVelocity = newVelocity;

		// Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		Vector3 movement = new Vector3 (ySpeed, 0.0f, xSpeed);
		GetComponent<Rigidbody>().velocity = movement;

		GetComponent<Rigidbody>().angularVelocity = new Vector3 (0.0f, angularVelocity, 0.0f);

	}


	private void updateEngineSound (float speedRatio){

		float currentPitch = minPitch + speedRatio * (maxPitch - minPitch);

		engineSound.pitch = currentPitch;

		float currentVolume = minVolume + speedRatio * (maxVolume - minVolume);

		engineSound.volume = currentVolume;

		if (cameraManager.currentId != 0) {
			engineSound.volume = 0.0f;
		}

	}

	private void updateRpm (float speedRatio){


		if (speedRatio > 0.99f) {

			speedRatio = 0.99f;

		}

		steeringWheelManager.updateRpm (speedRatio);
		
	}



	void transmissionUpdate(){
		
		int inputGear = steeringWheelManager.currentGear;
		
		if (currentGear != inputGear && clutchRatio > 0.5) {
			
			currentGear = inputGear;
			
		}
		
	}
	
	float getEngineTurnOverRate (float velocity){
		// Should output RPM
		
		float defaultEngineTurnOverRate = 500.0f; // RPM
		
		if (currentGear == 0) {
			
			return defaultEngineTurnOverRate;
			
		}
		
		float engineTurnOverRate = velocity * 60.0f * getGearRatio ();
		
		
		
		engineTurnOverRate = engineTurnOverRate * (differentialRatio / (2 * Mathf.PI * wheelRadius));
		
		//		engineTurnOverRate = Mathf.Clamp (engineTurnOverRate, defaultEngineTurnOverRate, 8000.0f);
		engineTurnOverRate = Mathf.Clamp (engineTurnOverRate, defaultEngineTurnOverRate, 4500.0f);
		
		return engineTurnOverRate;
		
	}
	
	float getTractionForce(float wheelTorque){
		
		float tractionForce;
		
		if (brakeRatio > 0.2f) {
			
			if (currentVelocity >= 0.0f) {

				tractionForce = - 20000.0f * brakeRatio;
				
			} else {
				
				tractionForce = 20000.0f * brakeRatio;
				
			}
			
		} else {
			
			tractionForce = wheelTorque / wheelRadius;
			
		}
		
		return tractionForce;
		
	}
	
	float getGearRatio(){
		
		if (currentGear == 0) {
			
			return 0.0f;
			
		}
		if (currentGear == 1) {
			
			return gearRatio1;
			
		} else if (currentGear == 2) {
			
			return gearRatio2;
			
		} else if (currentGear == 3) {
			
			return gearRatio3;
			
		} else if (currentGear == 4) {
			
			return gearRatio4;
			
		} else if (currentGear == 5) {
			
			return gearRatio5;
			
		} else if (currentGear == -1) {
			
			return gearRatioR;
			
		} else {
			
			return 0.0f;
			
		}
		
		
	}
	
	
	float getMaxTorquePorsche(float rpm){
		
		float torque;
		
		float limit1 = 1000.0f;
		float limit2 = 4500.0f;
		float limit3 = 8000.0f;
		
		float value1 = 220.0f;
		float peak_value = 310.0f;
		
		if (rpm < limit1){
			
			torque = value1; 
			return torque;
			
		}
		
		float fraction_curve;
		
		if (rpm < limit2) {
			
			fraction_curve = rpm - limit1;
			fraction_curve = fraction_curve / (limit2 - limit1);
			
			torque = value1 + (peak_value - value1) * fraction_curve;
			return torque;
			
		}
		
		rpm = Mathf.Min(rpm, limit3);
		
		fraction_curve = rpm - limit2;
		fraction_curve = fraction_curve/(limit3-limit2);
		
		torque = peak_value + (value1 - peak_value)*fraction_curve;
		
		return torque;
		
	}
	
	float getWheelTorque (float engineTorque){
		
		float wheelTorque = engineTorque * getGearRatio() * differentialRatio;
		return wheelTorque;
		
	}
	

	
	float getNewVelocity (float velocity, float acceleration){
		
		float deltaTime = Time.time - prevTime;
		
		float newVelocity = velocity + deltaTime * acceleration;
		
		prevTime = Time.time;
		
		return newVelocity;
		
	}


}
