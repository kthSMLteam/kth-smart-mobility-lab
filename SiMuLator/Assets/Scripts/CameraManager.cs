﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {


	public Camera playerCamera;
	public Transform playerCameraTransform;
	public Transform playerTransform;

	private Camera lastCamera = null;

	private Transform lastCameraTransform;
	private Vector3 lastCameraTransformPosition;
	private Quaternion lastCameraTransformRotation;

	public int lastId = -99;
	public int currentId = 0;
	public int cameraMode = 0;

	public OVRCameraRig oculusCamera;

	public LineRenderer trajectoryLineRenderer;

	LogitechSteeringWheelManager steeringWheelManager;

	BotController currentBotController;

	
	//public Vector3 GTAPosition;
	//public Vector3 GTARotation;

	// Use this for initialization
	void Start () {
	
		GameObject player = GameObject.FindGameObjectWithTag("Player");

		playerTransform = player.GetComponent<Transform> ();

		playerCamera = player.GetComponentInChildren<Camera>();


		Transform[] transforms = player.GetComponentsInChildren<Transform>();

		for (int i = 0; i < transforms.Length; i++) {

			if( transforms[i].tag.Equals("Camera") ){

				playerCameraTransform = transforms[i];
				break;

			}

		}

		GameObject tempGameObject = GameObject.FindGameObjectWithTag ("SteeringWheelManager");
		steeringWheelManager = tempGameObject.GetComponent<LogitechSteeringWheelManager> ();



	}
	
	// Update is called once per frame
	void Update () {

		Debug.Log ("Camera Manager Update");

		updateOculusPosition (currentId);
			
		int pressedId = -99;

		if (currentId > 0) {
			
			trajectoryLineRenderer.GetComponent<TrajectoryDrawer> ().ShowTrajectory (currentId);
			
		}


		if (steeringWheelManager.newCameraView) {

			cameraMode = steeringWheelManager.cameraView;

			SwitchCameraView();

			steeringWheelManager.newCameraView = false;

		}





		if (Input.GetKeyDown(KeyCode.F1)) {

			// neutral Camera
			cameraMode = 0;
			SwitchCameraView();
			//SwitchCameras(currentId);
			
		}else if (Input.GetKeyDown(KeyCode.F2)) {

			// front Camera
			cameraMode = 1;
			SwitchCameraView();
			//SwitchCameras(currentId);
			
		}else if (Input.GetKeyDown(KeyCode.F3)) {

			// GTA Camera
			cameraMode = 2;
			SwitchCameraView();
			//SwitchCameras(currentId);
			
		}else if (Input.GetKeyDown(KeyCode.F4)) {

			// Bird View Camera
			cameraMode = 3;
			SwitchCameraView();
			//SwitchCameras(currentId);
			
		}

		if (Input.GetKeyDown(KeyCode.Alpha0)) {

			pressedId = 0;

		}else if (Input.GetKeyDown(KeyCode.Alpha1)) {
			
			pressedId = -1;
			
		}else if (Input.GetKeyDown(KeyCode.Alpha2)) {

			pressedId = -2;

		}else if (Input.GetKeyDown(KeyCode.Alpha3)) {
			
			pressedId = -3;
			
		}else if (Input.GetKeyDown(KeyCode.Alpha4)) {
			
			pressedId = -4;
			
		}else if (Input.GetKeyDown(KeyCode.Alpha5)) {
			
			pressedId = -5;
			
		}else if (Input.GetKeyDown(KeyCode.Alpha6)) {
			
			pressedId = -6;
			
		}else if (Input.GetKeyDown(KeyCode.Alpha7)) {
			
			pressedId = -7;
			
		}else if (Input.GetKeyDown(KeyCode.Alpha8)) {
			
			pressedId = -8;
			
		}else if (Input.GetKeyDown(KeyCode.Alpha9)) {
			
			pressedId = -9;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad0)) {
			
			pressedId = 0;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad1)) {
			
			pressedId = 1;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad2)) {
			
			pressedId = 2;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad3)) {
			
			pressedId = 3;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad4)) {
			
			pressedId = 4;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad5)) {
			
			pressedId = 5;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad6)) {
			
			pressedId = 6;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad7)) {
			
			pressedId = 7;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad8)) {
			
			pressedId = 8;
			
		}else if (Input.GetKeyDown(KeyCode.Keypad9)) {
			
			pressedId = 9;
			
		}

		//Debug.Log ("current Id = " + currentId);
		//Debug.Log ("Pressed Id = " + pressedId);

		updateSteeringWheelFeedback ();

		pressedId = steeringWheelManager.currentVehicle;

		if (pressedId != -99) {

			if (currentId == pressedId ){
				// Changing to same Id, do nothing
				return;
			}

//			SwitchCameras(pressedId);
			currentId = pressedId;

		}




	}

	void updateOculusPosition(int vehicleId){

		Debug.Log("updateOculusPosition()");

		vehicleId = 0;

		if ( vehicleId == 0 ){
			
			if ( lastCamera != null ){
				
				lastCamera.enabled = false;
				
			}
			
			playerCamera.enabled = false;
			//playerCamera.enabled = true;
			// PUT OCULUS HERE
			//oculusCamera.GetComponent<Transform>().position = new Vector3(0.0f, 2.0f, -2.0f) +
			//	playerCameraTransform.position;

			// Third player view, no rotation needed since there is only an offset in height
			//oculusCamera.GetComponent<Transform>().position = new Vector3(0.0f, 2.0f, 0.0f) +
			//	playerTransform.position;

			float yRotation = playerCameraTransform.rotation.eulerAngles.y;

			// Car cabin view
			// Vector3 cabinPosition = new Vector3(-0.3f, 1.05f, 1.9f);

			// Bus cabin view
			Vector3 cabinPosition = new Vector3(-0.67f, 2.00f, 7.80f);

			Vector3 cabinPositionRotated = Quaternion.AngleAxis(yRotation, Vector3.up) * cabinPosition;

			oculusCamera.GetComponent<Transform>().position = cabinPositionRotated +
				playerTransform.position;

			Quaternion cameraRotation = Quaternion.Euler(new Vector3(.0f, yRotation, .0f) );

			oculusCamera.GetComponent<Transform>().rotation = cameraRotation;
//			oculusCamera.GetComponent<Transform>().rotation = oculusCamera.GetComponent<Transform>().rotation + cameraRotation;



			lastCamera = playerCamera;
			lastId = vehicleId;
			
			return;
			
		}
		
		
		Camera botCamera;
		botCamera = null;
		
		Transform cameraTransform = null;
		
		GameObject[] botCars = GameObject.FindGameObjectsWithTag("BotCar");
		
		for (int i = 0; i < botCars.Length; i++){
			
			BotController botController = botCars[i].GetComponent<BotController>();
			
			if ( botController.botCarId == vehicleId ){
				
				botCamera = botCars[i].GetComponentInChildren<Camera>();
				currentBotController = botCars[i].GetComponent<BotController>();
				
				Transform[] transforms = botCars[i].GetComponentsInChildren<Transform>();
				
				for ( int j = 0; j < transforms.Length ; j++ ){
					
					if (  transforms[j].tag.Equals("Camera") ){
						
						cameraTransform = transforms[j];
						break;
						
					}
					
				}

				//oculusCamera.GetComponent<Transform>().position = new Vector3(0.0f, 10.0f, 0.0f);
				oculusCamera.GetComponent<Transform>().position = cameraTransform.position;

				Debug.Log("oculusCamera new position = " + cameraTransform.position);

			}
			
		}



	}

	void updateSteeringWheelFeedback(){

		if (currentId == 0) {

			steeringWheelManager.steeringFeedback = false;

		}else {
			
			steeringWheelManager.steeringFeedback = true;
			
		}

		if (currentBotController != null) {

			if (currentBotController.botCarId != 0) {

				float filteredLinearVelocity = currentBotController.filteredLinearVelocity;
				//Debug.Log ("filteredLinearVelocity = " + filteredLinearVelocity);
				//Debug.Log (filteredLinearVelocity);

				float filteredAngularVelocity = currentBotController.filteredAngularVelocity;
				//Debug.Log ("filteredAngularVelocity = " + filteredAngularVelocity);
				//Debug.Log (filteredAngularVelocity);

				float steering = currentBotController.currentSteering;

				//Debug.Log (steering);
				steering = steering / 12.0f;
				steering = Mathf.Clamp( steering , -1.0f, 1.0f );

				//Debug.Log ("Steering Feedback = " + steering);
				steeringWheelManager.steeringDesired = steering;

				if ( currentBotController.botCarId == currentId ){

					if (currentBotController.botCarId > 0){

						// Can achieve Higher speeds, must decrease RPM ratio
						steeringWheelManager.updateRpm( filteredLinearVelocity / (80.0f*(1000.0f/3600.0f)));

					}else{

						steeringWheelManager.updateRpm( filteredLinearVelocity / (40.0f*(1000.0f/3600.0f)));

					}

				}

			}

		} else {


			//Debug.Log("currentBotController is null!");

		}

	}

	void SwitchCameraView(){

		switchCameraViewPlayer ();

		if (lastCameraTransform != null) {

			if (cameraMode != 0) {

				lastCameraTransform.localPosition = GetCameraLocalPosition ();
				lastCameraTransform.localRotation = GetCameraLocalRotation ();

			} else {

				lastCameraTransform.localPosition = lastCameraTransformPosition;
				lastCameraTransform.localRotation = lastCameraTransformRotation;

			}

		} else {

			Debug.Log ("lastCameraTransform is NULL");

		}


	}


	void switchCameraViewPlayer(){

		if (cameraMode == 0) {

			playerCameraTransform.localPosition = new Vector3(-0.2f, 1.05f, 1.65f);
			playerCameraTransform.localRotation = Quaternion.identity;

		} else if(cameraMode == 1) {
			
			playerCameraTransform.localPosition = new Vector3(0.0f, 1.0f, 3.5f);
			playerCameraTransform.localRotation = Quaternion.identity;
			
		} else if(cameraMode == 2) {
			
			playerCameraTransform.localPosition = new Vector3(0.0f, 2.42f, -4.0f);
			playerCameraTransform.localRotation = Quaternion.Euler(10.0f, 0.0f, 0.0f);
			
		} else {
			
			playerCameraTransform.localPosition = new Vector3(0.0f, 20.0f, 0.0f);
			playerCameraTransform.localRotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
			
		}

	}

	void SwitchCameras(int vehicleId){

		Debug.Log ("Switching to camera: " + vehicleId);

		if (vehicleId == lastId) {
						
			Debug.Log ("Not switching!");
			return;
			
		}

		GameObject.FindGameObjectWithTag ("MonitoringScript").GetComponent<MonitoringManager> ().ResetBirdView ();
				//GameObject.FindGameObjectWithTag("TrajectoryDrawer").SetActive(vehicleId > 0);
		if (vehicleId > 0) {

			//GameObject.FindGameObjectWithTag("TrajectoryDrawer").GetComponent<TrajectoryDrawer>().ReshowTrajectory();
			GameObject.FindGameObjectWithTag("TrajectoryDrawer").GetComponent<TrajectoryDrawer> ().ShowTrajectory (vehicleId);

		} else {
						
			GameObject.FindGameObjectWithTag("TrajectoryDrawer").GetComponent<TrajectoryDrawer>().ShowTrajectory (vehicleId);

		}



		if ( vehicleId == 0 ){

			if ( lastCamera != null ){

				lastCamera.enabled = false;

			}

			playerCamera.enabled = false;
			//playerCamera.enabled = true;
			// PUT OCULUS HERE
//			oculusCamera.GetComponent<Transform>().position = new Vector3(0.0f, 10.0f, 0.0f);

			lastCamera = playerCamera;
			lastId = vehicleId;

			return;
			
		}


		Camera botCamera;
		botCamera = null;

		Transform cameraTransform = null;

		GameObject[] botCars = GameObject.FindGameObjectsWithTag("BotCar");

		for (int i = 0; i < botCars.Length; i++){
			
			BotController botController = botCars[i].GetComponent<BotController>();

			if ( botController.botCarId == vehicleId ){

				botCamera = botCars[i].GetComponentInChildren<Camera>();
				currentBotController = botCars[i].GetComponent<BotController>();

				Transform[] transforms = botCars[i].GetComponentsInChildren<Transform>();

				for ( int j = 0; j < transforms.Length ; j++ ){

					if (  transforms[j].tag.Equals("Camera") ){

						cameraTransform = transforms[j];
						break;

					}

				}

			}
						
		}


		//GameObject playerCar = GameObject.FindGameObjectWithTag("Player");
		//playerCamera = playerCar.GetComponentInChildren<Camera>();
				
		Vector3 newCameraTransformPosition;
		Quaternion newCameraTransformRotation;

		Transform newCameraTransform = cameraTransform;

		if (botCamera != null) {

			botCamera.enabled = false;
			//botCamera.enabled = true;
			// PUT OCULUS HERE

			//lastCameraTransform = cameraTransform;

			newCameraTransformPosition = cameraTransform.localPosition;
			newCameraTransformRotation = cameraTransform.localRotation;

			if ( cameraMode != 0 ){

				cameraTransform.localPosition = GetCameraLocalPosition();
				cameraTransform.localRotation = GetCameraLocalRotation();

			}


		} else {

			// Camera not found!
			return;

		}

		if (lastCamera != null) {

			lastCamera.enabled = false;
			
			lastCameraTransform.localPosition = lastCameraTransformPosition;
			lastCameraTransform.localRotation = lastCameraTransformRotation;
								
		}

		lastCameraTransform = cameraTransform;
		lastCameraTransformPosition = newCameraTransformPosition;
		lastCameraTransformRotation = newCameraTransformRotation;



		lastCamera = botCamera;
		lastId = vehicleId;


		// playerCamera.enabled = !playerCamera.enabled;


	}

	private Vector3 GetCameraLocalPosition(){

		if (cameraMode == 0) {

			// NOT USED
			return Vector3.zero;

		}else if(cameraMode == 1) {

			if ( currentId > 0 ){

				return new Vector3(0.0f, 1.0f, 8.0f);

			}else{

				return new Vector3(0.0f, 1.0f, 5.0f);

			}
			
		}else if(cameraMode == 2) {

			return new Vector3(0.0f, 5f, -5.0f);
			//return GTAPosition;
			
		}else{
			
			return new Vector3(0.0f, 40.0f, 0.0f);
			
		}


	}

	private Quaternion GetCameraLocalRotation(){

		if (cameraMode == 0) {
			
			// NOT USED
			return Quaternion.identity;
			
		}else if(cameraMode == 1) {
			
			return Quaternion.identity;
			
		}else if(cameraMode == 2) {
			
			// Positive rotation in X points downwards
			return Quaternion.Euler(20.0f, 0.0f, 0.0f);
			//return Quaternion.Euler(GTARotation);
			
		}else{
			
			return Quaternion.Euler(90.0f, 0.0f, 0.0f);
			
		}

	}


}
