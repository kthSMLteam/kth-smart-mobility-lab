﻿using UnityEngine;
//using UnityEditor;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;
using System.Xml;
using System.Text;

public class CommunicationsManager : MonoBehaviour {

	TcpClient mySocket;
	TcpClient egoSocket;

	NetworkStream networkStream;
	StreamReader streamReader;

	NetworkStream egoNetworkStream;
	StreamWriter streamWriter;

	public LineRenderer trajectoryLineRenderer;

	bool socketReady = false;

	public GameObject botCarPrefab1;
	public GameObject botCarPrefab2;
	public GameObject botCarPrefab3;
	public GameObject botCarPrefab4;
	public GameObject botCarPrefab5;
	public GameObject botBusPrefab;

	public GameObject botTruckPrefab;
	public GameObject botTrailerPrefab;
	public GameObject botExcavatorPrefab;
	public GameObject rockObstaclePrefab;
	public GameObject treePrefab;
	public GameObject monitoringScript;

	
	public GameObject botPersonPrefab0;
	public GameObject botPersonPrefab1;
	public GameObject botPersonPrefab2;
	public GameObject botPersonPrefab3;
	public GameObject botPersonPrefab4;
	public GameObject botPersonPrefab5;

	Boolean initLoadComplete;
	Boolean wasPaused;

//	String Host = "localhost";



	
//	String Host = "130.237.43.200"; // My own
	String Host = "130.237.50.246"; // Rui's
//	String Host = "130.237.43.135"; // Pedro's computer
	Int32 Port = 5017;
	Int32 egoPort = 55017;

//	
//	public Int32 Port = 5017;
//	public Int32 egoPort = 50017;

	public int playerId = 808;

	private float lastTime;
	private float desiredSendRate;

	// Use this for initialization
	void Start () {

		initLoadComplete = false;
		wasPaused = false;

//		EditorApplication.playmodeStateChanged = HandleOnPlayModeChanged;

//		GameObject cubeGameObject = GameObject.FindWithTag ("MovingCube");
//		if (cubeGameObject != null)
//		{
//			cubePlacer = cubeGameObject.GetComponent <CubePlacer>();
//		}
//		if (cubePlacer == null)
//		{
//			Debug.Log ("Cannot find 'CubePlacer' script");
//		}
	
		lastTime = Time.time;
		desiredSendRate = 10;

		setupSocket ();

		ProcessTreesString ();

	}
	
	// Update is called once per frame
	void Update () {

		// Console.WriteLine  ("Update");
		// Debug.Log (" Update() ");

		String receivedLine = readSocket ();
	
		float currentTime = Time.time;

		if (currentTime - lastTime > (1.0 / desiredSendRate)) {

			lastTime = currentTime;
			SendEgoState ();

		}




		if ( receivedLine.Equals ("") ) {

			return;

		}

		
//		Debug.LogWarning("Process Xml String");
		ProcessXmlString(receivedLine);

	}

	private void ProcessXmlString(String xmlString){

//		Debug.Log (xmlString);

		using (XmlReader reader = XmlReader.Create(new StringReader(xmlString))) {
			
			
			while (reader.ReadToFollowing("message")) {
				
				String messageTypeString = reader.GetAttribute ("type");

				if ( messageTypeString.Equals("vehicle_states_reply") ){
//				if ( messageTypeString.Equals("states_broadcast") ){
				
					ProcessStatesString(xmlString);

				}

//				if ( messageTypeString.Equals("trees") ){
//
//					ProcessTreesString(xmlString);
//					
//				}

//				if ( messageTypeString.Equals("trajectory") ){
//					
//					ProcessTrajectoryString(xmlString);
//					
//				}

				if ( messageTypeString.Equals("body_trajectory_command") ){

					//Debug.Log ("Received a body_trajectory_command");

					ProcessBodyTrajectoryCommandString(xmlString);
					
				}


			}

		}

	}

	private void ProcessBodyTrajectoryCommandString(String xmlString){
		
		trajectoryLineRenderer.GetComponent<TrajectoryDrawer>().ClearTrajectory ();
		
		ArrayList vector3Points = new ArrayList ();
		int bodyId;
		
		using (XmlReader reader = XmlReader.Create(new StringReader(xmlString))) {

			reader.ReadToFollowing("message");

			String bodyIdString = reader.GetAttribute ("body_id");

			bodyId = int.Parse(bodyIdString);

			while ( reader.ReadToFollowing("trajectory") ){

				while ( reader.ReadToFollowing("point") ){

					String xString = reader.GetAttribute("x");
					float xValue = float.Parse (xString);
					
					String yString = reader.GetAttribute("y");
					float yValue = float.Parse (yString);

					Vector3 trajectoryPoint = new Vector3(xValue, 0.25f, yValue);

					//Debug.Log (trajectoryPoint.ToString());

					vector3Points.Add(trajectoryPoint);
				
				}
				
			}
			
		}

		trajectoryLineRenderer.GetComponent<TrajectoryDrawer> ().SetTrajectory (bodyId, vector3Points);
		//trajectoryLineRenderer.GetComponent<TrajectoryDrawer>().DrawTrajectory (bodyId, vector3Points);
		
		
	}
	
//	private void ProcessTrajectoryString(String xmlString){
//
//		trajectoryLineRenderer.GetComponent<TrajectoryDrawer>().ClearTrajectory ();
//		
//		ArrayList vector3Points = new ArrayList ();
//
//		using (XmlReader reader = XmlReader.Create(new StringReader(xmlString))) {
//						
//			while ( reader.ReadToFollowing("point") ){
//				
//				String xString = reader.GetAttribute("x");
//				float xValue = float.Parse (xString);
//								
//				String yString = reader.GetAttribute("y");
//				float yValue = float.Parse (yString);
//								
//				Vector3 spawnPosition = new Vector3(xValue, 0.25f, yValue);
//
//				vector3Points.Add(spawnPosition);
//
//												
//			}
//			
//		}
//
//		trajectoryLineRenderer.GetComponent<TrajectoryDrawer>().DrawTrajectory (vector3Points);
//			
//		
//	}


	private void ProcessTreesString(){
	//private void ProcessTreesString(String xmlString){

		String xmlString = System.IO.File.ReadAllText(@"C:\Users\SMLADMIN\Documents\kth-smart-mobility-lab\SiMuLator\Assets\Xmls\trees_for_internet.xml");


		using (XmlReader reader = XmlReader.Create(new StringReader(xmlString))) {

			reader.ReadToFollowing("message");
			
			float pixelWidth = float.Parse( reader.GetAttribute("pixel_width") );
			float pixelHeight = float.Parse( reader.GetAttribute("pixel_height") );

			while ( reader.ReadToFollowing("tree") ){
				
				String xString = reader.GetAttribute("x");
				float xValue = float.Parse (xString);

				xValue = (xValue/pixelWidth)-0.5f;
//				xValue = xValue * 512.0f;
				xValue = xValue * 361.40f;

				String yString = reader.GetAttribute("y");
				float yValue = float.Parse (yString);
								
				yValue = (yValue/pixelHeight)-0.5f;
//				yValue = -yValue * 288.0f;
				yValue = -yValue * 203.29f;

				Vector3 spawnPosition = new Vector3(xValue, 0.0f, yValue);
				
				Quaternion spawnRotation = new Quaternion();

				float treeRotation = (float) UnityEngine.Random.Range(0.0f, 360.0f);

				spawnRotation.eulerAngles = new Vector3(0, treeRotation, 0);
					
				//GameObject newGameObject;
				//newGameObject = Instantiate (treePrefab, spawnPosition, spawnRotation) as GameObject;
				Instantiate (treePrefab, spawnPosition, spawnRotation);

			}
			
		}


	}

	private void ProcessStatesString(String xmlString){

		GameObject[] botCars = GameObject.FindGameObjectsWithTag("BotCar");

		ArrayList idsList = new ArrayList();
		
		for (int i = 0; i < botCars.Length; i++){

			BotController botController = botCars[i].GetComponent<BotController>();

			if ( botController != null ){

				idsList.Add( botController.botCarId );

			}
			
		}


		
		ArrayList states = new ArrayList ();
		ArrayList idsForMonitoring = new ArrayList();
				

		using (XmlReader reader = XmlReader.Create(new StringReader(xmlString))) {

			while ( reader.ReadToFollowing("body_info") ){

				float[] state = new float[3];

				String idString = reader.GetAttribute("id");
				int idValue = int.Parse(idString);

				String xString = reader.GetAttribute("x");
				float xValue = float.Parse (xString);
				xValue = xValue*32.0f;

				String yString = reader.GetAttribute("y");
				float yValue = float.Parse (yString);
				yValue = yValue*32.0f;

				String yawString = reader.GetAttribute("yaw");
				float yawValue = float.Parse (yawString);
				yawValue = -yawValue + 90.0f;

				String bodyType = reader.GetAttribute("body_type");


				Boolean isTrailer = false;
				Boolean isExcavator = false;
				Boolean isBus = false;
				Boolean isPerson = false;

				if ( bodyType != null ){

					if ( bodyType.Contains("trailer") ){

						isTrailer = true;

					}

					if ( bodyType.Contains("construction") ){

						isExcavator = true;
						
					}

					if ( bodyType.Contains("bus") ){
						
						isBus = true;
						
					}

					if ( bodyType.Contains("person") ){
						
						isPerson = true;
						
					}

				}


				String isObstacleString = reader.GetAttribute("obstacle");
				Boolean isObstacle = false;

				if ( isObstacleString != null ){

					if ( isObstacleString.Equals("True") || isObstacleString.Equals("true") ){

						isObstacle = true;

					}

				}
				
				state[0] = xValue;
				state[1] = yValue;		
				state[2] = yawValue;

				idsForMonitoring.Add (idValue);
				states.Add(state);

				Vector3 position = new Vector3(xValue, 0.0f, yValue);

				if ( idsList.Contains(idValue) ){

					botCars[idsList.IndexOf(idValue)].GetComponent<BotController>().UpdateBotCarState(position, yawValue);

				}else{

					Vector3 spawnPosition = position;
					Quaternion spawnRotation = Quaternion.identity;


					GameObject newGameObject = null;

					if (idValue == playerId ){

						continue;

					}

					if ( isObstacle ){

						newGameObject = Instantiate (rockObstaclePrefab, spawnPosition, spawnRotation) as GameObject;
						newGameObject.GetComponent<BotController>().SetBotCarId(idValue);
						continue;

					}

					if (idValue < 0 ){

						int tempIdValue = -idValue;


						if ( isExcavator ){
							
							newGameObject = Instantiate (botExcavatorPrefab, spawnPosition, spawnRotation) as GameObject;
						
						}else if( isBus ){

							newGameObject = Instantiate (botBusPrefab, spawnPosition, spawnRotation) as GameObject;

						}else if( isPerson ){

							tempIdValue = tempIdValue%6;

							switch (tempIdValue)
							{
							case 0:
								newGameObject = Instantiate (botPersonPrefab0, spawnPosition, spawnRotation) as GameObject;
								break;
							case 1:
								newGameObject = Instantiate (botPersonPrefab1, spawnPosition, spawnRotation) as GameObject;
								break;
							case 2:
								newGameObject = Instantiate (botPersonPrefab2, spawnPosition, spawnRotation) as GameObject;
								break;
							case 3:
								newGameObject = Instantiate (botPersonPrefab3, spawnPosition, spawnRotation) as GameObject;
								break;
							case 4:
								newGameObject = Instantiate (botPersonPrefab4, spawnPosition, spawnRotation) as GameObject;
								break;
							case 5:
								newGameObject = Instantiate (botPersonPrefab5, spawnPosition, spawnRotation) as GameObject;
								break;
							default:
								Debug.Log("Default case");
								break;
							}

						}else{

							tempIdValue = tempIdValue%5;

							if ( tempIdValue == 0 ){
								newGameObject = Instantiate (botCarPrefab1, spawnPosition, spawnRotation) as GameObject;
							}
							if ( tempIdValue == 1 ){
								newGameObject = Instantiate (botCarPrefab2, spawnPosition, spawnRotation) as GameObject;
							}
							if ( tempIdValue == 2 ){
								newGameObject = Instantiate (botCarPrefab3, spawnPosition, spawnRotation) as GameObject;
							}
							if ( tempIdValue == 3 ){
								newGameObject = Instantiate (botCarPrefab4, spawnPosition, spawnRotation) as GameObject;
							}
							if ( tempIdValue == 4 ){
								newGameObject = Instantiate (botCarPrefab5, spawnPosition, spawnRotation) as GameObject;
							}

						}

						//newGameObject.GetComponent<Camera>().enabled = false;

					}else{

						if ( isTrailer ){

//							spawnPosition = 
							newGameObject = Instantiate (botTrailerPrefab, spawnPosition, spawnRotation) as GameObject;

						}else{

							newGameObject = Instantiate (botTruckPrefab, spawnPosition, spawnRotation) as GameObject;

						}

					}

					if (!isPerson){

					newGameObject.GetComponentInChildren<Camera>().enabled = false;
					
					}

					newGameObject.GetComponent<BotController>().SetBotCarId(idValue);

				}

			}

		}

		monitoringScript.GetComponent<MonitoringManager>().UpdateBirdView(states,  idsForMonitoring);

	}

	public String getEgoStateString(Vector3 position, Vector3 rotation){

		String xmlString = "";

		Encoding utf8noBOM = new UTF8Encoding(true);  
		XmlWriterSettings settings = new XmlWriterSettings();  
		//settings.Indent = true;  
		settings.Encoding = utf8noBOM;

		using (var sw = new StringWriter()) {

			using (XmlWriter writer = XmlWriter.Create(sw, settings)) {

				writer.WriteStartDocument ();
				writer.WriteStartElement ("message");

				writer.WriteAttributeString ("type", "ego_state");

				writer.WriteStartElement ("body");

				writer.WriteAttributeString ("id", playerId.ToString() );

				
				float xValue = position.x;
				float yValue = position.z;
				float yawValue = rotation.y;

				writer.WriteAttributeString ("x", xValue.ToString());
				writer.WriteAttributeString ("y", yValue.ToString());
				writer.WriteAttributeString ("yaw", yawValue.ToString());

				writer.WriteEndElement ();

				writer.WriteEndElement ();
				writer.WriteEndDocument ();

			}

			xmlString = sw.ToString ();

		}

		return xmlString;

		// Debug.Log ("xmlString = " + xmlString);

	}

	public void SendEgoState(){

//	 	Debug.LogWarning("Sending ego state");

		if (!socketReady) {
			return;
		}

		GameObject player = GameObject.FindWithTag ("Player");
		Vector3 playerPosition = player.GetComponent<Transform>().position;
		Quaternion playerRotationQuaternion = player.GetComponent<Transform>().rotation;

		Vector3 playerRotation = playerRotationQuaternion.eulerAngles;

		String xmlString = getEgoStateString(playerPosition, playerRotation);

		xmlString = xmlString + "\r\n";

		streamWriter.Write(xmlString);
		streamWriter.Flush();

	}

	public void setupSocket(){

		socketReady  = true;

		Debug.LogWarning("Will try to setup socket #1...");

		try{
			mySocket = new TcpClient(Host, Port);
			networkStream = mySocket.GetStream();
			streamReader = new StreamReader(networkStream);

		}
		catch (Exception e){

			socketReady  = false;

			Debug.LogWarning("Socket (state receiver) error: " + e);			
//			Debug.LogError("Socket (state receiver) error: " + e);

		}
				
		Debug.LogWarning("Will try to setup scoket #2...");

		try{
						
			egoSocket = new TcpClient(Host, egoPort);
			egoNetworkStream = egoSocket.GetStream();
			streamWriter = new StreamWriter(egoNetworkStream);

		}
		catch (Exception e){

			socketReady  = false;

			Debug.LogWarning("Socket (ego sender) error: " + e);
//			Debug.LogError("Socket (ego sender) error: " + e);
			
		}

		Debug.LogWarning("Finished socket setups!");

	}

	public String readSocket(){

		if (!socketReady) {

			return "";

		}

		if (networkStream.DataAvailable) {

			return streamReader.ReadLine();

		}

		return "";

	}

	void CloseSockets(){

		try{
			
			mySocket.Close();
			Debug.Log("Closed the socket.");
			
		}
		catch (Exception e){
			
			Debug.Log("Trying to close socket: " + e);
			
		}
		
		try{

//			goSocket = new TcpClient(Host, egoPort);
//			egoNetworkStream = egoSocket.GetStream();
//			streamWriter = new StreamWriter(egoNetworkStream);

//			streamWriter.Close();
//			egoNetworkStream.Close();

//			egoSocket.
			String closeString = "CLOSE";
			closeString = closeString + "\r\n";

			streamWriter.Write(closeString);
			streamWriter.Flush();

			egoSocket.Close();
			Debug.LogError("Closed the ego socket.");
			
		}
		catch (Exception e){
			
			Debug.Log("Trying to close socket: " + e);
			Debug.LogError("Closed the ego socket error: "+ e);
			
		}

		socketReady = false;

	}

	void OnApplicationQuit()
	{

		CloseSockets ();

	}

//	void HandleOnPlayModeChanged()
//	{
//		// This method is run whenever the playmode state is changed.
//		
//		if (EditorApplication.isPaused)
//		{
//			if (!wasPaused)
//			{
////				Pause();
//				wasPaused = true;
//
//				Debug.LogError ("Calling Socket Pause() from PlayModeChangeHandler");
//				CloseSockets ();
//
//			}
//			else //.isStopped equivalent
//			{
////				Stop ();
////				Debug.LogError ("Calling Socket Close from PlayModeChangeHandler");
////				CloseSockets ();
//				initLoadComplete = false;
//			}
//		}
//		else if (initLoadComplete && (EditorApplication.isPlaying || EditorApplication.isPlayingOrWillChangePlaymode))
//		{
//			if (wasPaused)
//			{
//				wasPaused = false;
////				Play ();
//			}
//			else //.isStopped equivalent
//			{
////				Stop ();
////				Debug.LogError ("Calling Socket Close from PlayModeChangeHandler");
////				CloseSockets ();
//				initLoadComplete = false;
//			}
//		}
//	}

}
