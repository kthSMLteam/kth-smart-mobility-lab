﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;

public class TerrainScript : MonoBehaviour {

	Terrain terrain;
	int hmWidth; // heightmap width
	int hmHeight; // heightmap height

	public bool run;

	int laneletMaskWidth;
	int laneletMaskHeight;
	int[,] laneletMask;
	
	// Use this for initialization
	void Start () {



		if (!run) {

			return;

		}



		GameObject terrainManager = GameObject.FindGameObjectWithTag ("TerrainManager");
		terrain = terrainManager.GetComponentInChildren<Terrain> ();

		//Noob ();

		if (terrain == null) {

			Debug.LogError ("Terrain not found!");

		}

		//terrain.terrainData = new TerrainData();

		if (terrain.terrainData == null) {
			
			Debug.LogError ("Terrain.terrainData not found!");
			
		}

		Debug.Log("terrain.terrainData.heightmapResolution");
		Debug.Log(terrain.terrainData.heightmapResolution);

		Debug.Log("terrain.terrainData.heightmapWidth");
		Debug.Log(terrain.terrainData.heightmapWidth);
		Debug.Log("terrain.terrainData.heightmapHeight");
		Debug.Log(terrain.terrainData.heightmapHeight);

		//terrain.terrainData.heightmapResolution = 2*terrain.terrainData.heightmapResolution;
		terrain.terrainData.heightmapResolution = 550;
		//terrain.terrainData.heightmapScale = new Vector3 (0.1f, 0.1f, 0.1f);

		//terrain.terrainData.Set

		Debug.Log("terrain.terrainData.heightmapResolution");
		Debug.Log(terrain.terrainData.heightmapResolution);

		hmWidth = terrain.terrainData.heightmapWidth;
		hmHeight = terrain.terrainData.heightmapHeight;

		Debug.Log("terrain.terrainData.heightmapWidth");
		Debug.Log(terrain.terrainData.heightmapWidth);
		Debug.Log("terrain.terrainData.heightmapHeight");
		Debug.Log(terrain.terrainData.heightmapHeight);

		float[,] heights = terrain.terrainData.GetHeights (0, 0, terrain.terrainData.heightmapWidth, terrain.terrainData.heightmapHeight);





		//Terrain.activeTerrain.heightmapMaximumLOD = 0;

		//LevelHeight ();

		ReadLaneletMask ();


		for (int i = 0; i < terrain.terrainData.heightmapWidth; i++) {

			float iRatio = ((float)i)/((float)terrain.terrainData.heightmapWidth);
			int iMask = (int) ( iRatio*laneletMaskHeight );

			for (int j = 0; j < terrain.terrainData.heightmapHeight; j++) {

				float jRatio = ((float)j)/((float)terrain.terrainData.heightmapHeight);
				int jMask = (int) ( jRatio*laneletMaskWidth );

				heights[terrain.terrainData.heightmapWidth-i-1,j] = (float) 0.6*laneletMask[jMask, iMask];

				//heights[i,j] = ((float)i)/((float)terrain.terrainData.heightmapWidth);
				//heights[i,j] = ((float)j)/((float)terrain.terrainData.heightmapHeight);
				//Debug.Log (heights[i,j]);
				
				//heights[i,j] = 1.0f;
				
			}
			
		}

		terrain.terrainData.SetHeights (0, 0, heights);

		SpamGrass ();
			
	}

	void Noob(){

		//Terrain terrain;
		int detailIndexToMassPlace;
		int[] splatTextureIndicesToAffect;
		int detailCountPerDetailPixel = 0;

		detailIndexToMassPlace = 0;
		splatTextureIndicesToAffect = new int[1];
		splatTextureIndicesToAffect [0] = 0;


		if (!terrain) {
			Debug.Log("You have not selected a terrain object");
			return;
		}

		if (detailIndexToMassPlace >= terrain.terrainData.detailPrototypes.Length) {
			Debug.Log("You have chosen a detail index which is higher than the number of detail prototypes in your detail libary. Indices starts at 0");
			return;
		}
		
		if (splatTextureIndicesToAffect.Length > terrain.terrainData.splatPrototypes.Length) {
			Debug.Log("You have selected more splat textures to paint on, than there are in your libary.");
			return;
		}
		
		for (int i = 0; i < splatTextureIndicesToAffect.Length; i ++) {
			if (splatTextureIndicesToAffect[i] >= terrain.terrainData.splatPrototypes.Length) {
				Debug.Log("You have chosen a splat texture index which is higher than the number of splat prototypes in your splat libary. Indices starts at 0");
				return;
			}
		}
		
		if (detailCountPerDetailPixel > 16) {
			Debug.Log("You have selected a non supported amount of details per detail pixel. Range is 0 to 16");
			return;
		}
		
		int alphamapWidth = terrain.terrainData.alphamapWidth;
		int alphamapHeight = terrain.terrainData.alphamapHeight;
		int detailWidth = terrain.terrainData.detailResolution;
		int detailHeight = detailWidth;
		
		float resolutionDiffFactor = (float)alphamapWidth/detailWidth;
		
		
		float[,,] splatmap = terrain.terrainData.GetAlphamaps(0,0,alphamapWidth,alphamapHeight);
		
		
		int[,] newDetailLayer = new int[detailWidth,detailHeight];
		
		//loop through splatTextures
		for (int i = 0; i < splatTextureIndicesToAffect.Length; i++) {
			
			//find where the texture is present
			for (int j = 0; j < detailWidth; j++) {
				
				for (int k = 0; k < detailHeight; k++) {
					
					float alphaValue = splatmap[(int)(resolutionDiffFactor*j),(int)(resolutionDiffFactor*k),splatTextureIndicesToAffect[i]];
					
					newDetailLayer[j,k] = (int)Mathf.Round(alphaValue * ((float)detailCountPerDetailPixel)) + newDetailLayer[j,k];
					
				}
				
			}
			
		}
		
		terrain.terrainData.SetDetailLayer(0,0,detailIndexToMassPlace,newDetailLayer);	

	}

	void ReadLaneletMask(){


//		string text = System.IO.File.ReadAllText(@"C:\Users\Public\Documents\Unity Projects\SiMuLator01\Assets\Xmls\lanelet_mask_for_internet.xml");
		string text = System.IO.File.ReadAllText(@"C:\Users\SMLADMIN\Documents\kth-smart-mobility-lab\SiMuLator\Assets\Xmls\lanelet_mask_for_internet.xml");


		using (XmlReader reader = XmlReader.Create(new StringReader(text))) {
			
			
			while (reader.ReadToFollowing("message")) {
				
				string messageTypeString = reader.GetAttribute ("type");
				
				if ( messageTypeString.Equals("lanelet_mask_for_internet") ){
					
					Debug.Log ("Is a lanelet_mask_for_internet");
					
				}

				string pixelWidthString = reader.GetAttribute ("pixel_width");
				int pixelWidth = int.Parse(pixelWidthString);

				Debug.LogWarning ("pixelWidth = " + pixelWidthString);
//				Debug.LogWarning (pixelWidth);

				laneletMaskWidth = pixelWidth;

				string pixelHeightString = reader.GetAttribute ("pixel_height");
				int pixelHeight = int.Parse(pixelHeightString);

				
				Debug.LogWarning ("pixelHeight = " + pixelHeightString);
//				Debug.Log ("pixel_height");
//				Debug.Log (pixelHeight);

				laneletMaskHeight = pixelHeight;

				laneletMask = new int[pixelWidth,pixelHeight];

				while (reader.ReadToFollowing("pixel")) {
					
					int x = int.Parse(reader.GetAttribute ("x"));
					int y = int.Parse(reader.GetAttribute ("y"));

					laneletMask[x,y] = int.Parse(reader.GetAttribute ("value"));

					
				}



			}
			
		}

		Debug.Log("ReadLaneletMask completed");

	}
	
	
	void LevelHeight(){

		// get the heights of the terrain under this game object
		float[,] heights = terrain.terrainData.GetHeights(0,0,hmWidth,hmHeight);
		
		// we set each sample of the terrain in the size to the desired height
		for (int i=0; i<hmWidth; i++) {

			for (int j=0; j<hmHeight; j++){
				//heights[i,j] = 0.1;
				heights [i, j] = ((float)i)/((float)hmWidth);

			}
		}
		// set the new height
		terrain.terrainData.SetHeights(0,0,heights);
		
		
	}
	
	void SpamGrass(){
		
		
		int alphamapWidth = terrain.terrainData.alphamapWidth;
		int alphamapHeight = terrain.terrainData.alphamapHeight;
		int detailWidth = terrain.terrainData.detailResolution;
		int detailHeight = detailWidth;
		
		float resolutionDiffFactor = (float)alphamapWidth / detailWidth;
		
		int[] splatTextureIndicesToAffect = new int[1];
		splatTextureIndicesToAffect [0] = 0;
		
		if (splatTextureIndicesToAffect.Length > terrain.terrainData.splatPrototypes.Length) {
			Debug.Log ("You have selected more splat textures to paint on, than there are in your libary.");
			return;
		}
		
		
		for (int i = 0; i < splatTextureIndicesToAffect.Length; i ++) {
			if (splatTextureIndicesToAffect [i] >= terrain.terrainData.splatPrototypes.Length) {
				Debug.Log ("You have chosen a splat texture index which is higher than the number of splat prototypes in your splat libary. Indices starts at 0");
				return;
			}
		}
		
		
		
		
		int detailIndexToMassPlace = 0;
		
		if (detailIndexToMassPlace >= terrain.terrainData.detailPrototypes.Length) {
			Debug.Log ("You have chosen a detail index which is higher than the number of detail prototypes in your detail libary. Indices starts at 0");
			return;
		}
		
		int detailCountPerDetailPixel = 5;
		
		
		if (detailCountPerDetailPixel > 16) {
			Debug.Log ("You have selected a non supported amount of details per detail pixel. Range is 0 to 16");
			return;
		}
		
		
		float[,,] splatmap = terrain.terrainData.GetAlphamaps (0, 0, alphamapWidth, alphamapHeight);
		
		
		int[,] newDetailLayer = new int[detailWidth, detailHeight];
		
		//loop through splatTextures
		for (int i = 0; i < splatTextureIndicesToAffect.Length; i++) {


			//find where the texture is present
			for (int j = 0; j < detailWidth; j++) {

				float jRatio = ((float)j)/((float)detailWidth);
				int jMask = (int) ( jRatio*laneletMaskHeight );


				for (int k = 0; k < detailHeight; k++) {

					float kRatio = ((float)k)/((float)detailHeight);
					int kMask = (int) ( kRatio*laneletMaskWidth );

					float alphaValue = splatmap [(int)(resolutionDiffFactor * j), (int)(resolutionDiffFactor * k), splatTextureIndicesToAffect [i]];
					
					//newDetailLayer[j,k] = (int)Mathf.Round(alphaValue * ((float)detailCountPerDetailPixel)) + newDetailLayer[j,k];
					newDetailLayer [detailWidth -j -1, k] = (int) laneletMask[kMask, jMask];
					
				}
				
			}
			
		}

//		for (int i = 0; i < terrain.terrainData.heightmapWidth; i++) {
//			
//			float iRatio = ((float)i)/((float)terrain.terrainData.heightmapWidth);
//			int iMask = (int) ( iRatio*laneletMaskHeight );
//			
//			for (int j = 0; j < terrain.terrainData.heightmapHeight; j++) {
//				
//				float jRatio = ((float)j)/((float)terrain.terrainData.heightmapHeight);
//				int jMask = (int) ( jRatio*laneletMaskWidth );
//				
//				heights[terrain.terrainData.heightmapWidth-i-1,j] = (float) 0.6*laneletMask[jMask, iMask];
//
//			}
//			
//		}
		
		terrain.terrainData.SetDetailLayer (0, 0, detailIndexToMassPlace, newDetailLayer);	

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
